<?php  
ini_set('display_errors', 0); 
session_start();
include("database/dbcon.php"); 
include("functions.php");
include("pathConfig.php");   
$action=($_REQUEST['action']) ? $_REQUEST['action'] : "";
$empName=$_SESSION['LOGIN_CREDENTAILS']['EMP_NAME'];
$emptitle=$_SESSION['LOGIN_CREDENTAILS']['FIRST_NAME'];
$mobile_no=$_SESSION['LOGIN_CREDENTAILS']['MOBILE_NO'];
$email=$_SESSION['LOGIN_CREDENTAILS']['EMAIL'];
$empCode=$_SESSION['LOGIN_CREDENTAILS']['ID'];
$employeeId=$_SESSION['LOGIN_CREDENTAILS']['ID'];
$status="OnHold";
$approveByWhereClause= " AND approve_by LIKE '%".$employeeId."%'";
//$empIdWhereClause=" AND emp_id='".$employeeId."'";
 ?>
<?php include("submitNew.php"); ?>
<?php include("processNew.php"); ?>
<?php include("templates/top.php");?>
<?php include("expiry.php"); ?>

<body style="background:url(image/background.jpg);">
<script type="text/javascript" src="js/wz_tooltip.js"></script> 
  <div class="d-flex" id="wrapper">
     <!-- Sidebar -->
    <?php include("templates/sidebar.php") ?>
    <!-- /#sidebar-wrapper -->
    <!-- Page Content -->
    <div id="page-content-wrapper">
     <?php include("templates/navbar.php") ?>
      <div class="container-fluid">
       <div class="card mx-auto">
        <!--<div class="card-header bg-primary text-white text-center"><h5 class="float-left">YTY Group-Green Prospects-Employee Claim Settlement</h5></div>-->
        <p>&nbsp;</p>
		<?php 
		function moneyFormatIndia($num)
		{
			return number_format($num,2);
		}
		if(count($claimTypeArray)>0){
			$claimTypeName=array();
			foreach($claimTypeArray as $claimTye)	
			{
				$claimTypeName[$claimTye['CLAIM_TYPE_CODE']]=$claimTye['CLAIM_TYPE_NAME']." (".$claimTye['CLAIM_TYPE_CODE'].")";
			}
		}
		if($action=="" || $action=="View")
		{
		?>
			<style>
            .form-group{
                margin-bottom:6px;
            }
            </style>
             <table width="99%" border="0" cellspacing="2" cellpadding="0"  style="font-size: 13px;">
              <tbody>
                <tr>
                  <td width="64%" valign="top" style="padding-top:10px;">
                  <table width="100%" cellpadding="2" cellspacing="2">
                  <tr>
                      <td width="30%">
                      <div class="form-group">
                          <label for="code"><b>&nbsp;Employee Code</b></label>
                        </div>
                      </td>
                       <td width="7%" valign="top"><strong>:</strong></td>
                      <td width="63%" valign="top">
                        <div class="form-group">
                          <?= $empCode; ?>
                        </div>
                      </td>
                  </tr>
                 
                  <tr>
                      <td width="30%">
                      <div class="form-group">
                          <label for="department"><b>&nbsp;Email</b></label>
                        </div>
                      </td>
                        <td valign="top"><strong>:</strong></td>
                     <td width="63%" valign="top">
                        <div class="form-group">
                         <?= $email; ?>
                        </div>
                      </td>
                  </tr>
                  <tr>
                       <td width="30%">
                       <div class="form-group">
                        <label for="title"><b>&nbsp;Mobile</b></label>
                       </div>
                      </td>
                        <td valign="top"><strong>:</strong></td>
                      <td width="63%">
                        <div class="form-group">
                            <?= $mobile_no; ?>                 
                       </div>
                      </td>
                  </tr>
                </table>
                  </td>
                  <td width="36%" valign="top" align="center">
                  </td>
                </tr>
              </tbody>
            </table>
		<?php
		}
		?>
	</div>
		<?php
		//echo"<pre>";print_r($_REQUEST);die;
		if($_REQUEST['msg'])
		{
		?>
		   <center> <?= $error; ?><?= $_REQUEST['msg']; ?></center>
	   <?php
	    }
		?>
		<br/><br/>
<!-- Claim Header -->
		<?php 
		if($action=="")
		{
		?>
			<div class="card mx-auto">
			 <div class="card-header bg-primary text-white text-center" ><h5 class="float-left">Claims for approval</h5></div>
			  <div class="card-body">
				<table class="table table-hover table-bordered table-striped">
				  <thead class="bg-success text-white">
					<tr>
					  <th>Claim Type</th>
					  <th>Description</th>
					  <th>Amount(MYR)</th>
					  <th>Tax</th>
					  <th>Total</th>
					  <th style="width:20%; text-align:center">Action</th>
					</tr>
				  </thead>
				  <tbody>
					<?php
					//echo"<pre>";print_r($clientClaimData);die;
					if(count($clientClaimData)>0)
					{
						$claimDataArray=array();
						if(count($clientClaimData)>0)
						{
							$checkHeaderArr=array();
							foreach($clientClaimData as $claimVal){
								if(!in_array($claimVal['CLAIM_HEADER_ID'],$checkHeaderArr))
								{
									$checkHeaderArr[]=$claimVal['CLAIM_HEADER_ID'];
									$claimDataArray[$claimVal['CLAIM_ID']."_".$claimVal['CLAIM_DATE']."_".$claimVal['EMPLOYEE_NAME']."_".$claimVal['STATUS_DATE']."_".$claimVal['CURRENT_APPROVE_LEVEL']."_".$claimVal['DESIGNATION']][]=$claimVal['CLAIM_TYPE']."_".$claimVal[19]."_".$claimVal['CLAIM_TOT_AMT_MYR']."_".$claimVal['CLAIM_TOT_TAX_AMT']."_".$claimVal['CLAIM_TOTAL_AMT']."_".$claimVal['CLAIM_HEADER_ID'];
								}
							}
						}
						//echo"<pre>";print_r($claimDataArray);die;
						$sno=1;
						$claimIds="";
						$flag=0;
						foreach($claimDataArray as $keyVal=>$claimDetail)
						{
							list($key,$date,$empName,$statusDate,$currentAppLevl,$designaiton)=explode("_",$keyVal);
						?>
							<tr>
							  <td align="left" colspan="5" style="color:#0c76b5;"><strong style="color:#ad3c06;">Claim Id => <?php echo $key;?></strong>&nbsp;&nbsp;< Submitted by : <?= $empName; ?>, <?= $designaiton; ?>&nbsp;&nbsp;on <?php echo $date;?> ></td>
							 <td align="left"> <a data-toggle="modal" data-target="#myModal<?php echo $key;?>" class="btn btn-warning btn-sm"><i class="fa fa-share"></i>&nbsp;<strong>Approve Claim</strong></a></td>
							</tr>
						<?php
							foreach($claimDetail as $claim)
							{
								list($type,$desc,$amtMyr,$tax,$totalAmt,$headerId)=explode("_",$claim);
						?>
								<tr>
								  <td align="left"><?php echo $claimTypeName[$type];?></td>
								  <td align="left"><?php echo $desc;?></td>
								  <td align="left"><?php echo moneyFormatIndia($amtMyr);?></td>
								  <td align="left"><?php echo moneyFormatIndia($tax);?></td>
								  <td align="left"><?php echo moneyFormatIndia($totalAmt);?></td>
								  <td>
									  <a href="claim-approval.php?action=View&clmId=<?php echo $headerId;?>" target="_blank" class="btn btn-info btn-sm"><i class="fa fa-eye"></i>&nbsp;View</a>
<!--									  <a href="pending-claim.php?action=Edit&clmId=<?php echo $headerId;?>" class="btn btn-info btn-sm"><i class="fa fa-edit"></i>&nbsp;Edit</a>
-->								  </td>
								</tr>
						<?php
							}
							$sno++;
							?>
								<!-- Modal -->
								<div id="myModal<?php echo $key;?>" class="modal fade" role="dialog">
								  <div class="modal-dialog">
									<form name="approvalClaim" method="post" action="claim-approval.php">
										<input type="hidden" name="claimId" id="claimId" value="<?php echo $key;?>" >
									<!-- Modal content-->
									<div class="modal-content">
									  <div class="modal-header" style="background-color:#007bff">
										<h4 class="modal-title" style="color:#FFFFFF">Claim for approval</h4>
									  </div>
									  <div class="modal-body">
										<p><strong>Claim Id :-</strong> <?php echo $key;?></p>
									  </div>
									  <div class="modal-body">
										<p><strong>Status</strong></p><p><select class="form-control" name="status<?php echo $key;?>" required onChange="showHide(this.value,'<?php echo $key;?>')"><option value="Approve">Approve</option><option value="Reject">Reject</option></select></p>
									  </div>
									  <div class="modal-body" id="reasonField<?php echo $key;?>" style="display:none">
										<p><strong>Reason</strong></p><p><textarea name="reason<?php echo $key;?>" class="form-control" disabled="disabled" id="reason<?php echo $key;?>" required></textarea></p>
									  </div>
									  <div class="modal-footer">
										<button type="submit" class="btn btn-success" onClick="return confirm('Are you sure to approve it ? .\n Click ok to continue cancel to stop');">Save</button>
										<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
									  </div>
									</div>
									</form>
								  </div>
								</div>	
								<script>
								function showHide(status,key)
								{
									if(status=='Approve'){
										document.getElementById("reasonField"+key).style.display="none";
										document.getElementById("reason"+key).disabled=true;
										}
									else{
										document.getElementById("reasonField"+key).style.display="block";
										document.getElementById("reason"+key).disabled=false;
									}
								}
								</script>						
						<?php
						}
						$claimIds=ltrim($claimIds,",");
					}
					if(	count($clientClaimData)== 0)
					{
					?>
					<tr>
					  <th colspan="6"><font color="#FF0000">No claim found.</font></th>
					 </tr>
					<?php
					}
					?>
				  </tbody>
				</table>
			 </div>
			</div><br><br>
		<?php
		}
		if($action=="View")
		{	
			$clmId=$_REQUEST['clmId'];
			$viewClaimDataArray=array();
			foreach($clientClaimData as $value)
			{
				if($clmId!=$value['CLAIM_HEADER_ID'])
					continue;
					$viewClaimDataArray[]=$value;
			}	
			//echo"<pre>";print_r($viewClaimDataArray);
		?>	  
		  <div class="container-fluid">
		   <div class="card mx-auto">
			<div class="card-header bg-primary text-white text-center"><h5 class="float-left">YTY Group-Green Prospects-Employee Claim Settlement Form</h5></div><br>
			 <div class="card-body">    		        
				<table width="50%">
				  <tr style="display:none;">
					  <td>
					  <div class="form-group">
						  <label for="code"><b>Employee Code:</b></label>
						</div>
					  </td>
					  <td>
						<div class="form-group">
						  <?= $empNumber; ?>
						</div>
					  </td>
				  </tr>
				 <tr style="display:none;">
					<td>
					  <div class="form-group">
						  <label for="name"><b>Employee Name:</b></label>
						</div>
					  </td>
					  <td>
						<div class="form-group">
						  <?= $empName; ?> 
						</div>
					  </td>
				  </tr>
				  <tr style="display:none;">
					  <td>
					  <div class="form-group">
						  <label for="department"><b>Department Name:</b></label>
						</div>
					  </td>
					  <td>
						<div class="form-group">
						  <?= $empDept; ?> 
						</div>
					  </td>
				  </tr>
				   <tr style="display:none;">
					  <td>
					   <div class="form-group">
						<label for="title"><b>Title:</b></label>
					   </div>
					  </td>
					  <td>
						<div class="form-group">
							<?= $emptitle; ?>                
					   </div>
					  </td>
				  </tr>
				   <tr style="display:none;">
					  <td>
						<div class="form-group">
						  <label for="c_date"><b>Claim Date::</b></label>
						</div>
					  </td>
					  <td>
						<div class="form-group">
						  <?= $today_date; ?>
						</div>
					  </td>
				  </tr>
				  <tr>
					  <td align="center">
						<div class="form-group">
						  <label for="c_id"><b>Claim ID:</b></label>
						</div>
					   </td>
					  <td>
						<div class="form-group" style="background:#FFFFFF;background: #0258a3;border: none;color: white;width: 38%;height: calc(1.5em + .75rem + 2px);padding: .375rem .75rem;border-radius: .25rem; text-align:center;">
						<?php echo $viewClaimDataArray[0]['CLAIM_ID'];?>
						</div>
					  </td>
				  </tr>
				</table>
				  <!-- <input type="submit" class="btn btn-primary float-right" value="Submit & Email" style="background:#013243;"> -->
				</div>
			</div>
	<!-- Claim Header -->
			<div class="card mx-auto">
			 <div class="card-header bg-primary text-white text-center" ><h5 class="float-left">Claim Summary</h5></div>
			  <div class="card-body">
				<table class="table table-hover table-bordered table-striped">
				  <thead class="bg-success text-white">
					<tr>
					  <th>Claim Type</th>
					  <th>Description</th>
					  <th>Amount(MYR)</th>
					  <th>Tax</th>
					  <th>Total</th>
					</tr>
				  </thead>
				  <tbody>
						<tr>
						  <td align="left">
								<?php
								foreach($claimTypeArray as $claim)
								{
									if($viewClaimDataArray[0]['CLAIM_TYPE']!=$claim['CLAIM_TYPE_CODE'])
										continue;
										
									  echo $claim['CLAIM_TYPE_NAME']." (".$claim['CLAIM_TYPE_CODE'].")";
								}
								?>
						  </td>
						  <td align="left"><?php echo $viewClaimDataArray[0][19];?></td>
						  <td align="left"><?php echo moneyFormatIndia($viewClaimDataArray[0]['CLAIM_TOT_AMT_MYR']);?></td>
						  <td align="left"><?php echo moneyFormatIndia($viewClaimDataArray[0]['CLAIM_TOT_TAX_AMT']);?></td>
						  <td align="left"><?php echo moneyFormatIndia($viewClaimDataArray[0]['CLAIM_TOTAL_AMT']);?></td>
						</tr>
				  </tbody>
				</table>
			 </div>
			</div><br>
	<!-- Claim Lines -->
			 <div class="card mx-auto">
			  <div class="card-header bg-primary text-white text-center"><h5 class="float-left">Claim Details</h5>
			  </div>
			   <div  class="card-body">
				  <table class="table table-hover table-bordered table-striped" width="100%">
					<thead class="bg-success text-white">
					  <tr>
						<th width="10%">Bill Date</th>
						<th width="10%">Claim Type</th>
						<th width="10%">Description</th>
						<th width="10%">Bill Amount</th>
						<th width="10%">Tax</th>
						<th width="15%"><p style="margin-bottom:0px;">Total&nbsp;Amount<br>&nbsp;&nbsp;[INR]</p></th>
						<th width="15%">Attachment</th>
					  </tr>
					</thead>
					<tbody>
					<tbody>
					<?php
					if(count($clientClaimData)>0)
					{
						$sno=1;
						$thumbnialIcon="";
						$grandTotal=0;
						foreach($viewClaimDataArray as $value)
						{
							$filePath=UPLOAD_URL.$value['CLAIM_FILE_EXTENSION'];
							$targetPath=UPLOAD_URL;
							list($fileName,$ext)=explode(".",$value['CLAIM_FILE_EXTENSION']);
							if(in_array($ext,$convertFileExt))
							{
								 $fileExits=UPLOAD_URL.$fileName.".pdf";
								if(!file($fileExits))
									 ChangeFileIntoPdf($value['CLAIM_FILE_EXTENSION'], $targetPath);
									 
								 $thumbnialIcon=$fileExits;
							}
							else
							{
								$thumbnialIcon=$filePath;
							}
						?>
						  <tr>    
							<td width="5%"><?php echo date('Y-m-d',strtotime($value['CREATION_DATE']));?></td>
							<td align="left"><?php echo $claimTypeName[$value['CLAIM_TYPE']];?></td>
							<td align="left"  width="5%"><?php echo $value['CLAIM_DESCRIPTION'];?></td>
							<td align="left">
                            	<?php
								if($viewClaimDataArray[0]['CLAIM_TYPE']=='002')
								{
									if($value['KILOMETER']!="")
									{
								?>
                                    <div id="myModal<?php echo $value['CLAIM_DETAIL_ID'];?>" class="modal fade" role="dialog">
                                      <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header" style="background-color:#007bff">
                                            <h4 class="modal-title" style="color:#FFFFFF">Transport Detail</h4>
                                          </div>
                                          <div class="modal-body" style="text-align:center">
                                              <table align="center"  style="table-layout:fixed">
                                                  <thead>
                                                  </thead>
                                                  <tbody class="table">
                                                      <tr>
                                                          <td><strong>Show in bill amount ?</strong></td>
                                                          <td><strong>KM</strong></td>
                                                          <td><strong>Rate/Km</strong></td>
                                                          <td><strong>Total</strong></td>
                                                      </tr>
                                                      <tr>
                                                          <td style="width:130px"> <?php if($value['KILOMETER']!="") echo "Yes"; else echo "No";?></td>
                                                          <td style="width:130px"><?php if($value['KILOMETER']) echo moneyFormatIndia($value['KILOMETER']); else echo "0";?></td>
                                                          <td style="width:130px"><?php if($value['RATE_PER_KILOMETER']) echo moneyFormatIndia($value['RATE_PER_KILOMETER']); else echo "0";?></td>
                                                          <td style="width:130px"><?php if($value['KILOMETER']) echo moneyFormatIndia($value['KILOMETER']*$value['RATE_PER_KILOMETER']); else echo "0";?></td>
                                                      </tr>
                                                  </tbody>
                                              </table>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div> 
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal<?php echo $value['CLAIM_DETAIL_ID'];?>"  id="tptData" onMouseOver="Tip('View to Click')" onMouseOut="UnTip()"><strong><?php echo moneyFormatIndia($value['CLAIM_BILL_AMT']);?></strong></a>
                            <?php
									}
									else
									{
										echo moneyFormatIndia($value['CLAIM_BILL_AMT']);
									}
								}
								else
								{
									echo moneyFormatIndia($value['CLAIM_BILL_AMT']);													
								}
							 ?>
                            </td>
							<td align="left"><?php echo moneyFormatIndia($value['CLAIM_TAX_AMT']);?></td>
							<td align="left"><?php echo moneyFormatIndia(($value['CLAIM_BILL_AMT']+$value['CLAIM_TAX_AMT']));?></td>
							<?php
							$grandTotal+=(($value['CLAIM_BILL_AMT']+$value['CLAIM_TAX_AMT']));
							if(file($filePath))
							{
								//$im = new imagick($thumbnialIcon.'[0]');
								//$im->setImageFormat('jpg');
								//header('Content-Type: image/jpeg');
							?>
                            	<style>
									.imageClass{
										transform:scale(1);	
									}
									.imageClass:hover{
										transform:scale(4.1);	
									}
								</style>
								<td align="left"><a href="<?php echo UPLOAD_URL.$value['CLAIM_FILE_EXTENSION'];?>" target="_blank" onMouseOver="Tip('VIEW TO CLICK')" onMouseOut="UnTip()">View</a></td>
							<?php
							}
							else
							{
							?>
								<td align="left"><font color="#FF0000">No file found.</font></td>
							<?php
							}
							?>
						  </tr>
							<tr>
								<td colspan="10" style="padding:0px; width:100%">
								<table class="table table-hover table-bordered table-striped" cellpadding="0" cellspacing="0">
								</table>
								</td>
							</tr> 
						<?php
							$sno++;
						}
					}
					 ?>
							</tr> 
						<tr><td colspan="10"></td></tr>
						<tr>
							<td colspan="2">Total Claim Amount :</td>
							<td align="left" colspan="7"><?php echo moneyFormatIndia($grandTotal);?></td>
						  </tr> 
				  </table>
			   </div>
				<!-- <form id="signupform" action="signup.php" method="POST"> -->
				  <!-- <input type="submit" class="btn btn-primary float-left" value="Save Draft" style="background:#013243;"> -->
				  <!-- <input type="submit" class="btn btn-primary" value="Submit" style="margin-left:350px; background:#013243;"> -->
				  <!-- <input type="submit" class="btn btn-primary float-right" value="Submit & Email" style="background:#013243;"> -->
			  </div>
			</div>
		 <?php
		}
		?>
<!-- Claim Lines -->
        </div>
    </div>
    <!-- /#page-content-wrapper -->
  </div>

<?php require_once("templates/footer.php"); ?>


<style>
.card {
   position: relative !important;
    display: -ms-flexbox !important;
    display: flex!important;
    -ms-flex-direction: column!important;
    flex-direction: column!important;
    min-width: 0!important;
    word-wrap: break-word!important;
    background-color: #fff0!important;
    background-clip: border-box!important;
    border: 0px solid rgba(0,0,0,.125)!important;
    border-radius: .25rem!important;
}

</style>