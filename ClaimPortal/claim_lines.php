<?php include("templates/top.php") ?>
 
<body>

  <div class="d-flex" id="wrapper">

     <!-- Sidebar -->
      <?php include("templates/sidebar.php") ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <?php include("templates/navbar.php") ?><br><br>

      <div class="container-fluid">
       <div class="card mx-auto">
        <div class="card-header text-white text-center" style="background:#013243;"><h5 class="float-left">Claim Lines</h5></div>
         <div class="card-body">
          <!-- <form id="signupform" action="signup.php" method="POST"> -->
            <table class="table table-hover table-bordered table-striped">
              <thead class="bg-success text-white">
                <tr>
                  <th width="10%">Bill Date</th>
                  <th width="8%">Claim Type</th>
                  <th>Description</th>
                  <th width="5%">Bill Amount</th>
                  <th>Tax</th>
                  <th>Currency</th>
                  <th>Exchange Rate</th>
                  <th>Total Amount(Amount+Tax)in MYR</th>
                  <th>Attachment</th>
                </tr>
              </thead>
              <tbody>
                <tr>    
                  <td width="11%">05-13-2019</td>
                  <td>Medical</td>
                  <td align="right">auto</td>
                  <td align="right">50.00</td>
                  <td align="right">20.00</td>
                  <td align="right">MYR</td>
                  <td align="right">18:00</td>
                  <td align="right">70.00</td>
                  <td align="right">Choose File</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  
  </div>
  <!-- /#wrapper -->


<?php require_once("templates/footer.php"); ?>
<script>
 
</script>