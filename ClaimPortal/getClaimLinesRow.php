<?php 
include("database/dbcon.php"); 
include("processNew.php");
$row=$_REQUEST['row'];
list($claimType,$claimDesc,$desc)=explode("_",$_REQUEST['claimType']);

$count=$row+1;
//echo"<pre>";print_r($_REQUEST);
// get posted data into local variables
?>
  <table class="table table-hover table-bordered table-striped" style="table-layout: fixed; width: 180px:">
		<tbody>
		<tr>
		<td style="width:12%"><input type="text" readonly  name="bill_date_<?php echo $count;?>" id="bill_date_<?php echo $count;?>" onBlur="getMYRDetails(this.value,'<?php echo $count;?>')" class="form-control date_bill" style="width:140px; background-color:#FFFFFF" autocomplete="off" onClick="popUpCalendar('bill_date_<?php echo $count;?>');"> 
        </td>
		<td align="left" style="width:10%">
			<?php
			foreach($claimTypeArray as $claim)
			{
			?>
				<?php if($claimType==$claim['CLAIM_TYPE_CODE']) echo $claim['CLAIM_TYPE_NAME']." (".$claim['CLAIM_TYPE_CODE'].")";?>
			<?php
			}
			?>
		</td>
		<td align="left" id="desc_<?php echo $count;?>" style="width:10%">
      	  <input type="text"  name="desc_<?php echo $count;?>" id="desc_<?php echo $count;?>"  style="width:100%;" class="form-control" value="<?php echo $claimDesc;?>">
		</td>
		<td align="right" style="width:10%"><input type="text" autocomplete="off" onkeypress="return isNumberKey(event,this)" onBlur="changeVal('<?php echo $count;?>')"  name="bill_amount_<?php echo $count;?>" id="bill_amount_<?php echo $count;?>" onkeyup="calculateTotalAmt('<?php echo $count;?>')" class="form-control" style="width:120px"><?php if($claimType=='002') { ?><a class="btn btn-info btn-sm" onClick="return checkDate('<?php echo $count;?>')" data-toggle="modal"  id="tptData"><i class="fa fa-edit"></i></a>
                <input type="hidden" name="hidden_kilometer_<?php echo $count;?>" autocomplete="off"  id="hidden_kilometer_<?php echo $count;?>" class="form-control" style="width:120px">
                <input type="hidden" name="hidden_ratePerKm_<?php echo $count;?>" autocomplete="off" id="hidden_ratePerKm_<?php echo $count;?>" class="form-control" style="width:120px">                                
            <div id="myModal<?php echo $count;?>" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content"   style="width:120%">
                  <div class="modal-header" style="background-color:#007bff">
                    <h4 class="modal-title" style="color:#FFFFFF">Transport Detail</h4>
                  </div>
                  <div class="modal-body" style="text-align:center">
                      <table align="center">
                          <thead>
                          </thead>
                          <tbody class="table">
                              <tr>
                                  <td><strong>Show in bill amount ?</strong></td>
                                  <td><strong>KM</strong></td>
                                  <td><strong>Rate/Km</strong></td>
                                  <td><strong>Total</strong></td>
                              </tr>
                              <tr>
                                  <td><input type="checkbox" name="isBillAmt_<?php echo $count;?>" id="isBillAmt_<?php echo $count;?>" onClick="populateValueBillAmount('<?php echo $count;?>')" style="width:20px" ></td>
                                  <td><input type="text" disabled onkeypress="return isNumberKey(event,this)" onKeyUp="calculateTotalTransportAmt(this.value,'<?php echo $count;?>')"  name="kilometer_<?php echo $count;?>" autocomplete="off"  id="kilometer_<?php echo $count;?>" class="form-control" style="width:120px"></td>
                                  <td><input type="text" disabled onkeypress="return isNumberKey(event,this)" onKeyUp="calculateTotalTransportAmt(this.value,'<?php echo $count;?>')"  name="ratePerKm_<?php echo $count;?>" autocomplete="off" id="ratePerKm_<?php echo $count;?>" class="form-control" style="width:120px"></td>
                                  <td><input type="text" onkeypress="return isNumberKey(event,this)"  name="totalKmAmt_<?php echo $count;?>" autocomplete="off" id="totalKmAmt_<?php echo $count;?>" class="form-control" style="width:120px; background:#FFFCFC" readonly></td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>                                
		<?php } ?>
			<input type="hidden"  name="hidden_bill_amount_<?php echo $count;?>" autocomplete="off" id="hidden_bill_amount_<?php echo $count;?>" class="form-control" style="width:120px"></td>
		<td align="right" style="width:10%"><input type="text" autocomplete="off" onBlur="changeVal('<?php echo $count;?>')" onkeypress="return isNumberKey(event,this)"  name="tax_<?php echo $count;?>" onkeyup="calculateTotalAmt('<?php echo $count;?>')" id="tax_<?php echo $count;?>" class="form-control" style="width:120px" onfocus="this.select();" onmouseup="return false;"> 
		<input type="hidden"  name="hidden_tax_<?php echo $count;?>" autocomplete="off" id="hidden_tax_<?php echo $count;?>" class="form-control" style="width:120px">	
		</td>
		<td align="right" style="width:10%">
        <input type="text" readonly  name="total_amount_<?php echo $count;?>" id="total_amount_<?php echo $count;?>" class="form-control" style="width:120px"> 
        </td>
		<td align="right" style="width:10%">
			<body>
			  <a style="display:block;width:120px;height:30px;" class="btn btn-warning btn-sm"  onclick="document.getElementById('attachment_<?php echo $count;?>').click()"><strong><i class="fa fa-paperclip"> Attachment</i></strong></a>
			  <input type='file' name="attachment_<?php echo $count;?>" id="attachment_<?php echo $count;?>" style="display:none">
			</body>	
         </td>							
		<td align="right" style="width:8%">
		  <a class="btn btn-info btn-sm"><i class="fa fa-plus" onClick="addClaimLines('<?php echo $claimType;?>','<?php echo $count;?>')"></i></a>
		  <a class="btn btn-danger btn-sm"><i class="fa fa-minus" onClick="removeClaimLines('<?php echo $claimType;?>','<?php echo $count;?>')"></i></a>
		</td>
	</tr>	
	</tbody>
</table>
	
<div id="showCliamDiv_<?php echo $count;?>"></div>
