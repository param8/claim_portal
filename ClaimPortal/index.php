
<?php include("templates/top.php") ?>
 
<body>

    <div class="d-flex" id="wrapper">
     <!-- Sidebar -->
      <?php include("templates/sidebar.php") ?>
    <!-- /#sidebar-wrapper -->
    <!-- Page Content -->
    <div id="page-content-wrapper">

     <?php include("templates/navbar.php") ?>

     <?php include("database/dbcon.php") ?>

      <div class="container-fluid">
        <h1 class="mt-4">Welcome to our online Employee Claim Form</h1>
        <div class="pl-5">
        <h5 class="mt-4">You will receive an email</h5>
        <ul>
          <li>After you complete/save the cliam  Information</li>
          <li>After the claim is submitted, It will be sent to Head of Department for approval</li>
          <li>If the claim is not approved,Please re-apply for claim</li>
          <li>When your claim has been accepted and will be proccessed by account department to pay claim amount</li>
        </ul>
        <p>All claim applications not submitted within 30 days are dismissed/deleted.</p>
        <p>For technical assistance, contact <b>websupport@ytygroup.com.my</b>.</p>
        <p>For other questions, please contact our desktop office, <b>info@ytygroup.com.my</b> or via telephone <b>+6012 367 4448 (Malaysia) (C)</b> during normal working hours.</p>
        </div>  
      </div>
    </div>
    <!-- /#page-content-wrapper -->
  </div>
  
<?php require_once("templates/footer.php"); ?>