<?php  
ini_set('display_errors', 0);
ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M'); 
session_start();
$time = microtime(TRUE);
$mem = memory_get_usage();
include("AllQueryFunction.php");
include("pathConfig.php");   
$keyVal=array();
foreach($_REQUEST as $key=>$val){
	$keyVal[]=$key;
}
if($keyVal!="")
{
	$returnInvoiceArray=getInvoiceDetailBySegmentId($keyVal[0],$keyVal[1]);
	$vendorDetailsArray=$returnInvoiceArray[0];
	$invoiceDetailsArray=$returnInvoiceArray[1];
	$remarkDetails=getRemarkDetails($keyVal[0],$keyVal[1]);
}
if($_POST['remarkExists']=="1" && $_FILES['attachment']['name']!="")
{
	list($fName,$fExt)=explode(".",$_FILES['attachment']['name']);
	$oldFileName=$_POST['oldFileName'];
	$fileName=$keyVal[0]."_".$keyVal[1].".".$fExt;
	$tmpFile=$_FILES['attachment']['tmp_name'];
	$returnMsg=uploadSupportFile($fileName,$tmpFile,$oldFileName);
	$_POST['remarkExists']="";
	echo '<script language="javascript">';
	echo 'alert("Support file(s) has been uploaded successfully.")';
	echo '</script>';
	?>
	<script language="javascript">
		window.location = "ReportPCR.php?<?php echo $keyVal[0];?>&<?php echo $keyVal[1];?>";
	</script>
	<?php
	die;
}
//****************Code to insert and update remark by Deep Rana*********************************//
 if(isset($_POST['submit']) && $_POST['submit']=='Save')
 {
	$counter=$_POST['counter'];
	$oldFileName=$_POST['oldFileName'];
	if(count($counter)>0)
	{
		if($_POST['enterRemark'] > 0)
			$counter=$_POST['enterRemark']+$counter;
		else
			$counter=$counter+1;
		
		$insertUpdateArray=array();
		for($i=1;$i<=$counter;$i++)
		{
			$insertUpdateArray[]=array("PO_NUMBER"=>$keyVal[0],"INVOICE_NUMBER"=>$keyVal[1],"ITEM_DESC"=>$_POST['items_'.$i],"P_DATE"=>$_POST['itemDate_'.$i],"REMARK"=>$_POST['remark_'.$i],"oldFileExt"=>$_POST['oldFileExt']);
		}
		//echo"<pre>";print_r($insertUpdateArray);die;
		$returnVal=InsertUpdateRemark($insertUpdateArray);
		echo '<script>';
		echo 'alert("Remark has been inserted successfully.")';
		echo '<script>';
		//echo"<pre>";print_r($insertUpdateArray);die;
		header("location:ReportPCR.php?$keyVal[0]&$keyVal[1]");
		die;
	}
 }
 if(isset($_POST['ExportTo']) && $_POST['ExportTo']=='ExportToPdf')
 {
	list($segmentNo,$receiptNo)=explode("_",$_POST['segmentReceiptNo']);
	$returnInvoiceArray=getInvoiceDetailBySegmentId($segmentNo,$receiptNo);
	$ableForEntry=getDetailPCREntryEnabledDisabled($receiptNo);
	$approvalSignatureArr=getDetailsForPrintNameAndSignature($receiptNo);
	$vendorDetailsArray=$returnInvoiceArray[0];
	$invoiceDetailsArray=$returnInvoiceArray[1];
	$remarkDetails=getRemarkDetails($segmentNo,$receiptNo);
	
	$returnInvoiceNoArray=getInvoiceSummaryDetailByPoNubmer($segmentNo);
	if(count($returnInvoiceArray)>0)
	{
		$totalRetentionAmt=0;
		foreach($returnInvoiceNoArray as $amount)	
		{
			$totalRetentionAmt+=$amount['AMOUNT'];	
		}
	}
	//echo"<pre>";print_r($remarkDetails);die;
	require_once('TCPDF/tcpdf.php');
	$pdf = '<body>
<center>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:none;">
	  <tbody>
		<tr height="100">
		  <td style="border:none;">         
	<table width="100%" border="0" cellspacing="0" cellpadding="4">
	  <tbody>
		<tr>		  
		  <td>
            <b>YTY Group	<br>
            YTY Industry Sdn. Bhd. (176157-M)<br>
            Project Completion Report</b>
          </td>
          <td><img src="image/logo.jpg" class="logo"></td>		  
		</tr>
	  </tbody>
	</table>         
		  </td>
		</tr>
		
		<tr>
		<td style="border:none;"> 
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tbody>
		<tr>
		  <td width="12%" align="left"><p><strong>&nbsp;Supplier</strong></p></td>      
		  <td width="88%"><strong>&nbsp;&nbsp;</strong>'.$vendorDetailsArray[0]['VENDOR_NAME'].'</td>
		</tr>
		<tr>
		  <td width="12%" align="left"><p><strong>&nbsp;Address</strong></p></td>       
		  <td width="88%">&nbsp;&nbsp;'.$vendorDetailsArray[0]['ADDRESS'].'</td>
		</tr>
		<tr>
		  <td width="12%" align="left"><p><strong>&nbsp;lndentor&nbsp;ref.</strong></p></td>       
		  <td width="88%">&nbsp;&nbsp;</td>
		</tr>
		<tr>
		 <td width="12%" align="left"><p><strong>&nbsp;Oracle&nbsp;Ref&nbsp;No</strong></p></td>       
		  <td width="88%">&nbsp;&nbsp;'.$vendorDetailsArray[0]['SEGMENT1'].'</td>
		</tr>
		<tr>
		  <td width="12%" align="left"><p><strong>&nbsp;Project&nbsp;Name</strong></p></td>      
		  <td width="88%">&nbsp;&nbsp;'.$vendorDetailsArray[0]['TASK_DESCRIPTION'].'</td>
		</tr>
		<tr>
		  <td width="12%" align="left"><p><strong>&nbsp;Project<br>&nbsp;Location</strong></p></td>       
		  <td width="88%">&nbsp;&nbsp;'.$vendorDetailsArray[0]['CAPEX_NO'].'</td>
		</tr>
		<tr>
		 <td width="12%" align="left"><p><strong>&nbsp;Prepared&nbsp;by</strong></p></td>       
		  <td width="88%">&nbsp;&nbsp;</td>
		</tr>
	   
		</tbody>
        </table>
        </td>
        </tr>
        
        <tr align="left">
        <td style="border:none; margin-top:10px; padding-top:5px" align="left" valign="top"> 
        <table width="40%" border="0" cellspacing="0" cellpadding="0" style="float:left;">
		<tbody>
		 <tr>
		  <td width="30%" align="left"><p><b>&nbsp;Project Total Amt</b></p></td> 
		  <td width="40%">&nbsp;'.number_format($vendorDetailsArray[0]['PO_AMT'],2).'</td>
		</tr>
		<tr>
		  <td width="30%" align="left"><p><b>&nbsp;GST Tax</b></p></td> 
		  <td width="40%">&nbsp;'.number_format($vendorDetailsArray[0]['TAX'],2).'</td>
		</tr>
		<tr>
		  <td width="30%"  align="left"><p><b>&nbsp;Grand Total amount</b></p></td>
          <td width="40%">&nbsp;'.number_format(($vendorDetailsArray[0]['PO_AMT']+$vendorDetailsArray[0]['TAX']),2).'</td>
		</tr>		
	  </tbody>
		</table>        
	  </td>
		</tr> 
         <tr align="left">
        <td style="border:none; width:100%" align="left" valign="top">   
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="float:left;">
		<tbody> 
        <tr>
		  <td style="border:none; width:20%"><b>Payments&nbsp;Terms&nbsp;:</b></td>
          <td style="border:none; width:40%" align="left">'.$vendorDetailsArray[0]['PAYMENT_TERM'].'</td>
		</tr>
        </tbody>
        </table>
        </td>
        </tr>
		<tr> 
            <td style="border:none;" valign="top"> 
				<p></p>  
                <table width="90%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td width="13%" align="center"><p><strong>Date</strong></p></td>
                  <td width="18%" align="center"><p><strong>Description</strong></p></td>
                  <td width="10%" align="center"><p><strong>Tax Invoice No.</strong></p></td>
                  <td width="6%" align="center"><p><strong>Claim %</strong></p></td>
                  <td width="7%" align="center"><p><strong>Qty (ltems)</strong></p></td>
                  <td width="14%" align="center"><p><strong>Amount Claimable<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(RM)</strong></p></td>
                  <td width="16%" align="center"><p><strong>Date of Payment by YTY</strong></p></td>
                  <td width="27%" align="center"><p><strong>Total Balance Outstanding<br>(RM)</strong></p></td>
                </tr>
                    <tr  height="40">
                          <td align="center">'.$invoiceDetailsArray[0]['INVOICE_DATE'].'</td>
                          <td align="center">'.$invoiceDetailsArray[0]['INV_DESC'].'</td>
                          <td align="center">'.$invoiceDetailsArray[0]['INVOICE_NUM'].'</td>
                          <td align="center"></td>
                          <td align="center">'.$invoiceDetailsArray[0]['QTY'].'</td>
                          <td align="center">'.number_format($invoiceDetailsArray[0]['INV_AMOUNT'],2).'</td>
                          <td align="center">'.$invoiceDetailsArray[0]['TBP'].'</td>
                          <td align="center">'.number_format(($vendorDetailsArray[0]['PO_AMT']-$invoiceDetailsArray[0]['INV_AMOUNT']),2).'</td>
                        </tr>';
						$payOutRm=($vendorDetailsArray[0]['PO_AMT']-$invoiceDetailsArray[0]['INV_AMOUNT']);
                      $pdf.='<tr  height="40">
                      <td align="center" style="border:none;"> </td>
                      <td align="center" style="border:none;"></td>
                      <td align="center" style="border:none;"></td>
                      <td align="center" style="border:none;"></td>
                      <td align="center" style="border:none;"></td>
                      <td align="center" ></td>
                      <td align="center"><p>TOTAL PAY·OUT %	</p></td>
                      <td align="center"></td>
                </tr>
                <tr height="40">
                      <td align="center"  style="border:none;"></td>
                      <td align="center" style="border:none;"></td>
                      <td align="center" style="border:none;"></td>
                      <td align="center"  style="border:none;"></td>
                      <td align="center"  style="border:none;"></td>
                      <td align="center"></td>
                      <td align="center"><p>TOTAL PAY OUT (RM)	</p></td>
                      <td align="center">'.number_format($payOutRm,2).'</td>
                </tr>
              </tbody>
            </table>

            </td>
		</tr>    
		<tr> 
            <td style="border:none;margin-top:20px;padding-top:20px">  
			<p></p>
              <table width="98%" border="0" cellspacing="0" cellpadding="0"> 
              <tbody>
                <tr>
                  <td width="12%" align="center"><p><strong>S.No</strong></p></td>
                  <td width="17%" align="center"><p><strong>Main Contract Value (RM)</strong></p></td>
                  <td width="20%" align="center"><p><strong>Previous  Certified Claims after deducting Advance+ Retention amount (RM)</strong></p></td>
                  <td width="14%" align="center"><p><strong>Present Claim (RM)</strong></p></td>
                  <td width="16%" align="center"><p><strong>Total Outstanding Balance<br>(RM)</strong></p></td>
                  <td width="23%" align="center"><p><strong>Remarks</strong></p></td>
                </tr>
                <tr  height="40">
                  <td align="center">1</td>
                  <td align="center">'.number_format($vendorDetailsArray[0]['PO_AMT'],2).'</td>
                  <td align="center">'.number_format($totalRetentionAmt,2).'</td>
                  <td align="center">'.number_format($invoiceDetailsArray[0]['INV_AMOUNT'],2).'</td>
				  <td align="center">'.number_format($payOutRm,2).'</td>
                  <td align="center"></td>
                </tr>
              </tbody>
            </table>
            </td>
		</tr>    
		<tr> 
                <td style="border:none;" valign="top">  
				<p></p>  
                  <table width="100%" border="1" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td width="40%" style="text-align:center;"><p><strong>Items</strong></p></td>
                      <td width="20%" style="text-align:center;"><p><strong>Date</strong></p></td>
                      <td width="40%" style="text-align:center;"><p><strong>Remarks</strong></p></td>
                    </tr>';
					if(count($remarkDetails)>0)
					{
						$sno=1;
						foreach($remarkDetails as $val)
						{
                     	 $pdf.='<tr>
								  <td align="center" style="border:none; padding:0px;">'.$val['ITEM_DESC'].'</td>
								  <td align="center" style="border:none;padding:0px;">'.date('Y-m-d',strtotime($val['P_DATE'])).'</td>
								  <td align="center" style="border:none; padding:0px;">'.$val['REMARK'].'</td>
								</tr>';
						}
					}
                  $pdf.='</tbody>
                </table>
                </td>
		</tr> 
		<tr> 
            <td style="border:none;" valign="top"> 
			<h1></h1><p></p><p></p><p></p>  
              <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tbody>';
			  if(count($approvalSignatureArr))
			  {
				  foreach($approvalSignatureArr as $sigDetails)
				  {
					  if($sigDetails['SIGNATURE'])
					  	$img=$sigDetails['SIGNATURE']->load();
					  //$data = $arr['BLOBDATA']->load();
                       $pdf.=' <tr>
                          <td width="15%" align="left"><p> Approved by :</p></td>
                          <td width="25%" align="left"><p> Name : <br> '.$sigDetails['APPROVER_NAME'].' <br>'.$sigDetails['DESIGNATION'].'('.$sigDetails['DEPARTMENT'].')</p></td>
                          <td width="35%" align="left" style="border-right:none"><p style="float:left; line-height: 60px;"> Signature : ';
						  if($sigDetails['SIGNATURE']) { 
						  	$pdf.='<span><img src="data:image/png;base64,'.base64_encode($img).'" style="float:right; width:50px; padding-left:35px; padding-top:8px"/></span>';
						  }
						  $pdf.='</p></td>
                          <td width="25%" align="left"><p> Date :  '.$sigDetails['CREATION_DATE'].'</p></td>
                        </tr>';
				  }
			  }
	          $pdf.=' </tbody>
            </table>
            </td>
		</tr>    
	  </tbody>
	</table>
    </center>
<style>
@media print {
  h1 {page-break-before: always;}
}

table { 
	
	border-collapse: collapse; 
	margin:10px auto;
	}

/* Zebra striping */
tr:nth-of-type(odd) { 
	background: #ffffff; 
	}

th { 
	background:#ECECEC; 
	color: white; 
	font-weight: bold; 
	}

td, th { 
	padding: 10px 4px;

border: 1px solid #1c1c1c;

font-size: 12px;
	}

.logo{width:165px; height:65px;}
/* 
Max width before this PARTICULAR table gets nasty
This query will take effect for any screen smaller than 760px
and also iPads specifically.
*/
@media 
only screen and (max-width: 760px),
(min-device-width: 768px) and (max-device-width: 1024px)  {

	
	/* Force table to not be like tables anymore */
	table, thead, tbody, th, td, tr { 
		display: block; 
	}
	
	/* Hide table headers (but not display: none;, for accessibility) */
	thead tr { 
		position: absolute;
		top: -9999px;
		left: -9999px;
	}
	
	tr { border: 1px solid #ccc; }
	
	td { 
		/* Behave  like a "row" */
		border: none;
		border-bottom: 1px solid #eee; 
		position: relative;
		padding-left: 50%; 
	}

	td:before { 
		/* Now like a table header */
		position: absolute;
		/* Top/left values mimic padding */
		top: 6px;
		left: 6px;
		width: 45%; 
		padding-right: 10px; 
		white-space: nowrap;
		/* Label the data */
		content: attr(data-column);

		color: #000;
		font-weight: bold;
	}

}

</style>
</body>
';
	//echo"<pre>";print_r($pdf);die;
	$tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	
	// set default monospaced font
	$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	// set title of pdf
	$tcpdf->SetTitle('Project Completion Report');
	
	// set margins
	$tcpdf->SetMargins(10, 10, 10, 10);
	$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	
	// set header and footer in pdf
	$tcpdf->setPrintHeader(false);
	$tcpdf->setPrintFooter(false);
	$tcpdf->setListIndentWidth(3);
	
	// set auto page breaks
	$tcpdf->SetAutoPageBreak(TRUE, 20);
	
	// set image scale factor
	$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	$tcpdf->AddPage();
	
	$tcpdf->SetFont('times', '', 10.5);
	$tcpdf->writeHTML($pdf, true, false, false, false, '');
	// Close and output PDF document
	// This method has several options, check the source code documentation for more information.
	$tcpdf->Output($_POST['segmentReceiptNo'].'.pdf', 'D'); // code to download file by Deep Rana
	//============================================================+
	exit();
 }
//echo"<pre>";print_r($_POST);die;
 ?>
<body>
<script type="text/javascript" src="js/wz_tooltip.js"></script> 
<body>
<style>
.btnSub {
	background-color:#058dff;
	color:#FFF;
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
	height:40px;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
.btnSub1 {
	background-color:#058dff;
	color:#FFF;
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
</style>
<center>
<?php 
if(count($vendorDetailsArray)>0)
{
	$returnInvoiceNoArray=getInvoiceSummaryDetailByPoNubmer($keyVal[0]);
	if(count($returnInvoiceArray)>0)
	{
		$totalRetentionAmt=0;
		foreach($returnInvoiceNoArray as $amount)	
		{
			$totalRetentionAmt+=$amount['AMOUNT'];	
		}
	}
	//echo"<pre>";print_r($remarkDetails);die;
	?>
    <table width="90%" border="0" cellspacing="0" cellpadding="0" style="border:none;">
      <tr>
          <td align="right" style="border:none;">
            <form id="pdfFile" name="pdfFile" method="POST" enctype="multipart/form-data">                         <a href="javascript:void(0)" style="text-decoration:none" class="btnSub1" onClick="SaveAsPdf()"><strong>Save as pdf</strong></a>
            	<input type="hidden" name="ExportTo" value="ExportToPdf" />
                <input type="hidden" name="segmentReceiptNo" value="<?php echo $keyVal[0]."_".$keyVal[1];?>" />
            </form>
          </td>
    </tr>
    </table>
	<table width="90%" border="0" cellspacing="0" cellpadding="0" style="border:none;">
	  <tbody>
		<tr>
		  <td style="border:none;">         
	<table width="100%" border="1" cellspacing="0" cellpadding="0">
	  <tbody>
		<tr>		  
		  <td>
            <b>YTY Group	<br>
            YTY Industry Sdn. Bhd. (176157-M)<br>
            Project Completion Report</b>
          </td>
          <td><img src="image/logo.jpg" class="logo"></td>		  
		</tr>
	  </tbody>
	</table>         
		  </td>
		</tr>
		
		<tr>
			<td style="border:none;">         
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tbody>
		<tr>
		  <td width="10%" align="left"><p><strong>Supplier</strong></p></td>      
		  <td width="90%"><?php echo $vendorDetailsArray[0]['VENDOR_NAME'];?></td>
		</tr>
		<tr>
		  <td width="10%" align="left"><p><strong>Address</strong></p></td>       
		  <td width="90%"><?php echo $vendorDetailsArray[0]['ADDRESS'];?></td>
		</tr>
		<tr>
		  <td width="10%" align="left"><p><strong>lndentor ref.</strong></p></td>       
		  <td width="90%"></td>
		</tr>
		<tr>
		 <td width="10%" align="left"><p><strong>Oracle Ref No</strong></p></td>       
		  <td width="90%"><?php echo $vendorDetailsArray[0]['SEGMENT1'];?></td>
		</tr>
		<tr>
		  <td width="10%" align="left"><p><strong>Project Name</strong></p></td>      
		  <td width="90%"><?php echo $vendorDetailsArray[0]['TASK_DESCRIPTION'];?></td>
		</tr>
		<tr>
		  <td width="10%" align="left"><p><strong>Project Location</strong></p></td>       
		  <td width="90%"><?php echo $vendorDetailsArray[0]['CAPEX_NO'];?></td>
		</tr>
		<tr>
		 <td width="10%" align="left"><p><strong>Prepared by</strong></p></td>       
		  <td width="90%"></td>
		</tr>
	   
		</tbody>
        </table>
        </td>
        </tr>
        
        <tr align="left">
        <td style="border:none; margin-top:1px; padding-top:1px" align="left" valign="top">   
            <table width="30%" border="0" cellspacing="0" cellpadding="0" style="float:left;">
            <tbody>
             <tr>
              <td width="33%" align="left"><p><b>Project Total Amt</b></p></td> 
              <td width="67%"><?php echo number_format($vendorDetailsArray[0]['PO_AMT'],2);?></td>
            </tr>
            <tr>
              <td width="33%" align="left"><p><b>GST Tax</b></p></td> 
              <td width="67%"><?php echo number_format($vendorDetailsArray[0]['TAX'],2);?></td>
            </tr>
            <tr>
              <td width="33%"  align="left"><p><b>Grand Total amount</b></p></td>
              <td width="67%"><?php echo number_format(($vendorDetailsArray[0]['PO_AMT']+$vendorDetailsArray[0]['TAX']),2);?></td>
            </tr>		
          </tbody>
            </table> 
          </td>
      	</tr> 
         <tr align="left">
        <td style="border:none;" align="left" valign="top">   
        <table width="50%" border="0" cellspacing="0" cellpadding="0" style="float:left;">
		<tbody> 
        <tr>
		  <td style="border:none; width:45px"><b>Payments&nbsp;Terms&nbsp;:</b></td>
          <td style="border:none;"><?php echo $vendorDetailsArray[0]['PAYMENT_TERM'];?></td>
		</tr>
        </tbody>
        </table>
        </td>
        </tr>
		<tr> 
            <td style="border:none;" valign="top">   
            <script>
			function submitFormFun()
			{
				var fileName=document.getElementById('attachment').value;
				var ext = fileName.split(".").pop().toLowerCase();
				if((ext!="pdf" && ext!="xlsx" && ext!="xls") &&  ext!="") {
					alert('Please upload .pdf,.xlsx,.xls extension file only.');
					document.getElementById('attachment').value="";
					return false;
				}
				else
				{
				 	if(document.getElementById('remarkExists').value=="0")
					{
						alert('Firstly enter the remark and then proceed.');
						return false;
					}
					else if(fileName!="")
					{
						if(confirm('Are you sure to upload the file. \n  Click ok to continue. Cancel to stop.'))
						{
							document.forms['attFile'].submit();
							return true;
						}
						else
						{
							return false;
						}
					}
				}
			}
			function SaveAsPdf()
			{
				document.forms['pdfFile'].submit();	
			}
			</script> 
				<?php
                if(count($invoiceDetailsArray)==0)
                {
                ?>
                <p><font color="#F5080C"><b>Note : </b>This pcr and invoice number has already been paid.</font></p>
                <?php
                }
                ?>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td width="10%" align="center"><p><strong>Date</strong></p></td>
                  <td width="20%" align="center"><p><strong>Description</strong></p></td>
                  <td width="10%" align="center"><p><strong>Tax Invoice No.</strong></p></td>
                  <td width="6%" align="center"><p><strong>Claim %</strong></p></td>
                  <td width="7%" align="center"><p><strong>Qty (ltems)</strong></p></td>
                  <td width="14%" align="center"><p><strong>Amount Claimable<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(RM)</strong></p></td>
                  <td width="16%" align="center"><p><strong>Date of Payment by YTY</strong></p></td>
                  <td width="30%" align="center"><p><strong>Total Balance Outstanding (RM)</strong></p></td>
                </tr>
                <?php
				$ableForEntry=getDetailPCREntryEnabledDisabled($keyVal[1]);
				$approvalSignatureArr=getDetailsForPrintNameAndSignature($keyVal[1]);
				//echo"<pre>";print_r($vendorDetailsArray);
				//echo"<pre>";print_r($invoiceDetailsArray);
				if(count($invoiceDetailsArray)>0)
				{
					$payOut=0;
					$payOutRm=0;
					foreach($invoiceDetailsArray as $invoice)
					{
				?>
                        <tr  height="40">
                          <td align="center"><?php echo $invoice['INVOICE_DATE'];?></td>
                          <td align="center"><?php echo $invoice['INV_DESC'];?></td>
                          <td align="center"><?php echo $invoice['INVOICE_NUM'];?></td>
                          <td align="center"><?php echo "";?></td>
                          <td align="center"><?php echo $invoice['QTY'];?></td>
                          <td align="center"><?php echo number_format($invoice['INV_AMOUNT'],2);?></td>
                          <td align="center"><?php echo $invoice['TBP'];?></td>
                          <td align="center"><?php echo number_format(($vendorDetailsArray[0]['PO_AMT']-($totalRetentionAmt+$invoice['INV_AMOUNT'])),2);?></td>
                        </tr>
                <?php
						$payOutRm+=($vendorDetailsArray[0]['PO_AMT']-($totalRetentionAmt+$invoice['INV_AMOUNT']));
					}
				}
				else
				{
					?>
                    <tr  height="40">
                      <td width="5%" align="center" colspan="8"><font color="#FF0206"><strong>No record found.</strong></font></td>
                    </tr>
                    <?php
					
				}
				?>
                  <tr  height="40">
                      <td align="center" style="border:none;">
                      <?php
					  if($ableForEntry[0]['TOTAL']=='1')
					  {
						?>
                        <form id="attFile" name="attFile" method="POST" enctype="multipart/form-data">                         <a href="javascript:void(0)" style="text-decoration:none" class="btnSub1"  onclick="document.getElementById('attachment').click()" onBlur="return submitFormFun()"><strong>Support File(s)</strong></a>
                          <input type='file' name="attachment" id="attachment" style="display:none">
                          <input type="hidden" name="oldFileName" id="oldFileName" value="<?php echo $remarkDetails[0]['EXTENSION'];?>" />
                          <input type="hidden" name="remarkExists" id="remarkExists" value="<?php if(count($remarkDetails) > 0) echo "1"; else echo "0";?>" />
                        </form>
                      <?php
					  }
					  ?>
                      </td>
                      <td align="left" style="border:none;" valign="top">
					  <?php 
					  $filePath=UPLOAD_URL.SUPPORT_DOCUMENT.$remarkDetails[0]['EXTENSION'];
					  if(file_exists($filePath))
					  {
						?>
                        <a href="<?php echo SUPPORT_DOCUMENT.$remarkDetails[0]['EXTENSION'];?>" style="text-decoration:none" class="btnSub1" target="_blank"><strong>Download</strong></a>                        
						<?php  
					  }
					  ?>
					  </td>
                      <td align="center" style="border:none;"></td>
                      <td align="center" style="border:none;"></td>
                      <td align="center" style="border:none;"></td>
                      <td align="center" ></td>
                      <td align="center"><p>TOTAL PAY OUT %	</p></td>
                      <td align="center"></td>
                </tr>
                <tr height="40">
                      <td align="center"  style="border:none;"></td>
                      <td align="center" style="border:none;"></td>
                      <td align="center" style="border:none;"></td>
                      <td align="center"  style="border:none;"></td>
                      <td align="center"  style="border:none;"></td>
                      <td align="center"></td>
                      <td align="center"><p>TOTAL PAY OUT (RM)	</p></td>
                      <td align="center"><?php echo number_format($payOutRm,2);?></td>
                </tr>
              </tbody>
            </table>
            </td>
		</tr>    
		<tr> 
            <td style="border:none;" valign="top">      
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td width="10%" align="center"><p><strong>S.No</strong></p></td>
                  <td width="20%" align="center"><p><strong>Main Contract Value (RM)</strong></p></td>
                  <td width="23%" align="center"><p><strong>Previous  Certified Claims after deducting Advance+ Retention amount (RM)</strong></p></td>
                  <td width="14%" align="center"><p><strong>Present Claim (RM)</strong></p></td>
                  <td width="16%" align="center"><p><strong>Total Outstanding Balance (RM)</strong></p></td>
                  <td width="20%" align="center"><p><strong>Remarks</strong></p></td>
                </tr>
                <tr  height="40">
                  <td align="center">1</td>
                  <td align="center"><?php echo number_format($vendorDetailsArray[0]['PO_AMT'],2);?></td>
                  <td align="center"><a href="InvoiceSummary.php?poNumber=<?php echo $keyVal[0];?>" target="_blank"><strong><?php echo number_format($totalRetentionAmt,2);?></strong></a></td>
                  <td align="center"><?php echo number_format($invoiceDetailsArray[0]['INV_AMOUNT'],2);?></td>
                  <td align="center"><?php echo number_format($payOutRm,2);?></td>
                  <td align="center"><?php echo "";?></td>
                </tr>
              </tbody>
            </table>
            </td>
		</tr>    
		<tr> 
                <td style="border:none;" valign="top">
                <form name="remarkForm" id="remarkForm" method="post">
                  <input type="hidden" name="oldFileExt" id="oldFileExt" value="<?php echo $remarkDetails[0]['EXTENSION'];?>" />	
                <a style="color:#000000; text-decoration:underline; font-size:18px;"><strong>Summary of Work Completion  Progress Justification</strong></a>&nbsp;&nbsp;&nbsp;&nbsp;
					<?php
					  if($ableForEntry[0]['TOTAL']=='1')
					  {
						?><input type="text" name="counter" id="counter" onKeyUp="return addMoreRemark(this.value)" style="width:10%; border:1px solid #000000; height: calc(1.5em + .75rem + 2px);" placeholder="Add more remark" maxlength="3" autocomplete="off"/>  
                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody style="border: 2px solid #4D4D4D;">
                    <tr>
                      <td width="40%" style="text-align:center; background-color: #d9d9d9;"><p><strong>Items</strong></p></td>
                      <td width="20%" style="text-align:center; background-color: #d9d9d9;"><p><strong>Date</strong></p></td>
                      <td width="40%" style="text-align:center; background-color: #d9d9d9;"><p><strong>Remarks</strong></p></td>
                    </tr>
                    <?php
					if(count($remarkDetails)>0)
					{
						$sno=1;
						foreach($remarkDetails as $val)
						{
					?>
                        <tr>
                              <td align="center" style="border:none; padding:0px;"><input type="text" name="items_<?php echo $sno;?>" id="items_<?php echo $sno;?>" style="width:80%; " value="<?php echo $val['ITEM_DESC'];?>"/></td>
                              <td align="center" style="border:none;padding:0px;"><input type="date" name="itemDate_<?php echo $sno;?>" id="itemDate_<?php echo $sno;?>" style="width:100%" value="<?php echo date('Y-m-d',strtotime($val['P_DATE']));?>" /></td>
                              <td align="center" style="border:none;padding:0px;"><textarea name="remark_<?php echo $sno;?>" id="remark_<?php echo $sno;?>" cols="70"><?php echo $val['REMARK'];?></textarea></td>
                        </tr>
                    <?php
						$sno++;
						}
					}
					else
					{
					?>
                      <tr>
                          <td align="center" style="border:none; padding:0px;""><input type="text" name="items_1" id="items_1" style="width:80%;"/></td>
                          <td align="center" style="border:none;padding:0px;""><input type="date" name="itemDate_1" id="itemDate_1" style="width:100%" /></td>
                          <td align="center" style="border:none; padding:0px;""><textarea name="remark_1" id="remark_1" cols="70"></textarea></td>
                    </tr>
                    <?php
					}
					?>
                    <tr>
                        <td colspan="3" align="right" id="rowDiv" style="border:none; padding:0px;"> </td>
                     </tr> 
                    <tr>
                    	<input type="hidden" name="enterRemark" id="enterRemark" value="<?php if($sno >0) echo $sno-1; else echo "0";?>" />
                        <td colspan="3" align="right"><input type="submit" name="submit" id="submit" class="btnSub" value="Save" onClick="return submitForm();"></td>
                      </tr> 
                  </tbody>
                </table>
                	<?php
					  }
					  else
					  {
						?>
                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tbody style="border: 2px solid #4D4D4D;">
                                <tr>
                                  <td width="40%" style="text-align:center; background-color: #d9d9d9;"><p><strong>Items</strong></p></td>
                                  <td width="20%" style="text-align:center; background-color: #d9d9d9;"><p><strong>Date</strong></p></td>
                                  <td width="40%" style="text-align:center; background-color: #d9d9d9;"><p><strong>Remarks</strong></p></td>
                                </tr>
                                <?php
                                if(count($remarkDetails)>0)
                                {
                                    $sno=1;
                                    foreach($remarkDetails as $val)
                                    {
                                ?>
                                    <tr>
                                          <td align="center" style="border:none; padding:0px;"><?php echo $val['ITEM_DESC'];?></td>
                                          <td align="center" style="border:none;padding:0px;"><?php echo date('Y-m-d',strtotime($val['P_DATE']));?></td>
                                          <td align="center" style="border:none;padding:0px;"><?php echo $val['REMARK'];?></td>
                                    </tr>
                                <?php
                                    $sno++;
                                    }
                                }
                                else
                                {
                                ?>
                                  <tr>
                                      <td align="center" colspan="3" style="border:none; padding:0px;""><font color="red"><strong>No remark found</strong></font></td>
                                </tr>
                                <?php
                                }
                                ?>
                              </tbody>
                        </table>
                        <?php  
					  }
					  ?>
                </form>
                </td>
		</tr>    
		<tr> 
            <td style="border:none;" valign="top"> 
            <p></p>     
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
              <?php
			  //echo "<pre>";print_r($approvalSignatureArr);die;
			  if(count($approvalSignatureArr))
			  {
				  foreach($approvalSignatureArr as $sigDetails)
				  {
					  if($sigDetails['SIGNATURE'])
					  	$img=$sigDetails['SIGNATURE']->load();
					  //$data = $arr['BLOBDATA']->load();
					 ?>
                        <tr>
                          <td width="10%" align="left"><p> Approved by :</p></td>
                          <td width="20%" align="left"><p> Name :  <?php echo $sigDetails['APPROVER_NAME'];?> <br><?php echo $sigDetails['DESIGNATION'];?> (<?php echo $sigDetails['DEPARTMENT'];?>)</p></td>
                          <td width="13%" align="left" style="border-right:none"><p> Signature :</p></td>
                          <td width="10%" align="left" style="border-left:none"><p><?php  if($sigDetails['SIGNATURE']) { ?><img src="data:image/png;base64,<?php echo base64_encode($img);?>" height="60" width="100" style="vertical-align:bottom" /> <?php }?></p></td>
                          <td width="14%" align="left"><p> Date :  <?php echo $sigDetails['CREATION_DATE'];?></p></td>
                        </tr>
                <?php
				  }
			  }
				  ?>
<!--                <tr>
                  <td width="10%" align="left"><p> Confirmed by :</p></td>
                  <td width="20%" align="left"><p> Name : <br> Mr Naveen Gupta<br>VP Civil & Structural</p></td>
                  <td width="23%" align="left"><p> Signature :</p></td>
                  <td width="14%" align="left"><p> Date :</p></td>
                </tr>
                <tr>
                  <td width="10%" align="left"><p> Confirmed by :</p></td>
                  <td width="20%" align="left"><p> Name : <br> Mr Rakesh Pahwa<br>VP (Purchasing)</p></td>
                  <td width="23%" align="left"><p> Signature :</p></td>
                  <td width="14%" align="left"><p> Date :</p></td>
                </tr>
                <tr>
                  <td width="10%" align="left"><p> Approved by :</p></td>
                  <td width="20%" align="left"><p> Name : <br> Mr Kanungo<br>Sr. Vice President</p></td>
                  <td width="23%" align="left"><p> Signature :</p></td>
                  <td width="14%" align="left"><p> Date :</p></td>
                </tr>
                <tr>
                  <td width="10%" align="left"><p> Approved by :</p></td>
                  <td width="20%" align="left"><p> Name : <br> Mr Andrew <br>Director Business Operations</p></td>
                  <td width="23%" align="left"><p> Signature :</p></td>
                  <td width="14%" align="left"><p> Date :</p></td>
                </tr>
                <tr>
                  <td width="10%" align="left"><p> Approved by :</p></td>
                  <td width="20%" align="left"><p> Name : <br> Mr Jayanta <br>CFO</p></td>
                  <td width="23%" align="left"><p> Signature :</p></td>
                  <td width="14%" align="left"><p>Date :</p></td>
                </tr>
-->              </tbody>
            </table>
            </td>
		</tr>    
	  </tbody>
	</table>
    <script>
	function addMoreRemark(enterVal)
	{
		if(!Number(enterVal))	
		{
			alert("Please enter numeric value only.");	
			document.getElementById("counter").value="";
			document.getElementById("rowDiv").innerHTML="";
			return false;
		}
		else
		{
			if(enterVal>100)
			{
				alert("You can not  enter more than hundred.");	
				document.getElementById("counter").value="";
				document.getElementById("rowDiv").innerHTML="";
				return false;
			}
			document.getElementById("submit").disabled=true;
			var enterRemark=document.getElementById("enterRemark").value;
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					document.getElementById("rowDiv").innerHTML=this.responseText;
					document.getElementById("submit").disabled=false;
				}
			};
			xmlhttp.open("GET", "getRemarkLinesRow.php?counter="+enterVal+"&enterRemark="+enterRemark, true);
			xmlhttp.send();
		}
	}
	function submitForm()
	{
		if(confirm("Are you sure to save it ? \n Click ok to continue. Cancel to stop."))
		{
			var counter=(Number(document.getElementById("counter").value)+1);
			//var enterRemark=Number(document.getElementById("enterRemark").value);
			//if($enterRemark > 0)
				//$row=$enterRemark+$i;
			//else
				//$row=$i+1;
			
			var display="";
			for(var i=1;i<=counter; i++)
			{
				if(document.getElementById("items_"+i).value=="")
				{
					var display=display+'Items field cannot be left blank at row '+i+'. \n';
				}
				if(document.getElementById("itemDate_"+i).value=="")
				{
					var display=display+'Date field cannot be left blank at row '+i+'. \n';
				}
				if(document.getElementById("remark_"+i).value=="")
				{
					var display=display+'Remark field cannot be left blank at row '+i+'. \n';
				}
			}
			if(display!=""){
				alert(display);
				return false;
			}
			else
				return true;
		}
		else
		{
			return false;
		}
	}
	</script>
    <?php
}
else
{
	?>
    
    <?php
}
?>
</center>


<style>
table { 
	
	border-collapse: collapse; 
	margin:10px auto;
	}

/* Zebra striping */
tr:nth-of-type(odd) { 
	background: #ffffff; 
	}

th { 
	background:#ECECEC; 
	color: white; 
	font-weight: bold; 
	}

td, th { 
	padding: 10px 4px;

border: 2px solid #1c1c1c;

font-size: 16px;
	}
	
	input
	{
		border-radius: 10px;

box-shadow: 0px 0px 12px #f2f2f2;

border: 1px solid #dfdfdf;

padding: 0px 19px;
height: calc(1.5em + .75rem + 2px);
	}
	textarea
	{
		border-radius: 10px;

box-shadow: 0px 0px 12px #f2f2f2;

border: 1px solid #dfdfdf;

padding: 0px 19px;
height: calc(1.5em + .75rem + 2px);
	}
	
.logo{width:165px; height:65px;}
/* 
Max width before this PARTICULAR table gets nasty
This query will take effect for any screen smaller than 760px
and also iPads specifically.
*/
@media 
only screen and (max-width: 760px),
(min-device-width: 768px) and (max-device-width: 1024px)  {

	
	/* Force table to not be like tables anymore */
	table, thead, tbody, th, td, tr { 
		display: block; 
	}
	
	/* Hide table headers (but not display: none;, for accessibility) */
	thead tr { 
		position: absolute;
		top: -9999px;
		left: -9999px;
	}
	
	tr { border: 1px solid #ccc; }
	
	td { 
		/* Behave  like a "row" */
		border: none;
		border-bottom: 1px solid #eee; 
		position: relative;
		padding-left: 50%; 
	}

	td:before { 
		/* Now like a table header */
		position: absolute;
		/* Top/left values mimic padding */
		top: 6px;
		left: 6px;
		width: 45%; 
		padding-right: 10px; 
		white-space: nowrap;
		/* Label the data */
		content: attr(data-column);

		color: #000;
		font-weight: bold;
	}

}

</style>
</body>

