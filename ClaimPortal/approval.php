<?php include("templates/top.php") ?>
 
<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
   <?php include("templates/sidebar.php") ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <?php include("templates/navbar.php") ?><br><br>

      <div class="container-fluid">
       <div class="card mx-auto" style="width: 660px;">
        <div class="card-header text-white text-center" style="background:#013243;"><h3 class="float-left">Approval Sequence</h3></div>
         <div class="card-body">
          <form id="signupform" action="signup.php" method="POST">
            <table width="500px">
              <tr>
                  <td>
                    <div class="form-group">
                      <label for="code"><b>Authority Level:</b></label>
                      <!-- <input type="text"  name="name" id="name"  class="form-control pull-right" required="">   -->
                      <select name="department" class="form-control" style="width:300px;">
                        <option value="">Select Authority Level</option>
                        <option value="Level 1">Level 1</option>
                        <option value="Level 2">Level 2</option>
                      </select>
                    </div>
                    <td>
                    <div class="form-group">
                      <label for="name"><b>Authrised Superior:</b></label>
                      <input type="text"  name="name" id="name" value="" class="form-control pull-right" style="width:300px;">  
                    </div>
                  </td>
                  </td>
              </tr>
              <!-- <tr>
                <td>
                    <div class="form-group">
                      <label for="name"><b>Authrised Superior:</b></label>
                    </div>
                  </td>
                  <td>
                    <div class="form-group">
                      <input type="text"  name="name" id="name" value="" class="form-control pull-right">  
                    </div>
                  </td>
              </tr> -->
              <!-- <tr>
                <td>
                  <div class="form-group">
                      <label for="name"><b>Message:</b></label>
                    </div>
                  </td>
                  <td>
                    <div class="form-group">
                      <textarea cols="5" rows="5" class="form-control"></textarea>
                    </div>
                  </td>
              </tr> -->
            </table>
               <input type="submit" class="btn btn-primary float-right" value="Approve">
               <!-- <input type="submit" class="btn btn-danger " value="Decline"> -->
            </form>
         </div>
      </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->


<?php require_once("templates/footer.php"); ?>