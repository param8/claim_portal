<?php  
ob_start();
ini_set('display_errors', 1); 
include("processNew.php"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Employee Portal</title>
    <!-- Bootstrap -->
    <link href="https://medisafetech.co.in/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://medisafetech.co.in/fonts/font-awesome/css/font-awesome.min.css"
        type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="https://medisafetech.co.in/css/jquery.animateSlider.css">
    <link rel="stylesheet" href="https://medisafetech.co.in/css/styles.css">
    <link rel="stylesheet" href="https://medisafetech.co.in/css/animate.css">
    <script src="https://medisafetech.co.in/js/ControlFunctions.js" type="text/javascript"></script>
    <link href="https://medisafetech.co.in/css/fullcalendar.css" rel="stylesheet" />
    <link href="https://medisafetech.co.in/css/fullcalendar.print.css" rel="stylesheet"
        media="print" />
    <!-- Resource style -->
    <script src="https://medisafetech.co.in/js/modernizr.js"></script>
    <!-- Modernizr -->
    <script>

        function validate() {

            document.getElementById("hidurl").value = location.hash;


            if (document.getElementById("txtUserid").value == "") {

                document.getElementById("txtUserid").focus();
                alert("Please Enter User ID");
                return false;

            }

            if (document.getElementById("txtPassword").value == "") {
                document.getElementById("txtPassword").focus();
                alert("Please Enter PassWord");
                return false;

            }
            return true;

        }
        function empty() {
         
            document.getElementById("lblmsg").style.visibility = "hidden";
        }
    </script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form name="form1" method="post" action="login.php" id="form1">
 

<div class="menu-section">
<div class="logo">
                    <img src="image/logo_dual.svg"  />
                  
                    </div>
    <div class="container"> 
    
      <h1 style="font-size: 20px;color: #005baa;text-transform: uppercase;font-weight: 600;text-align: center;">Please enter your User ID & Password to sign in!</h1>		
		<div class="row">
			<div class="col-sm-12">
					<div class="row">
                   
						<div class="well-lg">
							<div class="col-md-4">
							</div>
							<div class="col-md-4 well-lg">
                            
											
								<div class="form-horizontal main-login formdesign">
									<div class="form-group">
										
									  <label>User :</label>
                                      <input name="txtUserid" type="text" id="txtUserid" title="Enter User Id" class="form-control" placeholder="User Id" onkeypress="empty()" />
									</div>
									<div class="form-group">
                                     <label>Password :</label>
										<input name="txtPassword" type="password" id="txtPassword" title="Enter Password" class="form-control" placeholder="Password" onkeypress="empty()" />
									</div>
									  <div class="form-group">
									  <?php
									  $msg="";
									  if($_REQUEST['msg']=='1')
									  	$msg="Password does not match.";
									  else if($_REQUEST['msg']=='0')
									  	$msg="Invalid login details.";
									  else if($_REQUEST['msg']=='2')
									  	$msg="Inactive account.";
										
										?>
										<span id="lblmsg" style="color:#FF0000; font-weight:bold"><?php echo $msg;?></span>
									</div>
									<div class="form-group">
										<label class="checkbox-inline">
											<input type="checkbox">
											<strong style="color:#0a8dff;">Remember me</strong></label>
										<input type="submit" name="btnLogin" value="Sign In" onclick="return validate();" id="btnLogin" title="Click To Login" class="btn btn-warning pull-right loginbtndesign" />
									</div>
								  
									<div class="form-group form-group-lg text-right ">
										<a href="javascript:void(0);" onclick='return check()'><strong style="color:#0a8dff;">Forgot Password </strong></a>
										|<a href="javascript:void(0);" onclick='return check()' style="color:#0a8dff;"> Problem Signing In ?</a>
										<p id="ptag" style="display: none;margin-top: 14px; text-align:left">
											<strong style="color:#001F51;font-size: 11px;">Please contact ES department at oraclesupport@ytygroup.com.my with your employee code.</strong></p>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<a href="#top" class="back-top"><i class="fa fa-chevron-circle-up"></i>Back to top</a>
			
		
		
		</div>
	</div>
    <div class="footer">
            <div class="container">
                <div class="row">
                <div class="col-md-12">
                  <p >Copyright &copy; 2015. All Rights Reserved.</p>
                </div>
                </div>
            </div>
		</div>
    
    </div>
        <input type="hidden" name="InvalidCode" id="InvalidCode" />
        <input type="hidden" name="hidurl" id="hidurl" value="0" />
        <input type="hidden" name="hiduser" id="hiduser" value="0" />
        <script type="text/javascript" language="javascript">
            CheckCode('InvalidCode');
        </script>
        <script>
            function check() {
                var lTable = document.getElementById("ptag");
                lTable.style.display = (lTable.style.display == "table") ? "none" : "table";

            }
        </script>
    </form>
</body>
</html>
