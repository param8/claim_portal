<!-- Sidebar -->
<style>
.badge {
  position: absolute;
  top: 0px;
  right: 70px;
  padding: 6px 9px;
  border-radius: 50%;
  background-color: red;
  color: white;
}
.profilelink{
	text-align: center;
}

.profilelink .prblue{
	background: #0258a3;
    height: 50px;
}
.profilelink .prgray{
	    height: 98px;
    background: #dedede;
}

.profilelink img{
	width: 90px;
    height: 90px;
    position: absolute;
    top: 66px;
    left: 69px;
    border-radius: 50px;
}
.modal-dialog .close {
	-webkit-appearance: none;
	padding: 3px;
	cursor: pointer;
	background: 0 0;
	border: 1px solid #ccc;
	border-radius: 50%;
	position: absolute;
	right: -10px;
	top: -10px;
	background-color: #fff;
	opacity: 1;
	height: 30px;
	width: 30px;
	z-index: 9999;
}
.modal-dialog {
	margin: 10% auto;
}
.modal-dialog .text-box {
	max-height: 150px;
	overflow: auto;
	min-height: 150px;
	border-bottom: 1px solid #ccc;
	margin-bottom: 10px;
	font-size: 13px;
}
.footer {
    padding: 20px 0 10px;
    color: #fff;
    text-align: center;
    background: #001a31;
    border-bottom: 3px solid #001a31;
}
.loginbtndesign {
    background-color: #005baa;
    border: 1px solid #005baa;
    padding: 5px 20px;
}
.PROFILEPIC {
    width: 400px !important;
    margin: 100px auto !important;
}
.PROFILEPICHEADER {
    min-height: 16.42857143px!important;
    padding: 9px!important;
    border-bottom: 1px solid #e5e5e5!important;
    background: #005baa!important;
    color: white!important;
    font-weight: bolder!important;
}
form-control {
    display: block;
    width: 100%;
    height: calc(1.5em + .75rem + 2px);
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.1 !important;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
</style>
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class=" text-white navbar-mine" ><img src="https://flatsindelhi.in/geosysinventry/assets/images/logo.png"></div>
      <div class="list-group list-group-flush imgsidebar">
      <div class="profilelink">
      <div class="prblue"></div>
      <div class="prgray"></div>
      <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog PROFILEPIC">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header PROFILEPICHEADER">
        <h5 class="modal-title">Upload Photo</h5>
      </div>
      <form name="updatePic" id="updatePic" method="post" enctype="multipart/form-data" action="UpdateProfilePicScript.php">
      <div class="modal-body" style="padding-bottom:50px;">
        	<input type="file" name="profile_pic" id="profile_pic" required   class="form-control"   />
            <p style="color:red; text-align:left; font-weight: bold;" id="errormessage"></p><br>
            <input type="submit"  value="Modify"   class="btn btn-warning pull-right loginbtndesign" style="color:rgb(255, 255, 255); " onClick="return checkImageExt()" />
            <input type="button"  value="Close"   class="btn btn-danger pull-right loginbtndesign" class="close" data-dismiss="modal" />
      </div>
      </form>
    </div>

  </div>
</div>
  <script>
	function checkImageExt()
	{
		var fileExtension = ['jpeg', 'jpg', 'JPG'];
		obj=document.getElementById('profile_pic');
		if(obj.value!="")
		{
			//*************Code to check file Size*************
			var FileSize = obj.files[0].size / 1024 / 1024; // in MB
			if (FileSize > 1) {
				document.getElementById('errormessage').innerHTML="File should be less than 1MB.";
				 $(obj).val(''); //for clearing with Jquery
				 return false;
			} 	
			else
			{
				document.getElementById('errormessage').innerHTML="";
			}
			if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1)
			{
				document.getElementById('errormessage').innerHTML="Only '.jpeg','.jpg', '.JPG' formats are allowed.";
				return false; 
			}
			else
			{
				document.getElementById('errormessage').innerHTML="";
				return true;	
			}
		}
	}
  </script>    
       <?php
                  if($userDetails[0]['PHOTO'])
                  {
                    $img=$userDetails[0]['PHOTO']->load();
                    ?>
                    <a href="" title="Update Profile Picture" data-toggle="modal" data-target="#myModal"><img src="data:image/png;base64,<?php echo base64_encode($img);?>"></a>
                    <?php
                  }
                  else
                  {
                    $img='https://flatsindelhi.in/geosysinventry/assets/images/user.png';
                    ?>
                     <a href="" title="Update Profile Picture" data-toggle="modal" data-target="#myModal"><img src="<?php echo $img;?>"></a>
                    <?php
                  }
                  ?>
                  
                  <p style="margin-top:-37px; font-size: 15px; font-weight:bold;"><?php echo $_SESSION['LOGIN_CREDENTAILS']['EMP_NAME'];?></p>
     </div> 
      
      <?php if(strpos($_SERVER['SCRIPT_NAME'],"settle-form.php")){?>
        <a href="settle-form.php" class="list-group-item list-group-item-action active">Apply new claim</a>
		<?php } else{?>
         <a href="settle-form.php" class="list-group-item list-group-item-action ">Apply new claim</a>
        <?php }?>
        
         <?php if(strpos($_SERVER['SCRIPT_NAME'],"claim-approval.php")){?>
        <a href="claim-approval.php" class="list-group-item list-group-item-action active">Claims for approval <span class="badge"><?php echo $totalClaimFor[0][TOTAL_CLAIM];?></span></a>
        <?php } else{?>
         <a href="claim-approval.php" class="list-group-item list-group-item-action ">Claims for approval<span class="badge"><?php echo $totalClaimFor[0][TOTAL_CLAIM];?></span></a>
        <?php }?>
        
         <?php if(strpos($_SERVER['SCRIPT_NAME'],"declined-claim.php")){?>
        <a href="declined-claim.php" class="list-group-item list-group-item-action active">Declined claims</a>
        <!-- <a href="#" class="list-group-item list-group-item-action">Profile</a> -->
         <?php } else{?>
         <a href="declined-claim.php" class="list-group-item list-group-item-action ">Declined claims</a>
        <?php }?>
        
         <?php if(strpos($_SERVER['SCRIPT_NAME'],"approved-claim.php")){?>
        <a href="approved-claim.php" class="list-group-item list-group-item-action active">Approved claims</a>
         <?php } else{?>
         <a href="approved-claim.php" class="list-group-item list-group-item-action ">Approved claims</a>
        <?php }?>
        
         <?php if(strpos($_SERVER['SCRIPT_NAME'],"pending-claim.php")){?>
        <a href="pending-claim.php" class="list-group-item list-group-item-action active">Pending claims</a>
         <?php } else{?>
         <a href="pending-claim.php" class="list-group-item list-group-item-action ">Pending claims</a>
        <?php }?>
      </div>
    </div>                   

<!--sidebar-wrapper -->
