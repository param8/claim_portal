  <nav class="navbar navbar-expand-lg navbar-mine border-bottom" >
        <span class="text-white" id="menu-toggle" style="font-size: 1.2rem;">Self Service Portal for Employee Claim Settlement</span>

        <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link text-white">Welcome&nbsp; <?php echo $empName;?></span>&nbsp;&nbsp;&nbsp;|&nbsp;</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link text-white" href="dashboard.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-white" href="logout.php">Logout</a>
            </li>
          </ul>
        </div>
      </nav>