<?php 
//*********************Function to convert Doc file into PDF File********************
function ChangeFileIntoPdf($filePath, $target)
{
	$myCommand = "export HOME=$target/ &&  /usr/bin/libreoffice6.1 --headless --convert-to pdf  $target"."/"."$filePath --outdir $target 2>&1"; 
	$retunVal=shell_exec($myCommand); 
	return $retunVal;
}
//*********************Constants********************
$convertFileExt=array("xlsx","txt","pptx","docx","tif","doc","xls","odt","ott");
?>