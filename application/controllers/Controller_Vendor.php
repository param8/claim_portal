<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_Vendor extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Vendor';

		$this->load->model('Model_vendor');
	  }

	/* 
	* It only redirects to the manage category page
	*/
	public function index()
	{
		$data = $this->Model_vendor->getVendorData();
		$this->data['array'] = $data;   
		$this->render_template('vendor/index', $this->data);	
	}	

	/*
	* It checks if it gets the category id and retreives
	* the category information from the category model and 
	* returns the data into json format. 
	* This function is invoked from the view page.
	*/
	public function fetchVendorDataById($id) 
	{
		if($id) {
			$data = $this->Model_vendor->getVendorDataById($id);
			echo json_encode($data);
		}

		return false;
	}

	/*
	* Fetches the category value from the category table 
	* this function is called from the datatable ajax function
	*/
	public function fetchCategoryData()
	{
		$result = array('data' => array());

		$data = $this->Model_vendor->getCategoryData();

		foreach ($data as $key => $value) {

			// button
			$buttons = '';
			$buttons .= '<button type="button" class="btn btn-warning btn-sm" onclick="editFunc('.$value['id'].')" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></button>';

			if($value['id']!=1 && $value['id']!=2) {
				$buttons .= ' <button type="button" class="btn btn-danger btn-sm" onclick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}
				

			$status = '<span class="label label-success">'.$value['unit'].'</span>';

			$result['data'][$key] = array(
				$value['name'],
				$status,
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}

	/*
	* Its checks the category form validation 
	* and if the validation is successfully then it inserts the data into the database 
	* and returns the json format operation messages
	*/
	public function create()
	{
		$response = array();
		$this->form_validation->set_rules('Vendor_name', 'Vendor Name', 'trim|required');
		$this->form_validation->set_rules('mobile_no', 'Mobile no', 'trim|required');

		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'vendor_name' => $this->input->post('Vendor_name'),
        		'vendor_code' => $this->input->post('vendor_code'),	
        		'email' => $this->input->post('email'),	
        		'mobile_no' => $this->input->post('mobile_no'),	
        		'address' => $this->input->post('address'),	
        		'status' => $this->input->post('status'),	
        	);
			//echo"<pre>";print_r($data);die;
        	$create = $this->Model_vendor->create($data);
        	if($create == true) {
				$this->session->set_flashdata('success', 'Successfully created');
				redirect('Controller_Vendor/index', 'refresh');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the vendor information';	
				$this->render_template('vendor/index', $response);	
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }
	}

	/*
	* Its checks the category form validation 
	* and if the validation is successfully then it updates the data into the database 
	* and returns the json format operation messages
	*/
	public function Update()
	{

		$response = array();
		$this->form_validation->set_rules('Vendor_name1', 'Vendor Name', 'trim|required');
		$this->form_validation->set_rules('mobile_no1', 'Mobile no', 'trim|required');

		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
			$id=$this->input->post('vendor_id');
        	$data = array(
        		'vendor_name' => $this->input->post('Vendor_name1'),
        		'vendor_code' => $this->input->post('vendor_code1'),	
        		'email' => $this->input->post('email1'),	
        		'mobile_no' => $this->input->post('mobile_no1'),	
        		'address' => $this->input->post('address1'),	
        		'status' => $this->input->post('status1'),	
        	);
			//echo"<pre>";print_r($id);die;
        	$create = $this->Model_vendor->update($data,$id);
        	if($create == true) {
				$this->session->set_flashdata('success', 'Successfully updated.');
				redirect('Controller_Vendor/index', 'refresh');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the vendor information';	
				$this->render_template('vendor/index', $response);	
        	}
        }

	}

	/*
	* It removes the category information from the database 
	* and returns the json format operation messages
	*/
	public function remove()
	{
		$delete_vendor_id = $this->input->post('delete_vendor_id');
		$response = array();
		//echo"<pre>";print_r($delete_vendor_id);die;
		if($delete_vendor_id) {
			$delete = $this->Model_vendor->remove($delete_vendor_id);
			if($delete == true) {
				$this->session->set_flashdata('success', 'Successfully deleted.');
				redirect('Controller_Vendor/index', 'refresh');
			}
			else {
				$this->session->set_flashdata('danger', 'Error in database.');
				redirect('Controller_Vendor/index', 'refresh');
			}
		}
	}

}