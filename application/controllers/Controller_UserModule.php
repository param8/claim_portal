<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_UserModule extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'User-type wise module';

		$this->load->model('Model_user_module');
		$this->load->model('Model_users');
        $this->load->library('encryption');		

	  }

	/* 
	* It only redirects to the manage category page
	*/
	public function index()
	{
		$data = $this->Model_user_module->getUserTypeDetails("");
		$this->data['array'] = $data;
		$this->render_template('permission/index', $this->data);	
	}	
	public function indexUser()
	{
		$data = $this->Model_user_module->getUserTypeDetails('') ;
		$userData = $this->Model_users->getUserData('') ;
		$this->data['array'] = $data;
		$this->data['$userData'] = $userData;
	//	echo"<pre>";print_r($userData);die;
		$this->render_template('permission/indexUser', $this->data);	
	}	

	public function fetchUserTypeDataById($id)
	{
		$data = $this->Model_user_module->getUserTypeDetails($id);
		echo json_encode($data);
	}

	public function ChangePassword()
	{
			$password= $this->input->post('password');
			$new_pass= $this->input->post('new_pass');
			$confirm_pass= $this->input->post('confirm_pass');
			$old_pass= $this->input->post('old_pass');
			$userId= $this->session->userdata['id'];
			//echo"<pre>";print_r($userId);die;
			if($password==$old_pass){
			if($new_pass==$confirm_pass){
				$data=array("password"=>$confirm_pass);
				$update = $this->Model_users->ChangePassword($data,$userId);				
				if($update == true) {
					//echo "<script type='text/javascript'>alert('Password has been sucessfully updated.');";
					redirect('dashboard', 'refresh');
						
				}
				else {
					$response['success'] = false;
					$response['messages'] = 'Error in the database while creating the information';		
					redirect('dashboard', 'refresh');
					
				}
			}
		}
	}

	public function do_upload(){    
 	  $registration_id= $this->input->post('registration_id');
      $config['upload_path']= 'assets/images/';
      $config['allowed_types']= 'gif|jpg|png';
      $this->load->library('upload', $config);
	  $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
	  $_FILES['image']['name']=$registration_id.".".$ext;
	  @unlink('assets/images/'.$registration_id.".".$ext); // This is a relative path to the file
	 // echo"<pre>";print_r($this->upload->do_upload('image'));die;  
      if (!$this->upload->do_upload('image')){
        $error = array('error' => $this->upload->display_errors());
        $this->load->view('dashboard', $error);
      }else{
        $upload_data = $this->upload->data();
		//echo "<script type='text/javascript'>alert('Profile Image been updated sucessfully.');;
       	redirect('dashboard', 'refresh');
      }
      	redirect('dashboard', 'refresh');
  }
	public function profileClientRegistration()
	{
		$response = array();
		//echo"<pre>";print_r($this->input->post());die;
		$this->form_validation->set_rules('profile_client_name', 'Client Name', 'trim|required');
		$this->form_validation->set_rules('profile_mobile_no', 'Mobile No', 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
		
		if ($this->form_validation->run() == TRUE) {
			// $password = $this->encryption->encrypt($this->input->post('password1'));
			$client_id = $this->input->post('profile_client_id');
			$data = array(
				'username' => $this->input->post('profile_contact_person_name'),
				'email' => $this->input->post('profile_email'),				
				'mobile_no' => $this->input->post('profile_mobile_no'),
				'address' => $this->input->post('profile_address'),
			);
			//echo"<pre>";print_r($data);die;
			$create = $this->Model_users->UpdateClientRegistration($data,$client_id);
			if($create == true) {
				$_SESSION['email']=$this->input->post('profile_email');
				$_SESSION['mobile_no']=$this->input->post('profile_mobile_no');
				$_SESSION['username']= $this->input->post('profile_contact_person_name');
				$_SESSION['address']=$this->input->post('profile_address');
				//echo "<script type='text/javascript'>alert('Profile has been updated sucessfully.');;
					redirect('dashboard', 'refresh');
			}
			else {
				$response['success'] = false;
				$response['messages'] = 'Error in the database while creating the information';	
				redirect('dashboard', 'refresh');
			}
		}
		else {
			$response['success'] = false;
			foreach ($_POST as $key => $value) {
				$response['messages'][$key] = form_error($key);
			}
		}
	}  
	/*
	* Its checks the category form validation 
	* and if the validation is successfully then it inserts the data into the database 
	* and returns the json format operation messages
	*/
	public function create()
	{
		$response = array();
		$this->form_validation->set_rules('user_type', 'User Type', 'trim|required');
		//$this->form_validation->set_rules('priority', 'Priority', 'trim|required');

		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'user_type' => $this->input->post('user_type'),
        		'remark' => $this->input->post('remark'),	
        	);
			//echo"<pre>";print_r($data);die;
        	$create = $this->Model_user_module->create($data);
        	if($create == true) {
				$this->session->set_flashdata('success', 'Successfully created');
				redirect('Controller_UserModule/index', 'refresh');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the user type information';	
				$this->render_template('Controller_UserModule/index', $response);	
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }
	}

	/*
	* Its checks the category form validation 
	* and if the validation is successfully then it updates the data into the database 
	* and returns the json format operation messages
	*/
	public function update()
	{

		$response = array();
		$this->form_validation->set_rules('edit_user_type', 'User Type', 'trim|required');
	//	$this->form_validation->set_rules('edit_priority', 'Priority', 'trim|required');

		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
			$user_type_id=$this->input->post('user_type_id');
        	$data = array(
        		'user_type' => $this->input->post('edit_user_type'),
        		'priority' => $this->input->post('edit_priority'),	
        		'remark' => $this->input->post('edit_remark'),	
        	);
			//echo"<pre>";print_r($user_type_id);die;
        	$create = $this->Model_user_module->UpdateUserType($data,$user_type_id);
        	if($create == true) {
				$this->session->set_flashdata('success', 'Successfully updated');
				redirect('Controller_UserModule/index', 'refresh');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the user type information';	
				$this->render_template('Controller_UserModule/index', $response);	
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }
	}

	/*
	* It removes the category information from the database 
	* and returns the json format operation messages
	*/
	public function remove()
	{
		$delete_User_id = $this->input->post('delete_User_id');
		$response = array();
		if($delete_User_id) {
			$delete = $this->Model_user_module->delete($delete_User_id);
			if($delete == true) {
				$this->session->set_flashdata('success', 'Successfully deleted.');
				redirect('Controller_UserModule/index', 'refresh');
			}
			else {
				$this->session->set_flashdata('danger', 'Error in database.');
				redirect('Controller_UserModule/index', 'refresh');
			}
		}
	}
	
	public function assignModule()
	{
		$data = $this->Model_user_module->getUserTypeDetails("");
		$this->data['array'] = $data;
		$this->render_template('permission/assignModule', $this->data);	
	}
	public function getUserTypeDetails()
	{
	    return 	$data = $this->Model_user_module->getUserTypeDetails("");
	}
	public function getModuleMenuLink()
	{
	    return 	$data = $this->Model_user_module->getModuleMenuLink("");
	}
	public function AssignModuleLink()
	{

		$response = array();
		$userTypeArra=$this->input->post('userTypeArra');
		if(count($userTypeArra)>0){
			$errorFlag=0;
			$moduleLinkArray=array();
			foreach($userTypeArra as $userTypeId)
			{
				if($this->input->post('user_type_'.$userTypeId)!="")
				{
					$linkArray=$this->input->post('moduleLinkArr_'.$userTypeId);
					foreach($linkArray as $link)
					{
						if($this->input->post('module_link_'.$userTypeId.'_'.$link)!="")
						{
							$buttonArray=$this->input->post('buttonPermission_'.$userTypeId.'_'.$link);
							foreach($buttonArray as $button)
							{
								if($this->input->post('button_'.$userTypeId.'_'.$link.'_'.$button)!="")
								{
									$errorFlag=1;
									$moduleLinkArray[$userTypeId][$link][]=$button;
								}
							}
						}
					}
				}
			}
		}
        if ($errorFlag==1 && count($moduleLinkArray)>0) {
			$create = $this->Model_user_module->InsertUpdateModuleLink($moduleLinkArray,$userTypeArra);
			$this->session->set_flashdata('success', 'Successfully assigned.');
			redirect('Controller_UserModule/assignModule', 'refresh');
       	}
		else {
			$this->session->set_flashdata('error', 'Error in the database while creating the assign link information.');
			redirect('Controller_UserModule/assignModule', 'refresh');
		}
     }
	 
	public function GetUserTypeWisePermissionDetail($userTypeId)
	{
		return $data = $this->Model_user_module->GetUserTypeWisePermissionDetail($userTypeId);
	}
	
	public function AddUser()
	{
		$response = array();
		$this->form_validation->set_rules('user_name', 'User Name', 'trim|required');
		$this->form_validation->set_rules('mobile_no', 'Mobile No', 'trim|required');
		$this->form_validation->set_rules('user_type_id', 'User Type', 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
            $password = $this->encryption->encrypt($this->input->post('password'));
        	$data = array(
        		'username' => $this->input->post('user_name'),
        		'email' => $this->input->post('email'),	
        		'mobile_no' => $this->input->post('mobile_no'),	
				'address' => $this->input->post('address'),	
				'password' => $password,
				'user_type_id' => $this->input->post('user_type_id'),	
        	);
           // echo"<pre>";print_r($data);die;
        	$create = $this->Model_users->AddUser($data);
        	if($create == true) {
				$this->session->set_flashdata('success', 'Successfully created');
				redirect('Controller_UserModule/indexUser', 'refresh');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the project information';	
				$this->render_template('Controller_UserModule/indexUser', $response);	
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }
	}
    public function fetchUserDataById($id) 
	{
		// alert("hello");
		if($id) {
			$data = $this->Model_users->getUserData($id);
			$decriptPass=$this->encryption->decrypt($data['password']); 
			unset($data['password']);
			if($decriptPass)
				$data['password']=$decriptPass;
			else
				$data['password']="";
				
			echo json_encode($data);
		}

		return false;
	}
	public function UpdateUser()
	{
		$response = array();
		if($this->input->post()) {
			$this->form_validation->set_rules('edit_username', 'User Name', 'trim|required');
			$this->form_validation->set_rules('edit_mobile_no', 'Mobile no', 'trim|required');
			$this->form_validation->set_rules('edit_user_type_id', 'User Type', 'trim|required');
			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
			$id=$this->input->post('edit_user_id');
	        if ($this->form_validation->run() == TRUE) {
                $password = $this->encryption->encrypt($this->input->post('edit_password'));
            	$data = array(
            		'username' => $this->input->post('edit_username'),
            		'email' => $this->input->post('edit_email'),	
            		'mobile_no' => $this->input->post('edit_mobile_no'),	
    				'address' => $this->input->post('edit_address'),	
    				'password' => $password,
    				'user_type_id' => $this->input->post('edit_user_type_id'),	
            	);
				//echo"<pre>";print_r($id);die;
	        	$update = $this->Model_users->UpdateUser($data,$id);
	        	if($update == true) {
					$response['success'] = true;
					$this->session->set_flashdata('success', 'Successfully updated.');
					redirect('Controller_UserModule/indexUser', 'refresh');
	        	}
	        	else {
	        		$response['success'] = false;
					$this->session->set_flashdata('success', 'Error in the database while updated the brand information.');
					redirect('Controller_UserModule/indexUser', 'refresh');
	        	}
	        }
	        else {
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        }
		}
		else {
			$response['success'] = false;
			$this->session->set_flashdata('success', 'Error please refresh the page again !!.');
			redirect('Controller_Project/index', 'refresh');
		}
	}
	public function removeUser()
	{
		$user_id = $this->input->post('delete_user_id');
		$response = array();
		if($user_id) {
			$delete = $this->Model_users->delete($user_id);
			if($delete == true) {
				$response['success'] = true;
				$this->session->set_flashdata('success', 'Successfully removed.');
				redirect('Controller_UserModule/indexUser', 'refresh');
			}
			else {
				$this->session->set_flashdata('success', 'Error in the database while removing the project information.');
				redirect('Controller_UserModule/indexUser', 'refresh');
			}
		}
	}
    
}