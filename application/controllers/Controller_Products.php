<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_Products extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Products';
		$this->load->model('model_products');
		$this->load->model('model_category');
		$this->load->model('model_stores');
		$this->load->model('Model_vendor');
	}

    /* 
    * It only redirects to the manage product page
    */
	public function index()
	{
		$this->render_template('products/index', $this->data);	
	}

    /*
    * It Fetches the products data from the product table 
    * this function is called from the datatable ajax function
    */
	public function fetchProductData()
	{
		$result = array('data' => array());

		$data = $this->model_products->getProductData();
		//echo"<pre>";print_r($data);die;
		foreach ($data as $key => $value) {

           $store_data = $this->model_stores->getStoresData($value['store_id']);
            $category_data = $this->model_category->getCategoryDataById($value['category_id']);

			// button
            $buttons = '';
    			$buttons .= '<a href="'.base_url('Controller_Products/update/'.$value['id']).'" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>';

    			$buttons .= ' <button type="button" class="btn btn-danger btn-sm" onclick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			

			$img = '<img src="'.base_url($value['image']).'" alt="'.$value['name'].'" class="img-circle" width="50" height="50" />';

            $availability = ($value['availability'] == 1) ? '<span class="label label-success">New</span>' : '<span class="label label-warning">Old</span>';

            $qty_status = '';
            if($value['qty'] <= 10) {
                $qty_status = '<span class="label label-warning">Low !</span>';
            } else if($value['qty'] <= 0) {
                $qty_status = '<span class="label label-danger">Out of stock !</span>';
            }

			$result['data'][$key] = array(
				$img,
                $category_data['name']." (".$category_data['unit'].")",
				$value['name'],
				$value['price'],
                $value['qty'] . ' ' . $qty_status,
                $store_data['name'],
				$buttons
			);
		} // /foreach
		//echo"<pre>";print_r($result);die;
		echo json_encode($result);
	}	
	public function fetchProductDataForAssign()
	{
		$result = array('data' => array());
		$catId="";
		if(isset($_REQUEST['cat']))
			$catId=$_REQUEST['cat'];
			
		$data = $this->model_products->getProductDataByCatId($catId);
		foreach ($data as $key => $value) {	
            $store_data = $this->model_stores->getStoresData($value['store_id']);
            $category_data = $this->model_category->getCategoryDataById($value['category_id']);
            $buttons = '';
				if($value['qty']>0)
				{
    			$buttons .= '<button type="button" class="btn btn-info btn-sm" onclick="assignToUser('.$value['id'].','.$value['qty'].')" data-toggle="modal" data-target="#assignModal"><i class="fa fa-share-square-o"></i> <strong>Assign</strong></button>';
				}
    			$buttons .= ' <button type="button" class="btn btn-info btn-sm" onclick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="glyphicon glyphicon-fast-backward"></i> <strong>Return</strong></button>';
			

			$img = '<img src="'.base_url($value['image']).'" alt="'.$value['name'].'" class="img-circle" width="50" height="50" />';

            $availability = ($value['availability'] == 1) ? '<span class="label label-success">New</span>' : '<span class="label label-warning">Old</span>';

            $qty_status = '';
            if($value['qty'] <= 10) {
                $qty_status = '<span class="label label-warning">Low !</span>';
            } else if($value['qty'] <= 0) {
                $qty_status = '<span class="label label-danger">Out of stock !</span>';
            }


			$result['data'][$key] = array(
				$img,
                $category_data['name']." (".$category_data['unit'].")",
				$value['name'],
				$value['price'],
                $value['qty'] . ' ' . $qty_status,
                $store_data['name'],
				$buttons
			);
		} // /foreach
		echo json_encode($result);
	}	

    /*
    * If the validation is not valid, then it redirects to the create page.
    * If the validation for each input field is valid then it inserts the data into the database 
    * and it stores the operation message into the session flashdata and display on the manage product page
    */
	
	public function AssignTo()
	{
		
            // true case
		$userAssign=$this->input->post('assignForVl');
		if($userAssign=='user')
		{
			$data = array(
			'product_id' => $this->input->post('productId'),
			'assign_to' => $this->input->post('assignTo'),
			'department' => $this->input->post('Department'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'gender' => $this->input->post('gender'),
			'remark' => $this->input->post('remark'),
			'assign_for' => $this->input->post('assignForVl'),
			'location' => $this->input->post('location'),
			'quantity' =>1,
			);
		}
		else
		{
			$data = array(
			'product_id' => $this->input->post('productId'),
			'assign_to' => $this->input->post('placeassignTo'),
			'department' => $this->input->post('placeDepartment'),
			'phone' => $this->input->post('placephone'),
			'remark' => $this->input->post('placeremark'),
			'assign_for' => $this->input->post('assignForVl'),
			'location' => $this->input->post('placeassignTo'),
			'quantity' => $this->input->post('plQty'),
			);
		}
		
		$create = $this->model_products->AssignTo($data);
		//echo"<pre>";print_r($create);die;
		if($create == true) {
		$this->session->set_flashdata('success', 'Successfully assigned.');
		redirect('Controller_Members/index', 'refresh');
        }
	}
	public function create()
	{
        // echo 'came';
        // exit();
		///if(!in_array('createProduct', $this->permission)) {
           // redirect('dashboard', 'refresh');
        //}

		$this->form_validation->set_rules('product_name', 'Product name', 'trim|required');
		// $this->form_validation->set_rules('sku', 'SKU', 'trim|required');
		$this->form_validation->set_rules('price', 'Price', 'trim|required');
		$this->form_validation->set_rules('qty', 'Qty', 'trim|required');
		$this->form_validation->set_rules('category', 'Category', 'trim|required');
		
	
        if ($this->form_validation->run() == TRUE) {
            // true case
        	$upload_image = $this->upload_image();
        	$bill_copy = $this->upload_bill_copy();
			//echo"<pre>";print_r($this->input->post());die;
        	$data = array(
        		'name' => $this->input->post('product_name'),
        		// 'sku' => $this->input->post('sku'),
        		'price' => $this->input->post('price'),
        		'qty' => $this->input->post('qty'),
        		'image' => $upload_image,
        		'bill_copy' => $bill_copy,
        		'description' => $this->input->post('description'),
        		'attribute_value_id' => json_encode($this->input->post('attributes_value_id')),
        		'brand_id' => json_encode($this->input->post('brands')),
        		'category_id' => $this->input->post('category'),
                'store_id' => $this->input->post('store'),
                'expiry_date' => $this->input->post('expiry_date'),
                'bar_code' => $this->input->post('barcode'),
                'product_entry' => $this->input->post('enterBy'),
                'serial_no' => $this->input->post('serial_no'),
                'other_remark' => $this->input->post('otherRemark'),
                'date_of_purchase' => $this->input->post('purchase_date'),
        		'availability' => $this->input->post('availability'),
        		'vendor_id' => $this->input->post('vendor'),
        	);
			//echo"<pre>";print_r($dataExtraDets);die;
        	$create = $this->model_products->create($data);
        	if($create == true) {
				$dataExtraDets = array(
					'ram' => $this->input->post('RAM'),
					'hdd' => $this->input->post('HDD'),
					'mother_board' => $this->input->post('mother_board'),
					'power_supply' => $this->input->post('power_supply'),
					'display_screen' => $this->input->post('screen'),
					'Other' => $this->input->post('Other'),
					'unique_id' =>rand(10,1000000),
					'product_id' =>$create,
				);
				$create = $this->model_products->create_pro_extra_detail($dataExtraDets);
        		$this->session->set_flashdata('success', 'Successfully created');
        		redirect('Controller_Products/create', 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('errors', 'Error occurred!!');
        		redirect('Controller_Products/create', 'refresh');
        	}
        }
        else {
            // false case

        	// attributes 
			$this->data['category'] = $this->model_category->getActiveCategroy();        	
			$this->data['stores'] = $this->model_stores->getActiveStore();        	
			$this->data['vendor'] = $this->Model_vendor->getVendorDataById("");        	
			//echo"<pre>";print_r($this->data['vendor']);die;
            $this->render_template('products/create', $this->data);
        }	
	}

    /*
    * This function is invoked from another function to upload the image into the assets folder
    * and returns the image path
    */
	public function upload_image()
    {
    	// assets/images/product_image
        $config['upload_path'] = 'assets/images/product_image';
        $config['file_name'] =  uniqid();
        $config['allowed_types'] = 'gif|jpg|png|pdf|PDF|txt';
        $config['max_size'] = '1000';

        // $config['max_width']  = '1024';s
        // $config['max_height']  = '768';
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('product_image'))
        {
            $error = $this->upload->display_errors();
            return $error;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $type = explode('.', $_FILES['product_image']['name']);
            $type = $type[count($type) - 1];
            
            $path = $config['upload_path'].'/'.$config['file_name'].'.'.$type;
            return ($data == true) ? $path : false;            
        }
    }
	public function upload_bill_copy()
    {
    	// assets/images/product_image
        $config['upload_path']  = 'assets/images/product_image';
        $config['file_name'] =  uniqid();
        $config['allowed_types'] = 'gif|jpg|png|pdf|PDF|txt|xls|xlsx';
        $config['max_size'] = '1000';

        // $config['max_width']  = '1024';s
        // $config['max_height']  = '768';
       $this->load->library('upload',$config);
	   //$this->upload->initialize($config);
		//echo"<pre>";print_r($this->upload->do_upload('bill_copy'));die;
        if ( ! $this->upload->do_upload('bill_copy'))
        {
            $error = $this->upload->display_errors();
            return $error;
        }
        else
        {
		
            $data = array('upload_data' => $this->upload->data());
            $type = explode('.', $_FILES['bill_copy']['name']);
            $type = $type[count($type) - 1];
            
            $path = $config['upload_path'].'/'.$config['file_name'].'.'.$type;
            return ($data == true) ? $path : false;            
        }
    }

    /*
    * If the validation is not valid, then it redirects to the edit product page 
    * If the validation is successfully then it updates the data into the database 
    * and it stores the operation message into the session flashdata and display on the manage product page
    */
	public function update($product_id)
	{      
        if(!$product_id) {
            redirect('dashboard', 'refresh');
        }

		$this->form_validation->set_rules('product_name', 'Product name', 'trim|required');
		// $this->form_validation->set_rules('sku', 'SKU', 'trim|required');
		$this->form_validation->set_rules('price', 'Price', 'trim|required');
		$this->form_validation->set_rules('qty', 'Qty', 'trim|required');
		$this->form_validation->set_rules('category', 'Category', 'trim|required');
        if ($this->form_validation->run() == TRUE) {
            // true case
            
        	$data = array(
        		'name' => $this->input->post('product_name'),
        		// 'sku' => $this->input->post('sku'),
        		'price' => $this->input->post('price'),
        		'qty' => $this->input->post('qty'),
        		'description' => $this->input->post('description'),
        		'attribute_value_id' => json_encode($this->input->post('attributes_value_id')),
        		'brand_id' => json_encode($this->input->post('brands')),
        		'category_id' => $this->input->post('category'),
                'store_id' => $this->input->post('store'),
                'expiry_date' => $this->input->post('expiry_date'),
                'bar_code' => $this->input->post('barcode'),
                'serial_no' => $this->input->post('serial_no'),
                'other_remark' => $this->input->post('otherRemark'),
                'date_of_purchase' => $this->input->post('purchase_date'),
        		'availability' => $this->input->post('availability'),
        		'vendor_id' => $this->input->post('vendor'),
        	);
			$dataExtraDets = array(
				'ram' => $this->input->post('RAM'),
				'hdd' => $this->input->post('HDD'),
				'mother_board' => $this->input->post('mother_board'),
				'power_supply' => $this->input->post('power_supply'),
				'display_screen' => $this->input->post('screen'),
				'Other' => $this->input->post('Other'),
			);
			$create = $this->model_products->update_pro_extra_detail($dataExtraDets,$product_id,$this->input->post('category'));
            
//            if($_FILES['product_image']['size'] > 0) {
//                $upload_image = $this->upload_image();
//                $upload_image = array('image' => $upload_image);
//                
//                $this->model_products->update($upload_image, $product_id);
//            }
            if($_FILES['bill_copy']['size'] > 0) {
                $bill_copy = $this->upload_bill_copy();
                $bill_copy = array('bill_copy' => $bill_copy);
                
                $this->model_products->update($bill_copy, $product_id);
            }
			//echo"<pre>";print_r($product_data);die;
            $update = $this->model_products->update($data, $product_id);
            if($update == true) {
                $this->session->set_flashdata('success', 'Successfully updated');
				$this->data['category'] = $this->model_category->getActiveCategroy();           
				$this->data['stores'] = $this->model_stores->getActiveStore();          
				$this->data['vendor'] = $this->Model_vendor->getVendorDataById("");        	
				$product_data = $this->model_products->getProductData($product_id);
				//echo"<pre>";print_r($product_data);die;
				$this->data['product_data'] = $product_data;
				$this->render_template('products/edit', $this->data); 
            }
            else {
                $this->session->set_flashdata('errors', 'Error occurred!!');
                redirect('Controller_Products/update/'.$product_id, 'refresh');
            }
        }
        else {
            // attributes 
            $this->data['category'] = $this->model_category->getActiveCategroy();           
            $this->data['stores'] = $this->model_stores->getActiveStore();          
			$this->data['vendor'] = $this->Model_vendor->getVendorDataById("");        	
            $product_data = $this->model_products->getProductData($product_id);
			//echo"<pre>";print_r($product_data);die;
            $this->data['product_data'] = $product_data;
            $this->render_template('products/edit', $this->data); 
        }   
	}

    /*
    * It removes the data from the database
    * and it returns the response into the json format
    */
	public function remove()
	{
        $product_id = $this->input->post('product_id');

        $response = array();
        if($product_id) {
            $delete = $this->model_products->remove($product_id);
            if($delete == true) {
                $response['success'] = true;
                $response['messages'] = "Successfully removed"; 
            }
            else {
                $response['success'] = false;
                $response['messages'] = "Error in the database while removing the product information";
            }
        }
        else {
            $response['success'] = false;
            $response['messages'] = "Refersh the page again!!";
        }

        echo json_encode($response);
	}
	public function ReturnProduct()
	{
        $product_id = $this->input->post('asProId');
        $returnBy = $this->input->post('return_by');
        $to_warehouse = $this->input->post('to_warehouse');
        $data = array("product_id"=>$product_id,"returnBy"=>$returnBy,"to_warehouse"=>$to_warehouse);
        if($product_id) {
			$create = $this->model_products->ReturnBy($data);
			//echo"<pre>";print_r($create);die;
			if($create == true) {
			$this->session->set_flashdata('success', 'Successfully returned');
			redirect('Controller_Members/index', 'refresh');
			}
		}
	}
	public function GetAssignProducts()
	{
        $product_id = $this->input->post('pId');
        $response = array();
        $combine = array();
        if($product_id) {
            $listDetails = $this->model_products->GetAssignProductDetails($product_id,'Issue');
			$warehouseLists = $this->model_stores->getStoresData();
			$listingVal=array();
			foreach($listDetails as $list)
			{
				$listingVal[]=array("proId"=>$list['assign_product_id'],"aTo"=>$list['assign_to']);
			}
			$listing=array();
			foreach($warehouseLists as $warelist)
			{
				($warelist['active']==1) ?  $status='Active' : $status='Scrap';
				$listing[]=array("id"=>$warelist['id']."_".$warelist['active'],"name"=>$warelist['name'].' ('.$status.')');
			}
			//echo"<pre>";print_r($listing);die;
			$response[]=$listingVal;
			$response[]=$listing;
			echo json_encode($response); 
        }
	}
	public function GetActiveCategory()
	{
        $response = array();
		$listDetails = $this->model_category->getActiveCategroy();     
		$listingVal=array();
		foreach($listDetails as $list)
		{
			$listingVal[]=array("category_id"=>$list['id'],"category_name"=>$list['name']);
		}
		echo json_encode($listingVal); 
	}
	
	public function saleScrap()
	{
		$listDetails = $this->model_products->GetScrapAllDetails();     
		$this->data['array']=$listDetails;
		$this->render_template('products/indexScrap', $this->data);	
	}
	public function GetProdductDetails()
	{
		return $listDetails = $this->model_products->GetProdductDetails();     
	}
	public function createScrap()
	{
        $productArray =explode("_",$this->input->post('item'));
        $measurement = $this->input->post('item_qty');
        $unit_sale = $this->input->post('unit_sale');
        $cumulative = $this->input->post('cumulative');
        $remark = $this->input->post('remark');
        $scrap_for = $this->input->post('scrap_for');
		if($this->input->post('scrap_id')!=0)
		{
			$data = array("scrap_unit"=>$this->input->post('scrap_unit')+$unit_sale,"remark"=>$remark);
			$update = $this->model_products->updateScrapData($data,$this->input->post('scrap_id'));
			if($update == true) {
				$this->session->set_flashdata('success', 'Successfully updated.');
				redirect('Controller_Products/saleScrap', 'refresh');
				}
		}
		else
		{
			$data = array("item_id"=>$productArray[0],"item_name"=>$productArray[1],"measurement"=>$measurement,"scrap_unit"=>$unit_sale,"cumulative_unit"=>$cumulative,"remark"=>$remark,"scrap_for"=>$scrap_for);
        if($data) {
			$create = $this->model_products->insertScrapData($data);
			//echo"<pre>";print_r($create);die;
			if($create == true) {
				$this->session->set_flashdata('success', 'Successfully inserted.');
				redirect('Controller_Products/saleScrap', 'refresh');
				}
			}
		}
	}
	public function fetchScrapDetail($id)
	{
		$listDetails = $this->model_products->GetScrapDetails($id);    
		echo json_encode($listDetails); 
	}
	public function updateSalesUnit($ids)
	{
		//echo"<pre>";print_r($ids);die;
		list($scrapId,$salesUnit,$cumulative)=explode("_",$ids);
		$listDetails = $this->model_products->updateSalesUnit($salesUnit,$scrapId,$cumulative);    
		echo json_encode($listDetails); 
	}
	public function TransferAsset()
	{
		$stores = $this->model_stores->getActiveStore();
		$this->data['store']=$stores;
		$this->render_template('products/transferProducts', $this->data); 
	}
	public function ShowWareHouse()
	{
		$whId=$this->uri->segments[3];
		$stores = $this->model_stores->getActiveStore();
		if($stores){
			$storeArr=array();
			foreach($stores as $val){
				if($whId!=$val['id'])
					$storeArr[]=$val;
			}
		}
		//echo"<pre>";print_r($storeArr);die;
		echo json_encode($storeArr);
	}
	public function GetAllProductByWareHId()
	{
		$whId=$this->uri->segments[3];
		$stores = $this->model_products->GetAllProductByWarehouseId($whId);
		$html="";
		$html.='<table id="manageTable" class="table table-bordered table-striped">
		  <thead style="background-color:#3c8dbc; color:#ffffff">
		  <tr>
			<th><input type="checkbox" name="selectAll" onclick="CheckAllProduct(0)" id="selectAll" style="width:20px"/></th>
			<th>Product</th>
			<th>Model</th>
			<th>Quantity</th>
		  </tr>
		 </thead>';
		if(count($stores)>0){
			foreach($stores as $val){
				$html.='<tr>
					<td><input type="checkbox" name="checkProduct'.$val['id'].'" id="checkProduct'.$val['id'].'" style="width:20px"/><input type="hidden" name="checkProductsArr[]" value="'.$val['id'].'" /></td>
					<td>'.$val['name'].'</td>
					<td>'.$val['serial_no'].'</td>
					<td>'.$val['qty'].'</td>
				</tr>';
			}
		}
		else
		{
		$html.='<tr>
					<td  colspan="4"><font color="red"><strong>No product for transfer.</strong></font></td>
				</tr>';	
		}
		$html.='</table>';
		//echo"<pre>";print_r($storeArr);die;
		echo json_encode($html);
	}
	
	public function transferToWarehouse()//checkProduct26
	{
		$toWarehouse=$this->input->post('transferTo');
		$fromWHId=$this->input->post('fromWHId');
		$checkProductsArr=$this->input->post('checkProductsArr');
		if(count($checkProductsArr)>0){
			$updateProduct=array();
			$insertTransferHistory=array();
			foreach($checkProductsArr as $pId){
				if($this->input->post('checkProduct'.$pId)=='on'){
					$updateProduct[$pId]=array("store_id"=>$toWarehouse,"status"=>'Inprocess');
					$insertTransferHistory[]=array("from_warehouse_id"=>$fromWHId,"to_warehouse_id"=>$toWarehouse,"product_id"=>$pId);
				}
			}
		}
		//echo"<pre>";print_r($updateProduct);die;
		$stores = $this->model_products->InsertUpadateTransferHistroy($updateProduct,$insertTransferHistory);
		redirect('Controller_Products/TransferAsset', 'refresh');
	}
	public function AcceptAsset()
	{
		$stores = $this->model_products->getStoreWiseAllProductRequestNew();
		$storeProductArr=array();
		if(count($stores)>0){
			foreach($stores as $val){
				$storeProductArr[$val['store_id']."_".$val['store_name']][]=$val;
			}
		}
		//echo"<pre>";print_r($storeProductArr);die;
		$this->data['store_wise_products']=$storeProductArr;
		$this->render_template('products/productRequest', $this->data); 
	}
	
	public function AcceptRequest()//checkProduct26
	{
		//echo"<pre>";print_r($this->input->post());die;
		$toWarehouse=$this->input->post('transferTo');
		$checkProductsArr=$this->input->post('checkProductsArr');
		if(count($checkProductsArr)>0){
			$updateProduct=array();
			$insertTransferHistory=array();
			foreach($checkProductsArr as $pId){
				if($this->input->post('checkProduct'.$pId)=='on'){
					$updateProduct[$pId]=array("store_id"=>$toWarehouse,"status"=>'Accepted');
				}
			}
		}
		//echo"<pre>";print_r($updateProduct);die;
		$stores = $this->model_products->InsertUpadateTransferHistroy($updateProduct,$insertTransferHistory);
		$this->session->set_flashdata('success', 'Request successfully accepted.');
		redirect('Controller_Products/AcceptAsset', 'refresh');
	}
	
	public function TransferAssetReport()
	{
		$storeId=$this->input->post('searchSite');
	    $from_date=$this->input->post('from_date');
	    $to_date=$this->input->post('to_date');
		$data= $this->model_stores->getStoresListing();
		$stores = $this->model_stores->getFactoryTransferDataList($storeId,$from_date,$to_date);
		$storeProductArr=array();
		if(count($stores)>0){
			$from_store_name='';
			foreach($stores as $val){
			//	echo $val['from_store_name'];die;
				if($val['from_store_name']==''){
					$from_store_name='Factory';
				}
				else{
					$from_store_name=$val['from_store_name'];
				}
				$storeProductArr[$from_store_name."_".$val['to_store_name']][]=$val;
			}
		}
	// echo"<pre>";print_r($storeProductArr);die;
	    $this->data['site_details']= $data;
		$this->data['store_wise_products']=$storeProductArr;
		$this->data['store_id']=$storeId;
		$this->data['from_date']=$from_date;
		$this->data['to_date']=$to_date;
		$this->render_template('products/assetTransferReport', $this->data); 
	}
	
	
// 	public function TransferAssetReport()
// 	{
// 		$storeId=$this->input->post('searchCat');
// 		$stores = $this->model_products->getTansferAssetDetail($storeId);
// 		$storeProductArr=array();
// 		if(count($stores)>0){
// 			foreach($stores as $val){
// 				$storeProductArr[$val['from_store_name']."_".$val['to_store_name']][]=$val;
// 			}
// 		}
// 		//echo"<pre>";print_r($storeProductArr);die;
// 		$this->data['store_wise_products']=$storeProductArr;
// 		$this->data['store_id']=$storeId;
// 		$this->render_template('products/assetTransferReport', $this->data); 
// 	}
	public function getActiveStore()
	{
		return $stores = $this->model_stores->getActiveStore();
	}
    public function scrap_report()
	{
		//print_r($this->input->post());die;
		$from_date=$this->input->post('from_date');
		$to_date=$this->input->post('to_date');
	    $listDetails = $this->model_products->GetScrapAllDetailsreport($from_date,$to_date);     
		$this->data['array']=$listDetails;
		$this->data['from_date'] = $from_date;   
		$this->data['to_date'] = $to_date; 
		$this->render_template('products/sale_scrap_report', $this->data);	
	}
	public function AcceptNotification(){
		//echo"<pre>";print_r($this->input->post());die;
		$sid=$this->input->post('sid');
		$stock_to_site_id=$this->input->post('stock_to_site_id');
		$fpid=$this->input->post('fpid');
		$status=$this->input->post('status');
		$accept_qty=$this->input->post('accept_qty');
		$update = $this->model_products->AcceptNotification($sid,$stock_to_site_id,$fpid,$status,$accept_qty);
		if($update == true) {
			// $mail_id = $this->model_products->getmailid($sid,$pid);
			// // print_r($mail_id['email']);die;
			// $this->load->library('email'); 

			// $this->email->from('shivanee00skb@gmail.com', 'Your Name'); 
			// $this->email->to($mail_id['email']);
			// $this->email->subject('Email Test'); 
			// $this->email->message('Testing the email class.'); 
	  
			// //Send mail 
			// if($this->email->send()) {
			// $this->session->set_flashdata("email_sent","Email sent successfully."); }
			// else {
			// $this->session->set_flashdata("email_sent","Error in sending Email."); 
			// redirect('Controller_Products/AcceptAsset', 'refresh'); 
			//   } 

			$this->session->set_flashdata('success', 'Successfully created.');
			redirect('Controller_Products/AcceptAsset', 'refresh');
				
		}
		else {
			$response['success'] = false;
			$response['messages'] = 'Error in the database while creating the information';		
			$this->render_template('products/productRequest', $response);	
			
		}
	}
	public function SiteStockReport()
	{
	    //echo"<pre>";print_r($this->input->post());die;
	    $siteId=$this->input->post('searchSite');
	    $from_date=$this->input->post('from_date');
	    $to_date=$this->input->post('to_date');
	    $data= $this->model_stores->getStoresDataListing();
	    $data1=array();
	    if($siteId)
	        $data1= $this->model_stores->getSiteStockReport($siteId,$from_date,$to_date);
   
	    $this->data['site_details']= $data;
	    $this->data['product_details']= $data1;
	    $this->data['store_id']=$siteId;
	    $this->data['from_date']=$from_date;
	    $this->data['to_date']=$to_date;
		$this->render_template('products/SiteStockReport', $this->data);	
	}
	
}

