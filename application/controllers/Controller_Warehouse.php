<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_Warehouse extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();
		$this->load->library('encryption');
		$this->data['page_title'] = 'Warehouse';
		$this->load->model('Model_user_module');
		$this->load->model('model_stores');
		$this->load->model('Model_category');
		$this->load->model('Model_project');
	}

	/* 
    * It only redirects to the manage stores page
    */
	public function index()
	{
	    $data= $this->model_stores->getStoresData();
	    $this->data['store_details']= $data;
		$this->render_template('warehouse/index', $this->data);	
	}	
	public function indexSiteChainage()
	{
	    $data= $this->model_stores->getStoresListing();
	    $data1= $this->model_stores->getStoreChainData('');
		//echo"<pre>";print_r($data1);die;
	    $this->data['store_details']= $data;
	    $this->data['chain_details']= $data1;
		$this->render_template('warehouse/indexSiteChainage', $this->data);	
	}	
	public function indexFactory()
	{
	    $data= $this->model_stores->getFactoryData('');
	    $this->data['product_details']= $data;
		$this->render_template('warehouse/indexFactoryStock', $this->data);	
	}
	public function indexSite()
	{
		$data=array();
		if($this->input->post('store')!="")
	    	$data= $this->model_stores->getSiteDataData('');
			
	    $this->data['product_details']= $data;
	    $this->data['site_id']= $this->input->post('store');
		$this->render_template('warehouse/indexSiteStock', $this->data);	
	}
	public function getSiteDetails()
	{
		return $data = $this->Model_project->getSiteDetails();
	}
	public function transferStock()
	{
	    $data= $this->model_stores->getStoresDataListing();
	    $data1= $this->model_stores->getFactoryData('');
	    $this->data['site_details']= $data;
	    $this->data['product_details']= $data1;
		$this->render_template('warehouse/transferStock', $this->data);	
	}
	public function transferStockReport()
	{
	    //echo"<pre>";print_r($this->input->post());die;
	    $siteId=$this->input->post('searchSite');
	    $data= $this->model_stores->getStoresDataListing();
	    $data1=array();
	    if($siteId)
	        $data1= $this->model_stores->getFactoryTransferData($siteId);
	        
	    $this->data['site_details']= $data;
	    $this->data['product_details']= $data1;
	    $this->data['store_id']=$siteId;
		$this->render_template('warehouse/transferStockReport', $this->data);	
	}

	/*
	* It retrieve the specific store information via a store id
	* and returns the data in json format.
	*/
	public function fetchStoresDataById($id) 
	{
		if($id) {
			$data = $this->model_stores->getStoresData($id);
			echo json_encode($data);
		}
	}
	
	public function fetchStoresChainDataById($id) 
	{
		if($id) {
			$data = $this->model_stores->getStoreChainData($id);
			echo json_encode($data);
		}
	}
	
	public function fetchStockDataById($id) 
	{
		if($id) {
			$data = $this->model_stores->getFactoryData($id);
			echo json_encode($data);
		}
	}
	public function fetchProductDetailByCatId($id) 
	{
		if($id) {
			$data = $this->model_stores->getProductDetailByCatId($id);
			//echo"<pre>";print_r($data);die;
			echo json_encode($data);
		}
	}
	public function fetchProductDetailByCatIdForSite($id,$siteId) 
	{
		if($id) {
			$data = $this->model_stores->fetchProductDetailByCatIdForSite($id,$siteId);
			//echo"<pre>";print_r($data);die;
			echo json_encode($data);
		}
	}
	public function password_hash($pass = '')
	{
		if($pass) {
			$password = password_hash($pass, PASSWORD_DEFAULT);
			return $password;
		}
	}

	public function create()
	{

		$response = array();
		$this->form_validation->set_rules('store_name', 'Site name', 'trim|required');
		$this->form_validation->set_rules('active', 'Active', 'trim|required');
		$this->form_validation->set_rules('site_manager_id', 'Site Manager', 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'name' => $this->input->post('store_name'),
        		'coordinator_id' => $this->input->post('coordinator_id'),
        		'site_manager_id' => $this->input->post('site_manager_id'),
        		'active' => $this->input->post('active'),	
        		'company_id' => $this->session->userdata['company_id'],	
        	);
           
			//echo"<pre>";print_r($data);die;
        	$create = $this->model_stores->create($data);
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully created.');
        		redirect('Controller_Warehouse');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the brand information';	
        		redirect('Controller_Warehouse/index', 'refresh');
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        			redirect('Controller_Warehouse/index', 'refresh');
        	}
        }
	}	
	public function createSiteChain()
	{
		$response = array();
		$this->form_validation->set_rules('chain_name', 'Chain name', 'trim|required');
		$this->form_validation->set_rules('active', 'Active', 'trim|required');
		$this->form_validation->set_rules('site_id', 'Site', 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'chainage_name' => $this->input->post('chain_name'),
        		'site_id' => $this->input->post('site_id'),
        		'active' => $this->input->post('active'),	
        		'entry_by' =>$this->session->userdata['id'],
        	);
           
			//echo"<pre>";print_r($data);die;
        	$create = $this->model_stores->createSiteChain($data);
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully created.');
        		redirect('Controller_Warehouse/indexSiteChainage');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the brand information';	
        		redirect('Controller_Warehouse/indexSiteChainage', 'refresh');
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        			redirect('Controller_Warehouse/index', 'refresh');
        	}
        }
	}	
	public function createFactoryStock()
	{

		$response = array();
        $this->form_validation->set_rules('stock_name', 'Stock Name', 'trim|required');
        $this->form_validation->set_rules('qty', 'Quantity', 'trim|required');
        $this->form_validation->set_rules('unit', 'Unit', 'trim|required');
        $this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
		//echo"<pre>";print_r($this->input->post());die;
        if ($this->form_validation->run() == TRUE) {
			$data= array();
			$dataUpdate= array();
			if($this->input->post('stock_name')=='New')
			{
				$data = array(
					'stock_name' => $this->input->post('stock'),
					'qty' => $this->input->post('qty'),
					'category_id' => $this->input->post('unit'),
					'entry_by' => $this->session->userdata['id'],	
				);
			}
			else
			{
				$dataUpdate= array(
					'stock_name' => $this->input->post('stock_name'),
					'qty' => $this->input->post('qty'),
					'category_id' => $this->input->post('unit'),
				);
			}
        	$create = $this->model_stores->createFactoryStock($data,$dataUpdate);
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully created.');
        		redirect('Controller_Warehouse/indexFactory');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the brand information';	
        		redirect('Controller_Warehouse/indexFactory', 'refresh');
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        			redirect('Controller_Warehouse/indexFactory', 'refresh');
        	}
        }
	}
	public function createSiteStock()
	{
		$response = array();
        $this->form_validation->set_rules('site', 'Site Name', 'trim|required');
        $this->form_validation->set_rules('stock_name', 'Stock Name', 'trim|required');
        $this->form_validation->set_rules('qty', 'Quantity', 'trim|required');
        $this->form_validation->set_rules('unit', 'Unit', 'trim|required');
        $this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
		//echo"<pre>";print_r($this->input->post());die;
        if ($this->form_validation->run() == TRUE) {
			$data= array();
			$dataUpdate= array();
			if($this->input->post('stock_name')=='New')
			{
				$data = array(
					'name' => $this->input->post('stock'),
					'store_id' => $this->input->post('site'),
					'qty' => $this->input->post('qty'),
					'total_quantity' => $this->input->post('qty'),
					'category_id' => $this->input->post('unit'),
					'entry_by' => $this->session->userdata['id'],	
				);
			}
			else
			{
				$dataUpdate= array(
					'stock_name' => $this->input->post('stock_name'),
					'store_id' => $this->input->post('site'),
					'qty' => $this->input->post('qty'),
					'category_id' => $this->input->post('unit'),
				);
			}
			//echo"<pre>";print_r($dataUpdate);die;
        	$create = $this->model_stores->createSiteStock($data,$dataUpdate);
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully created.');
        		redirect('Controller_Warehouse/indexSite');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the brand information';	
        		redirect('Controller_Warehouse/indexSite', 'refresh');
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        			redirect('Controller_Warehouse/indexSite', 'refresh');
        	}
        }
	}
	public function transferToWarehouse()
	{
       // echo"<pre>";print_r($this->input->post());die;
		$response = array();
        $this->form_validation->set_rules('transferTo', 'Site Name', 'trim|required');
        $this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
        //echo"<pre>";print_r($this->input->post());die;
        if ($this->form_validation->run() == TRUE) {
            $factoryIdsArr=$this->input->post('checkProductsArr');
			$updateProduct=array();
			$insertTransferHistory=array();
			foreach($factoryIdsArr as $pId){
				if($this->input->post('checkProduct'.$pId)=='on'){
					$updateProduct[$pId]=array("used_qty"=>$this->input->post('assign_qty'.$pId));
					$prodCatArr=explode("@_@",$this->input->post('ProductsName'.$pId));
					$insertTransferHistory[]=array("factory_product_id"=>$pId,"name"=>$prodCatArr[0],"category_id"=>$prodCatArr[1],"received_qty"=>$this->input->post('assign_qty'.$pId),"store_id"=>$this->input->post('transferTo'),"status"=>'Inprocess',"entry_by"=>$this->session->userdata['id']);
				}
			}
            //echo"<pre>";print_r($insertTransferHistory);die;
        	$create = $this->model_stores->InsertUpdateTransferDetails($insertTransferHistory,$updateProduct);
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully transfer.');
        		redirect('Controller_Warehouse/transferStock');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the brand information';	
        		redirect('Controller_Warehouse/transferStock', 'refresh');
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        			redirect('Controller_Warehouse/transferStock', 'refresh');
        	}
        }
	}	

	/*
    * If the validation is not valid, then it provides the validation error on the json format
    * If the validation for each input is valid then it updates the data into the database and 
    returns a n appropriate message in the json format.
    */

	public function update()
	{
		$response = array();
        $id=$this->input->post('site_id');
		if($id) {
			$this->form_validation->set_rules('edit_store_name', 'Warehouse name', 'trim|required');
			$this->form_validation->set_rules('edit_active', 'Active', 'trim|required');
			$this->form_validation->set_rules('edit_site_manager_id', 'Site Manager', 'trim|required');
			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

	        if ($this->form_validation->run() == TRUE) {
	        	$data = array(
	        		'name' => $this->input->post('edit_store_name'),
            		'coordinator_id' => $this->input->post('edit_coordinator_id'),
            		'site_manager_id' => $this->input->post('edit_site_manager_id'),
	        		'active' => $this->input->post('edit_active'),	
	        	);
                //echo"<pre>";print_r($data);die;
	        	$update = $this->model_stores->update($data, $id);
	        	if($update == true) {
	        		$this->session->set_flashdata('success', 'Successfully updated.');
        		    redirect('Controller_Warehouse');
	        	}
	        	else {
	        		$this->session->set_flashdata('success', 'Error in the database while updated the brand information.');
        		    redirect('Controller_Warehouse');
	        	}
	        }
	        else {
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        	redirect('Controller_Warehouse');
	        }
		}
		else {
			$response['success'] = false;
    		$response['messages'] = 'Error please refresh the page again!!';
		}

		echo json_encode($response);
	}
	public function updateSiteChain()
	{
        $site_chainage_id=$this->input->post('site_chainage_id');
		$response = array();
		$this->form_validation->set_rules('edit_chain_name', 'Chain name', 'trim|required');
		$this->form_validation->set_rules('edit_active', 'Active', 'trim|required');
		$this->form_validation->set_rules('edit_site_id', 'Site', 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
		if ($this->form_validation->run() == TRUE) {
			$data = array(
				'chainage_name' => $this->input->post('edit_chain_name'),
				'site_id' => $this->input->post('edit_site_id'),
				'active' => $this->input->post('edit_active'),	
			);
		  //echo"<pre>";print_r($site_chainage_id);die;
			$update = $this->model_stores->updateSiteChain($data, $site_chainage_id);
			if($update == true) {
				$this->session->set_flashdata('success', 'Successfully updated.');
				redirect('Controller_Warehouse/indexSiteChainage');
			}
			else {
				$this->session->set_flashdata('success', 'Error in the database while updated the brand information.');
				redirect('Controller_Warehouse/indexSiteChainage');
			}
		}
		else {
			$response['success'] = false;
			foreach ($_POST as $key => $value) {
				$response['messages'][$key] = form_error($key);
			}
			redirect('Controller_Warehouse');
		}
	}
	public function UpdateFactoryStock()
	{
		$response = array();
   		$factory_stock_id=$this->input->post('factory_stock_id');
		if($factory_stock_id) {
			$this->form_validation->set_rules('edit_stock', 'Warehouse name', 'trim|required');
			$this->form_validation->set_rules('edit_qty', 'Active', 'trim|required');
			$this->form_validation->set_rules('edit_unit', 'Site Manager', 'trim|required');
			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

            if ($this->form_validation->run() == TRUE) {
            	$data = array(
            		'stock_name' => $this->input->post('edit_stock'),
            		'qty' => $this->input->post('edit_qty'),
            		'unit' => $this->input->post('edit_unit'),
            	);

                //echo"<pre>";print_r($data);die;
            	$create = $this->model_stores->UpdateFactoryStock($data,$factory_stock_id);
            	if($create == true) {
            		$this->session->set_flashdata('success', 'Successfully updated.');
            		redirect('Controller_Warehouse/indexFactory');
            	}
            	else {
            		$this->session->set_flashdata('error', 'Error please refresh the page again!!.');
            		redirect('Controller_Warehouse/indexFactory');
            	}
            }
            else {  
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        	redirect('Controller_Warehouse');
	        }
		}
		else {
    		$this->session->set_flashdata('error', 'Error please refresh the page again!!.');
    		redirect('Controller_Warehouse/indexFactory');
		}

	}

	/*
	* If checks if the store id is provided on the function, if not then an appropriate message 
	is return on the json format
    * If the validation is valid then it removes the data into the database and returns an appropriate 
    message in the json format.
    */  
	public function remove()
	{
		$store_id = $this->input->post('store_id');
		$response = array();
		if($store_id) {
			$delete = $this->model_stores->remove($store_id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error in the database while removing the brand information";
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
		}

		echo json_encode($response);
	}
	public function removeChain()
	{
		$site_chainage_id = $this->input->post('site_chainage_id');
		//echo"<pre>";print_r($site_chainage_id);die;
		$response = array();
		if($site_chainage_id) {
			$delete = $this->model_stores->removeChain($site_chainage_id);
			if($delete) {
				$this->session->set_flashdata('success', 'Successfully deleted.');
				redirect('Controller_Warehouse/indexSiteChainage');
			}
			else {
				$this->session->set_flashdata('success', 'Error in the database while deleted the information.');
				redirect('Controller_Warehouse/indexSiteChainage');
			}
		}
	}
	public function removeStock()
	{
		$id = $this->input->post('factory_stock_id');
		$response = array();
		if($id) {
			$delete = $this->model_stores->removeStock($id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error in the database while removing the brand information";
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
		}

		echo json_encode($response);
	}
	public function removeSiteStock()
	{
		$id = $this->input->post('factory_stock_id');
		$response = array();
		if($id) {
			$delete = $this->model_stores->removeSiteStock($id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error in the database while removing the brand information";
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
		}

		echo json_encode($response);
	}
	public function getUserDetails($userTypeId)
	{
		return $data = $this->model_stores->getUserDetails($userTypeId);
	}	
	public function getActiveCategroy()
	{
		return $data = $this->Model_category->getActiveCategroy();
	}
	public function fetchsiteStockDataById($id) 
	{
		if($id) {
			$data = $this->model_stores->fetchsiteStockDataById($id);
			echo json_encode($data);
		}
	}
		public function UpdateSiteStock()
	{
	//	echo"<pre>";print_r($this->input->post());die;
		$response = array();
        $this->form_validation->set_rules('editsite', 'Site Name', 'trim|required');
        $this->form_validation->set_rules('editstock_name', 'Stock Name', 'trim|required');
        $this->form_validation->set_rules('editqty', 'Quantity', 'trim|required');
        $this->form_validation->set_rules('editunit', 'Unit', 'trim|required');
        $this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
		//echo"<pre>";print_r($this->input->post());die;
        if ($this->form_validation->run() == TRUE) {
			$data= array();
			$id=$this->input->post('edit_id');
				$data= array(
					'name' => $this->input->post('editstock_name'),
					'store_id' => $this->input->post('editsite'),
					'qty' => $this->input->post('editqty'),
					'total_quantity' => $this->input->post('editqty'),
					'category_id' => $this->input->post('editunit'),
				);
		
		//	echo"<pre>";print_r($data);die;
        	$create = $this->model_stores->UpdateSiteStock($data,$id);
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully created.');
        		redirect('Controller_Warehouse/indexSite');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the brand information';	
        		redirect('Controller_Warehouse/indexSite', 'refresh');
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        			redirect('Controller_Warehouse/indexSite', 'refresh');
        	}
        }
	}
		public function transfersiteStock()
	{
	    $data= $this->model_stores->getStoresDataListing1();
	   // $data1= $this->model_stores->getFactoryData1('');
	    $this->data['site_details']= $data;
	   // $this->data['product_details']= $data1;
		$this->render_template('warehouse/transferSiteStock', $this->data);	
	}
	public function gettransfersiteStockData($id)
	{   
		//echo"<pre>";print_r($id);die;
		$data= $this->model_stores->gettransfersiteStockData($id);
	//	echo"<pre>";print_r($data);die;
		$html='';
		$used_qty=0;
		if(count($data)>0){
			foreach($data as $detail)
			{
				$actQty=round(($detail['qty']),2);
				//$actQty=round(($detail['total_quantity']-$detail['qty']),2);
			   //echo'<pre>';print_r($detail);
			   $html.='<tr>
						<td><input type="checkbox" onclick="enableDisabledRemark('.$detail['id'].')" name="checkProduct'.$detail['id'].'" id="checkProduct'.$detail['id'].'" style="width:20px"/>
						<input type="hidden" name="checkProductsArr[]" value="'.$detail['id'].'" /></td>
						<input type="hidden" name="ProductsName'.$detail['id'].'" value="'.$detail['name']."@_@".$detail['cat_id'].'" /></td>
						<td>'.$detail['name'].'</td> 
						<td>'.$detail['total_quantity'].'</td>'; 
						// if($detail['used_qty']!=0) {$used_qty=$detail['used_qty'];} else {$used_qty= "0";}
						//$html.='<td>'.$used_qty.'</td>';
						// if($detail['qty']!=0){$round= round(($detail['total_quantity']-$detail['qty']),2); }else{ $round= $detail['qty'];}
						// $html.='<td>'.$round.'</td> 
						$html.='<td>'.$detail['qty'].'</td>
						<td>'.$detail['cat_name'].'</td> 
						<td><input type="number" step="0.00" disabled onChange="return checkQtyExist(this.value,'.$actQty.','.$detail['id'].')" name="assign_qty'.$detail['id'].'" class="form-control" id="assign_qty'.$detail['id'].'" min="1" required /></td> 
					</tr>';
				}
				$html.='<tr>
					<td style="text-align:right" colspan="7">
						<button type="submit" class="btn btn-primary" onClick="return CheckAllProduct(1);">Transfer</button>
					</td>
				</tr>';
			}
			else{
				$html.='
				<tr>
				<td colspan="7" style="text-align:left"><font color="#FF0000"><strong>No data found.</strong></font></td>
				</tr>
				';
			}
		echo json_encode($html);die;
	}
	public function getStoresSiteData()
	{
		$id=$this->input->post('id');
	    $data= $this->model_stores->getStoresSiteData($id);
		$dropDown='<select class="form-control" id="transferTo" name="transferTo"    required>
		<option value="">Select</option>';
			
		if(count($data)>0){
			foreach($data as $value){
			   $dropDown.= '<option value="'.$value['id'].'" >'.$value['name'].'</option>';
			}
		}
		echo"<pre>";print_r($dropDown);die;
	//	echo json_encode($dropDown);die;
		 
	}
public function transfersiteToWarehouse()
	{
		 //echo"<pre>";print_r($this->input->post());die;

		$response = array();
		$this->form_validation->set_rules('transferfrom', 'Site Name', 'trim|required');
        $this->form_validation->set_rules('transferTo', 'Site Name', 'trim|required');
        $this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
        //echo"<pre>";print_r($this->input->post());die;
        if ($this->form_validation->run() == TRUE) {
            $factoryIdsArr=$this->input->post('checkProductsArr');
			$updateProduct=array();
			$insertTransferHistory=array();
			foreach($factoryIdsArr as $pId){
				if($this->input->post('checkProduct'.$pId)=='on'){
					$updateProduct[$pId]=array("used_qty"=>$this->input->post('assign_qty'.$pId));
					$prodCatArr=explode("@_@",$this->input->post('ProductsName'.$pId));
					// echo"<pre>";print_r($prodCatArr);die;
					$insertTransferHistory[]=array("id"=>$pId,"name"=>$prodCatArr[0],"category_id"=>$prodCatArr[1],"received_qty"=>$this->input->post('assign_qty'.$pId),"store_id"=>$this->input->post('transferTo'),"from_store_id"=>$this->input->post('transferfrom'),"status"=>'Inprocess',"entry_by"=>$this->session->userdata['id']);
				}
			}
			// echo"<pre>";print_r($updateProduct);
			 //echo"<pre>";print_r($insertTransferHistory);die;
        	$create = $this->model_stores->InsertUpdateTransfersiteDetails($insertTransferHistory,$updateProduct);
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully transfer.');
        		redirect('Controller_Warehouse/transferSiteStock');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the brand information';	
        		redirect('Controller_Warehouse/transferSiteStock', 'refresh');
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        			redirect('Controller_Warehouse/transferStock', 'refresh');
        	}
        }
	}	


}