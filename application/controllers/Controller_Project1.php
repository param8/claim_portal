<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_Project extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Project';
		$this->load->library('encryption');
		$this->load->model('Model_project');
		$this->load->model('Model_user_module');
		$this->load->model('Model_stores');
	  }

	/* 
	* It only redirects to the manage project page
	*/
	public function index()
	{
		$data = $this->Model_project->getProjectData();
		$this->data['array'] = $data;   
		$this->render_template('project/index', $this->data);	
	}	
	public function assignTo()
	{
		$data = $this->Model_project->getProjectData();
		$data1=array();
		$data2=array();
		$rejectDpr=array();
        //echo"<pre>";print_r($this->input->post('site_stock')); die;
        $id=$this->input->post('site_stock');
		$data1 = $this->Model_project->getprojectAssignDetails($id);
		if($this->input->post('site_stock')) {
			$id =$this->input->post('site_stock');
			$data1 = $this->Model_project->getprojectAssignDetails($id);
			if(count($data1)>0)
				$data2 = $this->Model_project->getDPRRejectData($data1[0]['id']);
		}
		//echo"<pre>";print_r($data1);
		if(count($data1)>0)
    		$rejectDpr = $this->Model_project->getDPRRejectData($data1[0]['id']);
    		
		$this->data['array'] = $data;  
		$this->data['projectStock'] = $data1;
		$this->data['rejectDpr'] = $rejectDpr;
		$this->data['site_stock'] =$this->input->post('site_stock');
        //echo"<pre>";print_r($this->data); die;
		$this->render_template('project/assignToProjectWise', $this->data);	
	}	
	
	public function dprReport()
	{
		$data = $this->Model_project->getProjectData();
		$data1 = $this->Model_project->getprojectAssignDetails($id = "");
		
		$this->data['array'] = $data;   
		$this->data['projectStock'] = $data1;
		$this->data['site'] = $this->input->post('site');
		$this->render_template('project/dprReport', $this->data);	
	}	
	
	public function SearchDPR()
	{
		$data = $this->Model_project->getProjectData();
		$data1 = $this->Model_project->getprojectAssignDetails($id = "");
		$this->data['array'] = $data;   
		$this->data['projectStock'] = $data1;
		$this->data['date'] = $this->input->post('from_date');
		$this->data['type'] = $this->input->post('type');
		$this->data['site'] = $this->input->post('site');
		//echo"<pre>";print_r($this->data);die;
		$this->render_template('project/dprReport', $this->data);	
	}	
	public function SearchDPRDateRange()
	{
		$data = $this->Model_project->getProjectData();
		$data1 = $this->Model_project->getprojectAssignDetails($id = "");
		$this->data['array'] = $data;   
		$this->data['projectStock'] = $data1;
		$this->data['from_date'] = $this->input->post('from_date');
		$this->data['to_date'] = $this->input->post('to_date');
		$this->data['type'] = $this->input->post('type');
		$this->data['site'] = $this->input->post('site');
		//echo"<pre>";print_r($this->data);die;
		$this->render_template('project/dprDateRangeReport', $this->data);	
	}	
	public function UpdateDprStatus()
	{
		$status=$this->input->post('status');
		$reason=$this->input->post('reason');
		$site_id=$this->input->post('site_id');
		$dpr_type=$this->input->post('dpr_type');
		$dpr_date=$this->input->post('dpr_date');
		$data=array("status_reason"=>$reason,"status"=>$status);
		if($site_id!="" && $dpr_type!="" && $dpr_date!="") 
		{
			$data = $this->Model_project->UpdateDprStatus($data,$site_id,$dpr_type,$dpr_date);
			$this->session->set_flashdata('success', 'Successfully Inserted.');
			redirect('Controller_Project/dprReport', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('danger', 'Error in database.');
			redirect('Controller_Project/dprReport', 'refresh');
		}
	}	
	
	public function insertUpdate()
	{
		$hiddenValuesArr=$this->input->post('hiddenPVal');
		$data=array();
		foreach($hiddenValuesArr as $value)
		{
			if($this->input->post('totalVal_'.$value)!="")
			{
				list($projectId,$productId)=explode("_",$value);
				$data[]= array(
					'project_id' => $projectId,
					'product_id' => $productId,	
					'project_qty' => $this->input->post('totalVal_'.$value),	
				);
			}
		}
		//echo"<pre>";print_r($data);die;
		if(count($data)>0)
		{
			$data = $this->Model_project->InsertUpdateData($data);
			$this->session->set_flashdata('success', 'Successfully assigned.');
			redirect('Controller_Project/assignStock', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('danger', 'Error in database.');
			redirect('Controller_Project/assignStock', 'refresh');
		}
	}	
	
	public function assignStock()
	{
		$data = $this->Model_project->getProjectData();
		$stockData = $this->Model_stores->getActiveStore();
		$this->data['array'] = $data;   
		$this->data['stockData'] = $stockData;   
		$this->render_template('project/assignStockProject', $this->data);	
	}	
	public function getActiveStore()
	{
		return $stockData = $this->Model_stores->getActiveStore();
	}	
	
	public function GetUsedPayment($prjId)
	{
		return $stockData = $this->Model_project->GetUsedPayment($prjId);
	}	
	

	/*
	* It checks if it gets the category id and retreives
	* the category information from the category model and 
	* returns the data into json format. 
	* This function is invoked from the view page.
	*/
	public function fetchprojectDataById($id) 
	{
		// alert("hello");
		if($id) {
			$data = $this->Model_project->getprojectDataById($id);
			$decriptPass=$this->encryption->decrypt($data['password']); 
			unset($data['password']);
			if($decriptPass)
				$data['password']=$decriptPass;
			else
				$data['password']="";
				
			echo json_encode($data);
		}

		return false;
	}
	
	public function assignStockReport() 
	{
		$warehouse=$this->input->post('warehouse');
		$data = $this->Model_project->getprojectAssignReportDetails($warehouse);
		//echo"<pre>";print_r($data);die;
		if(count($data)>0){
			$warehouseWiseDetailArr=array();
			foreach($data as $val){
				$warehouseWiseDetailArr[$val['name']][$val['project_name']."@_@".$val['project_coordinator']."@_@".$val['mobile_no']."@_@".$val['email_id']."@_@".$val['address']][]=array($val['item'],$val['serial_no'],$val['project_qty'],$val['cat']." (".$val['unit'].")",$val['product_project_id'],$val['status'],$val['product_id']);
			}
			$this->data['array'] = $warehouseWiseDetailArr;  
			$this->render_template('project/assignStockProjectReport',$this->data);	
			return false;
		}
		else
		{
			$this->data['array'] = array();  
			$this->render_template('project/assignStockProjectReport',$this->data);	
			return false;
		}
	}


	/*
	* Fetches the category value from the category table 
	* this function is called from the datatable ajax function
	*/
	public function fetchProductDataByWhouseId($id,$projectId)
	{
		$data = $this->Model_project->getProductDetailsByWId($id);
		//echo"<pre>";print_r($data);die;
		if(count($data)>0)
		{
			$innerData='<form action="'.base_url('Controller_Project/insertUpdate').'" method="post">
					<table id="manageTable" class="table table-bordered">
					  <tr style="background-color:#E3E3E3; color:#000000">
						<td><strong>Item</strong></td>
						<td><strong>Unit</strong></td>
						<td><strong>Qty</strong></td>
						<td><strong>Assign Unit</strong></td>
					  </tr>';
					  foreach($data as $val)
					  {
						 if($val['qty']==0)
						 	continue;
						 
						 $innerData.='<tr>
							<td>'.$val['name'].'</td>
							<td>('.$val['unit'].')</td>
							<td><input type="text" style="border:0px; width:20px;" readonly="true" name="qunty_'.$projectId."_".$val['id'].'" id="qunty_'.$projectId."_".$val['id'].'" value="'.$val['qty'].'" /><input type="hidden" style="border:0px; width:20px;" readonly="true" name="hiddenQunty_'.$projectId."_".$val['id'].'" id="hiddenQunty_'.$projectId."_".$val['id'].'" value="'.$val['qty'].'" /></td>
							<td><input type="text" name="totalVal_'.$projectId."_".$val['id'].'" id="totalVal_'.$projectId."_".$val['id'].'" class="form-control" onkeypress="return isNumberKey(event);"  onKeyUp="return checkQuantity(this.value,'.$projectId.','.$val['id'].','.$val['qty'].');" /><input type="hidden" name="hiddenPVal[]"  class="form-control" value="'.$projectId."_".$val['id'].'" /></td>
						  </tr>';
					  }
					 $innerData.='
					  <tr>
						<td colspan="4" style="text-align:right"><button class="btn btn-primary" onClick="SubmitFormValue()">Save</button</td>
					  </tr>
					 </table></form>';
		}
		else
		{
			$innerData='<table id="manageTable" class="table table-bordered">
						  <tr style="background-color:red; color:#FFFFFF">
							<td><strong>No data found.</strong></td>
						</table>';
		}
		echo json_encode($innerData);
	}
	
	public function GetAssignPaymentDetails()
	{
		$prId=$this->input->post('prId');
		$data = $this->Model_project->getPaymentDetailsByProjectId($prId);
		///echo"<pre>";print_r($data);die;
		
		if(count($data)>0)
		{
			$innerData='<table id="manageTable" class="table table-bordered">
					  <tr style="background-color:#E3E3E3; color:#000000">
						<td><strong>Assign To</strong></td>
						<td><strong>Remark</strong></td>
						<td><strong>Payment</strong></td>
					  </tr>';
					  $totalAmt=0;
					  foreach($data as $val)
					  {
						 $innerData.='<tr>
							<td>'.$val['assign_to'].'</td>
							<td>'.$val['remark'].'</td>
							<td>'.number_format($val['payment'],2).'</td>
						  </tr>';
						  $totalAmt+=$val['payment'];
					  }
					 $innerData.='<tr><td colspan="2" style="text-align:right"><strong>Total Payment</strong></td><td>'.number_format($totalAmt,2).'</td></tr></table>';
		}
		else
		{
			$innerData='<table id="manageTable" class="table table-bordered">
						  <tr style="background-color:red; color:#FFFFFF">
							<td><strong>No data found.</strong></td>
						</table>';
		}
		echo json_encode($innerData);
	}
	
	/*
	* Its checks the category form validation 
	* and if the validation is successfully then it inserts the data into the database 
	* and returns the json format operation messages
	*/
	public function create()
	{
		$response = array();
		$this->form_validation->set_rules('Project_name', 'Project Name', 'trim|required');
		$this->form_validation->set_rules('project_coordinator', 'Project Coordinator', 'trim|required');
		$this->form_validation->set_rules('mobile_no', 'Mobile no', 'trim|required');
		$this->form_validation->set_rules('user_type_id', 'User Typr', 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'project_name' => $this->input->post('Project_name'),
        		'project_coordinator' => $this->input->post('project_coordinator'),	
        		'email_id' => $this->input->post('email'),	
        		'mobile_no' => $this->input->post('mobile_no'),	
				'project_cost' => $this->input->post('project_cost'),	
				'address' => $this->input->post('address'),	
				'user_type_id' => $this->input->post('user_type_id'),	
        	);
            $password = $this->encryption->encrypt($this->input->post('password'));
        	$dataUserPass = array(
        		'mobile_no' => $this->input->post('mobile_no'),
				'password' => $password,	
				'user_type_id' => $this->input->post('user_type_id'),	
        	);
			// echo"<pre>";print_r($dataUserPass);die;
        	$create = $this->Model_project->create($data,$dataUserPass);
        	if($create == true) {
				$this->session->set_flashdata('success', 'Successfully created');
				redirect('Controller_Project/index', 'refresh');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the project information';	
				$this->render_template('project/index', $response);	
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }
	}
	public function Update()
	{
		$response = array();
		if($this->input->post()) {
			$this->form_validation->set_rules('edit_project_name', 'Project Name', 'trim|required');
			$this->form_validation->set_rules('edit_project_coordinator', 'Project Coordinator', 'trim|required');
			$this->form_validation->set_rules('edit_mobile_no', 'Mobile no', 'trim|required');
			$this->form_validation->set_rules('edit_user_type_id', 'User Type', 'trim|required');
			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
			$id=$this->input->post('edit_project_id');
	        if ($this->form_validation->run() == TRUE) {
				$data = array(
					'project_name' => $this->input->post('edit_project_name'),
					'project_coordinator' => $this->input->post('edit_project_coordinator'),	
					'email_id' => $this->input->post('edit_email'),	
					'mobile_no' => $this->input->post('edit_mobile_no'),	
					'project_cost' => $this->input->post('edit_project_cost'),	
					'address' => $this->input->post('edit_address'),	
					'user_type_id' => $this->input->post('edit_user_type_id'),	
				);
				$password = $this->encryption->encrypt($this->input->post('edit_password'));
				$dataUserPass = array(
					'mobile_no' => $this->input->post('edit_mobile_no'),
					'password' => $password,	
					'user_type_id' => $this->input->post('edit_user_type_id'),
				);
				// echo"<pre>";print_r($data);die;
	        	$update = $this->Model_project->update($data,$dataUserPass, $id);
	        	if($update == true) {
					$response['success'] = true;
					$this->session->set_flashdata('success', 'Successfully updated.');
					redirect('Controller_Project/index', 'refresh');
	        	}
	        	else {
	        		$response['success'] = false;
					$this->session->set_flashdata('success', 'Error in the database while updated the brand information.');
					redirect('Controller_Project/index', 'refresh');
	        	}
	        }
	        else {
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        }
		}
		else {
			$response['success'] = false;
			$this->session->set_flashdata('success', 'Error please refresh the page again !!.');
			redirect('Controller_Project/index', 'refresh');
		}
	}
	public function remove()
	{
		$project_id = $this->input->post('delete_project_id');
		$response = array();
		if($project_id) {
			$delete = $this->Model_project->remove($project_id);
			if($delete == true) {
				$response['success'] = true;
				$this->session->set_flashdata('success', 'Successfully removed.');
				redirect('Controller_Project/index', 'refresh');
			}
			else {
				$this->session->set_flashdata('success', 'Error in the database while removing the project information.');
				redirect('Controller_Project/index', 'refresh');
			}
		}
	}
	
	public function AssignToUser()
	{
		$userAssign=$this->input->post('assignForVl');
		$productIdsArray=$this->input->post('productIds');
		if(count($productIdsArray)>0)
		{
			if($userAssign=='user')
			{
				foreach($productIdsArray as $proId)
				{
					if($this->input->post('pQty_'.$proId)!="" && $this->input->post('assignTo_'.$proId)!="")
					{
						$data[]= array(
						'project_id' => $this->input->post('projectId'),
						'product_id' => $proId,
						'assign_to' => $this->input->post('assignTo_'.$proId),
						'department' =>"",
						'email' => "",
						'phone' =>"",
						'gender' =>"",
						'remark' =>"",
						'assign_for' => $this->input->post('assignForVl'),
						'location' =>"",
						'quantity' => $this->input->post('plQty_'.$proId),
						'entry_type' => "Instock",
						'entry_by' =>$this->session->userdata['id'],
						);
					}
				}
				$create = $this->Model_project->AssignToUser($data);
				if($create == true) {
				$this->session->set_flashdata('success', 'Successfully assigned.');
				redirect('Controller_Project/assignTo', 'refresh');
				}
			}
		}
		else
		{
			$this->session->set_flashdata('danger', 'Error in insertion.');
			redirect('Controller_Project/assignTo', 'refresh');
		}
	}
	
	public function PaymentToUser()
	{
		$userAssign=$this->input->post('assignForVl');
		//echo"<pre>";print_r($this->input->post());die;
		if($userAssign=='user')
		{
			$data[]= array(
			'project_id' => $this->input->post('projectId'),
			'assign_to' => $this->input->post('assignTo'),
			'remark' => $this->input->post('remark'),
			'assign_for' => $this->input->post('assignForVl'),
			'payment' => $this->input->post('paym'),
			'entry_type' =>'Payment',
			);
		}
		//echo"<pre>";print_r($data);die;
		$create = $this->Model_project->AssignToUser($data);
		//echo"<pre>";print_r($create);die;
		if($create == true) {
		$this->session->flashdata('type',NULL);
		$this->session->set_flashdata('success', 'Successfully assigned.');
		redirect('Controller_Project/assignTo', 'refresh');
        }
	}
	public function GetAssignProducts()
	{
        $prjId = $this->input->post('prId');
        $product_id = $this->input->post('pId');
        $response = array();
        $combine = array();
        if($product_id) {
            $listDetails = $this->Model_project->GetAssignProductDetails($prjId,$product_id,'Issue');
			$listingVal=array();
			foreach($listDetails as $list)
			{
				$listingVal[]=array("proProjId"=>$list['assign_product_project_id'],"aTo"=>$list['assign_to']." ( ".$list['department']." )");
			}
			//echo"<pre>";print_r($listingVal);die;
			//$response[]=$listingVal;
			echo json_encode($listingVal); 
        }
	}
	public function ReturnProduct()
	{
		$returnProjId = $this->input->post('asProjId');
		$returnProdId = $this->input->post('aspId');
		$wareHouseVal= $this->input->post('to_warehouse');
		list($whId,$qty)=explode("_",$wareHouseVal);
		$response = array();
		if($returnProjId) {
			$update = $this->Model_project->ReturnProduct($returnProjId,$returnProdId,$whId);
			if($update == true) {
				$this->session->set_flashdata('success', 'Successfully Return.');
				redirect('Controller_Project/assignStockReport', 'refresh');
			}
			else {
				$this->session->set_flashdata('danger', 'Error in database.');
				redirect('Controller_Project/assignStockReport', 'refresh');
			}
		}
	}

	public function ReturnProductByUser()
	{
		//echo"<pre>";print_r($this->input->post());die;
		$ProId= $this->input->post('ProId');
		$ProjId= $this->input->post('ProjId');
		$proProjId= $this->input->post('return_by');
		$remark= $this->input->post('remark');
		$response = array();
		if($proProjId) {
			$update = $this->Model_project->ReturnProductByUser($ProId,$ProjId,$proProjId,$remark);
			if($update == true) {
				$this->session->set_flashdata('success', 'Successfully Returned.');
				redirect('Controller_Project/assignTo', 'refresh');
			}
			else {
				$this->session->set_flashdata('danger', 'Error in database.');
				redirect('Controller_Project/assignTo', 'refresh');
			}
		}
	}
	//******************************PANEL CODE START HERE BY DEEP RANA************************
	public function Panel()
	{
		$data = $this->Model_project->getPanelData();
		$this->data['array'] = $data;   
		$this->render_template('project/panelIndex', $this->data);	
	}
	public function getPanelData()
	{
		$data = $this->Model_project->getPanelData();
		return $data;
	}
	
	public function createPanel()
	{
		$response = array();
		$this->form_validation->set_rules('Panel_name', 'Panel Name', 'trim|required');
		$this->form_validation->set_rules('Priority', 'Priority', 'trim|required');

        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'panel_name' => $this->input->post('Panel_name'),
        		'panel_priority' => $this->input->post('Priority'),	
        		'unit_area' => $this->input->post('unit_area'),	
        		'b_l' => $this->input->post('b_l'),	
        	);
			//echo"<pre>";print_r($data);die;
        	$create = $this->Model_project->createPanel($data);
        	if($create == true) {
				$this->session->set_flashdata('success', 'Successfully created');
				redirect('Controller_Project/Panel', 'refresh');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the project information';	
				$this->render_template('project/Panel', $response);	
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }
	}
	
	public function fetchPanelDataById($id) 
	{
		if($id) {
			$data = $this->Model_project->getpanelDataById($id);
			echo json_encode($data);
		}
	}
	
	public function UpdatePanel()
	{
		//echo"<pre>";print_r($this->input->post());die;
		$id=$this->input->post('edit_Panel_id');
		$this->form_validation->set_rules('edit_Panel_name', 'Panel Name', 'trim|required');
		$this->form_validation->set_rules('edit_Priority', 'Priority', 'trim|required');
        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'panel_name' => $this->input->post('edit_Panel_name'),
        		'panel_priority' => $this->input->post('edit_Priority'),
        		'unit_area' => $this->input->post('edit_unit_area'),
        		'b_l' => $this->input->post('edit_b_l'),	
        	);
			//echo"<pre>";print_r($data);die;
        	$create = $this->Model_project->UpdatePanel($data,$id);
        	if($create == true) {
				$this->session->set_flashdata('success', 'Successfully updated.');
				redirect('Controller_Project/Panel', 'refresh');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the project information';	
				$this->render_template('project/Panel', $response);	
        	}
        }
	}	

	public function removePanel()
	{
		$panel_id = $this->input->post('delete_Panel_id');
		$response = array();
		if($panel_id) {
			$delete = $this->Model_project->removePanel($panel_id);
			if($delete == true) {
				$response['success'] = true;
				$this->session->set_flashdata('success', 'Successfully removed.');
				redirect('Controller_Project/Panel', 'refresh');
			}

			else {
				$this->session->set_flashdata('success', 'Error in the database while removing the project information.');
				redirect('Controller_Project/Panel', 'refresh');
			}
		}
	}
	
	public function getDailyProgressDtails($type,$panel_id,$flag,$date,$site)
	{
		return $returnData = $this->Model_project->getDailyProgressDtails($type,$panel_id,$flag,$date,$site);
	}
	
	public function getDailyProgressDtailsByDateRange($type,$panel_id,$flag,$fromDate,$toDate,$site)
	{
		return $returnData = $this->Model_project->getDailyProgressDtailsByDateRange($type,$panel_id,$flag,$fromDate,$toDate,$site);
	}

	public function insertDPR()
	{
		$response = array();
		//echo"<pre>";print_r($this->input->post());die;
		if($this->input->post('changeSubmit')==1)
		{
			$this->session->set_flashdata('type', $this->input->post('type'));
			$this->session->set_flashdata('dpr_date', $this->input->post('dpr_date'));
			redirect('Controller_Project/assignTo', 'refresh');
			die;
		}
		if($this->input->post()) {
				$panelIds=$this->input->post('panelIds');
				if(count($panelIds)>0)
				{
					foreach($panelIds as $val)
					{
						if($this->input->post('cummulative_area_'.$val)!="" || $this->input->post('cummulative_nos_'.$val))
						$data[]= array(
							'panel_id' => $val,
							'dpr_type' => $this->input->post('type'),
							'dpr_date' => date('Y-m-d',strtotime($this->input->post('from_date'))),	
							'unit_area_sqm' => $this->input->post('ua_sqm_'.$val),	
							'last_month_no' => round($this->input->post('last_month_nos_'.$val),3),	
							'last_month_area' => round($this->input->post('last_month_area_'.$val),3),	
							'month_no' => round($this->input->post('current_month_nos_'.$val),3),	
							'month_area' => round($this->input->post('current_month_area_'.$val),3),	
							'cummulative_no' => round($this->input->post('cummulative_nos_'.$val),3),	
							'cummulative_area' => round($this->input->post('cummulative_area_'.$val),3),	
							'bl' => $this->input->post('bl_'.$val),	
							'remark' => $this->input->post('remark_'.$val),	
							'project_id' => $this->input->post('site'),	
    						'entry_by' =>$this->session->userdata['id'],
						);
					}
				}
				//echo"<pre>";print_r($data);die;
	        	$update = $this->Model_project->insertDPR($data);
	        	if($update == true) {
					$response['success'] = true;
					$this->session->set_flashdata('success', 'Successfully Inserted.');
					$this->session->set_flashdata('type', $this->input->post('type'));
					$this->session->set_flashdata('site', $this->input->post('site'));
					redirect('Controller_Project/assignTo', 'refresh');
	        }
	        else {
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        }
		}
		else {
			$response['success'] = false;
			$this->session->set_flashdata('success', 'Error please refresh the page again !!.');
			redirect('Controller_Project/assignTo', 'refresh');
		}
	}
	// #####################
	public function getUserTypeDetails()
	{
		return $data = $this->Model_user_module->getUserTypeDetails("");
	}	
	public function getSiteDetails()
	{
		return $data = $this->Model_project->getSiteDetails();
	}	
}