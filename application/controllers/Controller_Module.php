<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_Module extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'User-type wise module';

		$this->load->model('Model_modules');
	//	$this->load->model('Model_users');
        $this->load->library('encryption');		

	  }

	/* 
	* It only redirects to the manage category page
	*/
	public function index()
	{
		$data = $this->Model_modules->getUserTypeDetails();
		$this->data['array'] = $data;
		$this->render_template('permission/permission_index', $this->data);	
    }	
    public function create()
	{
        // echo"<pre>";print_r($this->input->post());die;
		$response = array();
		$this->form_validation->set_rules('module_link', 'User Type', 'trim|required');
		$this->form_validation->set_rules('priority', 'Priority', 'trim|required');

		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'link_name' => $this->input->post('module_link'),
				'link_controller' => $this->input->post('controller_link'),	
				'priority' => $this->input->post('priority'),	
        	);
			// echo"<pre>";print_r($data);die;
        	$create = $this->Model_modules->create($data);
        	if($create == true) {
				$this->session->set_flashdata('success', 'Successfully created');
				redirect('Controller_UserModule/index', 'refresh');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the user type information';	
				$this->render_template('Controller_UserModule/index', $response);	
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }
	}
	   
}