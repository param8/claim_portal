<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends Admin_Controller 

{

	public function __construct()

	{

		parent::__construct();



		$this->load->model('Model_auth');

	}



	/* 

		Check if the login form is submitted, and validates the user credential

		If not submitted it redirects to the login page

	*/

	public function login()

	{

		$this->logged_in();

		$this->form_validation->set_rules('email', 'Email', 'required');

        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == TRUE) {

            // true case
            //echo"<pre>";print_r($this->input->post());die;
           	$email_exists = $this->Model_auth->check_email($this->input->post('email'));
           	if($email_exists == TRUE) {

           		$login = $this->Model_auth->login($this->input->post('email'), $this->input->post('password'));
           		if($login) {
					$ButtonArr=array();
					$menuLink="";
					if($login['user_type_id']=='1')
					{
					    if($login['id']==2)// for factory login
					    {
    						$ButtonArr=array("All");
    						 $menuLink.='<li id="dashboardMainMenu">
									  <a href="'.base_url('dashboard').'">
										<i class="fa fa-dashboard"></i> <span>Dashboard</span>
									  </a>
									</li>
										<li id="categoryNav">
										  <a href="'.base_url('Controller_Category/').'">
											<i class="fa fa-cubes"></i> <span>Category</span>
										  </a>
										</li>
										<li id="storeNav1">
										  <a href="'.base_url('Controller_Products/saleScrap').'">
											<i class="fa fa-institution"></i> <span>Scrap</span>
										  </a>
										</li>
										<li id="categoryNav">
										  <a href="'.base_url('Controller_Warehouse/indexFactory').'">
											<i class="fa fa-user"></i> <span>Add Stock</span>
										  </a>
										</li><li id="categoryNav">
										  <a href="'.base_url('Controller_Warehouse/transferStock').'">
											<i class="fa fa-user"></i> <span>Transfer Stock</span>
										  </a>
										</li>
										<li id="categoryNav">
										  <a href="'.base_url('Controller_Warehouse/transferStockReport').'">
											<i class="fa fa-user"></i> <span>Transfer Stock Report</span>
										  </a>
										</li>
										<li id="categoryNav">
										  <a href="'.base_url('Controller_Products/scrap_report').'">
											<i class="fa fa-user"></i> <span>Scrap Report</span>
										  </a>
										
    									</li><li><a href="'.base_url('auth/logout').'">
    									<i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>';
					    }
					    else
					    {
    						$ButtonArr=array("All");
    						 $menuLink.='<li id="dashboardMainMenu">
									  <a href="'.base_url('dashboard').'">
										<i class="fa fa-dashboard"></i> <span>Dashboard</span>
									  </a>
									</li>
										<li id="categoryNav">
										  <a href="'.base_url('Controller_UserModule/').'">
											<i class="fa fa-user"></i> <span>Add User-Type</span>
										  </a>
										</li><li id="categoryNav">
										  <a href="'.base_url('Controller_UserModule/indexUser').'">
											<i class="fa fa-user"></i> <span>Add User</span>
										  </a>
										</li>
										<li id="categoryNav">
										  <a href="'.base_url('Controller_UserModule/assignModule').'">
											<i class="fa fa-user"></i> <span>Assign Module</span>
										  </a>
										</li>
										<li id="siteStockNav">
										  <a href="'.base_url('Controller_Warehouse/indexSite').'">
											<i class="fa fa-institution"></i> <span>Add Stock</span>
										  </a>
										</li>
										<li id="storeNav">
										  <a href="'.base_url('Controller_Warehouse/').'">
											<i class="fa fa-institution"></i> <span>Site</span>
										  </a>
										</li>
										<li id="storeNav">
										  <a href="'.base_url('Controller_Products/saleScrap').'">
											<i class="fa fa-institution"></i> <span>Scrap</span>
										  </a>
										</li>
							
										<li id="categoryNav">
										  <a href="'.base_url('Controller_Category/').'">
											<i class="fa fa-cubes"></i> <span>Category</span>
										  </a>
										</li>
										<li id="categoryNav">
										  <a href="'.base_url('Controller_Vendor/').'">
											<i class="fa fa-user"></i> <span>Vendor</span>
										  </a>
										</li>
										<li id="categoryNav">
										  <a href="'.base_url('Controller_Project/Panel').'">
											<i class="fa fa-user"></i> <span>Panel</span>
										  </a>
										</li>
										<li id="categoryNav">
										  <a href="'.base_url('Controller_Project/assignTo').'">
											<i class="fa fa-user"></i> <span>DPR</span>
										  </a>
										</li>
										<li id="categoryNav">
										  <a href="'.base_url('Controller_Project/dprReport').'">
											<i class="fa fa-user"></i> <span>DPR Report</span>
										  </a>
										</li>
										<li id="categoryNav">
										  <a href="'.base_url('Controller_Project/SearchDPRDateRange').'">
											<i class="fa fa-user"></i> <span>DPR Date Range Report</span>
										  </a>
										</li>
										<li id="categoryNav">
											<a href="'.base_url('Controller_Products/create').'">
											<i class="fa fa-circle-o"></i> Add Product</a>
										</li>
										<li id="categoryNav">
										  <a href="'.base_url('Controller_Members').'">
										  <i class="fa fa-circle-o"></i> Issue & Return</a></li>
										</li>
										<li id="categoryNav">
											<a href="'.base_url('Controller_Members/Report').'">
											<i class="fa fa-circle-o"></i>Product Assign Report</a></li>
										</li>
										<li id="categoryNav">
											<a href="'.base_url('Controller_Products/TransferAsset').'">
											<i class="fa fa-circle-o"></i>Transfer Stock</a></li>
										</li>
										<li id="categoryNav">
											<a href="'.base_url('Controller_Products/AcceptAsset').'">
											<i class="fa fa-circle-o"></i>Stock Notification</a></li>
										</li>
										<li id="categoryNav">
										  <a href="'.base_url('Controller_Project/assignStock').'">
											<i class="fa fa-user"></i> <span>Assign Stock</span>
										  </a>
										</li>
                                        <li class="treeview" id="mainProductNav">
                                              <a href="javascript:void(0)">
                                                <i class="fa fa-cube"></i>
                                                <span>Reports</span>
                                                <span class="pull-right-container">
                                                  <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                              </a>
                                              <ul class="treeview-menu">
                                                <li id="manageProductNav"><a href="'.base_url('Controller_Project/assignStockReport').'"><i class="fa fa-circle-o"></i>Assign Stock Report</a></li>
                                                <li id="manageProductNav"><a href="'.base_url('Controller_Products/TransferAssetReport').'"><i class="fa fa-circle-o"></i>Transfer Stock Report</a></li>
                                              </ul>
                                            </li>
    										</li><li><a href="'.base_url('auth/logout').'">
    										<i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>';
					    }
					}
					else
					{
						$permissionDataArr = $this->Model_auth->check_Menu_Link($login['user_type_id']);
						$menuLink=$permissionDataArr[0];
						$ButtonArr=$permissionDataArr[1];
					}
           			$logged_in_sess = array(
           				'id' => $login['id'],
				        'username'  => $login['username'],
				        'password'  => $this->input->post('password'),
				        'email'     => $login['email'],
						'mobile_no'=> $login['mobile_no'],
						'address'=> $login['address'],
						'company_id'     => $login['company_id'],
						'user_type'     => $login['user_type_id'],
						'store_id'     => $login['store_id'],
						'Menu'     => $menuLink,
						'ButtonPermisssion'     => $ButtonArr,
				        'logged_in' => TRUE

					);
					//echo"<pre>";print_r($logged_in_sess);die;
					if($this->input->post('stock')=='stock')
					{
						$this->session->set_userdata($logged_in_sess);
						redirect('dashboard', 'refresh');
					}
					else
					{
						header('Location: ../ClaimPortal/dashboard.php?user='.$login['id']);
						die;
					}
           		}

           		else {

           			$this->data['errors'] = 'Incorrect username/password combination';

           			$this->load->view('login', $this->data);

           		}

           	}

           	else {

           		$this->data['errors'] = 'Email/Mobile no does not exists';
           		$this->load->view('login', $this->data);
           	}	

        }

        else {

            // false case

            $this->load->view('login');

        }	

	}



	/*

		clears the session and redirects to login page

	*/

	public function logout()

	{

		$this->session->sess_destroy();

		redirect('auth/login', 'refresh');

	}



}

