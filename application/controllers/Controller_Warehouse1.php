<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_Warehouse extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();
		$this->load->library('encryption');
		$this->data['page_title'] = 'Warehouse';
		$this->load->model('Model_user_module');
		$this->load->model('model_stores');
		$this->load->model('Model_category');
		$this->load->model('Model_project');
	}

	/* 
    * It only redirects to the manage stores page
    */
	public function index()
	{
	    $data= $this->model_stores->getStoresData();
	    $this->data['store_details']= $data;
		$this->render_template('warehouse/index', $this->data);	
	}	
	public function indexFactory()
	{
	    $data= $this->model_stores->getFactoryData('');
	    $this->data['product_details']= $data;
		$this->render_template('warehouse/indexFactoryStock', $this->data);	
	}
	public function indexSite()
	{
	    $data= $this->model_stores->getSiteDataData('');
	    $this->data['product_details']= $data;
		$this->render_template('warehouse/indexSiteStock', $this->data);	
	}
	public function getSiteDetails()
	{
		return $data = $this->Model_project->getSiteDetails();
	}
	public function transferStock()
	{
	    $data= $this->model_stores->getStoresDataListing();
	    $data1= $this->model_stores->getFactoryData('');
	    $this->data['site_details']= $data;
	    $this->data['product_details']= $data1;
		$this->render_template('warehouse/transferStock', $this->data);	
	}
	public function transferStockReport()
	{
	    //echo"<pre>";print_r($this->input->post());die;
	    $siteId=$this->input->post('searchSite');
	    $data= $this->model_stores->getStoresDataListing();
	    $data1=array();
	    if($siteId)
	        $data1= $this->model_stores->getFactoryTransferData($siteId);
	        
	    $this->data['site_details']= $data;
	    $this->data['product_details']= $data1;
	    $this->data['store_id']=$siteId;
		$this->render_template('warehouse/transferStockReport', $this->data);	
	}

	/*
	* It retrieve the specific store information via a store id
	* and returns the data in json format.
	*/
	public function fetchStoresDataById($id) 
	{
		if($id) {
			$data = $this->model_stores->getStoresData($id);
			echo json_encode($data);
		}
	}
	public function fetchStockDataById($id) 
	{
		if($id) {
			$data = $this->model_stores->getFactoryData($id);
			echo json_encode($data);
		}
	}
	public function fetchProductDetailByCatId($id) 
	{
		if($id) {
			$data = $this->model_stores->getProductDetailByCatId($id);
			//echo"<pre>";print_r($data);die;
			echo json_encode($data);
		}
	}
	public function fetchProductDetailByCatIdForSite($id,$siteId) 
	{
		if($id) {
			$data = $this->model_stores->fetchProductDetailByCatIdForSite($id,$siteId);
			//echo"<pre>";print_r($data);die;
			echo json_encode($data);
		}
	}
	public function password_hash($pass = '')
	{
		if($pass) {
			$password = password_hash($pass, PASSWORD_DEFAULT);
			return $password;
		}
	}

	public function create()
	{

		$response = array();
		$this->form_validation->set_rules('store_name', 'Site name', 'trim|required');
		$this->form_validation->set_rules('active', 'Active', 'trim|required');
		$this->form_validation->set_rules('site_manager_id', 'Site Manager', 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'name' => $this->input->post('store_name'),
        		'coordinator_id' => $this->input->post('coordinator_id'),
        		'site_manager_id' => $this->input->post('site_manager_id'),
        		'active' => $this->input->post('active'),	
        		'company_id' => $this->session->userdata['company_id'],	
        	);
           
			//echo"<pre>";print_r($data);die;
        	$create = $this->model_stores->create($data);
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully created.');
        		redirect('Controller_Warehouse');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the brand information';	
        		redirect('Controller_Warehouse/index', 'refresh');
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        			redirect('Controller_Warehouse/index', 'refresh');
        	}
        }
	}	
	public function createFactoryStock()
	{

		$response = array();
        $this->form_validation->set_rules('stock_name', 'Stock Name', 'trim|required');
        $this->form_validation->set_rules('qty', 'Quantity', 'trim|required');
        $this->form_validation->set_rules('unit', 'Unit', 'trim|required');
        $this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
		//echo"<pre>";print_r($this->input->post());die;
        if ($this->form_validation->run() == TRUE) {
			$data= array();
			$dataUpdate= array();
			if($this->input->post('stock_name')=='New')
			{
				$data = array(
					'stock_name' => $this->input->post('stock'),
					'qty' => $this->input->post('qty'),
					'category_id' => $this->input->post('unit'),
					'entry_by' => $this->session->userdata['id'],	
				);
			}
			else
			{
				$dataUpdate= array(
					'stock_name' => $this->input->post('stock_name'),
					'qty' => $this->input->post('qty'),
					'category_id' => $this->input->post('unit'),
				);
			}
        	$create = $this->model_stores->createFactoryStock($data,$dataUpdate);
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully created.');
        		redirect('Controller_Warehouse/indexFactory');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the brand information';	
        		redirect('Controller_Warehouse/indexFactory', 'refresh');
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        			redirect('Controller_Warehouse/indexFactory', 'refresh');
        	}
        }
	}
	public function createSiteStock()
	{
		$response = array();
        $this->form_validation->set_rules('site', 'Site Name', 'trim|required');
        $this->form_validation->set_rules('stock_name', 'Stock Name', 'trim|required');
        $this->form_validation->set_rules('qty', 'Quantity', 'trim|required');
        $this->form_validation->set_rules('unit', 'Unit', 'trim|required');
        $this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
		//echo"<pre>";print_r($this->input->post());die;
        if ($this->form_validation->run() == TRUE) {
			$data= array();
			$dataUpdate= array();
			if($this->input->post('stock_name')=='New')
			{
				$data = array(
					'name' => $this->input->post('stock'),
					'store_id' => $this->input->post('site'),
					'qty' => $this->input->post('qty'),
					'total_quantity' => $this->input->post('qty'),
					'category_id' => $this->input->post('unit'),
					'entry_by' => $this->session->userdata['id'],	
				);
			}
			else
			{
				$dataUpdate= array(
					'stock_name' => $this->input->post('stock_name'),
					'store_id' => $this->input->post('site'),
					'qty' => $this->input->post('qty'),
					'category_id' => $this->input->post('unit'),
				);
			}
			//echo"<pre>";print_r($dataUpdate);die;
        	$create = $this->model_stores->createSiteStock($data,$dataUpdate);
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully created.');
        		redirect('Controller_Warehouse/indexSite');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the brand information';	
        		redirect('Controller_Warehouse/indexSite', 'refresh');
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        			redirect('Controller_Warehouse/indexSite', 'refresh');
        	}
        }
	}
	public function transferToWarehouse()
	{

		$response = array();
        $this->form_validation->set_rules('transferTo', 'Site Name', 'trim|required');
        $this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');
        //echo"<pre>";print_r($this->input->post());die;
        if ($this->form_validation->run() == TRUE) {
            $factoryIdsArr=$this->input->post('checkProductsArr');
			$updateProduct=array();
			$insertTransferHistory=array();
			foreach($factoryIdsArr as $pId){
				if($this->input->post('checkProduct'.$pId)=='on'){
					$updateProduct[$pId]=array("used_qty"=>$this->input->post('assign_qty'.$pId));
					$prodCatArr=explode("@_@",$this->input->post('ProductsName'.$pId));
					$insertTransferHistory[]=array("factory_product_id"=>$pId,"name"=>$prodCatArr[0],"category_id"=>$prodCatArr[1],"qty"=>$this->input->post('assign_qty'.$pId),"total_quantity"=>$this->input->post('assign_qty'.$pId),"store_id"=>$this->input->post('transferTo'),"status"=>'Accepted',"entry_by"=>$this->session->userdata['id']);
				}
			}
            //echo"<pre>";print_r($insertTransferHistory);die;
        	$create = $this->model_stores->InsertUpdateTransferDetails($insertTransferHistory,$updateProduct);
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully transfer.');
        		redirect('Controller_Warehouse/transferStock');
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the brand information';	
        		redirect('Controller_Warehouse/transferStock', 'refresh');
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        			redirect('Controller_Warehouse/transferStock', 'refresh');
        	}
        }
	}	


	/*
    * If the validation is not valid, then it provides the validation error on the json format
    * If the validation for each input is valid then it updates the data into the database and 
    returns a n appropriate message in the json format.
    */

	public function update()
	{
		$response = array();
        $id=$this->input->post('site_id');
		if($id) {
			$this->form_validation->set_rules('edit_store_name', 'Warehouse name', 'trim|required');
			$this->form_validation->set_rules('edit_active', 'Active', 'trim|required');
			$this->form_validation->set_rules('edit_site_manager_id', 'Site Manager', 'trim|required');
			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

	        if ($this->form_validation->run() == TRUE) {
	        	$data = array(
	        		'name' => $this->input->post('edit_store_name'),
            		'coordinator_id' => $this->input->post('edit_coordinator_id'),
            		'site_manager_id' => $this->input->post('edit_site_manager_id'),
	        		'active' => $this->input->post('edit_active'),	
	        	);
                //echo"<pre>";print_r($data);die;
	        	$update = $this->model_stores->update($data, $id);
	        	if($update == true) {
	        		$this->session->set_flashdata('success', 'Successfully updated.');
        		    redirect('Controller_Warehouse');
	        	}
	        	else {
	        		$this->session->set_flashdata('success', 'Error in the database while updated the brand information.');
        		    redirect('Controller_Warehouse');
	        	}
	        }
	        else {
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        	redirect('Controller_Warehouse');
	        }
		}
		else {
			$response['success'] = false;
    		$response['messages'] = 'Error please refresh the page again!!';
		}

		echo json_encode($response);
	}
	public function UpdateFactoryStock()
	{
		$response = array();
   		$factory_stock_id=$this->input->post('factory_stock_id');
		if($factory_stock_id) {
			$this->form_validation->set_rules('edit_stock', 'Warehouse name', 'trim|required');
			$this->form_validation->set_rules('edit_qty', 'Active', 'trim|required');
			$this->form_validation->set_rules('edit_unit', 'Site Manager', 'trim|required');
			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

            if ($this->form_validation->run() == TRUE) {
            	$data = array(
            		'stock_name' => $this->input->post('edit_stock'),
            		'qty' => $this->input->post('edit_qty'),
            		'unit' => $this->input->post('edit_unit'),
            	);

                //echo"<pre>";print_r($data);die;
            	$create = $this->model_stores->UpdateFactoryStock($data,$factory_stock_id);
            	if($create == true) {
            		$this->session->set_flashdata('success', 'Successfully updated.');
            		redirect('Controller_Warehouse/indexFactory');
            	}
            	else {
            		$this->session->set_flashdata('error', 'Error please refresh the page again!!.');
            		redirect('Controller_Warehouse/indexFactory');
            	}
            }
            else {  
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        	redirect('Controller_Warehouse');
	        }
		}
		else {
    		$this->session->set_flashdata('error', 'Error please refresh the page again!!.');
    		redirect('Controller_Warehouse/indexFactory');
		}

	}

	/*
	* If checks if the store id is provided on the function, if not then an appropriate message 
	is return on the json format
    * If the validation is valid then it removes the data into the database and returns an appropriate 
    message in the json format.
    */
	public function remove()
	{
		$store_id = $this->input->post('store_id');
		$response = array();
		if($store_id) {
			$delete = $this->model_stores->remove($store_id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error in the database while removing the brand information";
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
		}

		echo json_encode($response);
	}
	public function removeStock()
	{
		$id = $this->input->post('factory_stock_id');
		$response = array();
		if($id) {
			$delete = $this->model_stores->removeStock($id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error in the database while removing the brand information";
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
		}

		echo json_encode($response);
	}
	public function removeSiteStock()
	{
		$id = $this->input->post('factory_stock_id');
		$response = array();
		if($id) {
			$delete = $this->model_stores->removeSiteStock($id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error in the database while removing the brand information";
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
		}

		echo json_encode($response);
	}
	public function getUserDetails($userTypeId)
	{
		return $data = $this->model_stores->getUserDetails($userTypeId);
	}	
	public function getActiveCategroy()
	{
		return $data = $this->Model_category->getActiveCategroy();
	}	
}