<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
           <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">  
           <?php
if(isset($_POST['tableHtml']) && isset($_POST['fileName']))
{
    $htmlContent=$_POST['tableHtml'];
    $fileName=$_POST['fileName'].'.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$fileName");
    echo $htmlContent; die;
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Sale Scrap Report
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Scrap</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
		
        <style>

.searchbtn{

margin-top: 24px;

padding: 5px 20px 5px;

background: #252525;

border: 1px solid #252525;

color: white;

}

 .modal-dialog {

     width: 80% !important;

     margin: 30px auto !important;

     }		  

 .modal-dialogAction {

     width: 40% !important;

     margin: 30px auto !important;

     }		  

 </style>
        <div class="box">
          
          <!-- /.box-header -->
          <div class="box-body">
          <form name="searchVal" action="<?php echo base_url('Controller_Products/scrap_report');?>" method="post">
                <div class="row">
               
                    <div class="col-md-3 col-xs-4">
                        <div class="form-group">
                          <label for="store">From date</label>
                          
                              <input type="date" name="from_date" id="from_date"  class="form-control"  value="<?php echo $from_date; ?>" placeholder="From Date" />  
                         
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-4">
                        <div class="form-group">
                          <label for="store">To date</label>
                         
                             <input type="date" name="to_date" id="to_date" class="form-control" value="<?php echo $to_date; ?>" placeholder="To Date" />  
                         
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                          <button onClick="serchCategoryWise()" class="searchbtn">Search</button>
                        </div>          <!-- /.box-header -->
                      </div>
                 </div>
              </form>
               <br/>
               <?php
          // print_r($_SESSION);
                              $permissionFlag="No";
                              if(in_array('All',$this->session->userdata['ButtonPermisssion']))
                                $permissionFlag="Yes";
                                
                              
                              if((array_key_exists('Assign_Stock_Report',$this->session->userdata['ButtonPermisssion'])) || $permissionFlag=="Yes")
                              {
                                  ?>
                                <table  class="table table-bordered table-striped" style="margin:0px;margin-bottom:5px" cellpadding="0" cellspacing="0">
                                  <tr>
									<td>
                                          <?php
                                          if((array_key_exists('Assign_Stock_Report',$this->session->userdata['ButtonPermisssion']) && in_array('Excel',$this->session->userdata['ButtonPermisssion']['Assign_Stock_Report'])) || $permissionFlag=="Yes")
                                          {
                                              ?>
									         <a href="javascript:void(0)" title="Download Excel" onclick="exportTableToExcel('manageTable', 'SaleScrapReport')">
									        <i class="fa fa-file-excel-o" style="font-size: 25px;" aria-hidden="true"></i>
									    </a>&nbsp;&nbsp;
									        <?php
									        }
                                          if((array_key_exists('Assign_Stock_Report',$this->session->userdata['ButtonPermisssion']) && in_array('Print',$this->session->userdata['ButtonPermisssion']['Assign_Stock_Report'])) || $permissionFlag=="Yes")
                                          {
									        ?>
									        <a href="javascript:void(0)" title="Print" onclick="printDiv('manageTable')" >
									            <i class="fa fa-print" style="font-size: 25px;" aria-hidden="true"></i>
									       </a>
									       <?php
                                          }
                                          ?>
									 </td>
								  </tr>
								</table>
							<?php
                              }
                              ?>
         
          <!-- <button onclick="printDiv('print')"  class="btn btn-primary btn-sm"> <i class="fa fa-print" aria-hidden="true">&nbsp;Print</i></button> 
               <div id="print"> -->
               <div  id="manageTable">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Item Name</th>
                <th>Measurement</th>
                <th>Scrap Unit</th>
                <th>Cumulative Unit</th>
                <th>Sale Unit</th>
                <th>Remark</th>
                <th>Entry Date</th>
                <th>Last Sale Date</th>
              </tr>
              </thead>
				<?php
				if(count($this->data['array'])>0)
				{
					foreach($this->data['array'] as $vDetails)	
					{
					?>
                      <tr>
                        <td><?php echo $vDetails['item_name']?></td>
                        <td><?php echo $vDetails['measurement']?></td>
                        <td><?php echo $vDetails['scrap_unit']?></td>
                        <td><?php echo $vDetails['cumulative_unit']?></td>
                        <td><?php echo $vDetails['sale_unit']?></td>
                        <td><?php echo $vDetails['remark']?></td>
                        <td><?php echo $vDetails['entry_date']?></td>
                        <td><?php if($vDetails['last_sale_date']!='0000-00-00 00:00:00') echo $vDetails['last_sale_date']?></td>
                       </tr>
                    <?php	
					}
				}
				?>
            </table>
          </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    <form role="form" method="post" id="exceldownload">
      <input type="hidden" name="tableHtml" id="tableHtml">
      <input type="hidden" name="fileName" id="fileName">
  </form>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- create brand modal -->
<!-- #############Add user dropdown #######-->
<!-- ################ -->

    
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script>
function refreshPage(Val)
{
	if(Val==1)
	{
		if(document.getElementById('Project_name').value!="" && document.getElementById('active').value!="")
			location.reload();
		else if(document.getElementById('edit_Project_name').value!="" && document.getElementById('edit_unit').value!="")
			location.reload();
		else
			return false;
	}
	else
	{
		location.reload();
	}
}

$("#hidePopUp").click(function(){
	//alert('Hii deep');
	$("#editModal").modal('hide');
});
</script>

<script type="text/javascript">
var manageTable;

$(document).ready(function() {
  // submit the create from 
});


// remove functions 
function removeFunc(id)
{
  document.getElementById('delete_project_id').value=id;
}

function getProductQty(id)
{ 
  var idsArr=id.split("_");
  document.getElementById('item_qty').value=idsArr[2];
  $.ajax({
    url: 'fetchScrapDetail/'+idsArr[0],
    type: 'post',
    dataType: 'json',
    success:function(response) {
	  if(response==null){
	  	document.getElementById('cumulative').value="0";
		document.getElementById('remark').value="";	  
		document.getElementById('scrap_id').value="";	
		document.getElementById('scrap_unit').value=0; 
		document.getElementById('sale_unit').style.display="none";
	  }
		
	  if(response.cumulative_unit!="" && response.cumulative_unit!="0")
	  	document.getElementById('cumulative').value=response.scrap_unit-response.sale_unit;
	  else
	  	document.getElementById('cumulative').value=response.scrap_unit-response.sale_unit;
		
		document.getElementById('remark').value=response.remark;
		document.getElementById('scrap_id').value=response.scrap_item_id;  
		document.getElementById('scrap_unit').value=response.scrap_unit;  

    }
  });
}
function checkExactQty(value)
{
	return true;
}
function submitSale()
{
	var cummulative=document.getElementById('cumulative').value;
	if(cummulative!=0){
		if(document.getElementById('sale_unit').value!="")
		{
			var scrap_id=document.getElementById('scrap_id').value;
			  $.ajax({
				url: 'updateSalesUnit/'+scrap_id+'_'+document.getElementById('sale_unit').value+'_'+document.getElementById('cumulative').value,
				type: 'post',
				dataType: 'json',
				success:function(response) {
					if(response)
					{
						alert('Successfully save..');
						location.reload();
					}
				}
			  });
		}
		else
		{
			var display=document.getElementById('sale_unit').style.display;
			if(display=="block")
			{
				alert('Please enter sale unit.');
				return false;
			}
			document.getElementById('sale_unit').style.display="block";
			
		}
	}
	return false;
}
function checkCumulatvieUnit(value)
{
	var cummulative=document.getElementById('cumulative').value;
	if(Number(cummulative) < Number(value)){
		alert('Enter value should not be greater than cumulative.');
		document.getElementById('sale_unit').value="";
		return false;
	}
}

</script>
<script>
	function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}
function exportTableToExcel(divName,filename) {
   var tableHtml=document.getElementById(divName).innerHTML; 
   document.getElementById("tableHtml").value=tableHtml;
   document.getElementById("fileName").value=filename;
   document.getElementById('exceldownload').submit();
}
	</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
