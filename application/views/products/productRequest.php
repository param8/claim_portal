

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>var $j = jQuery.noConflict(true);</script>

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">



<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <section class="content-header">

    <h1>

      Asset Notification
    </h1>

    <ol class="breadcrumb">

      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>

      <li class="active">Assets</li>

    </ol>

  </section>



  <!-- Main content -->

  <section class="content">

    <!-- Small boxes (Stat box) -->

    <div class="row">

      <div class="col-md-12 col-xs-12">



        <div id="messages"></div>



        <?php if($this->session->flashdata('success')): ?>

          <div class="alert alert-success alert-dismissible" role="alert">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <?php echo $this->session->flashdata('success'); ?>

          </div>

        <?php elseif($this->session->flashdata('error')): ?>

          <div class="alert alert-error alert-dismissible" role="alert">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <?php echo $this->session->flashdata('error'); ?>

          </div>

        <?php endif; ?>
        <div class="box">
          <!-- /.box-header -->

          <div class="box-body">
			<form method="post" action="<?= base_url('Controller_Products/AcceptRequest');?>" >
            <table id="manageTable" class="table table-bordered table-striped">

              <thead style="background-color:#3c8dbc; color:#ffffff">

              <tr>
                <th>S.No</th>
                <th>Stock</th>
                <th>Quantity</th>
                <th>Action</th>
				 <!-- (<input type="checkbox" name="selectAll" onclick="CheckAllProduct(0)" id="selectAll" style="width:20px"/>)</th> -->
              </tr>

              </thead>
				<?php
				$sno=1;
				if(count($this->data['store_wise_products'])>0)
				{
					foreach($this->data['store_wise_products'] as $key=>$value)
					{
						list($stId,$stName)=explode("_",$key);
						?>
						  <thead style="background-color:#D2D2D2; color:#000000">
						  <tr>
							<input type="hidden" name="transferTo" value="<?= $stId ;?>" />
							<th colspan="5"><?= $stName;?></th>
						  </tr>
						  </thead>
						  <?php
							foreach($value as $val)
							{
                				// print_r($val);
							?>
							  <tr>
								<td><?= $sno++ ;?></td>
								<td><?= $val['name'] ;?></td>
								<td><?= $val['received_qty'] ;?></td>
								<td><a href="javascript:void(0)" onclick="approve('<?php echo $val['transfer_stock_to_site_id']; ?>','<?php echo $val['store_id']; ?>','<?php echo $val['received_qty']; ?>','<?php echo $val['factory_product_id']; ?>')" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#approveModal">Accept</a>
                <!-- <a href="javascript:void(0)" onclick="reject('<?php echo $val['id']; ?>','<?php echo $val['store_id']; ?>')" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#PendingModal">Reject</a> -->
                </td>
								<!-- <td><input type="checkbox" name="checkProduct<?= $val['id'] ;?>" id="checkProduct<?= $val['id'] ;?>" style="width:20px"/><input type="hidden" name="checkProductsArr[]" value="<?= $val['id'] ;?>" /></td> -->
							  </tr>
						  <?php
							}
							?>
						<?php
					}
				}
				else
				{
				?>
                    <tr>
                    <td colspan="5" style="text-align:left"><font color="#FF0000"><strong>No data found.</strong></font></td>
                    </tr>
                <?php				
				}
				?>
                <!-- <tr>
                	<td colspan="5" style="text-align:right">
                          <button type="submit" class="btn btn-primary" onClick="return CheckAllProduct(1);">Save</button>
                    </td>
                </tr> -->
            </table>
			</form>
          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- col-md-12 -->

    </div>

    <!-- /.row -->

    

	<div class="modal fade" tabindex="-1" role="dialog" id="approveModal">
           <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong>Accept</strong></h4>
              </div>
        
              <form role="form" action="<?php echo base_url('Controller_Products/AcceptNotification') ?>" method="post" id="">
                <div class="modal-body">
                  <div class="row"style="padding: 22px;" >
                  <!-- <strong>Do you really want to Accept?</strong><br><br>	 -->
                  <input type="hidden"  id="stock_to_site_id" name="stock_to_site_id" >
                  <input type="hidden"  id="sid" name="sid" >
                  <input type="hidden"  id="qty" name="qty" >
                  <input type="hidden"  id="fpid" name="fpid" >
                  <input type="hidden"  id="status" name="status" value="Accepted" >  
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="brand_name"><strong>Recieved Quantity</strong><font color="#FF0000">*</font></label>
                      <input type="number" class="form-control" id="accept_qty" name="accept_qty" onChange="return checkQtyExist(this.value)"  placeholder="Enter Received Quantity"  required>
                    </div>
                  </div>                   
                </div>
                </div>
        
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Accept</button>
                </div>
              </form>
            </div><!-- /.modal-content -->
          </div>
  </div>
	<div class="modal fade" tabindex="-1" role="dialog" id="PendingModal">
           <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong>Reject</strong></h4>
              </div>
        
              <form role="form" action="<?php echo base_url('Controller_Products/RejectNotification') ?>" method="post" id="">
				          <div class="modal-body">
                    <div class="row"style="padding: 22px;" >
                        <strong>Do you really want to Reject?</strong>	<br>
                         <input type="hidden"  id="r_pid" name="r_pid" >
                         <input type="hidden"  id="r_sid" name="r_sid" >
                        <input type="hidden"  id="r_status" name="r_status" value="Rejected" > 
                        <lable><strong>Remark</strong></label>
                        <input type="text"  id="remark" name="remark" required class="form-control" >                    
                      </div>
                  </div>
        
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-danger">Reject</button>
                </div>
              </form>
        
        
            </div><!-- /.modal-content -->
          </div>
     </div>

  </section>

  <!-- /.content -->

</div>

<!-- /.content-wrapper -->



<!-- remove brand modal -->
<script type="text/javascript">
  function approve(stock_to_site_id,sid,qty,fpid){
	  document.getElementById('stock_to_site_id').value=stock_to_site_id;
	  document.getElementById('sid').value=sid;
	  document.getElementById('qty').value=qty;
	  document.getElementById('fpid').value=fpid;
  }
 
</script>
<script type="text/javascript">
  // function reject(pid,sid){
  // document.getElementById('r_pid').value=pid;
  // document.getElementById('r_sid').value=sid;
  // }
function checkQtyExist(enterVal)
{
    actualVal=	document.getElementById('qty').value;
        if(Number(enterVal) > Number(actualVal))
        {
            alert('Enter quantity should not be greater than actual quantity');
			document.getElementById('accept_qty').value=""
            return false;
        }
}
 
</script>

<script type="text/javascript">

var manageTable;

var base_url = "<?php echo base_url(); ?>";
// remove functions 
function CheckAllProduct(chek)
{
	var proIdsArr=document.getElementsByName('checkProductsArr[]');	
	var flag=0;
	for(var i=0;i<proIdsArr.length;i++){
		var pId=proIdsArr[i].value;
		if(chek==0)
		{
			if(document.getElementById('selectAll').checked==true)
			{
				document.getElementById('checkProduct'+pId).checked=true;
			}
			else
			{
				document.getElementById('checkProduct'+pId).checked=false;
			}
		}
		else
		{
			if(document.getElementById('checkProduct'+pId).checked==true)
				var flag=1;
		}
	}
	if(chek==1)
	{
		if(flag==0)
		{
			alert('Please select at least one checkbox to complete the Process.');
			return false;	
		}
	}
}
</script>

<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>