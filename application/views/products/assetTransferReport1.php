

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>var $j = jQuery.noConflict(true);</script>

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">



<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <section class="content-header">

    <h1>

      Transfer Asset Report
    </h1>

    <ol class="breadcrumb">

      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>

      <li class="active">Assets</li>

    </ol>

  </section>



  <!-- Main content -->

  <section class="content">

    <!-- Small boxes (Stat box) -->

    <div class="row">

      <div class="col-md-12 col-xs-12">



        <div id="messages"></div>



        <?php if($this->session->flashdata('success')): ?>

          <div class="alert alert-success alert-dismissible" role="alert">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <?php echo $this->session->flashdata('success'); ?>

          </div>

        <?php elseif($this->session->flashdata('error')): ?>

          <div class="alert alert-error alert-dismissible" role="alert">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <?php echo $this->session->flashdata('error'); ?>

          </div>

        <?php endif; ?>
          <style>

		   .searchbtn{

    margin-top: 24px;

    padding: 5px 20px 5px;

    background: #252525;

    border: 1px solid #252525;

    color: white;

		 }

			.modal-dialog {

				width: 80% !important;

				margin: 30px auto !important;

				}		  

			.modal-dialogAction {

				width: 40% !important;

				margin: 30px auto !important;

				}		  

			</style>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
          <?php
		  $CI =& get_instance();
		  $storeData=$CI->getActiveStore();
		  //echo"<pre>";print_r($storeData);die;
		  ?>
            <form name="searchVal" action="<?php echo base_url('Controller_Products/TransferAssetReport');?>" method="post">
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                          <label for="store">Transfer To</label>
                          <select class="form-control select_group" id="searchCat" name="searchCat" required>
                            <option value="">Select</option>
                            <?php
							foreach($storeData as $val)
							{
							?>
                                <option value="<?= $val['id'];?>" <?php if($this->data['store_id']==$val['id']) echo "selected";?>><?= $val['name'];?></option>
                            <?php
							}
							?>
                          </select>
                        </div>          <!-- /.box-header -->
                      </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                          <button onClick="serchCategoryWise()" class="searchbtn">Search</button>
                        </div>          <!-- /.box-header -->
                      </div>
                 </div>
              </form>
               <br/>
            <table id="manageTable" class="table table-bordered table-striped">

              <thead style="background-color:#3c8dbc; color:#ffffff">

              <tr>
                <th>S.No</th>
                <th>Product</th>
                <th>Category</th>
                <th>Model No</th>
                <th>Quantity</th>
                <th>Transfer Date</th>
                <th>Status</th>
              </tr>

              </thead>
				<?php
				$sno=1;
				if(count($this->data['store_wise_products'])>0)
				{
					foreach($this->data['store_wise_products'] as $key=>$value)
					{
						list($fromName,$toName)=explode("_",$key);
						?>
						  <thead style="background-color:#D2D2D2; color:#000000">
						  <tr>
							<th colspan="7">Transfer From :&nbsp;<?= $fromName;?>&nbsp;&nbsp;&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;&nbsp;To :&nbsp;<?= $toName;?></th>
						  </tr>
						  </thead>
						  <?php
							foreach($value as $val)
							{
							?>
							  <tr>
								<td><?= $sno++ ;?></td>
								<td><?= $val['name'] ;?></td>
								<td><?= $val['cat_name'] ;?></td>
								<td><?= $val['serial_no'] ;?></td>
								<td><?= $val['qty'] ;?></td>
								<td><?= $val['entry_date'] ;?></td>
								<td><?= $val['status'] ;?></td>
							  </tr>
						  <?php
							}
							?>
						<?php
					}
				}
				else
				{
				?>
                    <tr>
                    <td colspan="7" style="text-align:left"><font color="#FF0000"><strong>No data found.</strong></font></td>
                    </tr>
                <?php				
				}
				?>
            </table>
          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- col-md-12 -->

    </div>

    <!-- /.row -->

    



  </section>

  <!-- /.content -->

</div>

<!-- /.content-wrapper -->



<!-- remove brand modal -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>