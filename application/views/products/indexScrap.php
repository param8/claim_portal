<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Scrap
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Scrap</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; 
		
		if($this->session->userdata('user_type')!='PM')
		{
		?>

          <button class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Scrap</button>
          <br /> <br />
		<?php
		}
		?>
        <div class="box">
          
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Item Name</th>
                <th>Measurement</th>
                <th>Scrap Unit</th>
                <th>Cumulative Unit</th>
                <th>Sale Unit</th>
                <th>Remark</th>
                <th>Entry Date</th>
                <th>Last Sale Date</th>
              </tr>
              </thead>
				<?php
				if(count($this->data['array'])>0)
				{
					foreach($this->data['array'] as $vDetails)	
					{
					?>
                      <tr>
                        <td><?php echo $vDetails['item_name']?></td>
                        <td><?php echo $vDetails['measurement']?></td>
                        <td><?php echo $vDetails['scrap_unit']?></td>
                        <td><?php echo $vDetails['cumulative_unit']?></td>
                        <td><?php echo $vDetails['sale_unit']?></td>
                        <td><?php echo $vDetails['remark']?></td>
                        <td><?php echo $vDetails['entry_date']?></td>
                        <td><?php if($vDetails['last_sale_date']!='0000-00-00 00:00:00') echo $vDetails['last_sale_date']?></td>
                       </tr>
                    <?php	
					}
				}
				?>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- create brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Scrap</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Products/createScrap') ?>" method="post" id="">
      <input type="hidden" name="scrap_for" value="Factory" />
      <input type="hidden" name="scrap_id" id="scrap_id" value="" />
      <input type="hidden" name="scrap_unit" id="scrap_unit"  />
        <div class="modal-body">
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Item Name <font color="#FF0000">*</font></label>
                    <?php
					$ci=&get_instance();
					$details=$ci->GetProdductDetails();
					?>
                    <select id="item" name="item" class="form-control" onChange="getProductQty(this.value)" required>
                    	<option value="">--Select Item--</option>
                        <?php
						foreach($details as $det)
						{?>
							<option value="<?php echo $det['id']."_".$det['name']."_".$det['unit'];?>"><?php echo $det['name'];?></option>
                            <?php
						}
						?>
                    </select>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Measurement <font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" readonly id="item_qty" name="item_qty" placeholder="Enter Unit" autocomplete="off" required>
                  </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Scrap Unit <font color="#FF0000">*</font></label>
                    <input type="number" onKeyUp="return checkExactQty(this.value)" class="form-control" id="unit_sale" name="unit_sale" placeholder="Enter Unit Sale" autocomplete="off" required>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Cumulative </label>
                    <input type="text" readonly class="form-control" id="cumulative" name="cumulative" placeholder="Enter cumulative" autocomplete="off" >
                  </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-12">
                  <div class="form-group" style="text-align:right; ">
                    <input type="number" style="width:150px; float: right; display:none" class="form-control" id="sale_unit" name="sale_unit" placeholder="Enter sale unit" onKeyUp="return checkCumulatvieUnit(this.value)" autocomplete="off" >
                    <input type="button" name="submit" class="btn btn-primary" style="width:118px; float: right;" value="Sale" onClick="return submitSale()">
                  </div>
                
              </div>
             
           </div>
		  <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                    <label for="brand_name">Remark</label>
                    <input type="text" class="form-control" id="remark" name="remark" placeholder="Enter remark" autocomplete="off" >
                  </div>
              </div>
           </div>
        
<!-- #############Add user dropdown #######-->
<!-- ################ -->

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" >Save changes</button>
        </div>
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script>
function refreshPage(Val)
{
	if(Val==1)
	{
		if(document.getElementById('Project_name').value!="" && document.getElementById('active').value!="")
			location.reload();
		else if(document.getElementById('edit_Project_name').value!="" && document.getElementById('edit_unit').value!="")
			location.reload();
		else
			return false;
	}
	else
	{
		location.reload();
	}
}

$("#hidePopUp").click(function(){
	//alert('Hii deep');
	$("#editModal").modal('hide');
});
</script>

<script type="text/javascript">
var manageTable;

$(document).ready(function() {
  // submit the create from 
});


// remove functions 
function removeFunc(id)
{
  document.getElementById('delete_project_id').value=id;
}

function getProductQty(id)
{ 
  var idsArr=id.split("_");
  document.getElementById('item_qty').value=idsArr[2];
  $.ajax({
    url: 'fetchScrapDetail/'+idsArr[0],
    type: 'post',
    dataType: 'json',
    success:function(response) {
	  if(response==null){
	  	document.getElementById('cumulative').value="0";
		document.getElementById('remark').value="";	  
		document.getElementById('scrap_id').value="";	
		document.getElementById('scrap_unit').value=0; 
		document.getElementById('sale_unit').style.display="none";
	  }
		
	  if(response.cumulative_unit!="" && response.cumulative_unit!="0")
	  	document.getElementById('cumulative').value=response.scrap_unit-response.sale_unit;
	  else
	  	document.getElementById('cumulative').value=response.scrap_unit-response.sale_unit;
		
		document.getElementById('remark').value=response.remark;
		document.getElementById('scrap_id').value=response.scrap_item_id;  
		document.getElementById('scrap_unit').value=response.scrap_unit;  

    }
  });
}
function checkExactQty(value)
{
	return true;
}
function submitSale()
{
	var cummulative=document.getElementById('cumulative').value;
	if(cummulative!=0){
		if(document.getElementById('sale_unit').value!="")
		{
			var scrap_id=document.getElementById('scrap_id').value;
			  $.ajax({
				url: 'updateSalesUnit/'+scrap_id+'_'+document.getElementById('sale_unit').value+'_'+document.getElementById('cumulative').value,
				type: 'post',
				dataType: 'json',
				success:function(response) {
					if(response)
					{
						alert('Successfully save..');
						location.reload();
					}
				}
			  });
		}
		else
		{
			var display=document.getElementById('sale_unit').style.display;
			if(display=="block")
			{
				alert('Please enter sale unit.');
				return false;
			}
			document.getElementById('sale_unit').style.display="block";
			
		}
	}
	return false;
}
function checkCumulatvieUnit(value)
{
	var cummulative=document.getElementById('cumulative').value;
	if(Number(cummulative) < Number(value)){
		alert('Enter value should not be greater than cumulative.');
		document.getElementById('sale_unit').value="";
		return false;
	}
}

</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>