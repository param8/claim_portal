

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>var $j = jQuery.noConflict(true);</script>

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">



<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <section class="content-header">

    <h1>

      Transfer Assets
    </h1>

    <ol class="breadcrumb">

      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>

      <li class="active">Assets</li>

    </ol>

  </section>



  <!-- Main content -->

  <section class="content">

    <!-- Small boxes (Stat box) -->

    <div class="row">

      <div class="col-md-12 col-xs-12">



        <div id="messages"></div>



        <?php if($this->session->flashdata('success')): ?>

          <div class="alert alert-success alert-dismissible" role="alert">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <?php echo $this->session->flashdata('success'); ?>

          </div>

        <?php elseif($this->session->flashdata('error')): ?>

          <div class="alert alert-error alert-dismissible" role="alert">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <?php echo $this->session->flashdata('error'); ?>

          </div>

        <?php endif; ?>
        <div class="box">
          <!-- /.box-header -->

          <div class="box-body">

            <table id="manageTable" class="table table-bordered table-striped">

              <thead style="background-color:#3c8dbc; color:#ffffff">

              <tr>
                <th>S.No</th>
                <th>Warehouse</th>
                <th>Action</th>
              </tr>

              </thead>
				<?php
				$sno=1;
				foreach($this->data['store'] as $val)
				{
					?>
                      <tr>
                        <td><?= $sno++ ;?></td>
                        <td><?= $val['name'] ;?></td>
                        <td><button type="button" class="btn btn-info btn-sm" onclick="assignToUser(<?= $val['id'] ?>),getAllProductByWHId(<?= $val['id'] ?>)" data-toggle="modal" data-target="#assignModal"><i class="fa fa-share-square-o"></i> <strong>Click to Transfer</strong></button></td>
                      </tr>
                    <?php
				}
				?>
            </table>

          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- col-md-12 -->

    </div>

    <!-- /.row -->

    



  </section>

  <!-- /.content -->

</div>

<!-- /.content-wrapper -->



<!-- remove brand modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="assignModal">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <h4 class="modal-title"><strong>Transfer to warehouse</strong></h4>

      </div>
      <form role="form" action="<?php echo base_url('Controller_Products/transferToWarehouse') ?>" method="post" id="">
		<input type="hidden" name="fromWHId" id="fromWHId" />
        <div class="modal-body">
		  <div class="row">
              <div class="col-md-12">

                  <div class="form-group">

                    <label for="active">To Warehouse <font color="#FD0000">*</font></label>

                    <select class="form-control" id="transferTo" name="transferTo" required>
                   </select>

                  </div>
                  <div class="form-group" id="showDataProduct">
                  </div>

              </div>

          </div>

        </div>



        <div class="modal-footer">

          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

          <button type="submit" class="btn btn-primary" onClick="return CheckAllProduct(1);">Save changes</button>

        </div>



      </form>





    </div><!-- /.modal-content -->

  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->







<script type="text/javascript">

var manageTable;

var base_url = "<?php echo base_url(); ?>";
// remove functions 

function assignToUser(id)
{
  if(id) {
      $j.ajax({
		url: 'ShowWareHouse/'+id,
		type: 'post',
		dataType: 'json',
        success:function(data) {
			$('select[name="transferTo"]').empty();
			$('select[name="transferTo"]').append('<option value="">Select</option>');
			$.each(data, function(key, value)
			{
				$('select[name="transferTo"]').append('<option value="'+value.id+'">'+value.name+'</option>');
			})
		 }
      }); 
  }
}
function getAllProductByWHId(id)
{
  if(id) {
	  document.getElementById('fromWHId').value=id;
      $j.ajax({
		url: 'GetAllProductByWareHId/'+id,
		type: 'post',
		dataType: 'json',
        success:function(data) {
			document.getElementById('showDataProduct').innerHTML=data;
		 }
      }); 
  }
}
function CheckAllProduct(chek)
{
	var proIdsArr=document.getElementsByName('checkProductsArr[]');	
	var flag=0;
	for(var i=0;i<proIdsArr.length;i++){
		var pId=proIdsArr[i].value;
		if(chek==0)
		{
			if(document.getElementById('selectAll').checked==true)
			{
				document.getElementById('checkProduct'+pId).checked=true;
			}
			else
			{
				document.getElementById('checkProduct'+pId).checked=false;
			}
		}
		else
		{
			if(document.getElementById('checkProduct'+pId).checked==true)
				var flag=1;
		}
	}
	if(chek==1)
	{
		if(flag==0)
		{
			alert('Please select at least one checkbox to complete the Process.');
			return false;	
		}
	}
}
</script>

<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>