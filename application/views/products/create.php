
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>var $j = jQuery.noConflict(true);</script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">




<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add New Products
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Products</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>


        <div class="box">
         
          <!-- /.box-header -->
          <form role="form" action="<?php base_url('users/create') ?>" method="post" enctype="multipart/form-data">
              <div class="box-body">

                <?php echo validation_errors(); ?>

                <!--<div class="form-group">

                  <label for="product_image">Image</label>
                  <div class="kv-avatar">
                      <div class="file-loading">
                          <input id="product_image" name="product_image" type="file">
                         
                      </div>
                       <span id="validDis" style="color:red; font-weight:bold"></span>
                  </div>
                </div>-->
                <div class="form-group">
                  <label for="store"></label>
                    <div class="col-md-6 col-xs-6">
                      <select class="form-control select_group" id="enterBy" name="enterBy" onChange="showHideBarcode(this.value,1)">
                          <option value="Manually">Manually</option>
                          <option value="scanbar">Scan Barcode</option>
                      </select>
                 	 </div>
                    <div class="col-md-6 col-xs-6">
                      <select class="form-control select_group" id="enterBy" name="enterBy" onChange="showHideBarcode(this.value,2)">
                          <option value="Single">Single Product</option>
                          <option value="Multiple">Multiple Product</option>
                      </select>
                 	 </div>
                </div>
                <hr><br>
                <div class="form-group" id="BarcodeVisibilty" style="display:none">
                  <label for="product_name">Scan Barcode</label>
                  <input type="text" class="form-control" id="barcode" name="barcode" placeholder="Scan Barcode" autocomplete="off"/>
                </div>
                <div class="form-group">
                  <label for="category">Category <font color="#FD0004">*</font></label>
                  <select class="form-control select_group" id="category" name="category" onChange="showHideExtraDetail(this.value)" required>
                      <option value="">Select</option>
                    <?php foreach ($category as $k => $v): ?>
                      <option value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
               <!-- <div class="form-group" style="display:none" id="extraDetail">
               		<div class="row">
                    	<div class="col-sm-2">
                              <label for="brands">RAM <font color="#FD0004">*</font></label>
                              <input type="text" class="form-control" id="RAM" name="RAM" autocomplete="off"  />
                  		</div>
                    	<div class="col-sm-2">
                              <label for="brands">HDD <font color="#FD0004">*</font></label>
                              <input type="text" class="form-control" id="HDD" name="HDD" autocomplete="off"  />
                  		</div>
                    	<div class="col-sm-2">
                              <label for="brands">Mother Board<font color="#FD0004">*</font></label>
                              <input type="text" class="form-control" id="mother_board" name="mother_board" autocomplete="off"  />
                  		</div>
                    	<div class="col-sm-2">
                              <label for="brands">Screen</label>
                              <input type="text" class="form-control" id="screen" name="screen" autocomplete="off"  />
                  		</div>
                    	<div class="col-sm-2">
                              <label for="brands">Power Supply</label>
                              <input type="text" class="form-control" id="power_supply" name="power_supply" autocomplete="off" />
                  		</div>
                    	<div class="col-sm-2">
                              <label for="brands">Other(s)</label>
                              <input type="text" class="form-control" id="Other" name="Other" autocomplete="off"  />
                  		</div>
                  	</div>
                </div>-->
                <div class="form-group">
                  <label for="category">Vendor</label>
                  <select class="form-control select_group" id="vendor" name="vendor">
                      <option value="">Select</option>
                    <?php foreach ($this->data['vendor'] as  $ven): ?>
                      <option value="<?php echo $ven['vendor_id'] ?>"><?php echo $ven['vendor_name'] ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="product_name">Item <font color="#FD0004">*</font></label>
                  <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Enter product name" autocomplete="off" required/>
                </div>

                <!-- <div class="form-group"> -->
                  <!-- <label for="sku">SKU</label> -->
                  <!-- <input type="text" class="form-control" id="sku" name="sku" placeholder="Enter sku" autocomplete="off" /> -->
                <!-- </div> -->

                <div class="form-group">
                  <label for="price">Price </label>
                  <input type="text" class="form-control" id="price" name="price" placeholder="Enter price" autocomplete="off" />
                </div>

                <div class="form-group" id="qunatitySM" style="display:none">
                  <label for="qty">Qty</label>
                  <input type="text" class="form-control" id="qty" name="qty" value="1" placeholder="Enter Qty" autocomplete="off" />
                </div>

                <div class="form-group">
                  <label for="description">Serial No (Enter serial number with comma string.(If any more than one))</label>
                  <input type="text" class="form-control" id="serial_no" name="serial_no" placeholder="Enter Serial No" autocomplete="off">
                </div>
                <div class="form-group">
                  <label for="description">Description</label>
                  <textarea type="text" class="form-control" id="description" name="description" placeholder="Enter 
                  description" autocomplete="off">
                  </textarea>
                </div>
                <div class="form-group">
                  <label for="description">Other Remark</label>
                  <textarea type="text" class="form-control" id="otherRemark" name="otherRemark" placeholder="Enter Other Remark" autocomplete="off"></textarea>
                </div>


                <div class="form-group">
                  <label for="brands">Upload Bill</label>
                  <input type="file" class="form-control" id="bill_copy" name="bill_copy" />
                </div>
                <div class="form-group">
                  <label for="brands">Purchase Date <font color="#FD0004">*</font></label>
                  <input type="date" class="form-control" id="purchase_date" name="purchase_date" autocomplete="off" value="<?php echo date('Y-m-d');?>" required />
                </div>
                <div class="form-group">
                  <label for="brands">Warranty Expiry Date</label>
                  <input type="date" class="form-control" id="expiry_date" name="expiry_date" autocomplete="off" value="<?php echo date('Y-m-d',strtotime("+1 years"));?>" />
                </div>
                
                <div class="form-group">
                  <label for="store">Warehouse <font color="#FD0004">*</font></label>
                  <select class="form-control select_group" id="store" name="store" required>
                      <option value="">--SELECT--</option>
                    <?php foreach ($stores as $k => $v): ?>
                      <option value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary" onClick="return validImage()">Save Changes</button>
<!--                <a href="<?php //echo base_url('Controller_Products/') ?>" class="btn btn-warning">Back</a>
-->              </div>
            </form>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
function showHideExtraDetail(catId)
{
	if(catId==1 || catId==2)	
	{
		document.getElementById('extraDetail').style.display="block";
	}
	else
	{
		document.getElementById('extraDetail').style.display="none";
	}
}
function validImage()
{
	catId=document.getElementById('category').value;
	var display="";
	if(catId==1 || catId==2)	
	{
		if(document.getElementById('RAM').value=="")
		{
			display+="RAM field can not be left blank.\n";
		}
		if(document.getElementById('HDD').value=="")
		{
			display+="HDD field can not be left blank.\n";
		}
		if(document.getElementById('mother_board').value=="")
		{
			display+="Mother board field can not be left blank.\n";
		}
	}
	if(display!="")
	{
		alert(display);	
		return false;
	}
	else
		return true;
}
  $(document).ready(function() {
    $(".select_group").select2();
    $("#description").wysihtml5();

    $("#mainProductNav").addClass('active');
    $("#addProductNav").addClass('active');
    
    var btnCust = '<button type="button" class="btn btn-secondary" title="Add picture tags" ' + 
        'onclick="alert(\'Call your custom code here.\')">' +
        '<i class="glyphicon glyphicon-tag"></i>' +
        '</button>'; 
    $("#product_image").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        // defaultPreviewContent: '<img src="/uploads/default_avatar_male.jpg" alt="Your Avatar">',
        layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });

  });
  function showHideBarcode(chnag,checkFor)
  {
	  if(checkFor==1)
	  {
		if(chnag=='scanbar')
			document.getElementById('BarcodeVisibilty').style.display="block";
		else
			document.getElementById('BarcodeVisibilty').style.display="none";
	  }
	  if(checkFor==2)
	  {
		if(chnag=='Single')
			document.getElementById('qunatitySM').style.display="none";
		else
			document.getElementById('qunatitySM').style.display="block";
	  }
  }
</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>