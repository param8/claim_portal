

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Edit Products

    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Products</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        <div class="box">
         
          <!-- /.box-header -->
          <form role="form" action="<?php base_url('users/update') ?>" method="post" enctype="multipart/form-data">
              <div class="box-body">

                <?php echo validation_errors(); ?>

               <!--  <div class="form-group">
                  <label>Image Preview: </label>
                  <img src="<?php echo base_url() . $product_data['image'] ?>" width="150" height="150" class="img-circle">
                </div>

               <div class="form-group">
                  <label for="product_image">Update Image</label>
                  <div class="kv-avatar">
                      <div class="file-loading">
                          <input id="product_image" name="product_image" type="file">
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="product_name">Scan Barcode</label>
                  <input type="text" class="form-control" id="barcode" name="barcode" placeholder="Scan Barcode" autocomplete="off" value="<?php //echo $product_data['bar_code']; ?>"/>
                </div>
-->       
                <div class="form-group">
                  <?php $category_data = json_decode($product_data['category_id']); ?>
                  <label for="category">Category</label>
                  <select class="form-control select_group" id="category" name="category" onChange="showHideExtraDetail(this.value)">
                      <option value="">Select</option>
                    <?php foreach ($category as $k => $v): ?>
                      <option value="<?php echo $v['id'] ?>" <?php if($v['id']==$product_data['category_id']) echo "selected";?>><?php echo $v['name'] ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <!--<div class="form-group" style="display:<?php if($product_data['product_id']!="") echo "block"; else echo "none";?> " id="extraDetail">
               		<div class="row">
                    	<div class="col-sm-2">
                              <label for="brands">RAM <font color="#FD0004">*</font></label>
                              <input type="text" class="form-control" id="RAM" name="RAM" autocomplete="off"  value="<?php echo $product_data['ram']; ?>" />
                  		</div>
                    	<div class="col-sm-2">
                              <label for="brands">HDD <font color="#FD0004">*</font></label>
                              <input type="text" class="form-control" id="HDD" name="HDD" autocomplete="off" value="<?php echo $product_data['hdd']; ?>" />
                  		</div>
                    	<div class="col-sm-2">
                              <label for="brands">Mother Board<font color="#FD0004">*</font></label>
                              <input type="text" class="form-control" id="mother_board" name="mother_board" autocomplete="off" value="<?php echo $product_data['mother_board']; ?>" />
                  		</div>
                    	<div class="col-sm-2">
                              <label for="brands">Screen</label>
                              <input type="text" class="form-control" id="screen" name="screen" autocomplete="off" value="<?php echo $product_data['display_screen']; ?>" />
                  		</div>
                    	<div class="col-sm-2">
                              <label for="brands">Power Supply</label>
                              <input type="text" class="form-control" id="power_supply" name="power_supply" autocomplete="off" value="<?php echo $product_data['power_supply']; ?>" />
                  		</div>
                    	<div class="col-sm-2">
                              <label for="brands">Other(s)</label>
                              <input type="text" class="form-control" id="Other" name="Other" autocomplete="off"  value="<?php echo $product_data['other']; ?>" />
                  		</div>
                  	</div>
                </div>--> 
                <div class="form-group">
                  <label for="category">Vendor</label>
                  <select class="form-control select_group" id="vendor" name="vendor">
                      <option value="">Select</option>
                    <?php foreach ($this->data['vendor'] as  $ven): ?>
                      <option value="<?php echo $ven['vendor_id'] ?>" <?php if($product_data['vendor_id'] == $ven['vendor_id']) { echo "selected='selected'"; } ?>><?php echo $ven['vendor_name'] ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                 <div class="form-group">
                  <label for="product_name">Product name</label>
                  <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Enter product name" value="<?php echo $product_data['name']; ?>"  autocomplete="off"/>
                </div>

                <!-- <div class="form-group"> -->
                  <!-- <label for="sku">SKU</label> -->
                  <!-- <input type="text" class="form-control" id="sku" name="sku" placeholder="Enter sku" value="<?php echo $product_data['sku']; ?>" autocomplete="off" /> -->
                <!-- </div> -->

                <div class="form-group">
                  <label for="price">Price</label>
                  <input type="text" class="form-control" id="price" name="price" placeholder="Enter price" value="<?php echo $product_data['price']; ?>" autocomplete="off" />
                </div>
				
                <div class="form-group" style="display:<?php if($product_data['product_entry']=='Single') echo "none"; else echo "block"; ?>">
                  <label for="qty">Qty</label>
                  <input type="text" class="form-control" id="qty" name="qty" placeholder="Enter Qty" value="<?php echo $product_data['qty']; ?>" autocomplete="off" />
                </div>
                
                <div class="form-group">
                  <label for="description">Serial No (Enter serial number with comma string.(If any more than one))</label>
                  <input type="text" class="form-control" id="serial_no" name="serial_no" placeholder="Enter Serial No" value="<?php echo $product_data['serial_no']; ?>" autocomplete="off">
                </div>
                
                <div class="form-group">
                  <label for="description">Description</label>
                  <textarea type="text" class="form-control" id="description" name="description" placeholder="Enter 
                  description" autocomplete="off"><?php echo $product_data['description']; ?></textarea>
                </div>

                <div class="form-group">
                  <label for="description">Other Remark</label>
                  <textarea type="text" class="form-control" id="otherRemark" name="otherRemark" placeholder="Enter Other Remark" autocomplete="off"><?php echo $product_data['other_remark']; ?></textarea>
                </div>

                <div class="form-group">
                  <label for="brands">Upload Bill</label>
                  <input type="file" class="form-control" id="bill_copy" name="bill_copy" />
                  <a href="<?php echo base_url() . $product_data['bill_copy'] ?>" target="_blank">View</a>
                </div>
                
                <div class="form-group">
                  <label for="brands">Purchase Date <font color="#FD0004">*</font></label>
                  <input type="date" class="form-control" id="purchase_date" value="<?php echo $product_data['date_of_purchase']; ?>" name="purchase_date" autocomplete="off" required />
                </div>

                <div class="form-group">
                  <label for="brands">Warranty Expiry Date</label>
                  <input type="date" class="form-control" value="<?php echo $product_data['expiry_date']; ?>" id="expiry_date" name="expiry_date" autocomplete="off" />
                </div>
                <div class="form-group">
                  <label for="store">Warehouse</label>
                  <select class="form-control select_group" id="store" name="store">
                    <?php foreach ($stores as $k => $v): ?>
                      <option value="<?php echo $v['id'] ?>" <?php if($product_data['store_id'] == $v['id']) { echo "selected='selected'"; } ?> ><?php echo $v['name'] ?></option>
                    <?php endforeach ?>
                  </select>
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save Changes</button>
                <a href="<?php echo base_url('Controller_Products/') ?>" class="btn btn-warning">Back</a>
              </div>
            </form>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
function showHideExtraDetail(catId)
{
	if(catId==1 || catId==2)	
	{
		document.getElementById('extraDetail').style.display="block";
	}
	else
	{
		document.getElementById('extraDetail').style.display="none";
	}
}
  $(document).ready(function() {
    $(".select_group").select2();
    $("#description").wysihtml5();

    $("#mainProductNav").addClass('active');
    $("#manageProductNav").addClass('active');
    
    var btnCust = '<button type="button" class="btn btn-secondary" title="Add picture tags" ' + 
        'onclick="alert(\'Call your custom code here.\')">' +
        '<i class="glyphicon glyphicon-tag"></i>' +
        '</button>'; 
    $("#product_image").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        // defaultPreviewContent: '<img src="/uploads/default_avatar_male.jpg" alt="Your Avatar">',
        layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });

  });
</script>