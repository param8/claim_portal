

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>var $j = jQuery.noConflict(true);</script>

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<?php
if(isset($_POST['tableHtml']) && isset($_POST['fileName']))
{
    $htmlContent=$_POST['tableHtml'];
    $fileName=$_POST['fileName'].'.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$fileName");
    echo $htmlContent; die;
}
?>
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <section class="content-header">

    <h1>

     Transfer Site Asset Report
    </h1>

    <ol class="breadcrumb">

      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>

      <li class="active">Assets</li>

    </ol>

  </section>



  <!-- Main content -->

  <section class="content">

    <!-- Small boxes (Stat box) -->

    <div class="row">

      <div class="col-md-12 col-xs-12">



        <div id="messages"></div>



        <?php if($this->session->flashdata('success')): ?>

          <div class="alert alert-success alert-dismissible" role="alert">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <?php echo $this->session->flashdata('success'); ?>

          </div>

        <?php elseif($this->session->flashdata('error')): ?>

          <div class="alert alert-error alert-dismissible" role="alert">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <?php echo $this->session->flashdata('error'); ?>

          </div>

        <?php endif; ?>
          <style>

		   .searchbtn{

    margin-top: 24px;

    padding: 5px 20px 5px;

    background: #252525;

    border: 1px solid #252525;

    color: white;

		 }

			.modal-dialog {

				width: 80% !important;

				margin: 30px auto !important;

				}		  

			.modal-dialogAction {

				width: 40% !important;

				margin: 30px auto !important;

				}		  

			</style>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
          <?php
		  $CI =& get_instance();
		  $storeData=$CI->getActiveStore();
		  //echo"<pre>";print_r($storeData);die;
		  ?>
            <form name="searchVal" action="<?php echo base_url('Controller_Products/TransferAssetReport');?>" method="post">
                <div class="row">
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                          <label for="store">Site</label>
                          <select class="form-control select_group" id="searchSite" name="searchSite" required>
                            <option value="">Select</option>
                            <?php
							 foreach($this->data['site_details'] as $val)
                              {
							?>
                                <option value="<?= $val['id'];?>" <?php if($this->data['store_id']==$val['id']) echo "selected";?>><?= $val['name'];?></option>
                            <?php
							}
							?>
                          </select>
                        </div>          <!-- /.box-header -->
                      </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                          <label for="store">From Date</label>
                          <input type="date" name="from_date" id="from_date" class="form-control" value="<?php if(isset($this->data['from_date'])) { echo $this->data['from_date']; } else echo date('Y-m-01');?>"/>
                        </div>          <!-- /.box-header -->
                      </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                          <label for="store">To Date</label>
                          <input type="date" name="to_date" id="to_date" class="form-control" value="<?php if(isset($this->data['to_date'])) { echo $this->data['to_date']; } else echo date('Y-m-t');?>" />
                        </div>          <!-- /.box-header -->
                      </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                          <button onClick="serchCategoryWise()" class="searchbtn">Search</button>
                        </div>          <!-- /.box-header -->
                      </div>
                 </div>
              </form>
               <br/>
            <table  class="table table-bordered table-striped" style="margin:0px;margin-bottom:5px" cellpadding="0" cellspacing="0">
              <tr>
                <td>
                      <?php
//                      if((array_key_exists('Assign_Stock_Report',$this->session->userdata['ButtonPermisssion']) && in_array('Excel',$this->session->userdata['ButtonPermisssion']['Assign_Stock_Report'])) || $permissionFlag=="Yes")
//                      {
                          ?>
                         <a href="javascript:void(0)" title="Download Excel" onclick="exportTableToExcel('manageTable', 'Transfer Report')">
                        <i class="fa fa-file-excel-o" style="font-size: 25px;" aria-hidden="true"></i>
                    </a>&nbsp;&nbsp;
                        <?php
//                        }
//                      if((array_key_exists('Assign_Stock_Report',$this->session->userdata['ButtonPermisssion']) && in_array('Print',$this->session->userdata['ButtonPermisssion']['Assign_Stock_Report'])) || $permissionFlag=="Yes")
//                      {
                        ?>
                        <a href="javascript:void(0)" title="Print" onclick="printDiv('manageTable')" >
                            <i class="fa fa-print" style="font-size: 25px;" aria-hidden="true"></i>
                       </a>
                       <?php
//                      }
                      ?>
                 </td>
              </tr>
            </table>
              <div id="manageTable">
                <table id="" class="table table-bordered table-striped">
    
                  <thead style="background-color:#3c8dbc; color:#ffffff">
    
                  <tr>
                    <th>S.No</th>
                    <th style="width:50%">Stock Name</th>
                    <th>Transfer Qty</th>
                    <th>Receive Qty</th>
                    <th style="width:20%">Receive date</th>
                  </tr>
    
                  </thead>
                    <?php
                    $sno=1;
                    if(count($this->data['store_wise_products'])>0)
                    {
                        foreach($this->data['store_wise_products'] as $key=>$value)
                        {
                            list($fromName,$toName)=explode("_",$key);
                            ?>
                              <thead style="background-color:#D2D2D2; color:#000000">
                              <tr>
                                <th colspan="7">Transfer From :&nbsp;<?= $fromName;?>&nbsp;&nbsp;&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;&nbsp;To :&nbsp;<?= $toName;?></th>
                              </tr>
                              </thead>
                              <?php
                                foreach($value as $val)
                                {
                  //  print_r($val);
                                ?>
                                  <tr>
                                    <td><?= $sno++ ;?></td>
                                    <td style="width: 500px;"><?= $val['name'] ;?></td>
                    <td><?php if( $val['received_qty']!=0){echo $val['received_qty'] ;}else{echo $val['qty'] ;}?></td>
                                    <td><?= $val['qty'] ;?></td>
                                    <td><?= $val['entry_date'] ;?></td>
                                  </tr>
                              <?php
                                }
                                ?>
                            <?php
                        }
                    }
                    else
                    {
                    ?>
                        <tr>
                        <td colspan="7" style="text-align:left"><font color="#FF0000"><strong>No data found.</strong></font></td>
                        </tr>
                    <?php				
                    }
                    ?>
                </table>
               </div>
          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- col-md-12 -->

    </div>

    <!-- /.row -->
    <form role="form" method="post" id="exceldownload">
      <input type="hidden" name="tableHtml" id="tableHtml">
      <input type="hidden" name="fileName" id="fileName">
  </form>

  </section>

  <!-- /.content -->

</div>

<!-- /.content-wrapper -->

<script>
	function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}
function exportTableToExcel(divName,filename) {
   var tableHtml=document.getElementById(divName).innerHTML; 
   document.getElementById("tableHtml").value=tableHtml;
   document.getElementById("fileName").value=filename;
   document.getElementById('exceldownload').submit();
}
	</script>
<!-- remove brand modal -->
