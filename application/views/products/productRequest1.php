

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>var $j = jQuery.noConflict(true);</script>

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">



<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <section class="content-header">

    <h1>

      Asset Notification
    </h1>

    <ol class="breadcrumb">

      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>

      <li class="active">Assets</li>

    </ol>

  </section>



  <!-- Main content -->

  <section class="content">

    <!-- Small boxes (Stat box) -->

    <div class="row">

      <div class="col-md-12 col-xs-12">



        <div id="messages"></div>



        <?php if($this->session->flashdata('success')): ?>

          <div class="alert alert-success alert-dismissible" role="alert">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <?php echo $this->session->flashdata('success'); ?>

          </div>

        <?php elseif($this->session->flashdata('error')): ?>

          <div class="alert alert-error alert-dismissible" role="alert">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <?php echo $this->session->flashdata('error'); ?>

          </div>

        <?php endif; ?>
        <div class="box">
          <!-- /.box-header -->

          <div class="box-body">
			<form method="post" action="<?= base_url('Controller_Products/AcceptRequest');?>" >
            <table id="manageTable" class="table table-bordered table-striped">

              <thead style="background-color:#3c8dbc; color:#ffffff">

              <tr>
                <th>S.No</th>
                <th>Product</th>
                <th>Model No</th>
                <th>Quantity</th>
                <th>Accept (<input type="checkbox" name="selectAll" onclick="CheckAllProduct(0)" id="selectAll" style="width:20px"/>)</th>
              </tr>

              </thead>
				<?php
				$sno=1;
				if(count($this->data['store_wise_products'])>0)
				{
					foreach($this->data['store_wise_products'] as $key=>$value)
					{
						list($stId,$stName)=explode("_",$key);
						?>
						  <thead style="background-color:#D2D2D2; color:#000000">
						  <tr>
							<input type="hidden" name="transferTo" value="<?= $stId ;?>" />
							<th colspan="5"><?= $stName;?></th>
						  </tr>
						  </thead>
						  <?php
							foreach($value as $val)
							{
							?>
							  <tr>
								<td><?= $sno++ ;?></td>
								<td><?= $val['name'] ;?></td>
								<td><?= $val['serial_no'] ;?></td>
								<td><?= $val['qty'] ;?></td>
								<td><input type="checkbox" name="checkProduct<?= $val['id'] ;?>" id="checkProduct<?= $val['id'] ;?>" style="width:20px"/><input type="hidden" name="checkProductsArr[]" value="<?= $val['id'] ;?>" /></td>
							  </tr>
						  <?php
							}
							?>
						<?php
					}
				}
				else
				{
				?>
                    <tr>
                    <td colspan="5" style="text-align:left"><font color="#FF0000"><strong>No data found.</strong></font></td>
                    </tr>
                <?php				
				}
				?>
                <tr>
                	<td colspan="5" style="text-align:right">
                          <button type="submit" class="btn btn-primary" onClick="return CheckAllProduct(1);">Save</button>
                    </td>
                </tr>
            </table>
			</form>
          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- col-md-12 -->

    </div>

    <!-- /.row -->

    



  </section>

  <!-- /.content -->

</div>

<!-- /.content-wrapper -->



<!-- remove brand modal -->


<script type="text/javascript">

var manageTable;

var base_url = "<?php echo base_url(); ?>";
// remove functions 
function CheckAllProduct(chek)
{
	var proIdsArr=document.getElementsByName('checkProductsArr[]');	
	var flag=0;
	for(var i=0;i<proIdsArr.length;i++){
		var pId=proIdsArr[i].value;
		if(chek==0)
		{
			if(document.getElementById('selectAll').checked==true)
			{
				document.getElementById('checkProduct'+pId).checked=true;
			}
			else
			{
				document.getElementById('checkProduct'+pId).checked=false;
			}
		}
		else
		{
			if(document.getElementById('checkProduct'+pId).checked==true)
				var flag=1;
		}
	}
	if(chek==1)
	{
		if(flag==0)
		{
			alert('Please select at least one checkbox to complete the Process.');
			return false;	
		}
	}
}
</script>

<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>