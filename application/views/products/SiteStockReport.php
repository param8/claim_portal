<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>var $j = jQuery.noConflict(true);</script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">

<?php
if(isset($_POST['tableHtml']) && isset($_POST['fileName']))
{
    $htmlContent=$_POST['tableHtml'];
    $fileName=$_POST['fileName'].'.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$fileName");
    echo $htmlContent; die;
}
?>

<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <section class="content-header">

    <h1>

      Site Asset Report
    </h1>

    <ol class="breadcrumb">

      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>

      <li class="active">Assets</li>

    </ol>

  </section>



  <!-- Main content -->

  <section class="content">

    <!-- Small boxes (Stat box) -->

    <div class="row">

      <div class="col-md-12 col-xs-12">



        <div id="messages"></div>



        <?php if($this->session->flashdata('success')): ?>

          <div class="alert alert-success alert-dismissible" role="alert">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <?php echo $this->session->flashdata('success'); ?>

          </div>

        <?php elseif($this->session->flashdata('error')): ?>

          <div class="alert alert-error alert-dismissible" role="alert">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <?php echo $this->session->flashdata('error'); ?>

          </div>

        <?php endif;?>
          <style>

		   .searchbtn{

    margin-top: 24px;

    padding: 5px 20px 5px;

    background: #252525;

    border: 1px solid #252525;

    color: white;

		 }

			.modal-dialog {

				width: 80% !important;

				margin: 30px auto !important;

				}		  

			.modal-dialogAction {

				width: 40% !important;

				margin: 30px auto !important;

				}		  

			</style>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <form name="searchVal" action="<?php echo base_url('Controller_Products/SiteStockReport');?>" method="post">
                <div class="row">
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                          <label for="store">Site</label>
                          <select class="form-control select_group" id="searchSite" name="searchSite" required>
                            <option value="">Select</option>
                            <?php
                            $toWareHouse="";
                              foreach($this->data['site_details'] as $val)
                              {
                              ?>
                                <option value="<?= $val['id'];?>" <?php if($this->data['store_id']==$val['id']) { echo "selected"; $toWareHouse=$val['name']; } ?>><?= $val['name'];?></option>
                            <?php
                                }
                                ?>
                          </select>
                        </div>          <!-- /.box-header -->
                      </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                          <label for="store">From Date</label>
                          <input type="date" name="from_date" id="from_date" class="form-control" value="<?php if(isset($this->data['from_date'])) { echo $this->data['from_date']; } else echo date('Y-m-01');?>"/>
                        </div>          <!-- /.box-header -->
                      </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                          <label for="store">To Date</label>
                          <input type="date" name="to_date" id="to_date" class="form-control" value="<?php if(isset($this->data['to_date'])) { echo $this->data['to_date']; } else echo date('Y-m-t');?>" />
                        </div>          <!-- /.box-header -->
                      </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                          <button onClick="serchCategoryWise()" class="searchbtn">Search</button>
                        </div>          <!-- /.box-header -->
                      </div>
                 </div>
              </form>
               <br/>
               <?php
          // print_r($_SESSION);
                              $permissionFlag="No";
                              if(in_array('All',$this->session->userdata['ButtonPermisssion']))
                                $permissionFlag="Yes";
                                
                              
                              if((array_key_exists('Assign_Stock_Report',$this->session->userdata['ButtonPermisssion'])) || $permissionFlag=="Yes")
                              {
                                  ?>
                                <table  class="table table-bordered table-striped" style="margin:0px;margin-bottom:5px" cellpadding="0" cellspacing="0">
                                  <tr>
									<td>
                                          <?php
                                          if((array_key_exists('Assign_Stock_Report',$this->session->userdata['ButtonPermisssion']) && in_array('Excel',$this->session->userdata['ButtonPermisssion']['Assign_Stock_Report'])) || $permissionFlag=="Yes")
                                          {
                                              ?>
									         <a href="javascript:void(0)" title="Download Excel" onclick="exportTableToExcel('manageTable', 'Assignstockreport')">
									        <i class="fa fa-file-excel-o" style="font-size: 25px;" aria-hidden="true"></i>
									    </a>&nbsp;&nbsp;
									        <?php
									        }
                                          if((array_key_exists('Assign_Stock_Report',$this->session->userdata['ButtonPermisssion']) && in_array('Print',$this->session->userdata['ButtonPermisssion']['Assign_Stock_Report'])) || $permissionFlag=="Yes")
                                          {
									        ?>
									        <a href="javascript:void(0)" title="Print" onclick="printDiv('manageTable')" >
									            <i class="fa fa-print" style="font-size: 25px;" aria-hidden="true"></i>
									       </a>
									       <?php
                                          }
                                          ?>
									 </td>
								  </tr>
								</table>
							<?php
                              }
                              ?>
          <div id="manageTable">
            <table id="manageTable" class="table table-bordered table-striped">

              <thead style="background-color:#3c8dbc; color:#ffffff">

              <tr>
                <th>S.No</th>
                <th style="width:50%">Stock Name</th>
                <th> Qty</th>
                <th style="width:20%"> Date</th>
               
              </tr>

              </thead>
				 
				<?php
				$sno=1;
				if(count($this->data['product_details'])>0)
				{
					foreach($this->data['product_details'] as $value)
					{
						?>
							  <tr>
								<td><?= $sno++ ;?></td>
								<td><?= $value['name'] ;?></td>
								<td><?= $value['qty'] ;?></td>
                                <td><?= $value['entry_date'] ;?></td>
							  </tr>
					<?php
					}
				}
				else
				{
				?>
                    <tr>
                    <td colspan="7" style="text-align:left"><font color="#FF0000"><strong>No data found.</strong></font></td>
                    </tr>
                <?php				
				}
				?>
            </table>
          </div>
          </div>

          <!-- /.box-body -->

        </div>

        <!-- /.box -->

      </div>

      <!-- col-md-12 -->

    </div>

    <!-- /.row -->

    

    <form role="form" method="post" id="exceldownload">
      <input type="hidden" name="tableHtml" id="tableHtml">
      <input type="hidden" name="fileName" id="fileName">
  </form>

  </section>

  <!-- /.content -->

</div>

<!-- /.content-wrapper -->



<!-- remove brand modal -->
<script>
	function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}
function exportTableToExcel(divName,filename) {
   var tableHtml=document.getElementById(divName).innerHTML; 
   document.getElementById("tableHtml").value=tableHtml;
   document.getElementById("fileName").value=filename;
   document.getElementById('exceldownload').submit();
}
	</script>
