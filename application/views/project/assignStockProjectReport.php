 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<?php
if(isset($_POST['tableHtml']) && isset($_POST['fileName']))
{
    $htmlContent=$_POST['tableHtml'];
    $fileName=$_POST['fileName'].'.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$fileName");
    echo $htmlContent; die;
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Assign Stock Report       
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Report</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
          <!-- <form action="<?php echo base_url('Controller_Project/assignStockReport');?>" method="post">
          <div class="row">
          	<div class="col-md-2">
                <strong>Warehouse :</strong>
            </div>
          	<div class="col-md-6">
            <?php
			$CI     = & get_instance();
			$result = $CI->getActiveStore();
			?>
            <select name="warehouse" id="warehouse" class="form-control" required>
                <option value="">Select</option>
                 <?php 
                    foreach($result as $stock)	
                    {
                        ?>
                            <option value="<?php echo $stock['id']?>"><?php echo $stock['name']?></option>
                        <?php
                    }
                 ?>
            </select>
            </div>
          	<div class="col-md-4">
                <button class="btn btn-primary" >Save</button>
            </div>
          </div>
          </form> -->
          <br>
          <?php
          // print_r($_SESSION);
                              $permissionFlag="No";
                              if(in_array('All',$this->session->userdata['ButtonPermisssion']))
                                $permissionFlag="Yes";
                                
                              
                              if((array_key_exists('Assign_Stock_Report',$this->session->userdata['ButtonPermisssion'])) || $permissionFlag=="Yes")
                              {
                                  ?>
                                <table  class="table table-bordered table-striped" style="margin:0px;margin-bottom:5px" cellpadding="0" cellspacing="0">
                                  <tr>
									<td>
                                          <?php
                                          if((array_key_exists('Assign_Stock_Report',$this->session->userdata['ButtonPermisssion']) && in_array('Excel',$this->session->userdata['ButtonPermisssion']['Assign_Stock_Report'])) || $permissionFlag=="Yes")
                                          {
                                              ?>
									         <a href="javascript:void(0)" title="Download Excel" onclick="exportTableToExcel('manageTable', 'Assignstockreport')">
									        <i class="fa fa-file-excel-o" style="font-size: 25px;" aria-hidden="true"></i>
									    </a>&nbsp;&nbsp;
									        <?php
									        }
                                          if((array_key_exists('Assign_Stock_Report',$this->session->userdata['ButtonPermisssion']) && in_array('Print',$this->session->userdata['ButtonPermisssion']['Assign_Stock_Report'])) || $permissionFlag=="Yes")
                                          {
									        ?>
									        <a href="javascript:void(0)" title="Print" onclick="printDiv('manageTable')" >
									            <i class="fa fa-print" style="font-size: 25px;" aria-hidden="true"></i>
									       </a>
									       <?php
                                          }
                                          ?>
									 </td>
								  </tr>
								</table>
							<?php
                              }
                              ?>
         
          <!-- <button onclick="printDiv('print')"  class="btn btn-primary btn-sm"> <i class="fa fa-print" aria-hidden="true">&nbsp;Print</i></button> 
               <div id="print"> -->
               <div  id="manageTable">
                <table id="manageTable" class="table table-bordered table-striped">
                        <?php
                        if(count($this->data['array']) > 0)
                        {
                          foreach($this->data['array'] as $key=>$vDetails)	
                          {
                          ?>
                          <tr style="background-color:#3c8dbc; color:#FFFFFF">
                            <th style="width:20%" colspan="6"><?php echo $key?></th>
                           </tr>
						              <?php
                            foreach($vDetails as $projectDet=>$detail)	
                            {
							            	$explodeArray=explode("@_@",$projectDet);
                            ?>
                              <tr style="background-color:#CDCDCD; color:#000000">
                                <td colspan="1"><strong>Project -> <?php echo $explodeArray[0]?></strong></td>
                                <td colspan="2"><strong>Coordinator -> <?php echo $explodeArray[1]?></strong></td>
                                <td colspan="1"><strong>Mobile -> <?php echo $explodeArray[2]?></strong></td>
                               </tr>
                              <tr style="background-color:#ECE7E7; color:#000000">
                                <td><strong>Product</strong></td>
                                
                                <td><strong>Category/Unit</strong></td>
                                <td><strong>Quantity</strong></td>
                                <td><strong>Status</strong></td>
                                
                               </tr>
                                <?php
                                  foreach($detail as $val)	
                                  {
                                    ?>
                                      <tr>
                                        <td><?php echo $val[0]?></td>
                                       
                                        <td><?php echo $val[3]?></td>
                                        <td><?php echo $val[2]?></td>
                                        <td><?php echo $val[5]?></td>
                                       </tr>
                                    <?php
                                            }
                                          }
                                      }
                                    }
                                    else
                                    {
                                      ?>
                                  <tr style="border:2px solid #FF0606">
                                    <td style="color:red"><strong>No record found.</strong></td>
                                  </tr>
                              <?php
                                  }
                                  ?>
                         </table>
                      </div>
                 
            <div class="modal fade" tabindex="-1" role="dialog" id="removeModal">
            <div class="modal-dialogAction modal-sm" role="document" style="width: 30%; margin: 30px auto !important;">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Return Product</h4>
            </div>
            
            <form role="form" action="<?php echo base_url('Controller_Project/ReturnProduct') ?>" method="post" id="removeForm">
            <input type="hidden" name="asProjId" id="asProjId" >
            <input type="hidden" name="aspId" id="aspId" >
            
            
            <div class="modal-body">
            <div class="box-body">
            <div class="row">
         <!--   <div class="col-md-12">
                <div class="form-group">
                <label for="fname">Return By <font color="#FF0004">*</font></label>
                <select class="form-control" id="return_by" name="return_by" required>
                <option value="">Select</option>
                </select>
                </div>
            </div>-->
            <div class="col-md-12">
                <div class="form-group">
                <label for="fname">Return to Warehouse <font color="#FF0004">*</font></label>
                <select class="form-control" id="to_warehouse" name="to_warehouse" required>
                <option value="">Select</option>
                </select>
                </div>
            </div>
            </div>
            </div>
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-success">Return</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </form>
            
            
            </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <form role="form" method="post" id="exceldownload">
      <input type="hidden" name="tableHtml" id="tableHtml">
      <input type="hidden" name="fileName" id="fileName">
  </form>
    <!-- /.row -->
  </section>
  
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script>
function refreshPage(Val)
{
	if(Val==1)
	{
		if(document.getElementById('Project_name').value!="" && document.getElementById('active').value!="")
			location.reload();
		else if(document.getElementById('edit_Project_name').value!="" && document.getElementById('edit_unit').value!="")
			location.reload();
		else
			return false;
	}
	else
	{
		location.reload();
	}
}

$("#hidePopUp").click(function(){
	//alert('Hii deep');
	$("#editModal").modal('hide');
});
</script>

<script type="text/javascript">
var manageTable;

$(document).ready(function() {
  // submit the create from 
});

// edit function
function showProductDetails(id,projeId)
{ 
  $.ajax({
    url: 'fetchProductDataByWhouseId/'+id+'/'+projeId,
    type: 'post',
    dataType: 'json',
    success:function(response) {
		document.getElementById('displayStock_'+projeId).innerHTML=response;
    }
  });
}
// remove functions 
function removeFunc(id,pId)
{
  document.getElementById('delete_project_id').value=id;
}
function removeFunc(id,pId)
{
  if(id) {
		$.ajax({
			url: '<?php echo base_url('Controller_Products/GetAssignProducts'); ?>',
			type: 'POST',
			data: {
				pId: id
			},
			dataType: 'json',
			success: function(data) {
				document.getElementById("asProjId").value=id;
				document.getElementById("aspId").value=pId
//				$('select[name="return_by"]').empty();
//				$.each(data[0], function(key, value) {
//				$('select[name="return_by"]').append('<option value="'+ value.proId +'">'+ value.aTo +'</option>');
//				})
				$('select[name="to_warehouse"]').empty();
				$('select[name="to_warehouse"]').append('<option value="">Select</option>');
				$.each(data[1], function(key, value) {
				$('select[name="to_warehouse"]').append('<option value="'+ value.id +'">'+ value.name +'</option>');
				})
			}
		});  
	}
}
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
function checkQuantity(enterVal,ProjectId,pid,qty)
{
	if((Number(enterVal))==0)
	{
		document.getElementById('totalVal_'+ProjectId+'_'+pid).value="";
		return false;
	}
	if(Number(enterVal) > Number(qty))
	{
		alert("Enter value can't more than quantity.");
		document.getElementById('totalVal_'+ProjectId+'_'+pid).value="";
		document.getElementById('qunty_'+ProjectId+'_'+pid).value=(Number(document.getElementById('hiddenQunty_'+ProjectId+'_'+pid).value));
		return false;	
	}
	else
	{
		document.getElementById('qunty_'+ProjectId+'_'+pid).value=	(Number(document.getElementById('hiddenQunty_'+ProjectId+'_'+pid).value)-Number(enterVal));
	}
}
</script>
<script>
	function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}
function exportTableToExcel(divName,filename) {
   var tableHtml=document.getElementById(divName).innerHTML; 
   document.getElementById("tableHtml").value=tableHtml;
   document.getElementById("fileName").value=filename;
   document.getElementById('exceldownload').submit();
}
	</script>

