 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Assign Stock to Project
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Project</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php  echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
        <div class="box">
          <style>
			.modal-lg {
				width: 900px; !important;
				margin: 50px auto;!important;
			}
		.modal-sm {
				width: 400px; !important;
				margin: 50px auto;!important;
			}
			.badge {
			  position: absolute;
			  top: 0px;
			  right: 1spx;
			  padding: 3px 6px;
			  border-radius: 50%;
			  background-color: red;
			  color: white;
			}		 
           </style>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr style="background-color:#3c8dbc; color:#FFFFFF">
                <th>Project Name&nbsp;&nbsp;===>&nbsp;&nbsp;<?php if(count($this->data['projectStock'])>0) echo $this->data['projectStock'][0]['name']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Project Coordinator&nbsp;&nbsp;===>&nbsp;&nbsp;<?php if(count($this->data['projectStock'])>0) echo $this->data['projectStock'][0]['coordinator'];?></th>
              </tr>
              <tr>
              	<td>
                    <div class="container" style="width:100%; padding-left: 0px;">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="<?php if($this->session->flashdata('type')=='') echo "active"; ?>">
                              <a href="#home" role="tab" data-toggle="tab">
                                  <icon class="fa fa-home"></icon> Instock
                              </a>
                          </li>
                          <li><a href="#profile" role="tab" data-toggle="<?php if($this->session->userdata('user_type')=='9') echo "tab";?>">
                              <i class="fa fa-rupee"></i> Payment
                              </a>
                          </li>
                          <li class="<?php if($this->session->flashdata('type')!='') echo "active"; ?>">
                              <a href="#settings" role="tab" data-toggle="<?php if($this->session->userdata('user_type')=='9') echo "tab";?>">
                                  <i class="fa fa-cog"></i> DPR
                              </a>
                          </li>
                          <li>
                              <a href="#reject" role="tab" data-toggle="<?php if($this->session->userdata('user_type')=='9') echo "tab";?>">
                                  <i class="fa fa-ban"></i> Reject DPR <span class="badge"><?php if(count($this->data['rejectDpr'])>0) echo count($this->data['rejectDpr']); else echo "0";?></span>
                              </a>
                          </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div class="tab-pane fade <?php if($this->session->flashdata('type')=='') echo "active in"; ?>" id="home">
                            <form role="form" action="<?php echo base_url('Controller_Project/assignTo') ?>" method="post" id="assignStock">
                              <div class="row">
                              	<div class="col-sm-1"><strong>Site</strong></div>
                              	<div class="col-sm-3">
                                	<select name="site_stock" id="site_stock" class="form-control" required style="width:350px" onChange="return submitAssignStockForm(this.value,'assignStock')">
                                    <option value="">--SELECT SITE--</option>
                                    <?php
									$CI=& get_instance();
									$siteDetails=$CI->getSiteDetails();
									$sites['id']="";
									foreach($siteDetails as $sites)
									{
										?>
                                    	<option value="<?php echo $sites['id'];?>" <?php if(isset($this->data['site_stock']) && $this->data['site_stock']==$sites['id']) echo "selected";?>><?php echo $sites['name'];?></option>
                                     <?php
									}
									?>
                                    </select>
                                </div>
                              </div>
                              <br>
                            </form>
                            <form role="form" action="<?php echo base_url('Controller_Project/AssignToUser') ?>" method="post" id="assignStock">
                                <table id="manageTable" class="table table-bordered table-striped">
                                    <input type="hidden" name="assignForVl" id="assignForVl"  value="user" >
                                    <input type="hidden" name="projectId" id="projectId" value="<?php if(isset($this->data['site_stock'])) echo $this->data['site_stock'];?>">
                                    <?php
                                    if(count($this->data['projectStock']) > 0 && isset($this->data['site_stock']))
                                    {
										?>
										  <tr style="background-color:#ECE7E7; color:#000000">
											<td style="width:25%"><strong>Item</strong></td>
											<td style="width:20%"><strong>Unit</strong></td>
											<td style="width:20%"><strong>Available (Qty)</strong></td>
											<td style="text-align:center; width:10%"><strong>Issued&nbsp;(Qty)</strong></td>
											<td style="text-align:center; width:25%"><strong>Issued (To)</strong></td>
										   </tr>
										<?php
											foreach($this->data['projectStock'] as $val)	
											{
												?>
                                                <input type="hidden" name="productIds[]" id="productId" value="<?php echo $val['product_id'];?>">
                                                <input type="hidden" name="pQty_<?php echo $val['product_id'];?>" id="pQty_<?php echo $val['product_id'];?>" value="<?php echo $val['project_qty'];?>">
												  <tr>
													<td><?php echo $val['item']?></td>
													<td><?php echo $val['unit']?></td>
													<td><?php echo $val['project_qty']?></td>
													<td><input type="text" class="form-control" id="plQty_<?php echo $val['product_id'];?>" name="plQty_<?php echo $val['product_id'];?>" placeholder="" autocomplete="off" onkeypress="return isNumberKey(event,this)" onKeyUp="CheckQuantity(this.value,'<?php echo $val['product_id'];?>')"></td>
													<td><input type="text" class="form-control" id="assignTo_<?php echo $val['product_id'];?>" name="assignTo_<?php echo $val['product_id'];?>" placeholder="" autocomplete="off"></td>
                                                    	<?php
														if($val['project_qty'] >0)
														{
														?>
<!--                                                        <button type="button" class="btn btn-info btn-sm" onclick="assignToUser(<?php //echo $val['product_id'];?>,<?php //echo $val['project_id'];?>,<?php //echo $val['project_qty'];?>)" data-toggle="modal" data-target="#assignModal"><i class="fa fa-share-square-o"></i> <strong>Assign</strong></button>
-->                                                        <?php
														}
														?>
<!--                                                       <button type="button" class="btn btn-info btn-sm" onclick="ReturnProductFunc(<?php //echo $val['project_id'];?>,<?php //echo $val['product_id'];?>)" data-toggle="modal" data-target="#removeModal"><i class="glyphicon glyphicon-fast-backward"></i> <strong>Return</strong></button>
-->                                                    
												   </tr>
												<?php
											}
											?>
                                               <tr>
                                                    <td colspan="6">
                                                      <div class="box-footer" style="text-align:right">
                                                        <button type="submit" class="btn btn-primary" onClick="return confirmMessage()">Save</button>
                                                      </div>
                                                    </td>
                                               </tr>
                                            <?php
                                    }
                                    else
                                    {
                                        ?>
                                              <tr style="border:2px solid #FF0606">
                                                <td style="color:red"><strong>No record found.</strong></td>
                                               </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </form>
                          </div>
                          <div class="tab-pane fade" id="profile">
                              <h4>Payment Goes Here</h4>
                            <form role="form" action="<?php echo base_url('Controller_Project/PaymentToUser') ?>" method="post" id="assignForm">
                            <input type="hidden" name="assignForVl" id="assignForVl"  value="user" >
                            <input type="hidden" name="projectId" id="projectId"  value="<?php if(count($this->data['array'])>0) echo $this->data['array'][0]['project_id'];?>" >
                                <table id="manageTable" class="table table-bordered table-striped">
                                    <?php
                                    if(count($this->data['array'])>0)
                                    {
                                        $CI=& get_instance();
    									$totalPayAmt=$CI->GetUsedPayment($this->data['array'][0]['project_id']);
                                    }
									//echo"<pre>";print_r($this->session->flashdata('type'));die;
                                    if(count($this->data['projectStock']) > 0)
                                    {
										?>
                                            <input type="hidden" name="remainingBal" id="remainingBal" value="<?php echo ($this->data['array'][0]['project_cost']-$totalPayAmt[0]['USEDPAYMENT']);?>" />
										  <tr style="background-color:#ECE7E7; color:#000000">
<!--											<td style="width:15%"><strong>Total&nbsp;Payment</strong></td>
											<td style="width:15%"><strong>In-Used</strong></td>-->
											<td style="width:30%"><strong>Cash in hand
</strong></td>
											<td style="text-align:center; width:30%"><strong>Assigned To</strong></td>
											<td style="text-align:center; width:10%"><strong>Amount</strong></td>
											<td style="text-align:center; width:30%"><strong>Reason</strong></td>
										  </tr>
										  <tr>
<!--											<td><strong><?php //echo number_format($this->data['array'][0]['project_cost'],2);?></strong></td>
											<td><strong><a href="javascript:void(0)" style="color:green" data-toggle="modal" data-target="#assignPayment" onClick="return GelAllPaymentData('<?php //echo $this->data['array'][0]['project_id'];?>')"><?php //echo number_format(($totalPayAmt[0]['USEDPAYMENT']),2);?></a></strong></td>
-->											<td><strong><a href="javascript:void(0)" style="color:green" data-toggle="modal" data-target="#assignPayment" onClick="return GelAllPaymentData('<?php echo $this->data['array'][0]['project_id'];?>')"><?php echo number_format(($this->data['array'][0]['project_cost']-$totalPayAmt[0]['USEDPAYMENT']),2);?></a></strong></td>
                                            <td>
                                                  <input type="text" class="form-control" id="assignTo" name="assignTo" placeholder="Assign To" autocomplete="off" required>
                                             </td>
                                              <td>
                                                  <input type="text" class="form-control" onKeyUp="return CheckAmt(this.value)" id="paym" name="paym" placeholder="Amount" maxlength="10" autocomplete="off" required>
                                                </td>
                                                 <td>
                                                  <input type="text" class="form-control" id="remark" name="remark" placeholder="Remark" autocomplete="off">
                                            </td>
										  </tr>
                                          <tr>
                                          	<td colspan="4">
                                              <div class="box-footer" style="text-align:right">
                                                <button type="submit" class="btn btn-primary" onClick="return checkQuantityOnSave()">Save</button>
                                              </div>
                                            </td>
                                          </tr>
										<?php
                                    }
                                    else
                                    {
                                        ?>
                                              <tr style="border:2px solid #FF0606">
                                                <td style="color:red"><strong>No record found.</strong></td>
                                               </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                                </form>
                          </div>
                          
                          <div class="tab-pane fade <?php if($this->session->flashdata('type')!='') echo "active in"; ?>" id="settings">
                              <h4>CASTING DETAILS</h4>
                            <form role="form" action="<?php echo base_url('Controller_Project/insertDPR') ?>" method="post" id="submitValue">
                              <div class="row">
                              	<div class="col-sm-1"><strong>Site</strong></div>
                              	<div class="col-sm-3">
                                	<select name="site" id="site" class="form-control" required>
                                    <?php
									$CI=& get_instance();
									$siteDetails=$CI->getSiteDetails();
									$sites['id']="";
									foreach($siteDetails as $sites)
									{
										?>
                                    	<option value="<?php echo $sites['id'];?>" <?php if($this->session->flashdata('site')==$sites['id']) echo "selected";?>><?php echo $sites['name'];?></option>
                                     <?php
									}
									?>
                                    </select>
                                </div>
                              	<div class="col-sm-1"><strong>Type</strong></div>
                              	<div class="col-sm-3">
                                	<select name="type" id="type" class="form-control" onChange="changeHeaderPart(this.value)">
                                    	<option value="casting" <?php if($this->session->flashdata('type')=='casting') echo "selected";?>>Casting</option>
                                    	<option value="erection" <?php if($this->session->flashdata('type')=='erection') echo "selected";?>>Erection</option>
                                    	<option value="shiffting" <?php if($this->session->flashdata('type')=='shiffting') echo "selected";?>>Shiffting</option>
                                    </select>
                                </div>
                              	<div class="col-sm-1"><strong>Date</strong></div>
                              	<div class="col-sm-3">
                                	<input type="text" readonly name="from_date" id="from_date" class="form-control" value="<?php if($this->session->flashdata('dpr_date')!="") echo $this->session->flashdata('dpr_date'); else echo date('d-m-Y', strtotime(' -1 day')) ?>" />
                                </div>
                              </div>
                              <br>
                                <table id="manageTable" class="table table-bordered" cellpadding="0" cellspacing="0" style="overflow:scroll">
										  <tr style="background-color:#ECE7E7; color:#000000">
<!--											<td rowspan="3" style="width:1%"><strong>Sl.No.</strong></td>
-->											<td rowspan="3" style="text-align:center; width:10%"><strong>Panel&nbsp;Type</strong></td>
											<td rowspan="3" style="text-align:center; width:10%"><strong>BOQ</strong></td>
										  </tr>
										  <tr style="background-color:#ECE7E7; color:#000000">
											<td style="text-align:center; width:10%"><strong>Unit Area</strong></td>
											<td style="text-align:center; width:10%" colspan="2"><strong>In this month</strong></td>
											<!--<td style="text-align:center; width:10%" colspan="2"><strong><span id="last_month"><?php if($this->session->flashdata('type')=="") echo "Casting"; else echo ucfirst($this->session->flashdata('type')); ?>  till last Month</span></strong></td>-->
											<td style="text-align:center; width:20%" colspan="2"><strong><span id="cummulative"><?php if($this->session->flashdata('type')=="") echo "Casting"; else echo ucfirst($this->session->flashdata('type')); ?> Cummulative</span></strong></td>
											<td rowspan="2" style="text-align:center; width:15%"><strong>Remark</strong></td>
                                          </tr>
										  <tr style="background-color:#ECE7E7; color:#000000">
											<td style="text-align:center; width:10%"><strong>SQM</strong></td>
											<td style="text-align:center; width:10%"><strong>Nos</strong></td>
											<td style="text-align:center; width:10%"><strong>Area</strong></td>
											<!--<td style="text-align:center; width:10%"><strong>Nos</strong></td>
											<td style="text-align:center; width:10%"><strong>Area</strong></td>-->
											<td style="text-align:center; width:10%"><strong>Nos</strong></td>
											<td style="text-align:center; width:10%"><strong>Area</strong></td>
                                          </tr>
                                          <?php
										  $ci=& get_instance();
										  //echo"<pre>";print_r($sites['id']);die;
										  $panelDetail= $ci->getAllocatePanelSiteWise($sites['id']);
										  $slNo=1;
										  $date="";
										  if($this->session->flashdata('dpr_date')!="")
										   	$date=$this->session->flashdata('dpr_date');
										  foreach($panelDetail as $panel)
										  {
											  $dprDetails= $ci->getDailyProgressDtails($this->session->flashdata('type'),$panel['panel_id'],"Received",$date,$sites['id']);
											  $tillLastMonth= $ci->getDailyProgressDtails($this->session->flashdata('type'),$panel['panel_id'],"",$date,$sites['id']);
											  //echo"<pre>";print_r($this->session->flashdata('site'));
											  if(count($dprDetails)>0)
											  	$flag="Exists";
										  ?>
                                                <input type="hidden" name="panelIds[]" id="panelIds" value="<?php echo $panel['panel_id'];?>">
                                              <tr>
<!--                                                <td><?php //echo $slNo++;?></td>
-->                                                <td><?php echo $panel['panel_name'];?></td>
                                                <td><input type="text" name="bl_<?php echo $panel['panel_id'];?>" id="bl_<?php echo $panel['panel_id'];?>" class="form-control" style="width:80px; border:0px;" value="<?php if(count($dprDetails) >0) echo $dprDetails[0]['bl']; else echo $panel['b_l'];?>" /></td>
                                                <td><input type="text" name="ua_sqm_<?php echo $panel['panel_id'];?>" id="ua_sqm_<?php echo $panel['panel_id'];?>" class="form-control" style="width:80px; border:0px; " onkeypress="return isNumberKey(event,this)" value="<?php if(count($dprDetails) >0) echo $dprDetails[0]['unit_area_sqm']; else echo $panel['unit_area'];?>" onKeyUp="return caluculateTotalVal(this.value,<?php echo $panel['panel_id'];?>,'Nos')" onBlur="return caluculateTotalVal(this.value,<?php echo $panel['panel_id'];?>,'Nos'),addTotalValue();" /></td>
                                                <td><input type="text" name="current_month_nos_<?php echo $panel['panel_id'];?>" id="current_month_nos_<?php echo $panel['panel_id'];?>" class="form-control" style="width:80px; border:0px; " onkeypress="return isNumberKey(event,this)" value="<?php if(count($dprDetails) >0) echo $dprDetails[0]['month_no']?>" onKeyUp="return caluculateTotalVal(this.value,<?php echo $panel['panel_id'];?>,'Nos')" onBlur="return caluculateTotalVal(this.value,<?php echo $panel['panel_id'];?>,'Nos'),addTotalValue();" /></td>
                                                <td><input type="text" name="current_month_area_<?php echo $panel['panel_id'];?>" id="current_month_area_<?php echo $panel['panel_id'];?>" class="form-control" style="width:80px; border:0px; background-color:#fcfcfc" onkeypress="return isNumberKey(event,this)" readonly value="<?php if(count($dprDetails) >0) echo $dprDetails[0]['month_area']?>" onKeyUp="return caluculateTotalVal1(this.value,<?php echo $panel['panel_id'];?>,'Area')" /></td>
                                                <!--<td><input type="text" name="last_month_nos_<?php echo $panel['panel_id'];?>" id="last_month_nos_<?php echo $panel['panel_id'];?>" readonly class="form-control" style="width:80px; border:0px;background-color:#fcfcfc" onkeypress="return isNumberKey(event,this)" value="<?php if(count($tillLastMonth) >0) echo $tillLastMonth[0]['last_month_no']?>" onKeyUp="return caluculateTotalVal11(this.value,<?php if(count($dprDetails) >0) echo $panel['panel_id'];?>,'Nos')" /></td>
                                                <td><input type="text" name="last_month_area_<?php echo $panel['panel_id'];?>" id="last_month_area_<?php echo $panel['panel_id'];?>" readonly class="form-control" style="width:80px; border:0px; background-color:#fcfcfc" onkeypress="return isNumberKey(event,this)" value="<?php if(count($tillLastMonth) >0) echo $tillLastMonth[0]['last_month_area']?>" onKeyUp="return caluculateTotalVal11(this.value,<?php echo $panel['panel_id'];?>,'Area')" /></td>-->
                                                <td><input type="text" name="cummulative_nos_<?php echo $panel['panel_id'];?>" id="cummulative_nos_<?php echo $panel['panel_id'];?>" value="<?php if(count($dprDetails) >0) echo $dprDetails[0]['cummulative_no']?>" class="form-control" readonly style="width:80px; border:0px; background-color:#fcfcfc" onkeypress="return isNumberKey(event,this)" /></td>
                                                <td><input type="text" name="cummulative_area_<?php echo $panel['panel_id'];?>" id="cummulative_area_<?php echo $panel['panel_id'];?>" value="<?php if(count($dprDetails) >0) echo $dprDetails[0]['cummulative_area']?>" readonly class="form-control" style="width:80px; border:0px; background-color:#fcfcfc" onkeypress="return isNumberKey(event,this)"/></td>
                                                <td><input type="text" name="remark_<?php echo $panel['panel_id'];?>" id="remark_<?php echo $panel['panel_id'];?>" value="<?php if(count($dprDetails) >0) echo $dprDetails[0]['remark']?>" class="form-control" style="width:150px; border:0px; " /></td>
                                              </tr>
                                          <?php
										  }
										  ?>
                                          <tr>
                                          		<td colspan="3" style="text-align:right;background-color: #fcfcfc;padding-right: 5px;"><strong>Total</strong></td>
                                                <td><input type="text" name="InMonthNosVal" readonly id="InMonthNosVal"  class="form-control" style="width:80px; border:0px; background-color: #fcfcfc;"/></td>
                                                <td><input type="text" name="InMonthNosArea" readonly id="InMonthNosArea" class="form-control" style="width:80px; border:0px;background-color: #fcfcfc; " /></td>
                                                <!--<td><input type="text" name="ctlMonthNosVal" readonly id="ctlMonthNosVal" class="form-control" style="width:80px; border:0px; background-color: #fcfcfc;" /></td>
                                                <td><input type="text" name="ctlMonthNosArea" readonly id="ctlMonthNosArea" class="form-control" style="width:80px; border:0px; background-color: #fcfcfc;" /></td>-->
                                                <td><input type="text" name="cchNosVal"  readonly id="cchNosVal" class="form-control" style="width:80px; border:0px; background-color: #fcfcfc;" /></td>
                                                <td><input type="text" name="cchNosArea" readonly id="cchNosArea" class="form-control" style="width:80px; border:0px; background-color: #fcfcfc;" /></td>
                                          		<td style="text-align:right;background-color: #fcfcfc;">&nbsp;</td>
                                          </tr>
                                          <tr>
                                          	<td colspan="10">
                                              <div class="box-footer" style="text-align:right">
                                                <button type="submit" class="btn btn-primary" onClick="return submitFunction()">Save</button>
                                                <input type="hidden" name="changeSubmit" id="changeSubmit" value="0">
                                              </div>
                                            </td>
                                          </tr>
                                </table>
                            </form>
                          </div>
                          <div class="tab-pane fade" id="reject">
                              <h4>Reject DPR</h4>
                                <table id="manageTable" class="table table-bordered table-striped">
                                    <?php
                                    if(count($this->data['rejectDpr']) > 0)
                                    {
										?>
										  <tr style="background-color:#ECE7E7; color:#000000">
											<td style="text-align:left; width:10%"><strong>DPR&nbsp;Type</strong></td>
											<td style="text-align:left; width:15%"><strong>DPR&nbsp;Date</strong></td>
											<td style="text-align:left; width:10%"><strong>Status</strong></td>
											<td style="text-align:left; width:60%"><strong>Reason</strong></td>
											<td style="text-align:left; width:10%"><strong>Action</strong></td>
										  </tr>
                                          <?php
										  foreach($this->data['rejectDpr'] as $rejctData)
										  {
											  ?>
                                                <tr>
                                                    <td><?php echo $rejctData['dpr_type']?></td>
                                                    <td><?php echo $rejctData['dpr_date']?></td>
                                                    <td><?php echo $rejctData['status']?></td>
                                                    <td><?php echo $rejctData['status_reason']?></td>
                                                    <td> <form role="form" action="<?php echo base_url('Controller_Project/insertDPR') ?>" method="post" name="assignForm<?php echo $rejctData['dpr_date']?>" id="assignForm<?php echo $rejctData['dpr_date']?>">
                                                    <input type="hidden" name="type" id="type"  value="<?php echo $rejctData['dpr_type']?>" >
                                                    <input type="hidden" name="changeSubmit" id="changeSubmit"  value="1" >
                                                    <input type="hidden" name="dpr_date" id="dpr_date" value="<?php echo $rejctData['dpr_date']?>">
<button type="submit" class="btn btn-primary">Edit</button></form>
</td>
                                                </tr>
                                          <?php
										  }
                                    }
                                    else
                                    {
                                        ?>
                                              <tr style="border:2px solid #FF0606">
                                                <td style="color:red"><strong>No record found.</strong></td>
                                               </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                          </div>
                        </div>
                    </div>
                 </td>
               </tr>
             </thead>
           </table>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!--<div class="modal fade" tabindex="-1" role="dialog" id="assignModal">
  <div class="modal-dialogAction modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Assign Item</h4>
        <span style=" float: right;margin-bottom: -17px;margin-right: 151px;margin-top: -22px;">
            <select class="form-control" id="assignFor" name="assignFor" onChange="showHide(this.value)">
                <option value="user">Assign to User</option>
                <option value="place">Assign to Place</option>
            </select>
        </span>
      </div>
        <form role="form" action="<?php echo base_url('Controller_Project/AssignToUser') ?>" method="post" id="assignForm">
        <input type="hidden" name="assignForVl" id="assignForVl"  value="user" >
        <input type="hidden" name="projectId" id="projectId">
        <input type="hidden" name="productId" id="productId">
        <input type="hidden" name="pQty" id="pQty">
          <div class="box-body" id="assigntouser">
            <?php echo validation_errors(); ?>
            <div class="row">
           <div class="col-md-6">
            <div class="form-group">
              <label for="fname">Assign To <font color="#FF0004">*</font></label>
              <input type="text" class="form-control" id="assignTo" name="assignTo" placeholder="Assign To" autocomplete="off" required>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label for="username">Department <font color="#FF0004">*</font></label>
              <input type="text" class="form-control" id="Department" name="Department" placeholder="Department" autocomplete="off" required>
            </div>
            </div>

             <div class="col-md-6">
            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="Email" autocomplete="off">
            </div>
            </div>
            <div class="col-md-6">

            <div class="form-group">
              <label for="phone">Mobile No </label>
              <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" autocomplete="off" maxlength="10">
            </div>
            </div>
            </div>
            <div class="row">
           <div class="col-md-6">
            <div class="form-group">
              <label for="gender">Gender</label>
              <div class="radio">
                <label>
                  <input type="radio" name="gender" id="male" value="1" checked>
                  Male
                </label>
                <label>
                  <input type="radio" name="gender" id="female" value="2">
                  Female
                </label>
              </div>
              
              </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                  <label for="fname11">Location </label>
                  <input type="text" class="form-control" id="location" name="location" placeholder="Location" autocomplete="off">
                </div>
                </div>	                        
            </div>
            <div class="row">
            <div class="col-md-6">
            <div class="form-group">
              <label for="username">Quantity <font color="#FF0004">*</font></label>
              <input type="text" class="form-control" onKeyUp="return CheckQuantity(this.value)" id="plQty" name="plQty" placeholder="Quantity" maxlength="3" autocomplete="off" required>
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
              <label for="username">Remark</label>
              <input type="text" class="form-control" id="remark" name="remark" placeholder="Remark" autocomplete="off">
            </div>
            </div>

            </div>
            </div>
<!--          <div class="box-body" style="display:none" id="assigntoplace">
            <?php //echo validation_errors(); ?>
            <div class="row">
           <div class="col-md-6">
            <div class="form-group">
              <label for="fname22">Location <font color="#FF0004">*</font></label>
              <input type="text" class="form-control" id="placeassignTo" name="placeassignTo" placeholder="Location" autocomplete="off" required disabled>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label for="username">Department <font color="#FF0004">*</font></label>
              <input type="text" class="form-control" id="placeDepartment" name="placeDepartment" placeholder="Department" autocomplete="off" required disabled>
            </div>
            </div>

            <div class="col-md-6">

            <div class="form-group">
              <label for="phone">Mobile No</label>
              <input type="number" class="form-control" id="placephone" name="placephone" placeholder="Phone" autocomplete="off" maxlength="10" disabled>
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
              <label for="username">Quantity</label>
              <input type="text" class="form-control" onKeyUp="return CheckQuantity(this.value)" id="plQty" name="plQty" placeholder="Quantity" maxlength="3" autocomplete="off" disabled required>
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
              <label for="username">Remark</label>
              <input type="text" class="form-control" id="placeremark" name="placeremark" placeholder="Remark" autocomplete="off" disabled>
            </div>
            </div>
            </div>
            </div>
          <!-- /.box-body 

          <div class="box-footer" style="text-align:right">
            <button type="submit" class="btn btn-primary" onClick="return checkQuantityOnSave()">Save & Close</button>
          </div>
        </form>
    </div><!-- /.modal-content 
  </div><!-- /.modal-dialog 
</div><!-- /.modal 
--><!-- remove brand modal -->
<!--<div class="modal fade" tabindex="-1" role="dialog" id="removeModal">
  <div class="modal-dialogAction modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Return Product</h4>
      </div>
      <form role="form" action="<?php echo base_url('Controller_Project/ReturnProductByUser') ?>" method="post" id="removeForm">
        <input type="hidden" name="ProId" id="ProId" >
        <input type="hidden" name="ProjId" id="ProjId" >
        <div class="modal-body">
          <div class="box-body">
            <div class="row">
           <div class="col-md-12">
            <div class="form-group">
              <label for="fname">Return By <font color="#FF0004">*</font></label>
                <select class="form-control" id="return_by" name="return_by" required>
                    <option value="">Select</option>
                </select>
            </div>
            </div>
           <div class="col-md-12">
            <div class="form-group">
              <label for="fname">Remark </label>
                <input type="text" name="remark" id="remark" class="form-control">
            </div>
            </div>
            </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Return</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>


    </div><!-- /.modal-content 
  </div><!-- /.modal-dialog 
</div>--><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="assignPayment">
  <div class="modal-dialogAction modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Payment Details</h4>
      </div>
        <input type="hidden" name="ProId" id="ProId" >
        <input type="hidden" name="ProjId" id="ProjId" >
        <div class="modal-body">
          <div class="box-body" id="innerData">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
        </div>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /.content-wrapper -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script>
function caluculateTotalVal(enterVal,panel_id,type)
{
	if(type=='Nos')
	{ 
		var lastMnthNo=0;
		var currentMntNo=0;
		var lastMnthArea=0;
		var currentMntArea=0;
		var sqmNo=0;
		//if(document.getElementById('last_month_nos_'+panel_id).value!="")	
			//var lastMnthNo=Number(document.getElementById('last_month_nos_'+panel_id).value);
		if(document.getElementById('current_month_nos_'+panel_id).value!="")	
			var currentMntNo=Number(document.getElementById('current_month_nos_'+panel_id).value);
			
		if(document.getElementById('ua_sqm_'+panel_id).value!="")	
			var sqmNo=Number(document.getElementById('ua_sqm_'+panel_id).value);
			
		//if(document.getElementById('last_month_area_'+panel_id).value!="")	
			//var lastMnthArea=Number(document.getElementById('last_month_area_'+panel_id).value);
		if(document.getElementById('current_month_area_'+panel_id).value!="")	
			var currentMntArea=Number(document.getElementById('current_month_area_'+panel_id).value);
			
		document.getElementById('current_month_area_'+panel_id).value=Number(sqmNo*currentMntNo);	
		document.getElementById('cummulative_area_'+panel_id).value=Number(lastMnthArea+currentMntArea);		
		document.getElementById('cummulative_nos_'+panel_id).value=Number(lastMnthNo+currentMntNo);	
		
	}
	if(type=='Area')
	{
		var lastMnthNo=0;
		var currentMntNo=0;
		//if(document.getElementById('last_month_area_'+panel_id).value!="")	
			//var lastMnthNo=Number(document.getElementById('last_month_area_'+panel_id).value);
		if(document.getElementById('current_month_area_'+panel_id).value!="")	
			var currentMntNo=Number(document.getElementById('current_month_area_'+panel_id).value);
			
		document.getElementById('cummulative_area_'+panel_id).value=Number(lastMnthNo+currentMntNo);	
	}
}
function addTotalValue()
{
	var pIds=document.getElementsByName('panelIds[]');
	var flag=0;
	var cmnos=0;
	var cmarea=0;
	var lstmnos=0;
	var lstmarea=0;
	var lstmnos=0;
	var lstmarea=0;
	var cummnos=0;
	var cumarea=0;
	for(var i=0;i<pIds.length;i++)
	{
		var pId=pIds[i].value;
		if(document.getElementById('current_month_nos_'+pId).value!=""){	
			var cmnos= Number(cmnos)+Number(document.getElementById('current_month_nos_'+pId).value);
		}
		
		if(document.getElementById('current_month_area_'+pId).value!="")	
			var cmarea=(Number(cmarea)+Number(document.getElementById('current_month_area_'+pId).value));
			
		//if(document.getElementById('last_month_nos_'+pId).value!="")	
		//	var lstmnos=(Number(lstmnos)+Number(document.getElementById('last_month_nos_'+pId).value));
			
		//if(document.getElementById('last_month_area_'+pId).value!="")	
			//var lstmarea=(Number(lstmarea)+Number(document.getElementById('last_month_area_'+pId).value));
			
		if(document.getElementById('cummulative_nos_'+pId).value!="")	
			var cummnos=(Number(cummnos)+Number(document.getElementById('cummulative_nos_'+pId).value));
			
		if(document.getElementById('cummulative_area_'+pId).value!="")	
			var cumarea=(Number(cumarea)+Number(document.getElementById('cummulative_area_'+pId).value));
		
		//if(document.getElementById('current_month_nos_'+pId).value!="")	
			//document.getElementById('ctlMonthNosVal').value=(Number(document.getElementById('ctlMonthNosVal').value)+Number(document.getElementById('current_month_nos_'+pId).value));
	
	}
	//alert(cmnos);
	document.getElementById('InMonthNosVal').value= cmnos;
	document.getElementById('InMonthNosArea').value=cmarea;
	//document.getElementById('ctlMonthNosVal').value=lstmnos;
	//document.getElementById('ctlMonthNosArea').value=lstmarea;
	document.getElementById('cchNosVal').value=cummnos;
	document.getElementById('cchNosArea').value=cumarea;
}
$("#hidePopUp").click(function(){
	//alert('Hii deep');
	$("#editModal").modal('hide');
});
</script>

<script type="text/javascript">
var manageTable;

$(document).ready(function() {
  // submit the create from 
  addTotalValue();
});

// edit function
function showProductDetails(id,projeId)
{ 
  $.ajax({
    url: 'fetchProductDataByWhouseId/'+id+'/'+projeId,
    type: 'post',
    dataType: 'json',
    success:function(response) {
		document.getElementById('displayStock_'+projeId).innerHTML=response;
    }
  });
}
// remove functions 
function removeFunc(id,pId)
{
  document.getElementById('delete_project_id').value=id;
}
function changeHeaderPart(id)
{
	document.getElementById("changeSubmit").value=1;
	document.getElementById("submitValue").submit();
}
function submitAssignStockForm(value,id)
{
	document.getElementById(id).submit();
}

function ReturnProductFunc(id,pId)
{
	document.getElementById('ProId').value=pId;
	document.getElementById('ProjId').value=id;
  if(id) {
		$.ajax({
			url: '<?php echo base_url('Controller_Project/GetAssignProducts'); ?>',
			type: 'POST',
			data: {
				prId: id,
				pId: pId
			},
			dataType: 'json',
			success: function(data) {
				$('select[name="return_by"]').empty();
				$('select[name="return_by"]').append('<option value="">Select</option>');
				$.each(data, function(key, value) {
				$('select[name="return_by"]').append('<option value="'+ value.proProjId +'">'+ value.aTo +'</option>');
				})
			}
		});  
	}
}

function GelAllPaymentData(projId)
{
  if(projId) {
		$.ajax({
			url: '<?php echo base_url('Controller_Project/GetAssignPaymentDetails'); ?>',
			type: 'POST',
			data: {
				prId: projId
			},
			dataType: 'json',
			success: function(data) {
				document.getElementById('innerData').innerHTML=data;
			}
		});  
	}
}

function isNumberKey(evt, obj) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	var value = obj.value;
	var dotcontains = value.indexOf(".") != -1;
	if (dotcontains)
		if (charCode == 46) return false;
	if (charCode == 46) return true;
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}
function CheckAmt(enterVal)
{
	if(Number(enterVal))	
	{
		var actualQty=document.getElementById('remainingBal').value;
		if(Number(enterVal) > Number(actualQty))
		{
			alert('Enter amount should not be greater than actual amount');
			document.getElementById('paym').value="";
			document.getElementById('paym').focus();
			return false;
		}
		else
		{
			return true;
		}
		
	}
	else
	{
		alert('Please enter numeric value only !');
		document.getElementById('paym').value="";
		document.getElementById('paym').focus();
		return false;
	}
}
function CheckQuantity(enterVal,proId)
{
	if(enterVal=="")
	{
		document.getElementById("assignTo_"+proId).required = false;	
		return false;
	}
	if(Number(enterVal))	
	{
		var actualQty=document.getElementById('pQty_'+proId).value;
		if(Number(enterVal) > Number(actualQty))
		{
			alert('Enter quantity should not be greater than available (qty)');
			document.getElementById('plQty_'+proId).value="";
			document.getElementById('plQty_'+proId).focus();
			return false;
		}
		else
		{
			document.getElementById("assignTo_"+proId).required = true;
			return true;
		}
		
	}
	else
	{
		alert('Please enter numeric value only !');
		document.getElementById('plQty_'+proId).value="";
		document.getElementById('plQty_'+proId).focus();
		return false;
	}
}
function confirmMessage()
{
	var pIds=document.getElementsByName('productIds[]');
	var flag=0;
	for(var i=0;i<pIds.length;i++)
	{
		var pId=pIds[i].value;
		if(document.getElementById('plQty_'+pId).value!="")
		{
			flag=1;
		}
	}
		if(flag==0)
		{
			alert('Please fill at least one row to submit this page.');
			return false;
		}
}
function submitFunction()
{
	var pIds=document.getElementsByName('panelIds[]');
	var flag=0;
	for(var i=0;i<pIds.length;i++)
	{
		var pId=pIds[i].value;
		if(document.getElementById('cummulative_nos_'+pId).value!="" || document.getElementById('cummulative_area_'+pId).value!="")
		{
			flag=1;
		}
	}
		if(flag==0)
		{
			alert('Please fill at least one row to submit this page.');
			return false;
		}
}

</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
