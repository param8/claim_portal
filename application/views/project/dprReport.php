 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Daily Progress Report
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Project</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php  echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
        <div class="box">
          <style>
			.modal-lg {
				width: 900px; !important;
				margin: 50px auto;!important;
			}
		.modal-sm {
				width: 400px; !important;
				margin: 50px auto;!important;
			}		 
		 </style>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr style="background-color:#3c8dbc; color:#FFFFFF">
              <th>
              <?php 
			  if(count($this->data['projectStock'])==1)
			  {
				  ?>
                Project Name&nbsp;&nbsp;===>&nbsp;&nbsp;<?php if(count($this->data['projectStock'])==0) echo $this->data['projectStock'][0]['name']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?php
			  }
			  ?>
                Project Coordinator&nbsp;&nbsp;===>&nbsp;&nbsp;<?php if(count($this->data['projectStock'])>0) echo $this->data['projectStock'][0]['coordinator'];?></th>
				</th>
              </tr>
              <tr>
              	<td>
                    <div class="container" style="width:100%">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="active">
                              <a href="#settings" role="tab" data-toggle="tab">
                                  <i class="fa fa-cog"></i> DPR
                              </a>
                          </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div class="tab-pane fade active in" id="settings">
                              <h4>CASTING DETAILS</h4>
                            <form role="form" action="<?php echo base_url('Controller_Project/SearchDPR') ?>" method="post" id="">
                              <div class="row">
                              	<div class="col-sm-1"><strong>Site</strong></div>
                              	<div class="col-sm-3">
                                	<select name="site" id="site" class="form-control" required onChange="getMaxDate(this.value)">
                                    <?php
									$CI=& get_instance();
									$siteDetails=$CI->getSiteDetails();
									
									foreach($siteDetails as $sites)
									{
										?>
                                    	<option value="<?php echo $sites['id'];?>" <?php if($this->data['site']==$sites['id']) echo "selected";?>><?php echo $sites['name'];?></option>
                                     <?php
									}
									?>
                                    </select>
                                </div>
                              	<div class="col-sm-1"><strong>Type</strong></div>
                              	<div class="col-sm-3">
                                	<select name="type" id="type" class="form-control" onChange="changeHeaderPart(this.value)">
                                    	<option value="casting" <?php if(isset($this->data['type'])  && $this->data['type']=='casting') echo "selected";?>>Casting</option>
                                    	<option value="erection" <?php if(isset($this->data['type'])  && $this->data['type']=='erection') echo "selected";?>>Erection</option>
                                    	<option value="shiffting" <?php if(isset($this->data['type'])  && $this->data['type']=='shiffting') echo "selected";?>>Shiffting</option>
                                    </select>
                                </div>
                              	<div class="col-sm-1"><strong>Date</strong></div>
                              	<div class="col-sm-3">
                                	<input type="date"  name="from_date" id="from_date" class="form-control" value="<?php if(isset($this->data['type']))  echo $this->data['date'];?>"  required />
                                </div>
                              </div>
                              <div class="row">
                              <?php
							  	$flag="No";
								 if(isset($this->data['projectStock'][0]['coordinator']) && $this->data['projectStock'][0]['coordinator']==$this->session->userdata['username'] && isset($this->data['type']))
								  {
									$flag="Yes";
								 ?>                                
								  <div class="col-sm-6" style="text-align:left; margin-top:10px">
                                    <a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#commentModel" onClick="return fillUpdateDetail();">Approve DPR</a>
                                </div>
                                <?php
								  }
								  ?>
                                <div class="col-sm-<?php if($flag=='No') echo "12"; else echo "6";?>" style="text-align:right; margin-top:10px">
                                    <button type="submit" class="btn btn-primary"><strong>Search</strong></button>
                                </div>
                              </div>
                            </form>
                              <br>
                                <table id="manageTable" class="table table-bordered" cellpadding="0" cellspacing="0">
										  <tr style="background-color:#ECE7E7; color:#000000">
											<td rowspan="3" style="width:5%"><strong>Sl.No.</strong></td>
											<td rowspan="3" style="text-align:center; width:15%"><strong>Type of Panel</strong></td>
										  </tr>
										  <tr style="background-color:#ECE7E7; color:#000000">
											<td style="text-align:center; width:10%"><strong>Unit Area</strong></td>
											<td style="text-align:center; width:10%" colspan="2"><strong>In this month</strong></td>
											<!--<td style="text-align:center; width:10%" colspan="2"><strong><span id="last_month"><?php if(isset($this->data['type'])  && $this->data['type']!="") echo ucfirst($this->data['type']); else echo "Casting"; ?>  till last Month</span></strong></td>-->
											<td style="text-align:center; width:20%" colspan="2"><strong><span id="cummulative"><?php if(isset($this->data['type'])  && $this->data['type']!="") echo ucfirst($this->data['type']); else echo "Casting"; ?> Cummulative</span></strong></td>
											<td rowspan="2" style="text-align:center; width:15%"><strong>Remark</strong></td>
                                          </tr>
										  <tr style="background-color:#ECE7E7; color:#000000">
											<td style="text-align:center; width:10%"><strong>SQM</strong></td>
											<td style="text-align:center; width:10%"><strong>Nos</strong></td>
											<td style="text-align:center; width:10%"><strong>Area</strong></td>
											<!--<td style="text-align:center; width:10%"><strong>Nos</strong></td>
											<td style="text-align:center; width:10%"><strong>Area</strong></td>-->
											<td style="text-align:center; width:10%"><strong>Nos</strong></td>
											<td style="text-align:center; width:10%"><strong>Area</strong></td>
                                          </tr>
                                          <?php
										  $ci=& get_instance();
										  $panelDetail= $ci->getAllocatePanelSiteWise($this->data['site']);
										  $slNo=1;
										  $lstMnthNosTtl=0;
										  $lstMnthAraTtl=0;
										  $currntMnthNosTtl=0;
										  $currntMnthAraTtl=0;
										  $cumultveMnthNosTtl=0;
										  $cumultveMnthAraTtl=0;
										  if(isset($this->data['date']))
										  	$date=$this->data['date'];
											else
												$date=date('Y-m-d', strtotime(' -1 day'));
												
										  if(isset($this->data['type']))
										  	$type=$this->data['type'];
											else
												$type='casting';
												
										  foreach($panelDetail as $panel)
										  {
											  $site=($this->data['site']) ? $this->data['site'] : "";
											  $dprDetails= $ci->getDailyProgressDtails($type,$panel['panel_id'],"Received",$date,$site);
											  //$tillLastMonth= $ci->getDailyProgressDtails($this->data['type'],$panel['panel_id'],"",$this->data['date']);
											 // echo"<pre>";print_r($dprDetails['status']);
										  ?>
                                              <tr <?php if(count($dprDetails) >0 && $dprDetails[0]['status']=='Reject') echo 'style="background-color:#F8BFC0"';?>>
                                                <td style="text-align:center"><?php echo $slNo++;?></td>
                                                <td style="text-align:center"><?php echo $panel['panel_name'];?></td>
                                                <td style="text-align:center"><?php if(count($dprDetails) >0) echo $dprDetails[0]['unit_area_sqm']; else echo "-";?></td>
                                                <td style="text-align:center"><?php if(count($dprDetails) >0) echo $dprDetails[0]['month_no']; else echo "-";?></td>
                                                <td style="text-align:center"><?php if(count($dprDetails) >0) echo $dprDetails[0]['month_area']; else echo "-";?></td>
                                                <!--<td style="text-align:center"><?php if(count($dprDetails) >0) echo $dprDetails[0]['last_month_no']; else echo "-";?></td>
                                                <td style="text-align:center"><?php if(count($dprDetails) >0) echo $dprDetails[0]['last_month_area']; else echo "-";?></td>-->
                                                <td style="text-align:center"><?php if(count($dprDetails) >0) echo $dprDetails[0]['cummulative_no']; else echo "-";?></td>
                                                <td style="text-align:center"><?php if(count($dprDetails) >0) echo $dprDetails[0]['cummulative_area']; else echo "-";?></td>
                                                <td style="text-align:center"><?php if(count($dprDetails) >0) echo $dprDetails[0]['remark']; else echo "-";?></td>
                                              </tr>
                                          <?php
										  	if(count($dprDetails) >0)
											{
											  $lstMnthNosTtl+=$dprDetails[0]['last_month_no'];
											  $lstMnthAraTtl+=$dprDetails[0]['last_month_area'];
											  $currntMnthNosTtl+=$dprDetails[0]['month_no'];
											  $currntMnthAraTtl+=$dprDetails[0]['month_area'];
											  $cumultveMnthNosTtl+=$dprDetails[0]['cummulative_no'];
											  $cumultveMnthAraTtl+=$dprDetails[0]['cummulative_area'];
											}
										  }
										  ?>
                                          <tr>
                                          	<td style="text-align:right" colspan="3"><strong>Total</strong></td>
                                          	<td style="text-align:center"><?php echo $currntMnthNosTtl;?></td>
                                          	<td style="text-align:center"><?php echo $currntMnthAraTtl;?></td>
                                          	<!--<td style="text-align:center"><?php echo $lstMnthNosTtl;?></td>
                                          	<td style="text-align:center"><?php echo $lstMnthAraTtl;?></td>-->
                                          	<td style="text-align:center"><?php echo $cumultveMnthNosTtl;?></td>
                                          	<td style="text-align:center"><?php echo $cumultveMnthAraTtl;?></td>
                                          	<td style="text-align:center"></td>
                                          </tr>
                                </table>
                          </div>
                        </div>
                    </div>
                 </td>
               </tr>
             </thead>
           </table>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    <div class="modal fade " tabindex="-1" role="dialog" id="commentModel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Approve DPR</h4>
          </div>
    
          <form role="form" action="<?php echo base_url('Controller_Project/UpdateDprStatus') ?>" method="post" id="removeForm">
          <input type="hidden" name="delete_vendor_id" id="delete_vendor_id">
              <div class="modal-body">
                <p><strong>Status</strong></p><p><select class="form-control" name="status" required onChange="showHide(this.value)"><option value="Approve">Approve</option><option value="Reject">Reject</option></select></p>
              </div>
              <div class="modal-body" id="reasonField" style="display:none">
                <p><strong>Reason</strong></p><p><textarea name="reason" class="form-control" disabled="disabled" id="reason" required></textarea></p>
              </div>
              <div class="modal-footer">
              <input type="hidden" name="site_id" id="site_id" />
              <input type="hidden" name="dpr_type" id="dpr_type" />
              <input type="hidden" name="dpr_date" id="dpr_date" />
                <button type="submit" class="btn btn-success" onClick="return confirm('Are you sure to submit it ? .\n Click ok to continue cancel to stop');">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
          </form>
    
    
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script>
function showHide(status)
{
	if(status=='Approve'){
		document.getElementById("reasonField").style.display="none";
		document.getElementById("reason").disabled=true;
		}
	else{
		document.getElementById("reasonField").style.display="block";
		document.getElementById("reason").disabled=false;
	}
}
function fillUpdateDetail()
{
	document.getElementById("site_id").value=document.getElementById("site").value;
	document.getElementById("dpr_type").value=document.getElementById("type").value;
	document.getElementById("dpr_date").value=document.getElementById("from_date").value;
}
</script>	
  <script>

$("#hidePopUp").click(function(){
	//alert('Hii deep');
	$("#editModal").modal('hide');
});
</script>

<script type="text/javascript">
var manageTable;

$(document).ready(function() {
var site_id=document.getElementById("site").value;
//document.getElementById("from_date").disabled=true;
  $.ajax({
    url: 'fetchdprApproveDetails/'+site_id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
		document.getElementById("from_date").disabled=false;
		$("#from_date").attr("min",response);
    }
  });
});
function getMaxDate(site_id)
{
//document.getElementById("from_date").disabled=true;
  $.ajax({
    url: 'fetchdprApproveDetails/'+site_id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
        if(response){
    		document.getElementById("from_date").disabled=false;
    		$("#from_date").attr("min",response);
        }
    }
  });
}
// edit function
function changeHeaderPart(id)
{
	document.getElementById("changeSubmit").value=1;
	document.getElementById("submitValue").submit();
}
</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
