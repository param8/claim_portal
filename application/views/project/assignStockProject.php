 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Assign Stock to Project
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Project</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
        <div class="box">
          
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead style="background-color:#3c8dbc; color:#FFFFFF">
              <tr>
                <th>Project Name</th>
                <th>Project Coordinator</th>
                <th>Mobile no</th>
                <th>Action</th>
              </tr>
              </thead>
				<?php
				if($this->data['array'] > 0)
				{
					foreach($this->data['array'] as $vDetails)	
					{
					?>
                      <tr>
                        <td><?php echo $vDetails['project_name']?></td>
                        <td><?php echo $vDetails['project_coordinator']?></td>
                        <td><?php echo $vDetails['mobile_no']?></td>
                        <td>
                        	<select name="warehouse" id="warehouse" class="form-control" onChange="showProductDetails(this.value,<?php echo $vDetails['project_id']?>)">
                            	<option value="">Select</option>
								 <?php 
                                    foreach($this->data['stockData'] as $stock)	
                                    {
										?>
                                            <option value="<?php echo $stock['id']?>"><?php echo $stock['name']?></option>
                                        <?php
                                    }
                                 ?>
                         	</select>
                            <br>
                            <div id="displayStock_<?php echo $vDetails['project_id']?>"></div>
                            
                        </td>
                       </tr>
                    <?php	
					}
				}
				?>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script>
function refreshPage(Val)
{
	if(Val==1)
	{
		if(document.getElementById('Project_name').value!="" && document.getElementById('active').value!="")
			location.reload();
		else if(document.getElementById('edit_Project_name').value!="" && document.getElementById('edit_unit').value!="")
			location.reload();
		else
			return false;
	}
	else
	{
		location.reload();
	}
}

$("#hidePopUp").click(function(){
	//alert('Hii deep');
	$("#editModal").modal('hide');
});
</script>

<script type="text/javascript">
var manageTable;

$(document).ready(function() {
  // submit the create from 
});

// edit function
function showProductDetails(id,projeId)
{ 
  $.ajax({
    url: 'fetchProductDataByWhouseId/'+id+'/'+projeId,
    type: 'post',
    dataType: 'json',
    success:function(response) {
		document.getElementById('displayStock_'+projeId).innerHTML=response;
    }
  });
}
// remove functions 
function removeFunc(id,pId)
{
  document.getElementById('delete_project_id').value=id;
}
function removeFunc(id,pId)
{
  if(id) {
		$.ajax({
			url: '<?php echo base_url('Controller_Products/GetAssignProducts'); ?>',
			type: 'POST',
			data: {
				pId: id
			},
			dataType: 'json',
			success: function(data) {
				document.getElementById("asProjId").value=id;
				document.getElementById("aspId").value=pId
//				$('select[name="return_by"]').empty();
//				$.each(data[0], function(key, value) {
//				$('select[name="return_by"]').append('<option value="'+ value.proId +'">'+ value.aTo +'</option>');
//				})
				$('select[name="to_warehouse"]').empty();
				$('select[name="to_warehouse"]').append('<option value="">Select</option>');
				$.each(data[1], function(key, value) {
				$('select[name="to_warehouse"]').append('<option value="'+ value.id +'">'+ value.name +'</option>');
				})
			}
		});  
	}
}
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
function checkQuantity(enterVal,ProjectId,pid,qty)
{
	if((Number(enterVal))==0)
	{
		document.getElementById('totalVal_'+ProjectId+'_'+pid).value="";
		return false;
	}
	if(Number(enterVal) > Number(qty))
	{
		alert("Enter value can't more than quantity.");
		document.getElementById('totalVal_'+ProjectId+'_'+pid).value="";
		document.getElementById('qunty_'+ProjectId+'_'+pid).value=(Number(document.getElementById('hiddenQunty_'+ProjectId+'_'+pid).value));
		return false;	
	}
	else
	{
		document.getElementById('qunty_'+ProjectId+'_'+pid).value=	(Number(document.getElementById('hiddenQunty_'+ProjectId+'_'+pid).value)-Number(enterVal));
	}
}
</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
