 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<?php
if(isset($_POST['tableHtml']) && isset($_POST['fileName']))
{
    $htmlContent=$_POST['tableHtml'];
    $fileName=$_POST['fileName'].'.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$fileName");
    echo $htmlContent; die;
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Daily Progress Report(Date Range)
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Project</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php  echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
        <div class="box">
          <style>
			.modal-lg {
				width: 900px; !important;
				margin: 50px auto;!important;
			}
		.modal-sm {
				width: 400px; !important;
				margin: 50px auto;!important;
			}		 
		 </style>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered table-striped">
              <thead>
              <tr style="background-color:#3c8dbc; color:#FFFFFF">
              <th>
              <?php 
			  if(count($this->data['projectStock'])==1)
			  {
				  ?>
                Project Name&nbsp;&nbsp;===>&nbsp;&nbsp;<?php if(count($this->data['projectStock'])==0) echo $this->data['projectStock'][0]['name']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?php
			  }
			  ?>
                Project Coordinator&nbsp;&nbsp;===>&nbsp;&nbsp;<?php if(count($this->data['projectStock'])>0) echo $this->data['projectStock'][0]['coordinator'];?></th>
				</th>
              </tr>
              <tr>
              	<td>
                    <div class="container" style="width:100%">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="active">
                              <a href="#settings" role="tab" data-toggle="tab">
                                  <i class="fa fa-cog"></i> DPR
                              </a>
                          </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div class="tab-pane fade active in" id="settings">
                              <h4>CASTING DETAILS</h4>
                            <form role="form" action="<?php echo base_url('Controller_Project/SearchDPRDateWiseRange') ?>" method="post" id="">
                              <div class="row">
                              	<div class="col-sm-1"><strong>Site</strong></div>
                              	<div class="col-sm-3">
                                	<select name="site" id="site" class="form-control" required>
                                    <?php
									$CI=& get_instance();
									$siteDetails=$CI->getSiteDetails();
									$siteName=array();
									foreach($siteDetails as $sites)
									{
									    $siteName[$sites['id']]=$sites['name'];
										?>
                                    	<option value="<?php echo $sites['id'];?>" <?php if($this->data['site']==$sites['id']) echo "selected";?>><?php echo $sites['name'];?></option>
                                     <?php
									}
									?>
                                    </select>
                                </div>
                              	<div class="col-sm-1"><strong>Type</strong></div>
                              	<div class="col-sm-3">
                                	<select name="type" id="type" class="form-control" onChange="changeHeaderPart(this.value)">
                                    	<option value="casting" <?php if(isset($this->data['type'])  && $this->data['type']=='casting') echo "selected";?>>Casting</option>
                                    	<option value="erection" <?php if(isset($this->data['type'])  && $this->data['type']=='erection') echo "selected";?>>Erection</option>
                                    	<option value="shiffting" <?php if(isset($this->data['type'])  && $this->data['type']=='shiffting') echo "selected";?>>Shiffting</option>
                                    </select>
                                </div>
                              </div>
                              <div class="row" style="margin-top:20px">
                              	<div class="col-sm-1"><strong>From</strong></div>
                              	<div class="col-sm-3">
                                	<input type="date"  name="from_date" id="from_date" class="form-control" value="<?php if(isset($this->data['type']))  echo $this->data['from_date'];?>" required />
                                </div>
                              	<div class="col-sm-1"><strong>To</strong></div>
                              	<div class="col-sm-3">
                                	<input type="date"  name="to_date" id="to_date" class="form-control" value="<?php if(isset($this->data['type']))  echo $this->data['to_date'];?>" required />
                                </div>
                              
                                <div class="col-sm-3" style="text-align:right; margin-top:10px">
                                    <button type="submit" class="btn btn-primary"><strong>Search</strong></button>
                                </div>
                              </div>
                            </form>
                              <br>
                          </div>
                        </div>
                    </div>
                 </td>
               </tr>
             </thead>
           </table>
			  <?php
              $permissionFlag="No";
              if(in_array('All',$this->session->userdata['ButtonPermisssion']))
                $permissionFlag="Yes";
                
              
              if((array_key_exists('DPR_(Date_Range_Report)',$this->session->userdata['ButtonPermisssion'])) || $permissionFlag=="Yes")
              {
                  ?>
                <table  class="table table-bordered table-striped" style="margin:0px;margin-bottom:5px" cellpadding="0" cellspacing="0">
                  <tr>
                    <td>
                          <?php
                          if((array_key_exists('DPR_(Date_Range_Report)',$this->session->userdata['ButtonPermisssion']) && in_array('Excel',$this->session->userdata['ButtonPermisssion']['DPR_(Date_Range_Report)'])) || $permissionFlag=="Yes")
                          {
                              ?>
                             <a href="javascript:void(0)" title="Download Excel" onclick="exportTableToExcel('manageTable', 'DPRDateRangeReport')">
                            <i class="fa fa-file-excel-o" style="font-size: 25px;" aria-hidden="true"></i>
                        </a>&nbsp;&nbsp;
                            <?php
                            }
                          if((array_key_exists('DPR_(Date_Range_Report)',$this->session->userdata['ButtonPermisssion']) && in_array('Print',$this->session->userdata['ButtonPermisssion']['DPR_(Date_Range_Report)'])) || $permissionFlag=="Yes")
                          {
                            ?>
                            <a href="javascript:void(0)" title="Print" onclick="printDiv('manageTable')" >
                                <i class="fa fa-print" style="font-size: 25px;" aria-hidden="true"></i>
                           </a>
                           <?php
                          }
                          ?>
                     </td>
                  </tr>
                </table>
            <?php
              }
              ?>
            <div  id="manageTable" style="overflow:scroll">
            <table class="table table-bordered table-striped" cellpadding="0" cellspacing="0" border="1">
                      <tr style="background-color:#3c8dbc; color:#fff">
                        <td style="width:5%" colspan="<?php echo abs(round((strtotime($to_date)-strtotime($from_date))/86400)*2)+7?>"><strong>Site ==><?php if($this->data['site']) echo $siteName[$this->data['site']];?> <?php if(isset($this->data['type']))  echo ", Type ==>".ucfirst(strtolower($this->data['type']));?>&nbsp;(Period&nbsp;-&nbsp;<?php if(isset($this->data['type']))  echo date('d-m-Y',strtotime($this->data['from_date']));?>&nbsp;To&nbsp;<?php if(isset($this->data['type']))  echo date('d-m-Y',strtotime($this->data['to_date']));?>)</strong></td>
                      </tr>
                      <tr style="background-color:#ECE7E7; color:#000000">
                        <td rowspan="3" style="width:5%"><strong>Sl.No.</strong></td>
                        <td rowspan="3" style="text-align:center; width:15%; white-space:nowrap"><strong>Type of Panel</strong></td>
                        <td rowspan="3" style="text-align:center; width:10%; white-space:nowrap"><strong>Unit Area</strong></td>
                      </tr>
                        <tr>
                          <?php
						  for($i=strtotime($from_date);$i<=strtotime($to_date);$i = $i + 86400)
                         	{
                              ?>
                                <td colspan="2" style="text-align:center; width:10%; white-space:nowrap"><strong><?php echo date('d-m-Y',$i);?></strong></td>
                                <?php
                          }
                            ?>
                        <td style="text-align:center; width:10%; white-space:nowrap" colspan="2" ><strong>Total</strong></td>
                    </tr>
                     <tr>
                          <?php
						  for($i=strtotime($from_date);$i<=strtotime($to_date);$i = $i + 86400)
                          {
                              ?>
                                <td>Nos</td>
                                <td>Area</td>
                                <?php
                          }
                        ?>
                                <td>Nos</td>
                                <td>Area</td>
                        </tr>
                      <?php
                      $ci=& get_instance();
                      $panelDetail= $ci->getAllocatePanelSiteWise($this->data['site']);
                      $slNo=1;
                      $lstMnthNosTtl=0;
                      $lstMnthAraTtl=0;
                      $currntMnthNosTtl=0;
                      $currntMnthAraTtl=0;
                      $cumultveMnthNosTtl=0;
                      $cumultveMnthAraTtl=0;
                      if(isset($this->data['from_date']))
                        $from_date=$this->data['from_date'];
                        else
                            $date=date('Y-m-d', strtotime(' -1 day'));
                            
                      if(isset($this->data['to_date']))
                        $to_date=$this->data['to_date'];
                        else
                            $date=date('Y-m-d', strtotime(' -1 day'));
                            
                      if(isset($this->data['type']))
                        $type=$this->data['type'];
                        else
                            $type='casting';
                    
					  $totalNosArray=array();
					  $totalAreasArray=array();
					  $gtotalNos=0;
					  $gtotalAreas=0;
                      foreach($panelDetail as $panel)
                      {
                          $site=($this->data['site']) ? $this->data['site'] : "";
                              ?>
                              <tr>
                                <td style="text-align:center"><?php echo $slNo++;?></td>
                                <td style="text-align:center"><?php echo $panel['panel_name'];?></td>
                                <td style="text-align:center"><?php echo $panel['unit_area'];?></td>
                                <?php
								  $totalNos=0;
								  $totalAreas=0;
								  for($i=strtotime($from_date);$i<=strtotime($to_date);$i = $i + 86400)
                                  {
                                      $dprDetails= $ci->getDailyProgressDtails($type,$panel['panel_id'],'Received',date('Y-m-d',$i),$site);
									  $totalNos+=$dprDetails[0]['month_no'];
									  $totalAreas+=$dprDetails[0]['month_area'];
                                      ?>
                                        <td style="text-align:center"><?php if(count($dprDetails) >0) echo round($dprDetails[0]['month_no'],3); else echo "-";?></td>
                                        <td style="text-align:center"><?php if(count($dprDetails) >0) echo round($dprDetails[0]['month_area'],3); else echo "-";?></td>
                                <?php
									  $totalNosArray[date('Y-m-d',$i)]+=round($dprDetails[0]['month_no'],3);
									  $totalAreasArray[date('Y-m-d',$i)]+=round($dprDetails[0]['month_area'],3);
                                  }
                                  ?>
                                        <td style="text-align:center"><?php if($totalNos >0) echo round($totalNos,3); else echo "-";?></td>
                                        <td style="text-align:center"><?php if($totalAreas >0) echo round($totalAreas,3); else echo "-";?></td>
                              </tr>
                          <?php
						  $gtotalNos+=round($totalNos,3);
						  $gtotalAreas+=round($totalAreas,3);
                      }
                      ?>
                      <tr>
                        <td colspan="3" style="text-align:right"><strong>Total</strong></td>
						<?php
                        for($i=strtotime($from_date);$i<=strtotime($to_date);$i = $i + 86400)
                        {
                        ?>
                            <td><?php echo $totalNosArray[date('Y-m-d',$i)];?></td>
                            <td><?php echo $totalAreasArray[date('Y-m-d',$i)];?></td>
                        <?php
                        }
                        ?>
                            <td><?php echo $gtotalNos;?></td>
                            <td><?php echo $gtotalAreas;?></td>
                    </tr>           
			 </table>
            </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
  <form role="form" method="post" id="exceldownload">
      <input type="hidden" name="tableHtml" id="tableHtml">
      <input type="hidden" name="fileName" id="fileName">
  </form>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script>
function showHide(status)
{
	if(status=='Approve'){
		document.getElementById("reasonField").style.display="none";
		document.getElementById("reason").disabled=true;
		}
	else{
		document.getElementById("reasonField").style.display="block";
		document.getElementById("reason").disabled=false;
	}
}
function fillUpdateDetail()
{
	document.getElementById("site_id").value=document.getElementById("site").value;
	document.getElementById("dpr_type").value=document.getElementById("type").value;
	document.getElementById("dpr_date").value=document.getElementById("from_date").value;
}
</script>	
<script type="text/javascript">
// edit function
function changeHeaderPart(id)
{
	document.getElementById("changeSubmit").value=1;
	document.getElementById("submitValue").submit();
}
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}
function exportTableToExcel(divName,filename) {
   var tableHtml=document.getElementById(divName).innerHTML; 
   document.getElementById("tableHtml").value=tableHtml;
   document.getElementById("fileName").value=filename;
   document.getElementById('exceldownload').submit();
}

</script>

