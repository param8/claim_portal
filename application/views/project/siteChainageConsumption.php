 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<?php
if(isset($_POST['tableHtml']) && isset($_POST['fileName']))
{
    $htmlContent=$_POST['tableHtml'];
    $fileName=$_POST['fileName'].'.xls';
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$fileName");
    echo $htmlContent; die;
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Site Chainage Consumption
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Chainage Consumption</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php  echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
        <div class="box">
          <style>
			.modal-lg {
				width: 900px; !important;
				margin: 50px auto;!important;
			}
		.modal-sm {
				width: 400px; !important;
				margin: 50px auto;!important;
			}
			.badge {
			  position: absolute;
			  top: 0px;
			  right: 1spx;
			  padding: 3px 6px;
			  border-radius: 50%;
			  background-color: red;
			  color: white;
			}		 
           </style>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr style="background-color:#3c8dbc; color:#FFFFFF">
                <th>Project Name&nbsp;&nbsp;===>&nbsp;&nbsp;<?php if(count($this->data['projectStock'])>0) echo $this->data['projectStock'][0]['name']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Project Coordinator&nbsp;&nbsp;===>&nbsp;&nbsp;<?php if(count($this->data['projectStock'])>0) echo $this->data['projectStock'][0]['coordinator'];?></th>
              </tr>
              <tr>
              	<td>
                    <div class="container" style="width:100%; padding-left: 0px;">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="<?php if($this->data['display_flag']=='0') echo "active"; ?>">
                              <a href="#home" role="tab" data-toggle="tab">
                                  <icon class="fa fa-home"></icon> Daily Consumption
                              </a>
                          </li>
                          <li class="<?php if($this->data['display_flag']=='1') echo "active"; ?>">
                              <a href="#homereport" role="tab" data-toggle="tab">
                                  <icon class="fa fa-home"></icon> Daily Consumption Report
                              </a>
                          </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div class="tab-pane fade <?php if($this->data['display_flag']=='0') echo "active in"; ?>" id="home">
                            <form role="form" action="<?php echo base_url('Controller_Project/siteChainageConsumption') ?>" method="post" id="assignStock">
                              <div class="row">
                              	<div class="col-sm-1"><strong>Site</strong></div>
                              	<div class="col-sm-4">
                                	<select name="site_stock" id="site_stock" class="form-control" required style="width:250px" onChange="return getChainageDetailForm(this.value)">
                                    <option value="">--SELECT SITE--</option>
                                    <?php
									$CI=& get_instance();
									$siteDetails=$CI->getSiteDetails();
									$sites['id']="";
									foreach($siteDetails as $sites)
									{
										?>
                                    	<option value="<?php echo $sites['id'];?>" <?php if(isset($this->data['site_stock']) && $this->data['site_stock']==$sites['id']) echo "selected";?>><?php echo $sites['name'];?></option>
                                     <?php
									}
									?>
                                    </select>
                                </div>
                              	<div class="col-sm-1"><strong>Chainage</strong></div>
                              	<div class="col-sm-4" id="chainage_list">
                                	<select name="issue_to_chainage" id="issue_to_chainage" class="form-control" required style="width:250px">
                                    <option value="">--SELECT CHAIN--</option>
                                    <?php
									$CI=& get_instance();
									$siteChainDetails=$CI->getSiteChainDetails($this->data['site_stock']);
									$sites['id']="";
									foreach($siteChainDetails as $Chains)
									{
										?>
                                    	<option value="<?php echo $Chains['site_chainage_id'];?>" <?php if(isset($this->data['site_chainage']) && $this->data['site_chainage']==$Chains['site_chainage_id']) echo "selected";?>><?php echo $Chains['chainage_name'];?></option>
                                     <?php
									}
									?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-xs-6">
                                    <div class="form-group">
                                      <button  class="searchbtn">Search</button>
                                    </div>          <!-- /.box-header -->
                                </div>                              
                                </div>
                              <br>
                            </form>
                            <form role="form" action="<?php echo base_url('Controller_Project/insertDailyConsumption') ?>" method="post" id="assignStock">
                                <table id="manageTable" class="table table-bordered table-striped">
                                    <input type="hidden" name="chainage_id" id="chainage_id"  value="<?php if(isset($this->data['site_chainage'])) echo $this->data['site_chainage'];?>" >
                                    <input type="hidden" name="projectId" id="projectId" value="<?php if(isset($this->data['site_stock'])) echo $this->data['site_stock'];?>">
                                    <?php
                                    if(count($this->data['projectStock1']) > 0 && isset($this->data['site_stock']))
                                    {
										?>
										  <tr style="background-color:#ECE7E7; color:#000000">
											<td style="width:20%"><strong>Site Chainage</strong></td>
											<td style="width:50%"><strong>Item</strong></td>
											<td style="width:10%"><strong>Available (Qty)</strong></td>
											<td style="text-align:center; width:10%"><strong>Consumption&nbsp;(Qty)</strong></td>
											<td style="text-align:center; width:10%"><strong>Consumption&nbsp;(Date)</strong></td>
										   </tr>
										<?php
											foreach($this->data['projectStock1'] as $val)	
											{
												?>
                                                <input type="hidden" name="productIds[]" id="productId" value="<?php echo $val['product_id'];?>">
                                                <input type="hidden" name="pQty_<?php echo $val['product_id'];?>" id="pQty_<?php echo $val['product_id'];?>" value="<?php echo $val['quantity'];?>">
												  <tr>
													<td><?php echo $val['chainage_name']?></td>
													<td><?php echo $val['name']?></td>
													<td><?php echo $val['quantity']?></td>
													<td><input type="text" class="form-control" id="plQty_<?php echo $val['product_id'];?>" name="plQty_<?php echo $val['product_id'];?>" placeholder="" autocomplete="off" onkeypress="return isNumberKey(event,this)" onKeyUp="CheckQuantity(this.value,'<?php echo $val['product_id'];?>')"></td>
													<td><input type="date" class="form-control" id="date_<?php echo $val['product_id'];?>" name="date_<?php echo $val['product_id'];?>" value="<?php echo date('Y-m-d')?>" placeholder="" autocomplete="off"></td>
												   </tr>
												<?php
											}
											?>
                                               <tr>
                                                    <td colspan="6">
                                                      <div class="box-footer" style="text-align:right">
                                                        <button type="submit" class="btn btn-primary" onClick="return confirmMessage()">Save</button>
                                                      </div>
                                                    </td>
                                               </tr>
                                            <?php
                                    }
                                    else
                                    {
                                        ?>
                                              <tr style="border:2px solid #FF0606">
                                                <td style="color:red"><strong>No record found.</strong></td>
                                               </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </form>
                          </div>
                          <div class="tab-pane fade <?php if($this->data['display_flag']=='1') echo "active in"; ?>" id="homereport">
                            <form role="form" action="<?php echo base_url('Controller_Project/siteChainageConsumptionReport') ?>" method="post" id="assignStock">
                            <div class="row">
                            <div class="col-md-3 col-xs-6">
                                <div class="form-group">
                                  <label for="store">Site</label>
                                	<select name="site_stock_1" id="site_stock_1" class="form-control" required  onChange="return getChainageDetailFormReport(this.value)">
                                    <option value="">--SELECT SITE--</option>
                                    <?php
									$CI=& get_instance();
									$siteDetails=$CI->getSiteDetails();
									$sites['id']="";
									foreach($siteDetails as $sites)
									{
										?>
                                    	<option value="<?php echo $sites['id'];?>" <?php if(isset($this->data['site_stock_1']) && $this->data['site_stock_1']==$sites['id']) echo "selected";?>><?php echo $sites['name'];?></option>
                                     <?php
									}
									?>
                                    </select>
                                </div>          <!-- /.box-header -->
                              </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="form-group">
                                  <label for="store">Chainage</label>
                                    <div id="chainage_list_1">
                                	<select name="issue_to_chainage_1" id="issue_to_chainage_1" class="form-control" required >
                                    <option value="">--SELECT CHAIN--</option>
                                    <?php
									$CI=& get_instance();
									$siteChainDetails=$CI->getSiteChainDetails($this->data['site_stock_1']);
									$sites['id']="";
									foreach($siteChainDetails as $Chains)
									{
										?>
                                    	<option value="<?php echo $Chains['site_chainage_id'];?>" <?php if(isset($this->data['site_chainage_1']) && $this->data['site_chainage_1']==$Chains['site_chainage_id']) echo "selected";?>><?php echo $Chains['chainage_name'];?></option>
                                     <?php
									}
									?>
                                    </select>
                                    </div>
                                </div>          <!-- /.box-header -->
                              </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="form-group">
                                  <label for="store">From Date</label>
                                  <input type="date" name="from_date" id="from_date" class="form-control" value="<?php if(isset($this->data['from_date'])) echo $this->data['from_date']; else echo date('Y-m-01');?>">
                                </div>          <!-- /.box-header -->
                              </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="form-group">
                                  <label for="store">To Date</label>
                                  <input type="date" name="to_date" id="to_date" class="form-control" value="<?php if(isset($this->data['to_date'])) echo $this->data['to_date']; else echo date('Y-m-t');?>">
                                </div>          <!-- /.box-header -->
                              </div>
                            <div class="col-md-12 col-xs-6">
                                <div class="form-group" style="text-align:right">
                                  <button onclick="serchCategoryWise()" class="searchbtn">Search</button>
                                </div>          <!-- /.box-header -->
                              </div>
                            </div>
                            </form> 
                            <table  class="table table-bordered table-striped" style="margin:0px;margin-bottom:5px" cellpadding="0" cellspacing="0">
                              <tr>
                                <td>
                                      <?php
                //                      if((array_key_exists('Assign_Stock_Report',$this->session->userdata['ButtonPermisssion']) && in_array('Excel',$this->session->userdata['ButtonPermisssion']['Assign_Stock_Report'])) || $permissionFlag=="Yes")
                //                      {
                                          ?>
                                         <a href="javascript:void(0)" title="Download Excel" onclick="exportTableToExcel('manageTable1', 'Daily Consumption Report')">
                                        <i class="fa fa-file-excel-o" style="font-size: 25px;" aria-hidden="true"></i>
                                    </a>&nbsp;&nbsp;
                                        <?php
                //                        }
                //                      if((array_key_exists('Assign_Stock_Report',$this->session->userdata['ButtonPermisssion']) && in_array('Print',$this->session->userdata['ButtonPermisssion']['Assign_Stock_Report'])) || $permissionFlag=="Yes")
                //                      {
                                        ?>
                                        <a href="javascript:void(0)" title="Print" onclick="printDiv('manageTable1')" >
                                            <i class="fa fa-print" style="font-size: 25px;" aria-hidden="true"></i>
                                       </a>
                                       <?php
                //                      }
                                      ?>
                                 </td>
                              </tr>
                            </table>
                            <div id="manageTable1">
                                <table id="" class="table table-bordered table-striped" border="1px">
                                    <?php
                                    if(count($this->data['consumption_report']) > 0)
                                    {
                                        ?>
                                          <tr style="background-color:#ECE7E7; color:#000000">
                                            <td style="width:40%"><strong>Item</strong></td>
                                            <td style="width:10%"><strong>Available&nbsp;(Qty)</strong></td>
                                            <td style="text-align:center; width:10%"><strong>Consumption&nbsp;(Qty)</strong></td>
                                            <td style="text-align:center; width:10%"><strong>Consumption&nbsp;(Date)</strong></td>
                                            <td style="text-align:center; width:10%"><strong>Entry By</strong></td>
                                           </tr>
                                        <?php
                                            foreach($this->data['consumption_report'] as $key=>$detailsArr)	
                                            {
                                                list($site,$chainage)=explode("_",$key);
                                                ?>
                                                  <tr>
                                                    <td colspan="5"><strong>Site==><?php echo $site?>,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Chainage==><?php echo $chainage?></strong></td>
                                                  </tr>
                                                <?php
                                                foreach($detailsArr as $val)
                                                {
                                                ?>
                                                  <tr>
                                                    <td><?php echo $val['item']?></td>
                                                    <td><?php echo $val['total_qty']?></td>
                                                    <td><?php echo $val['quantity']?></td>
                                                    <td><?php echo date('d-m-Y',strtotime($val['consumption_date']));?></td>
                                                    <td><?php echo $val['username']?></td>
                                                   </tr>
                                                <?php
                                                }
                                            }
                                    }
                                    else
                                    {
                                        ?>
                                              <tr style="border:2px solid #FF0606">
                                                <td style="color:red"><strong>No record found.</strong></td>
                                               </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </div>
                          </div>
                        </div>
                    </div>
                 </td>
               </tr>
             </thead>
           </table>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
    <!-- /.row -->
<form role="form" method="post" id="exceldownload">
  <input type="hidden" name="tableHtml" id="tableHtml">
  <input type="hidden" name="fileName" id="fileName">
</form>
<script>
	function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}
function exportTableToExcel(divName,filename) {
   var tableHtml=document.getElementById(divName).innerHTML; 
   document.getElementById("tableHtml").value=tableHtml;
   document.getElementById("fileName").value=filename;
   document.getElementById('exceldownload').submit();
}
	</script>

<!--<div class="modal fade" tabindex="-1" role="dialog" id="assignModal">
  <div class="modal-dialogAction modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Assign Item</h4>
        <span style=" float: right;margin-bottom: -17px;margin-right: 151px;margin-top: -22px;">
            <select class="form-control" id="assignFor" name="assignFor" onChange="showHide(this.value)">
                <option value="user">Assign to User</option>
                <option value="place">Assign to Place</option>
            </select>
        </span>
      </div>
        <form role="form" action="<?php echo base_url('Controller_Project/AssignToUser') ?>" method="post" id="assignForm">
        <input type="hidden" name="assignForVl" id="assignForVl"  value="user" >
        <input type="hidden" name="projectId" id="projectId">
        <input type="hidden" name="productId" id="productId">
        <input type="hidden" name="pQty" id="pQty">
          <div class="box-body" id="assigntouser">
            <?php echo validation_errors(); ?>
            <div class="row">
           <div class="col-md-6">
            <div class="form-group">
              <label for="fname">Assign To <font color="#FF0004">*</font></label>
              <input type="text" class="form-control" id="assignTo" name="assignTo" placeholder="Assign To" autocomplete="off" required>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label for="username">Department <font color="#FF0004">*</font></label>
              <input type="text" class="form-control" id="Department" name="Department" placeholder="Department" autocomplete="off" required>
            </div>
            </div>

             <div class="col-md-6">
            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="Email" autocomplete="off">
            </div>
            </div>
            <div class="col-md-6">

            <div class="form-group">
              <label for="phone">Mobile No </label>
              <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" autocomplete="off" maxlength="10">
            </div>
            </div>
            </div>
            <div class="row">
           <div class="col-md-6">
            <div class="form-group">
              <label for="gender">Gender</label>
              <div class="radio">
                <label>
                  <input type="radio" name="gender" id="male" value="1" checked>
                  Male
                </label>
                <label>
                  <input type="radio" name="gender" id="female" value="2">
                  Female
                </label>
              </div>
              
              </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                  <label for="fname11">Location </label>
                  <input type="text" class="form-control" id="location" name="location" placeholder="Location" autocomplete="off">
                </div>
                </div>	                        
            </div>
            <div class="row">
            <div class="col-md-6">
            <div class="form-group">
              <label for="username">Quantity <font color="#FF0004">*</font></label>
              <input type="text" class="form-control" onKeyUp="return CheckQuantity(this.value)" id="plQty" name="plQty" placeholder="Quantity" maxlength="3" autocomplete="off" required>
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
              <label for="username">Remark</label>
              <input type="text" class="form-control" id="remark" name="remark" placeholder="Remark" autocomplete="off">
            </div>
            </div>

            </div>
            </div>
<!--          <div class="box-body" style="display:none" id="assigntoplace">
            <?php //echo validation_errors(); ?>
            <div class="row">
           <div class="col-md-6">
            <div class="form-group">
              <label for="fname22">Location <font color="#FF0004">*</font></label>
              <input type="text" class="form-control" id="placeassignTo" name="placeassignTo" placeholder="Location" autocomplete="off" required disabled>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label for="username">Department <font color="#FF0004">*</font></label>
              <input type="text" class="form-control" id="placeDepartment" name="placeDepartment" placeholder="Department" autocomplete="off" required disabled>
            </div>
            </div>

            <div class="col-md-6">

            <div class="form-group">
              <label for="phone">Mobile No</label>
              <input type="number" class="form-control" id="placephone" name="placephone" placeholder="Phone" autocomplete="off" maxlength="10" disabled>
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
              <label for="username">Quantity</label>
              <input type="text" class="form-control" onKeyUp="return CheckQuantity(this.value)" id="plQty" name="plQty" placeholder="Quantity" maxlength="3" autocomplete="off" disabled required>
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
              <label for="username">Remark</label>
              <input type="text" class="form-control" id="placeremark" name="placeremark" placeholder="Remark" autocomplete="off" disabled>
            </div>
            </div>
            </div>
            </div>
          <!-- /.box-body 

          <div class="box-footer" style="text-align:right">
            <button type="submit" class="btn btn-primary" onClick="return checkQuantityOnSave()">Save & Close</button>
          </div>
        </form>
    </div><!-- /.modal-content 
  </div><!-- /.modal-dialog 
</div><!-- /.modal 
--><!-- remove brand modal -->
<!--<div class="modal fade" tabindex="-1" role="dialog" id="removeModal">
  <div class="modal-dialogAction modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Return Product</h4>
      </div>
      <form role="form" action="<?php echo base_url('Controller_Project/ReturnProductByUser') ?>" method="post" id="removeForm">
        <input type="hidden" name="ProId" id="ProId" >
        <input type="hidden" name="ProjId" id="ProjId" >
        <div class="modal-body">
          <div class="box-body">
            <div class="row">
           <div class="col-md-12">
            <div class="form-group">
              <label for="fname">Return By <font color="#FF0004">*</font></label>
                <select class="form-control" id="return_by" name="return_by" required>
                    <option value="">Select</option>
                </select>
            </div>
            </div>
           <div class="col-md-12">
            <div class="form-group">
              <label for="fname">Remark </label>
                <input type="text" name="remark" id="remark" class="form-control">
            </div>
            </div>
            </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Return</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>


    </div><!-- /.modal-content 
  </div><!-- /.modal-dialog 
</div>--><!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="assignPayment">
  <div class="modal-dialogAction modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Payment Details</h4>
      </div>
        <input type="hidden" name="ProId" id="ProId" >
        <input type="hidden" name="ProjId" id="ProjId" >
        <div class="modal-body">
          <div class="box-body" id="innerData">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
        </div>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /.content-wrapper -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script>
$("#hidePopUp").click(function(){
	//alert('Hii deep');
	$("#editModal").modal('hide');
});
</script>

<script type="text/javascript">
var manageTable;

$(document).ready(function() {
  // submit the create from 
  addTotalValue();
});

// edit function
function getChainageDetailForm(siteId)
{ 
  $.ajax({
    url: 'fetchChainageDetailsById/'+siteId,
    type: 'post',
    dataType: 'json',
    success:function(response) {
		document.getElementById('chainage_list').innerHTML=response;
    }
  });
}
function getChainageDetailFormReport(siteId)
{ 
  $.ajax({
    url: 'fetchChainageDetailsReportById/'+siteId,
    type: 'post',
    dataType: 'json',
    success:function(response) {
		document.getElementById('chainage_list_1').innerHTML=response;
    }
  });
}
// remove functions 
function removeFunc(id,pId)
{
  document.getElementById('delete_project_id').value=id;
}
function changeHeaderPart(id)
{
	document.getElementById("changeSubmit").value=1;
	document.getElementById("submitValue").submit();
}

function isNumberKey(evt, obj) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	var value = obj.value;
	var dotcontains = value.indexOf(".") != -1;
	if (dotcontains)
		if (charCode == 46) return false;
	if (charCode == 46) return true;
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}
function CheckQuantity(enterVal,proId)
{
	if(enterVal=="")
	{
		document.getElementById("assignTo_"+proId).required = false;	
		return false;
	}
	if(Number(enterVal))	
	{
		var actualQty=document.getElementById('pQty_'+proId).value;
		if(Number(enterVal) > Number(actualQty))
		{
			alert('Enter quantity should not be greater than available (qty)');
			document.getElementById('plQty_'+proId).value="";
			document.getElementById('plQty_'+proId).focus();
			return false;
		}
		else
		{
			document.getElementById("assignTo_"+proId).required = true;
			return true;
		}
		
	}
	else
	{
		alert('Please enter numeric value only !');
		document.getElementById('plQty_'+proId).value="";
		document.getElementById('plQty_'+proId).focus();
		return false;
	}
}
function confirmMessage()
{
	var chain=document.getElementById('issue_to_chainage').value;
	if(chain=="")
	{
		alert('Please select chainage.');
		return false;
	}
	var pIds=document.getElementsByName('productIds[]');
	var flag=0;
	for(var i=0;i<pIds.length;i++)
	{
		var pId=pIds[i].value;
		if(document.getElementById('plQty_'+pId).value!="")
		{
			flag=1;
		}
	}
		if(flag==0)
		{
			alert('Please fill at least one row to submit this page.');
			return false;
		}
		document.getElementById('chainage_id').value=chain;
}
function submitFunction()
{
	var pIds=document.getElementsByName('panelIds[]');
	var flag=0;
	for(var i=0;i<pIds.length;i++)
	{
		var pId=pIds[i].value;
		if(document.getElementById('cummulative_nos_'+pId).value!="" || document.getElementById('cummulative_area_'+pId).value!="")
		{
			flag=1;
		}
	}
		if(flag==0)
		{
			alert('Please fill at least one row to submit this page.');
			return false;
		}
}

</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
