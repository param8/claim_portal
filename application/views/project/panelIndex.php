 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Panel
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Panel</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; 
		if($this->session->userdata('user_type')!='PM')
		{
		?>

          <button class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Panel</button>
          <br /> <br />
		<?php
		}
		?>
        <div class="box">
          
          <!-- /.box-header -->
          <div class="box-body">
          <div style="text-align: right;padding: 0px;margin-bottom: -12px;">
          <?php
            if($this->session->userdata('user_type')!='PM')
            {
            ?>
    
              <button class="btn btn-primary" data-toggle="modal" data-target="#AllocatePanel">Allocate Panel</button>
              <br /> <br />
            <?php
            }
            ?>
          </div>
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr style="background-color:#07173A; color:#ffffff">
                <th>S.No</th>
                <th>Panel</th>
                <th>Panel Priority</th>
                <th>Unit Area (SQM)</th>
                <th>BOQ</th>
                <th>Action</th>
              </tr>
              </thead>
				<?php
				if(count($this->data['array']) > 0)
				{
					$sno=1;
					foreach($this->data['array'] as $vDetails)	
					{
					?>
                      <tr>
                        <td><?php echo $sno++;?></td>
                        <td><?php echo $vDetails['panel_name']?></td>
                        <td><?php echo $vDetails['panel_priority']?></td>
                        <td><?php echo $vDetails['unit_area']?></td>
                        <td><?php echo $vDetails['b_l']?></td>
                        <td><a onclick="editFunc(<?php echo $vDetails['panel_id']?>)" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                 <button type="button" class="btn btn-danger btn-sm" onclick="removeFunc(<?php echo $vDetails['panel_id']?>)" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>
                        </td>
                       </tr>
                    <?php	
					}
				}
				else
				{
					?>
                      <tr>
                        <td colspan="3"><font color="#FF0000"><strong>No data found.</strong></font></td>
                      </tr>
                    <?php
				}
				?>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- create brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Panel</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Project/createPanel') ?>" method="post" id="">
        <div class="modal-body">
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Panel<font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="Panel_name" name="Panel_name" placeholder="Enter Panel name" autocomplete="off" required>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Priority<font color="#FF0000">*</font></label>
                    <input type="number" class="form-control" id="Priority" name="Priority" placeholder="Priority" autocomplete="off" maxlength="3" required>
                  </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Unit Area (SQM)<font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="unit_area" name="unit_area" placeholder="Enter Unit Area" autocomplete="off" required>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">BOQ</label>
                    <input type="text" class="form-control" id="b_l" name="b_l" placeholder="Enter BOQ" autocomplete="off">
                  </div>
              </div>
           </div>
           <div  class="row">
                  <div class="col-md-12" style="margin-top: 27px; text-align:right">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Save changes</button>
                  </div>
           </div>
          </div>
        </div>
      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- edit brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Panel</h4>
      </div>
      <form role="form" action="<?php echo base_url('Controller_Project/UpdatePanel') ?>" method="post" id="">
      <input type="hidden" name="edit_Panel_id" id="edit_Panel_id" >
        <div class="modal-body">
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Panel<font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="edit_Panel_name" name="edit_Panel_name" placeholder="Enter Panel name" autocomplete="off" required>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Priority<font color="#FF0000">*</font></label>
                    <input type="number" class="form-control" id="edit_Priority" name="edit_Priority" placeholder="Priority" autocomplete="off" maxlength="3" required>
                  </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Unit Area (SQM)<font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="edit_unit_area" name="edit_unit_area" placeholder="Enter Unit Area" autocomplete="off" required>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">BOQ</label>
                    <input type="text" class="form-control" id="edit_b_l" name="edit_b_l" placeholder="Enter BOQ" autocomplete="off">
                  </div>
              </div>
           </div>
           <div  class="row">
                  <div class="col-md-12" style="margin-top: 27px; text-align:right">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Save changes</button>
                  </div>
           </div>
              </div>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- remove brand modal -->
<div class="modal fade " tabindex="-1" role="dialog" id="removeModal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Remove Panel</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Project/removePanel') ?>" method="post" id="removeForm">
      <input type="hidden" name="delete_Panel_id" id="delete_Panel_id">
        <div class="modal-body">
          <p>Do you really want to remove?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger" onClick="refreshPage(0)">Delete</button>
        </div>
      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="AllocatePanel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><strong>Allocate Panel</strong></h4>
      </div>
      <form role="form" action="<?php echo base_url('Controller_Project/allocatePanel') ?>" method="post" id="">
		<input type="hidden" name="fromWHId" id="fromWHId" />
        <div class="modal-body">
		  <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                    <label for="active">Site<font color="#FD0000">*</font></label>
                    <select name="site" id="site" class="form-control" required onChange="return getPanelDetails(this.value)";>
                      <option value="">Select</option>
                    <?php
                    $CI=& get_instance();
                    $siteDetails=$CI->getSiteDetails();
                    
                    foreach($siteDetails as $sites)
                    {
                        ?>
                        <option value="<?php echo $sites['id'];?>" <?php if($this->data['site']==$sites['id']) echo "selected";?>><?php echo $sites['name'];?></option>
                     <?php
                    }
                    ?>
                    </select>
                  </div>
                  <div class="form-group" id="showDataPanel">
                  </div>

              </div>

          </div>

        </div>



        <div class="modal-footer">

          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

          <button type="submit" class="btn btn-primary" onClick="return CheckAllPanel(1);">Save changes</button>

        </div>



      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script>
function refreshPage(Val)
{
	if(Val==1)
	{
		if(document.getElementById('Panel_name').value!="" && document.getElementById('active').value!="")
			location.reload();
		else if(document.getElementById('edit_Panel_name').value!="" && document.getElementById('edit_unit').value!="")
			location.reload();
		else
			return false;
	}
	else
	{
		location.reload();
	}
}

$("#hidePopUp").click(function(){
	//alert('Hii deep');
	$("#editModal").modal('hide');
});
</script>

<script type="text/javascript">
var manageTable;

$(document).ready(function() {
  // submit the create from 
});

// edit function
function editFunc(id)
{ 
  $.ajax({
    url: 'fetchPanelDataById/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
		$("#edit_Panel_id").val(response[0].panel_id);
		$("#edit_Panel_name").val(response[0].panel_name);
		$("#edit_Priority").val(response[0].panel_priority);
		$("#edit_unit_area").val(response[0].unit_area);
		$("#edit_b_l").val(response[0].b_l);
		$('#editModal').modal('show');
    }
  });
}

// remove functions 
function removeFunc(id)
{
  document.getElementById('delete_Panel_id').value=id;
}

function getPanelDetails(id)
{ 
  $.ajax({
    url: 'fetchPanelDataBySiteId/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
		$("#showDataPanel").html(response);
    }
  });
}
function CheckAllPanel(chek)
{
	var panelIdsArr=document.getElementsByName('checkPanelArr[]');	
	var flag=0;
	for(var i=0;i<panelIdsArr.length;i++){
		var pId=panelIdsArr[i].value;
		if(chek==0)
		{
			if(document.getElementById('selectAll').checked==true)
			{
				document.getElementById('checkPanel'+pId).checked=true;
			}
			else
			{
				document.getElementById('checkPanel'+pId).checked=false;
			}
		}
		else
		{
			if(document.getElementById('checkPanel'+pId).checked==true)
				var flag=1;
		}
	}
	if(chek==1)
	{
		if(document.getElementById('site').value==""){
			alert('Please select site.');
			return false;
		}
		if(flag==0)
		{
			alert('Please select at least one checkbox to complete the Process.');
			return false;	
		}
	}
}
</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
