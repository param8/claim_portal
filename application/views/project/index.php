<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Project
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Project</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; 
		if($this->session->userdata('user_type')!='3')
		{
		?>

          <button class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Project</button>
          <br /> <br />
		<?php
		}
		?>
        <div class="box">
          
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Project Name</th>
                <th>Project Coordinator</th>
                <th>Email</th>
                <th>Mobile no</th>
                <th>Address</th>
                <th>Payment</th>
				<?php
                if($this->session->userdata('user_type')!='3')
                {
					?>
                <th>Action</th>
                <?php
				}
				?>
              </tr>
              </thead>
				<?php
				if($this->data['array'] > 0)
				{
					foreach($this->data['array'] as $vDetails)	
					{
					?>
                      <tr>
                        <td><?php echo $vDetails['project_name']?></td>
                        <td><?php echo $vDetails['project_coordinator']?></td>
                        <td><?php echo $vDetails['email_id']?></td>
                        <td><?php echo $vDetails['mobile_no']?></td>
                        <td><?php echo $vDetails['address']?></td>
                        <td><?php echo $vDetails['project_cost']?></td>
						<?php
                        if($this->session->userdata('user_type')!='3')
                        {
                            ?>
                            <td><a onclick="editFunc(<?php echo $vDetails['project_id']?>)" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                     <button type="button" class="btn btn-danger btn-sm" onclick="removeFunc(<?php echo $vDetails['project_id']?>)" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>
                            </td>
                            <?php
                            }
                            ?>
                       </tr>
                    <?php	
					}
				}
				?>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- create brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Project</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Project/create') ?>" method="post" id="">
        <div class="modal-body">
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Project Name <font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="Project_name" name="Project_name" placeholder="Enter Project name" autocomplete="off" required>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Project Coordinator <font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="project_coordinator" name="project_coordinator" placeholder="Enter Project Coordinator" autocomplete="off" required>
                  </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Mobile no <font color="#FF0000">*</font></label>
                    <input type="number" class="form-control" maxlength="12" id="mobile_no" name="mobile_no" placeholder="Enter mobile no" autocomplete="off" required>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Password <font color="red">*</font></label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Enter password" autocomplete="off" required>
                  </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" autocomplete="off">
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Payment</label>
                    <input type="text" class="form-control" id="project_cost" name="project_cost" placeholder="Enter Project Cost" autocomplete="off" >
                  </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                    <label for="brand_name">Address</label>
                    <input type="text" class="form-control" id="address" name="address" placeholder="Enter address" autocomplete="off" >
                  </div>
              </div>
           </div>
        
<!-- #############Add user dropdown #######-->
<!-- ################ -->

<div class="form-group">
          <?php
		  $ci=& get_instance();
		  $userDetails=$ci->getUserTypeDetails();
		  ?>
            <label for="active">User Type <font color="red">*</font></label>
            <select class="form-control" id="user_type_id" name="user_type_id" required>
              <option value="">--Select--</option>
            <?php
			foreach($userDetails as $userType)
			{
				if($userType['user_type_id']=='1')
					continue;
			?>
              <option value="<?php echo $userType['user_type_id'];?>"><?php echo $userType['user_type'];?></option>
            <?php
			}
			?>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" >Save changes</button>
        </div>
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- edit brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Project</h4>
      </div>
      <form role="form" action="<?php echo base_url('Controller_Project/Update') ?>" method="post" id="">
      <input type="hidden" name="edit_project_id" id="edit_project_id" >
        <div class="modal-body">
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Project Name <font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="edit_project_name" name="edit_project_name" placeholder="Enter Project name" autocomplete="off" required>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Project Coordinator <font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="edit_project_coordinator" name="edit_project_coordinator" placeholder="Enter Project Coordinator" autocomplete="off" required>
                  </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Mobile no <font color="#FF0000">*</font></label>
                    <input type="number" class="form-control" maxlength="12" id="edit_mobile_no" name="edit_mobile_no" placeholder="Enter mobile no" autocomplete="off" required>
                  </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label for="edit_brand_name">Password <font color="red">*</font></label>
                <input type="password" class="form-control" id="edit_password" name="edit_password" placeholder="Enter password" autocomplete="off" required>
              </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Email</label>
                    <input type="email" class="form-control" id="edit_email" name="edit_email" placeholder="Enter email" autocomplete="off">
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Payment</label>
                    <input type="text" class="form-control" id="edit_project_cost" name="edit_project_cost" placeholder="Enter Project Cost" autocomplete="off" >
                  </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                    <label for="brand_name">Address</label>
                    <input type="text" class="form-control" id="edit_address" name="edit_address" placeholder="Enter address" autocomplete="off" >
                  </div>
              </div>
    </div>
              <div class="form-group">
          <?php
		  $ci=& get_instance();
		  $userDetails=$ci->getUserTypeDetails();
		  ?>
            <label for="active">User Type <font color="red">*</font></label>
            <select class="form-control" id="edit_user_type_id" name="edit_user_type_id" required>
              <option value="">--Select--</option>
            <?php
			foreach($userDetails as $userType)
			{
        // print_R($userType);
				if($userType['user_type_id']=='1')
					continue;
			?>
              <option value="<?php echo $userType['user_type_id'];?>"><?php echo $userType['user_type'];?></option>
            <?php
			}
			?>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" >Save changes</button>
        </div>
    </div>
    

      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- remove brand modal -->
<div class="modal fade " tabindex="-1" role="dialog" id="removeModal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Remove Project</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Project/remove') ?>" method="post" id="removeForm">
      <input type="hidden" name="delete_project_id" id="delete_project_id">
        <div class="modal-body">
          <p>Do you really want to remove?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger" onClick="refreshPage(0)">Delete</button>
        </div>
      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script>
function refreshPage(Val)
{
	if(Val==1)
	{
		if(document.getElementById('Project_name').value!="" && document.getElementById('active').value!="")
			location.reload();
		else if(document.getElementById('edit_Project_name').value!="" && document.getElementById('edit_unit').value!="")
			location.reload();
		else
			return false;
	}
	else
	{
		location.reload();
	}
}

$("#hidePopUp").click(function(){
	//alert('Hii deep');
	$("#editModal").modal('hide');
});
</script>

<script type="text/javascript">
var manageTable;

$(document).ready(function() {
  // submit the create from 
});

// edit function
function editFunc(id)
{ 
  // alert("hello");
  $.ajax({
    url: 'fetchProjectDataById/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
      // alert(response.user_type_id);
		$("#edit_project_name").val(response.project_name);
		$("#edit_project_coordinator").val(response.project_coordinator);
		$("#edit_email").val(response.email_id);
		$("#edit_mobile_no").val(response.mobile_no);
		$("#edit_password").val(response.password);
		$("#edit_project_cost").val(response.project_cost);
		$("#edit_address").val(response.address);
		$("#edit_user_type_id").val(response.user_type_id);
		$("#edit_project_id").val(response.project_id);
		$('#editModal').modal('show');
    }
  });
}

// remove functions 
function removeFunc(id)
{
  document.getElementById('delete_project_id').value=id;
}


</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>