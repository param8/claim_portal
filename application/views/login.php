<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Start Your Session Here</title>

  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.6 -->

  <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

  <!-- Ionicons -->

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css')?>">

  <!-- iCheck -->

  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/square/blue.css')?>">



 

</head>

<body class="hold-transition skin-blue sidebar-mini" style="background:url('<?php echo base_url('assets/images/5.png');?>') ;background-size:cover;">

<div class="login-box">

      <div style="border-bottom: 1px dashed #cccccc;background: rgba(255, 255, 255, 0.86);">

                  <div class="row">

                 <div class="col-md-6" style="border-right: 1px dashed #cccccc;"> 

                 <p class="pleft">GeoSys India</p>

                  </div>

                  

                   <div class="col-md-6">

                   <p class="pright"> Login to Access Panel</p>

                  </div>

                  </div>

                   </div> 

  <!-- /.login-logo -->

  <div class="login-box-body">

    <p class="login-box-msg">Sign in to start your session</p>



    <form action="<?php echo base_url('auth/login') ?>" method="post">

      <div class="form-group has-feedback">

        <input type="text" class="form-control" name="email" id="email" placeholder="Email/Mobile No" autocomplete="off" style="height: 46px;">

        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

      </div>

      <div class="form-group has-feedback">

        <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" style="height: 46px;">

        <span class="glyphicon glyphicon-lock form-control-feedback"></span>

      </div>
      <div class="form-group has-feedback">
        Stock&nbsp;&nbsp;<input type="radio" class="form-control" name="stock" id="stock" value="stock" checked>
        Claim&nbsp;&nbsp;<input type="radio" class="form-control" name="stock" id="stock" value="claim">
      </div>
      <span style="color:red"><strong><?php if(isset($this->data['errors'])) echo $this->data['errors'];?></strong></span>

      <div class="row">

        <div class="col-xs-8">

          <div class="checkbox icheck">

            <label>

              <input type="checkbox"> Remember Me

            </label>

          </div>

        </div>

        <!-- /.col -->

        <div class="col-xs-4">

          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>

        </div>

        <!-- /.col -->

      </div>

    </form>





  </div>

  <!-- /.login-box-body -->

</div>

<!-- /.login-box -->



<!-- jQuery 2.2.3 -->

<script src="<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js')?>"></script>

<!-- Bootstrap 3.3.6 -->

<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>

<!-- iCheck -->

<script src="<?php echo base_url('assets/plugins/iCheck/icheck.min.js')?>"></script>

<script>

  $(function () {

    $('input').iCheck({

      checkboxClass: 'icheckbox_square-blue',

      radioClass: 'iradio_square-blue',

      increaseArea: '20%' // optional

    });

  });

</script>

</body>

</html>

