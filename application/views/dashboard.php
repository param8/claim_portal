<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
    
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <?php //if($is_admin == true): ?>

        <div class="row">
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
                <h3><?php echo $total_stores ?></h3>

                <h4><b>Total Site</b></h4>
              </div>
              <div class="icon">
                <i class="ion ion-android-home"></i>
              </div>
              <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
       
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3><?php echo $total_category ?></h3>

                <h4><b>Total Category</b></h4>
              </div>
              <div class="icon">
                <i class="fa fa-cubes"></i>
              </div>
              <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3><?php echo $total_products ?></h3>

               <h4><b>Total Factory Stock</b></h4>
              </div>
              <div class="icon">
                <i class="fa fa-cube"></i>
              </div>
              <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <!-- ./col -->
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
                <h3><?php echo $total_users ?></h3>

               <h4><b>Total User</b></h4>
              </div>
              <div class="icon">
                <i class="fa fa-institution"></i>
              </div>
              <a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <div class="panel">

            <div class="row">

                <div class="col-md-6 card-wrapper">
                    <div class="card">
                        <script type="text/javascript">
                          google.charts.load("current", {packages:["corechart"]});
                          google.charts.setOnLoadCallback(drawChart);
                          function drawChart() {
                            var data = google.visualization.arrayToDataTable([
                              ['SITE', 'TOTAL STOCK'],
                              <?php
                              if(count($this->data['total_wh_products'])>0)
                              {
                                  foreach($this->data['total_wh_products'] as $detail)
                                  {
                              ?>
                                    ['<?php echo $detail['WH_NAME'];?>', <?php echo $detail['TOTAL_PRODUCT'];?>],
                              <?php
                                  }
                              }
                              ?>
                            ]);
                    
                            var options = {
                              title: 'Site Total Stock',
                              is3D: true,
                            };
                    
                            var chart = new google.visualization.PieChart(document.getElementById('piechart_1'));
                            chart.draw(data, options);
                          }
                        </script>
                        <div id="piechart_1" style="width: 450px; height: 300px;"></div>
                    </div>
                </div>
                <div class="col-md-6 card-wrapper">
                    <div class="card">
                        <script type="text/javascript">
                          google.charts.load("current", {packages:["corechart"]});
                          google.charts.setOnLoadCallback(drawChart);
                          function drawChart() {
                            var data = google.visualization.arrayToDataTable([
                              ['USER TYPE', 'TOTAL USER'],
                              <?php
                              if(count($this->data['total_ut_users'])>0)
                              {
                                  foreach($this->data['total_ut_users'] as $detail)
                                  {
                              ?>
                                    ['<?php echo $detail['USER_TYPE'];?>', <?php echo $detail['TOTAL_USER'];?>],
                              <?php
                                  }
                              }
                              ?>
                            ]);
                    
                            var options = {
                              title: 'Role (Wise) Total Users',
                              is3D: true,
                            };
                    
                            var chart = new google.visualization.PieChart(document.getElementById('piechart_2'));
                            chart.draw(data, options);
                          }
                        </script>
                        <div id="piechart_2" style="width: 450px; height: 300px;"></div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-6 card-wrapper">
                    <div class="card">
                        <script type="text/javascript">
                          google.charts.load("current", {packages:["corechart"]});
                          google.charts.setOnLoadCallback(drawChart);
                          function drawChart() {
                            var data = google.visualization.arrayToDataTable([
                              ['SITE', 'TOTAL PANEL'],
                              <?php
                              if(count($this->data['total_st_pnl'])>0)
                              {
                                  foreach($this->data['total_st_pnl'] as $detail)
                                  {
                              ?>
                                    ['<?php echo $detail['SITE_NAME'];?>', <?php echo $detail['TOTAL_PANEL'];?>],
                              <?php
                                  }
                              }
                              ?>
                            ]);
                    
                            var options = {
                              title: 'Site Total Panel',
                              is3D: true,
                            };
                    
                            var chart = new google.visualization.PieChart(document.getElementById('piechart_3'));
                            chart.draw(data, options);
                          }
                        </script>
                        <div id="piechart_3" style="width: 450px; height: 300px;"></div>
                    </div>
                </div>
                <div class="col-md-6 card-wrapper">
                    <div class="card">
                        <script type="text/javascript">
                          google.charts.load("current", {packages:["corechart"]});
                          google.charts.setOnLoadCallback(drawChart);
                          function drawChart() {
                            var data = google.visualization.arrayToDataTable([
                              ['LOCATIONS', 'TOTAL USERS'],
                              <?php
                              if(count($this->data['total_site_users'])>0)
                              {
                                  foreach($this->data['total_site_users'] as $detail)
                                  {
                              ?>
                                    ['<?php echo $detail['USER_NAME'];?>', <?php echo $detail['TOTAL_SITE'];?>],
                              <?php
                                  }
                              }
                              ?>
                            ]);
                    
                            var options = {
                              title: 'Co-ordinator in Total Site',
                              is3D: true,
                            };
                    
                            var chart = new google.visualization.PieChart(document.getElementById('piechart_4'));
                            chart.draw(data, options);
                          }
                        </script>
                        <div id="piechart_4" style="width: 450px; height: 300px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
      <?php //endif; ?>
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    $(document).ready(function() {
      $("#dashboardMainMenu").addClass('active');
    }); 
  </script>
