 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Site Chainage
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Site Chainage</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

          <button class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Chain</button>
          <br /> <br />

        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Site Name</th>
                <th>Chain Name</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
              </thead>
                <?php
                    if(count($this->data['chain_details'])>0){
                        foreach($this->data['chain_details'] as $detail)
                        {
                			$buttons = '';
            				$buttons = '<button type="button" class="btn btn-warning btn-sm" onclick="editFunc('.$detail['site_chainage_id'].')" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></button>';
            				$buttons .= ' <button type="button" class="btn btn-danger btn-sm" onclick="removeFunc('.$detail['site_chainage_id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
                            $status = ($detail['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';
                            ?>
                                <tr>
                                    <td><?php echo $detail['name'];?></td> 
                                    <td><?php echo $detail['chainage_name'];?></td> 
                                    <td><?php echo $status;?></td> 
                                    <td><?php echo $buttons;?></td> 
                                </tr>
                            <?php
                        }
                    }
                    
                ?>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- create brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Chain</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Warehouse/createSiteChain') ?>" method="post" id="createForm">

        <div class="modal-body">

          <div class="form-group">
            <label for="brand_name">Chain Name <font color="red">*</font></label>
            <input type="text" class="form-control" id="chain_name" name="chain_name" placeholder="Enter Chain name" autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="brand_name">Site Name <font color="red">*</font></label>
            <select class="form-control" id="site_id" name="site_id">
              <option value="">--SELECT--</option>
				<?php
                foreach($this->data['store_details'] as $sites)
                {
                ?>
                  <option value="<?php echo $sites['id'];?>"><?php echo $sites['name'];?></option>
                <?php
                }
                ?>
            </select>
          </div>
          <div class="form-group">
            <label for="active">Status</label>
            <select class="form-control" id="active" name="active">
              <option value="1">Active</option>
              <option value="2">In-Active</option>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" onClick="return refreshPage('Add')">Save changes</button>
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- edit brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Chain</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Warehouse/updateSiteChain') ?>" method="post" id="createForm">
		<input type="hidden" name="site_chainage_id" id="site_chainage_id" />
        <div class="modal-body">

          <div class="form-group">
            <label for="brand_name">Chain Name <font color="red">*</font></label>
            <input type="text" class="form-control" id="edit_chain_name" name="edit_chain_name" placeholder="Enter Chain name" autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="brand_name">Site Name <font color="red">*</font></label>
            <select class="form-control" id="edit_site_id" name="edit_site_id">
              <option value="">--SELECT--</option>
				<?php
                foreach($this->data['store_details'] as $sites)
                {
                ?>
                  <option value="<?php echo $sites['id'];?>"><?php echo $sites['name'];?></option>
                <?php
                }
                ?>
            </select>
          </div>
          <div class="form-group">
            <label for="active">Status</label>
            <select class="form-control" id="edit_active" name="edit_active">
              <option value="1">Active</option>
              <option value="2">In-Active</option>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" onClick="return refreshPage('Add')">Save changes</button>
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- remove brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Remove Chain</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Warehouse/removeChain') ?>" method="post" id="removeForm">
		<input type="hidden" name="site_chainage_id" id="delete_site_chainage_id" />
        <div class="modal-body">
          <p>Do you really want to remove?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger">Delete</button>
        </div>
      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script type="text/javascript">
var manageTable;

$(document).ready(function() {

  $("#storeNav").addClass('active');

  // initialize the datatable 
  manageTable = $('#manageTable').DataTable({
    dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print'
        ], 
    'order': []
  });

});

// edit function
function editFunc(id)
{ 
  $.ajax({
    url: '<?php base_url();?>fetchStoresChainDataById/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
      $("#edit_chain_name").val(response.chainage_name);
      $("#edit_active").val(response.active);
      $("#edit_site_id").val(response.site_id);
      $("#site_chainage_id").val(response.site_chainage_id);
      $("#editModal").modal('show');
    }
  });
}
function refreshPage(form)
{
	if(form=='Add')
	{
		if(document.getElementById('store_name').value!="" && document.getElementById('site_manager_id').value!="")
		{
		    document.getElementById('createForm').submit();
		}
		else 
		{
			alert('Site,Site Manager can not be left blank.');
			return false;
		}
	}
	else if(form=='Edit')
	{
		if(document.getElementById('edit_store_name').value!="" && document.getElementById('edit_site_manager_id').value!="")
		{
			location.reload();
		}
		else 
		{
			alert('Site,Site Manager can not be left blank.');
			return false;
		}
	}
	else
	{
		location.reload();
	}
}
// remove functions 
function removeFunc(id)
{
  $("#delete_site_chainage_id").val(id);
}


</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
