 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Stock
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Stock</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

          <button class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Stock</button>
          <br /> <br />
		<style>
		.searchbtn {
			margin-top: 24px;
			padding: 5px 20px 5px;
			background: #252525;
			border: 1px solid #252525;
			color: white;
		}		
        </style>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <form name="searchVal" action="<?php echo base_url('Controller_Warehouse/indexSite');?>" method="post">
                <div class="row">
                    <div class="col-md-4 col-xs-4">
                        <div class="form-group">
                          <label for="store">Site </label>
                          <select class="form-control select_group" id="store" name="store"  onchange="getproductdata(this.value)" required>
                            <option value="">Select</option>
                            <?php
						   $CI =& get_instance();
						   $storeData=$CI->getSiteDetails();                             
							foreach($storeData as $val){
                             ?>
                                <option value="<?= $val['id'];?>" <?php if($this->data['site_id']==$val['id']) echo "selected";?>><?= $val['name'];?></option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>          <!-- /.box-header -->
                      </div>
    <!--                  <div class="col-md-4 col-xs-4">
                        <div class="form-group">
                          <label for="store">Stock </label>
                          <select name="product_id" id="product_id"  class="form-control">
                              <option value="">--Select--</option>
                               
                            </select>
                        </div>          <!-- /.box-header
                      </div>
    -->                <div class="col-md-4 col-xs-4">
                        <div class="form-group">
                          <button onClick="serchCategoryWise()" class="searchbtn">Search</button>
                        </div>          <!-- /.box-header -->
                      </div>
                 </div>
              </form>
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Site</th>
                <th>Stock</th>
                <th>Total&nbsp;qty</th>
                <th>Unit</th>
                <th>Action</th>
             </tr>
              </thead>
                <?php
                  $permissionFlag="No";
                  if(in_array('All',$this->session->userdata['ButtonPermisssion']))
                    $permissionFlag="Yes";
                    
                  if((array_key_exists('DPR_(Daily_Progress_Report)',$this->session->userdata['ButtonPermisssion'])) || $permissionFlag=="Yes")
                  {
                      if((array_key_exists('DPR_(Daily_Progress_Report)',$this->session->userdata['ButtonPermisssion']) && in_array('Excel',$this->session->userdata['ButtonPermisssion']['DPR_(Date_Range_Report)'])) || $permissionFlag=="Yes")
                      {
				      }
                      if((array_key_exists('DPR_(Daily_Progress_Report)',$this->session->userdata['ButtonPermisssion']) && in_array('Print',$this->session->userdata['ButtonPermisssion']['DPR_(Daily_Progress_Report)'])) || $permissionFlag=="Yes")
                      {
                      }
                  }
                    if(count($this->data['product_details'])>0){
                        foreach($this->data['product_details'] as $detail)
                        {
                       		//echo'<pre>';print_r($detail);die;
                			if($this->data['site_id']!=$detail['store_id'])
								continue;
                            ?>
                                <tr>
                                    <td width="25%"><?php echo $detail['site_name'];?></td> 
                                    <td width="40%"><?php echo $detail['stock_name'];?></td> 
                                    <td><?php echo $detail['qty'];?></td> 
                                    <td><?php echo $detail['cat_name'];?></td> 
                                    <td width="15%"><button type="button" class="btn btn-warning btn-sm"  onclick="editFunc('<?php echo $detail['id'];?>')" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-danger btn-sm" onclick="removeFunc('<?php echo $detail['id'];?>')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button></td> 
                                </tr>
                            <?php
                        }
                    }
                    
                ?>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- create brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Stock</h4>
      </div>

          <form role="form" action="<?php echo base_url('Controller_Warehouse/createSiteStock') ?>" method="post" id="createForm">

        <div class="modal-body">
          <div class="form-group">
            <label for="active">Site</label><font color="red">*</font>
        	<select name="site" id="site" class="form-control" required onChange="return blankCateDetails()";>
              <option value="">Select</option>
            <?php
			$CI=& get_instance();
			$siteDetails=$CI->getSiteDetails();
			
			foreach($siteDetails as $sites)
			{
				?>
            	<option value="<?php echo $sites['id'];?>" <?php if($this->data['site']==$sites['id']) echo "selected";?>><?php echo $sites['name'];?></option>
             <?php
			}
			?>
            </select>
          </div>
          <div class="form-group">
            <label for="active">Category</label><font color="red">*</font>
            <select class="form-control" id="unit" name="unit" required onChange="return getAllProductByCatId(this.value)">
              <option value="">Select</option>
              <?php
				$ci=&get_instance();
				$catDetails=$ci->getActiveCategroy();
				foreach($catDetails as $cat)
				{
					?>
				  	<option value="<?php echo $cat['id'];?>"><?php echo $cat['name']." (".$cat['unit'].")";?></option>
                  <?php
				}
			  ?>
            </select>
          </div>
          <div class="form-group">
            <label for="brand_name">Name <font color="red">*</font></label>
            <select name="stock_name" id="stock_name" class="form-control" onChange="return showHideProDiv(this.value)" required>
            	<option value="">--Select Stock--</option>
            	<option value="New">Add New Stock</option>
            </select>
            <div style="display:none; margin-top: 10px;" id="pro_div">
            <input type="text" class="form-control" id="stock" name="stock" placeholder="Enter name" autocomplete="off" required>
            </div>
          </div>
          <div class="form-group">
            <label for="brand_name">Quantity<font color="red">*</font></label>
            <input type="number" class="form-control" step="0.01" id="qty" name="qty" placeholder="Enter qunatity" autocomplete="off" required>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" onClick="return refreshPage('Add')">Save changes</button>
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- edit brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Stock</h4>
        </div>

            <form role="form" action="<?php echo base_url('Controller_Warehouse/UpdateSiteStock') ?>" method="post" id="createForm">

          <div class="modal-body">
          <input type="hidden" id="edit_id" name="edit_id" />
            <div class="form-group">
              <label for="active">Site</label><font color="red">*</font>
            <!-- <select name="editsite" id="editsite" class="form-control" required onChange="return blankCateDetails()";> -->
            <input type="hidden" name="editsite" id="editsite" class="form-control" required/>
            <input type="text" name="edit_site" id="edit_site" disabled class="form-control" required/>
            </div>
            <div class="form-group">
              <label for="active">Category</label><font color="red">*</font>
              <!-- <select class="form-control" id="editunit" name="editunit" required onChange="return getAllProductByCatIdforedit(this.value)"> -->
              <input type="hidden" name="editunit" id="editunit" class="form-control" required/>
              <input type="text" name="edit_unit" id="edit_unit" disabled class="form-control" required/>
            </div>
            <div class="form-group">
              <label for="brand_name">Name <font color="red">*</font></label>
              <input type="text" name="editstock_name" id="editstock_name" class="form-control" required/>
            </div>
            <div class="form-group">
              <label for="brand_name">Quantity<font color="red">*</font></label>
              <input type="number" class="form-control" step="0.01" id="editqty" name="editqty" placeholder="Enter qunatity" autocomplete="off" required>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" onClick="return refreshPage('Add')">Save changes</button>
          </div>

        </form>


      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- remove brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Remove Stock</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Warehouse/removeSiteStock') ?>" method="post" id="removeForm">
        <div class="modal-body">
          <p>Do you really want to remove?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger" onClick="refreshPage('Delete')">Delete</button>
        </div>
      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script type="text/javascript">
var manageTable;

$(document).ready(function() {

  $("#storeNav").addClass('active');

  // initialize the datatable 
  manageTable = $('#manageTable').DataTable({
    dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print'
        ], 
    'order': []
  });

});
function showHideProDiv(val)
{
	if(val=='New'){
		document.getElementById('pro_div').style.display="block";
		$('#stock').attr('disabled',false);
	}
	else{
		document.getElementById('pro_div').style.display="none";
		$('#stock').attr('disabled',true);
		
	}
}
function showHideProDivforedit(val)
{
	if(val=='New'){
		document.getElementById('pro_div_edit').style.display="block";
		$('#stock').attr('disabled',false);
	}
	else{
		document.getElementById('pro_div_edit').style.display="none";
		$('#stock').attr('disabled',true);
		
	}
}
// edit function
function editFunc(id)
{ 
  $.ajax({
    url: 'fetchsiteStockDataById/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
      $("#edit_id").val(response.id);
      $("#editsite").val(response.store_id);
      $("#editunit").val(response.category_id);
      $("#edit_site").val(response.store_name);
      $("#edit_unit").val(response.cat_name);
      $("#editstock_name").val(response.name);
      $("#editqty").val(response.qty);
      $("#editModal").modal('show');
    }
  });
}
function blankCateDetails()
{
   document.getElementById('unit').value=''
}

function getAllProductByCatId(id)
{ 
  var site_id=document.getElementById('site').value;
  if(site_id=="")
  {
    alert('Please select site name.');
    document.getElementById('unit').value="";
    return false;
  }

  document.getElementById('pro_div').style.display="none";	
  $.ajax({
    url: 'fetchProductDetailByCatIdForSite/'+id+"/"+site_id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
		var option='<option value="">--Select Stock--</option><option value="New">Add New Stock</option>';
		$("#stock_name").html('');
		for(var i=0;i<response.length;i++)
		{
			option+='<option value="'+response[i]['stock_name']+'">'+response[i]['stock_name']+'</option>';
		}
		$("#stock_name").append(option);    
	}
  });
}
function getAllProductByCatIdforedit(id)
{ 
  //alert(id);
  var site_id=document.getElementById('editsite').value;
  if(site_id=="")
  {
    alert('Please select site name.');
    document.getElementById('editunit').value="";
    return false;
  }

  document.getElementById('pro_div_edit').style.display="none";	
  $.ajax({
    url: 'fetchProductDetailByCatIdForSite/'+id+"/"+site_id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
		var option='<option value="">--Select Stock--</option><option value="New">Add New Stock</option>';
		$("#editstock_name").html('');
		for(var i=0;i<response.length;i++)
		{
			option+='<option value="'+response[i]['stock_name']+'">'+response[i]['stock_name']+'</option>';
		}
		$("#editstock_name").append(option);    
	}
  });
}
// remove functions 
function removeFunc(id)
{
  if(id) {
    $("#removeForm").on('submit', function() {

      var form = $(this);

      // remove the text-danger
      $(".text-danger").remove();

      $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: { factory_stock_id:id }, 
        dataType: 'json',
        success:function(response) {
          if(response.success === true) {
            $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
            '</div>');
            
            // hide the modal
            $("#removeModal").modal('hide');
            window.location.reload();   
          } else {

            $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
            '</div>'); 
          }
        }
      }); 

      return false;
    });
  }
}


</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
