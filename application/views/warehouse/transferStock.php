 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Transfer Stock
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Stock</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
        <?php
        //echo"<pre>";print_r($this->data['site_details']);die;
        ?>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
              <form role="form" action="<?php echo base_url('Controller_Warehouse/transferToWarehouse') ?>" method="post" id="">
                <table id="manageTable" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th colspan="3">Transfer to Site</th>
                  </tr>
                  <tr>
                    <td colspan="3">
                        <select name="transferTo" required class="form-control">
                            <option value="">--Select--</option>
                            <?php
                            foreach($this->data['site_details'] as $site)
                            {
                                ?>
                                    <option value="<?php echo $site['id'];?>"><?php echo $site['name'];?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                  </tr>
                  <tr style="background-color:#3c8dbc; color:#ffffff">
        			<th><input type="checkbox" name="selectAll" onclick="CheckAllProduct(0)" id="selectAll" style="width:20px"/></th>
                    <th>Name</th>
                    <th>Total qty</th>
                    <th>Stock at site</th>
                    <th>Stock in factory</th>
                    <th>Unit</th>
                    <th>Transfer Qty</th>
                  </tr>
                  </thead>
                    <?php
                        if(count($this->data['product_details'])>0){
                            foreach($this->data['product_details'] as $detail)
                            {
                                $actQty=round(($detail['qty']-$detail['used_qty']),2);
                                ?>
                                    <tr>
                    					<td><input type="checkbox" onclick="enableDisabledRemark('<?php echo $detail['factory_stock_id'];?>')" name="checkProduct<?php echo $detail['factory_stock_id'];?>" id="checkProduct<?php echo $detail['factory_stock_id'];?>" style="width:20px"/>
                    					<input type="hidden" name="checkProductsArr[]" value="<?php echo $detail['factory_stock_id'];?>" /></td>
                    					<input type="hidden" name="ProductsName<?php echo $detail['factory_stock_id'];?>" value="<?php echo $detail['stock_name']."@_@".$detail['cat_id'];?>" /></td>
                                        <td><?php echo $detail['stock_name'];?></td> 
                                        <td><?php echo $detail['qty'];?></td> 
                                        <td><?php if($detail['used_qty']!=0) echo ($detail['used_qty']); else echo "0";?></td> 
                                        <td><?php if($detail['used_qty']!=0) echo round(($detail['qty']-$detail['used_qty']),2); else echo $detail['qty'];?></td> 
                                        <td><?php echo $detail['cat_name'];?></td> 
                                        <td><input type="number" step="0.00" disabled onChange="return checkQtyExist(this.value,'<?php echo $actQty;?>','<?php echo $detail['factory_stock_id'];?>')" name="assign_qty<?php echo $detail['factory_stock_id'];?>" class="form-control" id="assign_qty<?php echo $detail['factory_stock_id'];?>" min="1" required /></td> 
                                    </tr>
                                <?php
                            }
                        }
                        
                    ?>
                    <tr>
                        <td style="text-align:right" colspan="7">
                            <button type="submit" class="btn btn-primary" onClick="return CheckAllProduct(1);">Transfer</button>
                        </td>
                    </tr>
                </table>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<script>
function CheckAllProduct(chek)
{
	var proIdsArr=document.getElementsByName('checkProductsArr[]');	
	var flag=0;
	for(var i=0;i<proIdsArr.length;i++){
		var pId=proIdsArr[i].value;
		if(chek==0)
		{
			if(document.getElementById('selectAll').checked==true)
			{
				document.getElementById('checkProduct'+pId).checked=true;
				document.getElementById('assign_qty'+pId).disabled=false;
			}
			else
			{
				document.getElementById('checkProduct'+pId).checked=false;
				document.getElementById('assign_qty'+pId).disabled=true;
			}
		}
		else
		{
			if(document.getElementById('checkProduct'+pId).checked==true)
				var flag=1;
		}
	}
	if(chek==1)
	{
		if(flag==0)
		{
			alert('Please select at least one checkbox to complete the Process.');
			return false;	
		}
	}
}
function checkQtyExist(enterVal,actualVal,pId)
{
  if(enterVal=="")  
  {
	document.getElementById('assign_qty'+pId).value=""
  }
  else{
        if(Number(enterVal) > Number(actualVal))
        {
            alert('Enter quantity should not be greater than actual quantity');
			document.getElementById('assign_qty'+pId).value=""
            return false;
        }
  }
}
function enableDisabledRemark(pId)
{
	if(document.getElementById('checkProduct'+pId).checked==true)
	{
    	document.getElementById('assign_qty'+pId).disabled=false;
	}
	else
	{
    	document.getElementById('assign_qty'+pId).disabled=true;
	}
}
</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
