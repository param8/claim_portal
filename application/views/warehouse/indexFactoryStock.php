 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Stock
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Stock</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

          <button class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Stock</button>
          <br /> <br />

        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Name</th>
                <th>Total qty</th>
                <th>Unit</th>
<!--                  <th>Action</th>
-->              </tr>
              </thead>
                <?php
                    if(count($this->data['product_details'])>0){
                        foreach($this->data['product_details'] as $detail)
                        {
                			$buttons = '';
            				//$buttons = '<button type="button" class="btn btn-warning btn-sm" onclick="editFunc('.$detail['factory_stock_id'].')" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></button>';
            				//$buttons .= ' <button type="button" class="btn btn-danger btn-sm" onclick="removeFunc('.$detail['factory_stock_id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
                            ?>
                                <tr>
                                    <td><?php echo $detail['stock_name'];?></td> 
                                    <td><?php echo $detail['qty'];?></td> 
                                    <td><?php echo $detail['cat_name'];?></td> 
                                    <td><?php echo $buttons;?></td> 
                                </tr>
                            <?php
                        }
                    }
                    
                ?>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- create brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Stock</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Warehouse/createFactoryStock') ?>" method="post" id="createForm">

        <div class="modal-body">
          <div class="form-group">
            <label for="active">Category</label><font color="red">*</font>
            <select class="form-control" id="unit" name="unit" required onChange="return getAllProductByCatId(this.value)">
              <option value="">Select</option>
              <?php
				$ci=&get_instance();
				$catDetails=$ci->getActiveCategroy();
				foreach($catDetails as $cat)
				{
					?>
				  	<option value="<?php echo $cat['id'];?>"><?php echo $cat['name']." (".$cat['unit'].")";?></option>
                  <?php
				}
			  ?>
            </select>
          </div>
          <div class="form-group">
            <label for="brand_name">Name <font color="red">*</font></label>
            <select name="stock_name" id="stock_name" class="form-control" onChange="return showHideProDiv(this.value)" required>
            	<option value="">--Select Stock--</option>
            	<option value="New">Add New Stock</option>
            </select>
            <div style="display:none; margin-top: 10px;" id="pro_div">
            <input type="text" class="form-control" id="stock" name="stock" placeholder="Enter name" autocomplete="off" required>
            </div>
          </div>
          <div class="form-group">
            <label for="brand_name">Quantity<font color="red">*</font></label>
            <input type="number" class="form-control" step="0.01" id="qty" name="qty" placeholder="Enter qunatity" autocomplete="off" required>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" onClick="return refreshPage('Add')">Save changes</button>
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- edit brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Stock</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Warehouse/UpdateFactoryStock') ?>" method="post" id="createForm">
        <input type="hidden" class="form-control" id="factory_stock_id" name="factory_stock_id" >
        <div class="modal-body">

          <div class="form-group">
            <label for="brand_name">Name <font color="red">*</font></label>
            <input type="text" class="form-control" id="edit_stock" name="edit_stock" placeholder="Enter name" autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="brand_name">Quantity<font color="red">*</font></label>
            <input type="number" class="form-control" step="0.01" id="edit_qty" name="edit_qty" placeholder="Enter qunatity" autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="active">Unit</label><font color="red">*</font>
            <select class="form-control" id="edit_unit" name="edit_unit" required>
              <option value="">Select</option>
              <option value="Number">Numbers (N)</option>
              <option value="Kilogram">Kilogram (Kg)</option>
              <option value="Meter">Meter (M)</option>
              <option value="Centimeter">Centimeter (Cm)</option>
              <option value="Tonne">Tonne (T)</option>
              <option value="Litre">Litre (L)</option>
              <option value="SQM">Square meter (SQM)</option>
              <option value="CUM">Cubic meter (CUM)</option>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" onClick="return refreshPage('Add')">Modify</button>
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- remove brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Remove Stock</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Warehouse/removeStock') ?>" method="post" id="removeForm">
        <div class="modal-body">
          <p>Do you really want to remove?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger" onClick="refreshPage('Delete')">Delete</button>
        </div>
      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script type="text/javascript">
var manageTable;

$(document).ready(function() {

  $("#storeNav").addClass('active');

  // initialize the datatable 
  manageTable = $('#manageTable').DataTable({
    dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print'
        ], 
    'order': []
  });

});
function showHideProDiv(val)
{
	if(val=='New'){
		document.getElementById('pro_div').style.display="block";
		$('#stock').attr('disabled',false);
	}
	else{
		document.getElementById('pro_div').style.display="none";
		$('#stock').attr('disabled',true);
		
	}
}
// edit function
function editFunc(id)
{ 
  $.ajax({
    url: 'fetchStockDataById/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
      $("#edit_stock").val(response.stock_name);
      $("#edit_qty").val(response.qty);
      $("#edit_unit").val(response.unit);
      $("#factory_stock_id").val(response.factory_stock_id);
      $("#editModal").modal('show');
    }
  });
}
function getAllProductByCatId(id)
{ 
  document.getElementById('pro_div').style.display="none";	
  $.ajax({
    url: 'fetchProductDetailByCatId/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
		var option='<option value="">--Select Stock--</option><option value="New">Add New Stock</option>';
		$("#stock_name").html('');
		for(var i=0;i<response.length;i++)
		{
			option+='<option value="'+response[i]['stock_name']+'">'+response[i]['stock_name']+'</option>';
		}
		$("#stock_name").append(option);    
	}
  });
}
// remove functions 
function removeFunc(id)
{
  if(id) {
    $("#removeForm").on('submit', function() {

      var form = $(this);

      // remove the text-danger
      $(".text-danger").remove();

      $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: { factory_stock_id:id }, 
        dataType: 'json',
        success:function(response) {
          if(response.success === true) {
            $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
            '</div>');
            
            // hide the modal
            $("#removeModal").modal('hide');
            window.location.reload();   
          } else {

            $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
            '</div>'); 
          }
        }
      }); 

      return false;
    });
  }
}


</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
