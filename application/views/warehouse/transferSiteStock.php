<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Transfer Site Stock
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Stock</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
        <?php
        //echo"<pre>";print_r($this->data['site_details']);die;
        ?>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
              <form role="form" action="<?php echo base_url('Controller_Warehouse/transfersiteToWarehouse') ?>" method="post" id="">
                <table id="manageTable" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th colspan="3">Transfer From Site</th>
                    <th colspan="3">Transfer to Site</th>
                  </tr>
                  <tr>
                  <td colspan="3">
                        <select name="transferfrom" id="transferfrom"  onchange="getdata(this.value),sitedata(this.value)" required class="form-control">
                            <option value="">--Select--</option>
                            <?php
                            foreach($this->data['site_details'] as $site)
                            {
                                ?>
                                    <option value="<?php echo $site['id'];?>"><?php echo $site['name'];?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td colspan="3">
                        <select name="transferTo" id="transferTo" required class="form-control">
                          <option value="">--Select--</option>
                           
                        </select>
                    </td>
                  </tr>
                  <tr style="background-color:#3c8dbc; color:#ffffff">
        			<th><input type="checkbox" name="selectAll" onclick="CheckAllProduct(0)" id="selectAll" style="width:20px"/></th>
                    <th>Name</th>
                    <th>Total qty</th>
                    <th>qty</th>
                    <!-- <th>Stock in factory</th> -->
                    <th>Unit</th>
                    <th>Transfer Qty</th>
                  </tr>
                  </thead>
                   <tbody id="siteid">
                   <tr>
                    <td colspan="7" style="text-align:left"><font color="#FF0000"><strong>No data found.</strong></font></td>
                    </tr>
                    </tbody>
                </table>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<script>
function CheckAllProduct(chek)
{
	var proIdsArr=document.getElementsByName('checkProductsArr[]');	
	var flag=0;
	for(var i=0;i<proIdsArr.length;i++){
		var pId=proIdsArr[i].value;
		if(chek==0)
		{
			if(document.getElementById('selectAll').checked==true)
			{
				document.getElementById('checkProduct'+pId).checked=true;
				document.getElementById('assign_qty'+pId).disabled=false;
			}
			else
			{
				document.getElementById('checkProduct'+pId).checked=false;
				document.getElementById('assign_qty'+pId).disabled=true;
			}
		}
		else
		{
			if(document.getElementById('checkProduct'+pId).checked==true)
				var flag=1;
		}
	}
	if(chek==1)
	{
		if(flag==0)
		{
			alert('Please select at least one checkbox to complete the Process.');
			return false;	
		}
	}
}
function checkQtyExist(enterVal,actualVal,pId)
{
  if(enterVal=="")  
  {
	document.getElementById('assign_qty'+pId).value=""
  }
  else{
        if(Number(enterVal) > Number(actualVal))
        {
            alert('Enter quantity should not be greater than actual quantity');
			document.getElementById('assign_qty'+pId).value=""
            return false;
        }
  }
}
function enableDisabledRemark(pId)
{
	if(document.getElementById('checkProduct'+pId).checked==true)
	{
    	document.getElementById('assign_qty'+pId).disabled=false;
	}
	else
	{
    	document.getElementById('assign_qty'+pId).disabled=true;
	}
}
</script>
<script>
 function getdata(id){
  $.ajax({
    url: 'gettransfersiteStockData/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
      //alert(response);
      $("#siteid").html(response);
    }
  });

}</script>
<script>
 function sitedata(id){
  $.ajax({
    url: 'getStoresSiteData',
    method:"POST",
    data:{id:id},
    success:function(data)
    {
     $('#transferTo').html(data);
    }
	
   });
}
</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
