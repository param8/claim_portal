 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Vendor
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Vendor</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

          <button class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Vendor</button>
          <br /> <br />

        <div class="box">
          
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Vendor Name</th>
                <th>Vendor Code</th>
                <th>Email</th>
                <th>Mobile no</th>
                <th>Address</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
              </thead>
				<?php
				if($this->data['array'] > 0)
				{
					foreach($this->data['array'] as $vDetails)	
					{
					?>
                      <tr>
                        <td><?php echo $vDetails['vendor_name']?></td>
                        <td><?php echo $vDetails['vendor_code']?></td>
                        <td><?php echo $vDetails['email']?></td>
                        <td><?php echo $vDetails['mobile_no']?></td>
                        <td><?php echo $vDetails['address']?></td>
                        <td><?php echo $vDetails['status']?></td>
                        <td><a onclick="editFunc(<?php echo $vDetails['vendor_id']?>)" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>

    			 <button type="button" class="btn btn-danger btn-sm" onclick="removeFunc(<?php echo $vDetails['vendor_id']?>)" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button></td>
                       </tr>
                    <?php	
					}
				}
				?>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- create brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Vendor</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Vendor/create') ?>" method="post" id="">
        <div class="modal-body">
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Vendor Name <font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="Vendor_name" name="Vendor_name" placeholder="Enter Vendor name" autocomplete="off" required>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Vendor code</label>
                    <input type="text" class="form-control" id="vendor_code" name="vendor_code" placeholder="Enter Vendor code" autocomplete="off">
                  </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Vendor email" autocomplete="off">
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Mobile no <font color="#FF0000">*</font></label>
                    <input type="number" class="form-control" maxlength="12" id="mobile_no" name="mobile_no" placeholder="Enter Vendor mobile no" autocomplete="off" required>
                  </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Address</label>
                    <input type="text" class="form-control" id="address" name="address" placeholder="Enter Vendor address" autocomplete="off" >
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="active">Status</label>
                    <select class="form-control" id="status" name="status" required>
                      <option value="Active">Active</option>
                      <option value="Inactive">Inactive</option>
                    </select>
                  </div>
              </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- edit brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Vendor</h4>
      </div>
      <form role="form" action="<?php echo base_url('Controller_Vendor/Update') ?>" method="post" id="updateForm">
      <input type="hidden" name="vendor_id" id="vendor_id">
        <div class="modal-body">
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Vendor Name <font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="Vendor_name1" name="Vendor_name1" placeholder="Enter Vendor name" autocomplete="off" required>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Vendor code</label>
                    <input type="text" class="form-control" id="vendor_code1" name="vendor_code1" placeholder="Enter Vendor code" autocomplete="off">
                  </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Email</label>
                    <input type="email" class="form-control" id="email1" name="email1" placeholder="Enter Vendor email" autocomplete="off">
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Mobile no <font color="#FF0000">*</font></label>
                    <input type="number" class="form-control" maxlength="12" id="mobile_no1" name="mobile_no1" placeholder="Enter Vendor mobile no" autocomplete="off" required>
                  </div>
              </div>
          </div>
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Address</label>
                    <input type="text" class="form-control" id="address1" name="address1" placeholder="Enter Vendor address" autocomplete="off" >
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="active">Status</label>
                    <select class="form-control" id="status1" name="status1" required>
                      <option value="Active">Active</option>
                      <option value="Inactive">Inactive</option>
                    </select>
                  </div>
              </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>

      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- remove brand modal -->
<div class="modal fade " tabindex="-1" role="dialog" id="removeModal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Remove Vendor</h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Vendor/remove') ?>" method="post" id="removeForm">
      <input type="hidden" name="delete_vendor_id" id="delete_vendor_id">
        <div class="modal-body">
          <p>Do you really want to remove?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger" onClick="refreshPage(0)">Delete</button>
        </div>
      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script>
function refreshPage(Val)
{
	if(Val==1)
	{
		if(document.getElementById('Vendor_name').value!="" && document.getElementById('active').value!="")
			location.reload();
		else if(document.getElementById('edit_Vendor_name').value!="" && document.getElementById('edit_unit').value!="")
			location.reload();
		else
			return false;
	}
	else
	{
		location.reload();
	}
}

$("#hidePopUp").click(function(){
	//alert('Hii deep');
	$("#editModal").modal('hide');
});
</script>

<script type="text/javascript">
var manageTable;

$(document).ready(function() {
  // submit the create from 
});

// edit function
function editFunc(id)
{ 
  $.ajax({
    url: 'fetchVendorDataById/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
		$("#Vendor_name1").val(response.vendor_name);
		$("#vendor_code1").val(response.vendor_code);
		$("#email1").val(response.email);
		$("#mobile_no1").val(response.mobile_no);
		$("#address1").val(response.address);
		$("#status1").val(response.status);
		$("#vendor_id").val(response.vendor_id);
		$('#editModal').modal('show');
    }
  });
}

// remove functions 
function removeFunc(id)
{
  document.getElementById('delete_vendor_id').value=id;
}


</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
