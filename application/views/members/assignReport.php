
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>var $j = jQuery.noConflict(true);</script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Assign Products Report
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Report</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
		<?php
		//cho"<pre>";print_r($this->data['user_data']);die;
		
		?>
        <div class="box">
        <form name="searchVal" action="<?php echo base_url();?>Controller_Members/Report" method="post">
        <input type="hidden" name="hiddenSearch" id="hiddenSearch" value="<?php if(isset($_REQUEST['searchCat'])) echo $_REQUEST['searchCat'];?>">
        <div class="col-md-6 col-xs-6">
            <div class="form-group">
              <label for="store">Category</label>
              <select class="form-control select_group" id="searchCat" name="searchCat" required>
              	<option value="">Select</option>
              </select>
            </div>          <!-- /.box-header -->
          </div>
        <div class="col-md-6 col-xs-6">
            <div class="form-group" style="margin-top: 29px;">
              <button onClick="serchCategoryWise()">Search</button>
            </div>          <!-- /.box-header -->
          </div>
          </form>
          <!-- /.box-header -->
          <style>
			.modal-dialog {
				width: 80% !important;
				margin: 30px auto !important;
				}		  
			.modal-dialogAction {
				width: 40% !important;
				margin: 30px auto !important;
				}		  
			</style>
          <div class="box-body">
            <table id="example" class="table table-bordered table-striped">
              <thead>
                  <tr bgcolor="#3c8dbc" style="color:#ffffff">
                    <th rowspan="2" width="10px">S.No</th>
                    <th rowspan="2">Categroy</th>
                    <th colspan="3" style="text-align:center">Total Assign</th>
                  </tr>
                  <tr bgcolor="#D2D2D2" style="color:#000000">
                    <td style="text-align:center"><strong>Issue</strong></td>
                    <td style="text-align:center"><strong>Return(Active)</strong></td>
                    <td style="text-align:center"><strong>Return(Scrap)</strong></td>
                  </tr>
              </thead>
              <?php 
			  //echo"<pre>";print_r($this->data['user_data']);die;
			  $sno=1;
			  foreach($this->data['user_data'] as $MainKey=>$subDetails)
			  {
				  list($catId,$catName)=explode("_",$MainKey);
					?>
                      <tr>
                        <td><?php echo $sno++;?></td>
                        <td><?php echo $catName;?></td>
                        <td style="text-align:center"><?php  if(isset($subDetails['Issue'])) echo '<strong><a href="#" data-toggle="modal" data-target="#myModal'.$catId.'_Issue"><font color="#009B2A">'.($subDetails['Issue']).'</font></a></strong>'; else echo '<font color="#FF0000"><strong>0</strong></font>';?>
                              <!-- Trigger the modal with a button -->
                              <!-- Modal -->
                              <div class="modal fade" id="myModal<?php echo $catId.'_Issue';?>" role="dialog">
                                <div class="modal-dialog">
                                
                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header" style="background:#5bc0de">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title"><font color="#ffffff"><strong><?php echo $catName;?></strong></font></h4>
                                    </div>
                                    <?php
										$CI     = & get_instance();
										$result = $CI->GetCategoryWiseProductReportByCatId($catId,'Issue');	
										//echo"<pre>";print_r($result);						
									?>
                                    <div class="modal-body">
                                        <table class="table table-bordered">
                                            <thead>
                                              <tr style="background:#B2B2B2; color:#ffffff">
                                                <th>Item</th>
                                                <th>Warehouse</th>
                                                <th style="text-align:center">Assign to user</th>
                                              </tr>
                                            </thead>
                                            <tbody>
											<?php
												$sno=1;
                                                foreach($result as $key=>$value)
												{	
													list($itm,$warehse)=explode("_",$key);				
                                            ?>
                                                  <tr>
                                                    <td style="text-align:left"><?php echo $itm;?></td>
                                                    <td style="text-align:left"><?php echo $warehse;?></td>
                                                    <td style="text-align:left" valign="top">
                                                        <table class="table table-bordered">
                                                        <thead>
                                                          <tr style="background:#458e92; color:#ffffff">
                                                            <th>S.No</th>
                                                            <th>User</th>
                                                            <th>Department</th>
                                                            <th>Mobile&nbsp;No</th>
                                                            <th>Email&nbsp;</th>
                                                            <th>Qty</th>
                                                            <th>Assign&nbsp;Date</th>
                                                            <th>Location</th>
                                                            <th>Assign&nbsp;For</th>
                                                            <th>Status</th>
                                                            <th>Remark</th>
                                                          </tr>
                                                        </thead>
														<?php
                                                            foreach($value as $val)
                                                            {	
                                                            ?>
                                                              <tr>
                                                                    <td><?php echo $sno++;?></td>
                                                                    <td><?php echo $val['assign_to'];?></td>
                                                                    <td><?php echo $val['department'];?></td>
                                                                    <td><?php echo $val['phone'];?></td>
                                                                    <td><?php echo $val['email'];?></td>
                                                                    <td><?php echo $val['quantity'];?></td>
                                                                    <td><?php echo date('Y-m-d',strtotime($val['entry_date']));?></td>
                                                                    <td><?php echo $val['location'];?></td>
                                                                    <td><?php echo $val['assign_for'];?></td>
                                                                    <td><?php echo $val['return_status'];?></td>
                                                                    <td><?php echo $val['remark'];?></td>
                                                              </tr>
                                                            <?php
                                                            }
                                                            ?>
                                                        </table>
                                                    </td>
                                                  </tr>
											<?php
											  }					
                                            ?>
                                            </tbody>
                                          </table>
                                      </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                  </div>
                                  
                                </div>
                              </div>
                        </td>
                        <td style="text-align:center"><?php  if(isset($subDetails['Active']))  echo '<strong><a href="#" data-toggle="modal" data-target="#myModal'.$catId.'_Active"><font color="#009B2A">'.$subDetails['Active'].'</font></a></strong>'; else echo '<font color="#FF0000"><strong>0</strong></font>';?>
                              <div class="modal fade" id="myModal<?php echo $catId.'_Active';?>" role="dialog">
                                <div class="modal-dialog">
                                
                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header" style="background:#5bc0de">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title"><font color="#ffffff"><strong><?php echo $catName;?></strong></font></h4>
                                    </div>
                                    <?php
										$CI     = & get_instance();
										$result = $CI->GetCategoryWiseProductReportByCatId($catId,'Active');	
									?>
                                    <div class="modal-body">
                                        <table class="table table-bordered">
                                            <thead>
                                              <tr style="background:#B2B2B2; color:#ffffff">
                                                <th>Item</th>
                                                <th>Warehouse</th>
                                                <th style="text-align:center">Assign to user</th>
                                              </tr>
                                            </thead>
                                            <tbody>
											<?php
												$sno=1;
                                                foreach($result as $key=>$value)
												{	
													list($itm,$warehse)=explode("_",$key);				
                                            ?>
                                                  <tr>
                                                    <td style="text-align:left"><?php echo $itm;?></td>
                                                    <td style="text-align:left"><?php echo $warehse;?></td>
                                                    <td style="text-align:left" valign="top">
                                                        <table class="table table-bordered">
                                                        <thead>
                                                          <tr style="background:#458e92; color:#ffffff">
                                                            <th>S.No</th>
                                                            <th>User</th>
                                                            <th>Department</th>
                                                            <th>Mobile&nbsp;No</th>
                                                            <th>Email&nbsp;</th>
                                                            <th>Qty</th>
                                                            <th>Assign&nbsp;Date</th>
                                                            <th>Return&nbsp;Date</th>
                                                            <th>Location</th>
                                                            <th>Status</th>
                                                            <th>Remark</th>
                                                          </tr>
                                                        </thead>
														<?php
                                                            foreach($value as $val)
                                                            {	
                                                            ?>
                                                              <tr>
                                                                    <td><?php echo $sno++;?></td>
                                                                    <td><?php echo $val['assign_to'];?></td>
                                                                    <td><?php echo $val['department'];?></td>
                                                                    <td><?php echo $val['phone'];?></td>
                                                                    <td><?php echo $val['email'];?></td>
                                                                    <td><?php echo $val['quantity'];?></td>
                                                                    <td><?php echo date('Y-m-d',strtotime($val['entry_date']));?></td>
                                                                    <td><?php echo $val['return_date'];?></td>
                                                                    <td><?php echo $val['location'];?></td>
                                                                    <td><?php echo $val['return_status'];?></td>
                                                                    <td><?php echo $val['remark'];?></td>
                                                              </tr>
                                                            <?php
                                                            }
                                                            ?>
                                                        </table>
                                                    </td>
                                                  </tr>
											<?php
											  }					
                                            ?>
                                            </tbody>
                                          </table>
                                      </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                  </div>
                                  
                                </div>
                              </div>
                        </td>
                        <td style="text-align:center"><?php  if(isset($subDetails['Scrap']))  echo '<strong><a href="#" data-toggle="modal" data-target="#myModal'.$catId.'_Scrap"><font color="#009B2A">'.$subDetails['Scrap'].'</font></a></strong>'; else echo '<font color="#FF0000"><strong>0</strong></font>';?>
                              <div class="modal fade" id="myModal<?php echo $catId.'_Scrap';?>" role="dialog">
                                <div class="modal-dialog">
                                
                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header" style="background:#5bc0de">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title"><font color="#ffffff"><strong><?php echo $catName;?></strong></font></h4>
                                    </div>
                                    <?php
										$CI     = & get_instance();
										$result = $CI->GetCategoryWiseProductReportByCatId($catId,'Scrap');	
										//echo"<pre>";print_r($oldDetailsArray);						
									?>
                                    <div class="modal-body">
                                        <table class="table table-bordered">
                                            <thead>
                                              <tr style="background:#B2B2B2; color:#ffffff">
                                                <th>Item</th>
                                                <th>Warehouse</th>
                                                <th style="text-align:center">Assign to user</th>
                                              </tr>
                                            </thead>
                                            <tbody>
											<?php
												$sno=1;
                                                foreach($result as $key=>$value)
												{	
													list($itm,$warehse)=explode("_",$key);				
                                            ?>
                                                  <tr>
                                                    <td style="text-align:left"><?php echo $itm;?></td>
                                                    <td style="text-align:left"><?php echo $warehse;?></td>
                                                    <td style="text-align:left" valign="top">
                                                        <table class="table table-bordered">
                                                        <thead>
                                                          <tr style="background:#458e92; color:#ffffff">
                                                            <th>S.No</th>
                                                            <th>User</th>
                                                            <th>Department</th>
                                                            <th>Mobile&nbsp;No</th>
                                                            <th>Email&nbsp;</th>
                                                            <th>Qty</th>
                                                            <th>Assign&nbsp;Date</th>
                                                            <th>Return&nbsp;Date</th>
                                                            <th>Location</th>
                                                            <th>Status</th>
                                                            <th>Remark</th>
                                                          </tr>
                                                        </thead>
														<?php
                                                            foreach($value as $val)
                                                            {	
                                                            ?>
                                                              <tr>
                                                                    <td><?php echo $sno++;?></td>
                                                                    <td><?php echo $val['assign_to'];?></td>
                                                                    <td><?php echo $val['department'];?></td>
                                                                    <td><?php echo $val['phone'];?></td>
                                                                    <td><?php echo $val['email'];?></td>
                                                                    <td><?php echo $val['quantity'];?></td>
                                                                    <td><?php echo date('Y-m-d',strtotime($val['entry_date']));?></td>
                                                                    <td><?php echo $val['return_date'];?></td>
                                                                    <td><?php echo $val['location'];?></td>
                                                                    <td><?php echo $val['return_status'];?></td>
                                                                    <td><?php echo $val['remark'];?></td>
                                                              </tr>
                                                            <?php
                                                            }
                                                            ?>
                                                        </table>
                                                    </td>
                                                  </tr>
											<?php
											  }					
                                            ?>
                                            </tbody>
                                          </table>
                                      </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                  </div>
                                  
                                </div>
                              </div>
                        </td>
                      </tr>
              <?php
			  }
			  ?>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Assing brand modal -->
<!-- remove brand modal -->



<script type="text/javascript">
var manageTable;
var base_url = "<?php echo base_url(); ?>";

$j(document).ready(function() {

  $j("#mainProductNav").addClass('active');

  // initialize the datatable 
  manageTable = $('#manageTable').DataTable({
    dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print'
        ] 
  });
});
$j(document).ready(function() {
	$.ajax({
		url: '<?php echo base_url('Controller_Products/GetActiveCategory'); ?>',
		type: 'POST',
		data: {
			pId:'1'
		},
		dataType: 'json',
		success: function(data) {
			$('select[name="searchCat"]').empty();
			$('select[name="searchCat"]').append('<option value="All">All</option>');
			$.each(data, function(key, value) {
			$('select[name="searchCat"]').append('<option value="'+ value.category_id +'">'+ value.category_name +'</option>');
			})
			if(document.getElementById("hiddenSearch").value)
			  document.getElementById("searchCat").value=document.getElementById("hiddenSearch").value;
			else
			  document.getElementById("searchCat").value="All";
		}
	});  
});
function removeFunc(id)
{
  if(id) {
		$.ajax({
			url: '<?php echo base_url('Controller_Products/GetAssignProducts'); ?>',
			type: 'POST',
			data: {
				pId: id
			},
			dataType: 'json',
			success: function(data) {
				document.getElementById("asProId").value=id;
				$('select[name="return_by"]').empty();
				$.each(data[0], function(key, value) {
				$('select[name="return_by"]').append('<option value="'+ value.proId +'">'+ value.aTo +'</option>');
				})
				$('select[name="to_warehouse"]').empty();
				$.each(data[1], function(key, value) {
				$('select[name="to_warehouse"]').append('<option value="'+ value.id +'">'+ value.name +'</option>');
				})
			}
		});  
	}
}
function issueProductDetail(id)
{
  if(id) {
		$.ajax({
			url: '<?php echo base_url('Controller_Members/GetProductByPId'); ?>',
			type: 'POST',
			data: {
				pId: id
			},
			dataType: 'json',
			success: function(data) {
				document.getElementById("htmlDisplay").innerHTML=data;
			}
		});  
	}
}

function showHide(enterVal)
{
	if(enterVal=='user')
	{
		document.getElementById('assigntoplace').style.display="none";	
		document.getElementById('assigntouser').style.display="block";
		document.getElementById('assignForVl').value=enterVal;
		
		document.getElementById('placeassignTo').disabled=true;
		document.getElementById('placeDepartment').disabled=true;
		document.getElementById('placephone').disabled=true;
		document.getElementById('plQty').disabled=true;
		document.getElementById('placeremark').disabled=true;
		
		document.getElementById('assignTo').disabled=false;
		document.getElementById('Department').disabled=false;
		document.getElementById('Department').disabled=false;
		document.getElementById('phone').disabled=false;
		document.getElementById('remark').disabled=false;
	}
	else
	{
		document.getElementById('assigntouser').style.display="none";	
		document.getElementById('assigntoplace').style.display="block";	
		document.getElementById('assignForVl').value=enterVal;
		document.getElementById('placeassignTo').disabled=false;
		document.getElementById('placeDepartment').disabled=false;
		document.getElementById('placephone').disabled=false;
		document.getElementById('plQty').disabled=false;
		document.getElementById('placeremark').disabled=false;
		
		document.getElementById('assignTo').disabled=true;
		document.getElementById('Department').disabled=true;
		document.getElementById('Department').disabled=true;
		document.getElementById('phone').disabled=true;
		document.getElementById('remark').disabled=true;
		
		
	}
}
function assignToUser(pId,qt)
{
	document.getElementById('productId').value=pId;
	document.getElementById('pQty').value=qt;
}
function CheckQuantity(enterVal)
{
	if(Number(enterVal))	
	{
		var actualQty=document.getElementById('pQty').value;
		if(Number(enterVal) > Number(actualQty))
		{
			alert('Enter quantity should not be greater than actual quantity');
			document.getElementById('plQty').value="";
			document.getElementById('plQty').focus();
			return false;
		}
		else
		{
			return true;
		}
		
	}
	else
	{
		alert('Please enter numeric value only !');
		document.getElementById('plQty').value="";
		document.getElementById('plQty').focus();
		return false;
	}
}
function checkQuantityOnSave()
{
		var actualQty=document.getElementById('pQty').value;
		if(Number(actualQty) <1)
		{
			alert('Sorry! No qunatity for assign');
			return false;
		}
		else
		{
			return true;	
		}
}
</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>