
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>var $j = jQuery.noConflict(true);</script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Assign and Return Product
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Report</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
		<?php
		//cho"<pre>";print_r($this->data['user_data']);die;
		
		?>
        <div class="box">
        <form name="searchVal" action="<?php echo base_url();?>Controller_Members/index" method="post">
        <input type="hidden" name="hiddenSearch" id="hiddenSearch" value="<?php if(isset($_REQUEST['searchCat'])) echo $_REQUEST['searchCat'];?>">
        <div class="col-md-6 col-xs-6">
            <div class="form-group">
              <label for="store">Category</label>
              <select class="form-control select_group" id="searchCat" name="searchCat" required>
              	<option value="">Select</option>
              </select>
            </div>          <!-- /.box-header -->
          </div>
        <div class="col-md-6 col-xs-6">
            <div class="form-group" style="margin-top: 29px;">
              <button onClick="serchCategoryWise()">Search</button>
            </div>          <!-- /.box-header -->
          </div>
          </form>
          <!-- /.box-header -->
          <style>
			.modal-dialog {
				width: 80% !important;
				margin: 30px auto !important;
				}		  
			.modal-dialogAction {
				width: 40% !important;
				margin: 30px auto !important;
				}		  
			</style>
          <div class="box-body">
            <table id="example" class="table table-bordered table-striped">
              <thead>
                  <tr bgcolor="#3c8dbc" style="color:#ffffff">
                    <th  width="10px">S.No</th>
                    <th>Categroy</th>
                    <th style="text-align:center">Total Product</th>
                  </tr>
              </thead>
              <?php 
			  //echo"<pre>";print_r($this->data['user_data']);die;
			  $sno=1;
			  foreach($this->data['user_data'] as $MainKey=>$subDetails)
			  {
				  list($catId,$catName)=explode("_",$MainKey);
				 	$oldcnt=0;
				  	if(isset($subDetails['Old']))
						$oldcnt=$subDetails['Old'];
					?>
                      <tr>
                        <td><?php echo $sno++;?></td>
                        <td><?php echo $catName;?></td>
                        <td style="text-align:center"><?php  if(isset($subDetails)) echo '<strong><a href="#" data-toggle="modal" data-target="#myModal'.$catId.'"><font color="#009B2A">'.($subDetails).'</font></a></strong>'; else echo '<font color="#FF0000"><strong>0</strong></font>';?>
                              <!-- Trigger the modal with a button -->
                              <!-- Modal -->
                              <div class="modal fade" id="myModal<?php echo $catId;?>" role="dialog">
                                <div class="modal-dialog">
                                
                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header" style="background:#5bc0de">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title"><font color="#ffffff"><strong><?php echo $catName;?></strong></font></h4>
                                    </div>
                                    <?php
										$CI     = & get_instance();
										$result = $CI->GetCategoryWiseProductByCatId($catId,'New');	
										//echo"<pre>";print_r($result);						
									?>
                                    <div class="modal-body">
                                        <table class="table table-bordered">
                                            <thead>
                                              <tr style="background:#B2B2B2; color:#ffffff">
                                                <th>Item</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Warehouse</th>
                                                <th style="text-align:center">Action</th>
                                              </tr>
                                            </thead>
                                            <tbody>
											<?php
                                                foreach($result as $val)
												{					
                                            ?>
                                                  <tr>
                                                    <td style="text-align:left"><?php echo $val['name'];?></td>
                                                    <td style="text-align:left"><?php echo $val['price'];?></td>
                                                    <td style="text-align:left"><?php echo $val['qty'];?></td>
                                                    <td style="text-align:left"><?php echo $val['warehouse'];?></td>
                                                    <td style="text-align:left">
                                                    <?php
													if($val['qty']>0 || $val['qty']=0)
													{
														?>
                                                        <button type="button" class="btn btn-info btn-sm" onclick="assignToUser(<?php echo $val['id'];?>,<?php echo $val['qty'];?>)" data-toggle="modal" data-target="#assignModal"><i class="fa fa-share-square-o"></i> <strong>Assign</strong></button>
                                                      <?php
													}
													?>
                                                       <button type="button" class="btn btn-info btn-sm" onclick="removeFunc(<?php echo $val['id'];?>)" data-toggle="modal" data-target="#removeModal"><i class="glyphicon glyphicon-fast-backward"></i> <strong>Return</strong></button>
                                                       <button type="button" class="btn btn-info btn-sm"  data-toggle="modal" data-target="#IssueModal" onclick="issueProductDetail(<?php echo $val['id'];?>)"><i class="glyphicon glyphicon-eye-open"></i> <strong>Issue</strong></button>
                                                       <a href="<?php echo base_url('Controller_Products/update'); ?>/<?php echo $val['id'];?>" target="_blank" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-edit"></i> <strong>Modify</strong></a>
            <!-- remove brand modal -->
                                                    </td>
                                                  </tr>
											<?php
											  }					
                                            ?>
                                            </tbody>
                                          </table>
                                      </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                  </div>
                                  
                                </div>
                              </div>
                        </td>
                      </tr>
              <?php
			  }
			  ?>
            </table>
            <div class="modal fade" tabindex="-1" role="dialog" id="assignModal">
              <div class="modal-dialogAction modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header btn-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Assign Product</h4>
                    <span style=" float: right;margin-bottom: -17px;margin-right: 151px;margin-top: -22px;">
                        <select class="form-control" id="assignFor" name="assignFor" onChange="showHide(this.value)">
                            <option value="user">Assign to User</option>
                            <option value="place">Assign to Place</option>
                        </select>
                    </span>
                  </div>
                    <form role="form" action="<?php echo base_url('Controller_Products/AssignTo') ?>" method="post" id="assignForm">
                    <input type="hidden" name="assignForVl" id="assignForVl"  value="user" >
                    <input type="hidden" name="productId" id="productId">
                    <input type="hidden" name="pQty" id="pQty">
                      <div class="box-body" id="assigntouser">
                        <?php echo validation_errors(); ?>
                        <div class="row">
                       <div class="col-md-6">
                        <div class="form-group">
                          <label for="fname">Assign To <font color="#FF0004">*</font></label>
                          <input type="text" class="form-control" id="assignTo" name="assignTo" placeholder="Assign To" autocomplete="off" required>
                        </div>
                        </div>
            
                        <div class="col-md-6">
                        <div class="form-group">
                          <label for="username">Department <font color="#FF0004">*</font></label>
                          <input type="text" class="form-control" id="Department" name="Department" placeholder="Department" autocomplete="off" required>
                        </div>
                        </div>
            
                         <div class="col-md-6">
                        <div class="form-group">
                          <label for="email">Email</label>
                          <input type="email" class="form-control" id="email" name="email" placeholder="Email" autocomplete="off">
                        </div>
                        </div>
                        <div class="col-md-6">
            
                        <div class="form-group">
                          <label for="phone">Mobile No</label>
                          <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" autocomplete="off" maxlength="10">
                        </div>
                        </div>
                        </div>
                        <div class="row">
                       <div class="col-md-6">
                        <div class="form-group">
                          <label for="gender">Gender</label>
                          <div class="radio">
                            <label>
                              <input type="radio" name="gender" id="male" value="1" checked>
                              Male
                            </label>
                            <label>
                              <input type="radio" name="gender" id="female" value="2">
                              Female
                            </label>
                          </div>
                          
                          </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label for="fname11">Location </label>
                              <input type="text" class="form-control" id="location" name="location" placeholder="Location" autocomplete="off">
                            </div>
                            </div>	                        
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                        <div class="form-group">
                          <label for="username">Remark</label>
                          <input type="text" class="form-control" id="remark" name="remark" placeholder="Remark" autocomplete="off">
                        </div>
                        </div>
            
                        </div>
                        </div>
                      <div class="box-body" style="display:none" id="assigntoplace">
                        <?php echo validation_errors(); ?>
                        <div class="row">
                       <div class="col-md-6">
                        <div class="form-group">
                          <label for="fname22">Location <font color="#FF0004">*</font></label>
                          <input type="text" class="form-control" id="placeassignTo" name="placeassignTo" placeholder="Location" autocomplete="off" required disabled>
                        </div>
                        </div>
            
                        <div class="col-md-6">
                        <div class="form-group">
                          <label for="username">Department <font color="#FF0004">*</font></label>
                          <input type="text" class="form-control" id="placeDepartment" name="placeDepartment" placeholder="Department" autocomplete="off" required disabled>
                        </div>
                        </div>
            
                        <div class="col-md-6">
            
                        <div class="form-group">
                          <label for="phone">Mobile No</label>
                          <input type="number" class="form-control" id="placephone" name="placephone" placeholder="Phone" autocomplete="off" maxlength="10" disabled>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                          <label for="username">Quantity</label>
                          <input type="text" class="form-control" onKeyUp="return CheckQuantity(this.value)" id="plQty" name="plQty" placeholder="Quantity" maxlength="3" autocomplete="off" disabled required>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                          <label for="username">Remark</label>
                          <input type="text" class="form-control" id="placeremark" name="placeremark" placeholder="Remark" autocomplete="off" disabled>
                        </div>
                        </div>
                        </div>
                        </div>
                      <!-- /.box-body -->
            
                      <div class="box-footer" style="text-align:right">
                        <button type="submit" class="btn btn-primary" onClick="return checkQuantityOnSave()">Save & Close</button>
                      </div>
                    </form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!-- remove brand modal -->
            <div class="modal fade" tabindex="-1" role="dialog" id="removeModal">
              <div class="modal-dialogAction modal-sm" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Return Product</h4>
                  </div>
            
                  <form role="form" action="<?php echo base_url('Controller_Products/ReturnProduct') ?>" method="post" id="removeForm">
                    <input type="hidden" name="asProId" id="asProId" >
                    <div class="modal-body">
                      <div class="box-body">
                        <div class="row">
                       <div class="col-md-12">
                        <div class="form-group">
                          <label for="fname">Return By <font color="#FF0004">*</font></label>
                            <select class="form-control" id="return_by" name="return_by" required>
                                <option value="">Select</option>
                            </select>
                        </div>
                        </div>
                       <div class="col-md-12">
                        <div class="form-group">
                          <label for="fname">Warehouse <font color="#FF0004">*</font></label>
                            <select class="form-control" id="to_warehouse" name="to_warehouse" required>
                                <option value="">Select</option>
                            </select>
                        </div>
                        </div>
                        </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-success">Return</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </form>
            
            
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <div class="modal fade" tabindex="-1" role="dialog" id="IssueModal">
              <div class="modal-dialogAction modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Issue Item</h4>
                  </div>
                    <div class="modal-body">
                      <div class="box-body">
                            <table class="table table-bordered" id="issueAddRow"></table>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Assing brand modal -->
<!-- remove brand modal -->



<script type="text/javascript">
var manageTable;
var base_url = "<?php echo base_url(); ?>";

$j(document).ready(function() {

  $j("#mainProductNav").addClass('active');

  // initialize the datatable 
  manageTable = $('#manageTable').DataTable({
    dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print'
        ] 
  });
});
$j(document).ready(function() {
	$.ajax({
		url: '<?php echo base_url('Controller_Products/GetActiveCategory'); ?>',
		type: 'POST',
		data: {
			pId:'1'
		},
		dataType: 'json',
		success: function(data) {
			$('select[name="searchCat"]').empty();
			$('select[name="searchCat"]').append('<option value="All">All</option>');
			$.each(data, function(key, value) {
			$('select[name="searchCat"]').append('<option value="'+ value.category_id +'">'+ value.category_name +'</option>');
			})
			if(document.getElementById("hiddenSearch").value)
			  document.getElementById("searchCat").value=document.getElementById("hiddenSearch").value;
			else
			  document.getElementById("searchCat").value="All";
		}
	});  
});
function removeFunc(id)
{
  if(id) {
		$.ajax({
			url: '<?php echo base_url('Controller_Products/GetAssignProducts'); ?>',
			type: 'POST',
			data: {
				pId: id
			},
			dataType: 'json',
			success: function(data) {
				document.getElementById("asProId").value=id;
				$('select[name="return_by"]').empty();
				$.each(data[0], function(key, value) {
				$('select[name="return_by"]').append('<option value="'+ value.proId +'">'+ value.aTo +'</option>');
				})
				$('select[name="to_warehouse"]').empty();
				$.each(data[1], function(key, value) {
				$('select[name="to_warehouse"]').append('<option value="'+ value.id +'">'+ value.name +'</option>');
				})
			}
		});  
	}
}
function issueProductDetail(id)
{
  if(id) {
		$.ajax({
			url: '<?php echo base_url('Controller_Members/GetProductByPId'); ?>',
			type: 'POST',
			data: {
				pId: id
			},
			dataType: 'json',
			success: function(data) {
				$('#issueAddRow').empty();
				var error=0;
				$('#issueAddRow').append('<thead><tr style="background:#458e92; color:#ffffff"><td>User</td><td>Department</td><td>Mobile&nbsp;No</td><td>Qty</th><td>Assign&nbsp;Date</td></tr></thead>');
				$.each(data, function(key, value) {
						$('#issueAddRow').append('<tr><td>'+value.assign_to+'</td><td>'+value.department+'</td><td>'+value.phone+'</td><td>'+value.quantity+'</td><td>'+value.issue_date+'</td></tr>');
						error=1;
				})
				if(error==0)
				{
					$('#issueAddRow').append('<tr><td colspan="5"><font color="#FF0000">No data found.</font></td></tr>');
				}
				//document.getElementById("htmlDisplay").innerHTML=data;
			}
		});  
	}
}

function showHide(enterVal)
{
	if(enterVal=='user')
	{
		document.getElementById('assigntoplace').style.display="none";	
		document.getElementById('assigntouser').style.display="block";
		document.getElementById('assignForVl').value=enterVal;
		
		document.getElementById('placeDepartment').disabled=true;
		document.getElementById('placephone').disabled=true;
		document.getElementById('plQty').disabled=true;
		document.getElementById('placeremark').disabled=true;
		document.getElementById('placeassignTo').disabled=true;
		
		document.getElementById('assignTo').disabled=false;
		document.getElementById('Department').disabled=false;
		document.getElementById('Department').disabled=false;
		document.getElementById('phone').disabled=false;
		document.getElementById('remark').disabled=false;
		document.getElementById('location').disabled=false;
	}
	else
	{
		document.getElementById('assigntouser').style.display="none";	
		document.getElementById('assigntoplace').style.display="block";	
		document.getElementById('assignForVl').value=enterVal;
		document.getElementById('placeDepartment').disabled=false;
		document.getElementById('placephone').disabled=false;
		document.getElementById('plQty').disabled=false;
		document.getElementById('placeremark').disabled=false;
		document.getElementById('placeassignTo').disabled=false;
		
		document.getElementById('assignTo').disabled=true;
		document.getElementById('Department').disabled=true;
		document.getElementById('Department').disabled=true;
		document.getElementById('phone').disabled=true;
		document.getElementById('remark').disabled=true;
		document.getElementById('location').disabled=true;
		
	}
}
function assignToUser(pId,qt)
{
	document.getElementById('productId').value=pId;
	document.getElementById('pQty').value=qt;
}
function CheckQuantity(enterVal)
{
	if(Number(enterVal))	
	{
		var actualQty=document.getElementById('pQty').value;
		if(Number(enterVal) > Number(actualQty))
		{
			alert('Enter quantity should not be greater than actual quantity');
			document.getElementById('plQty').value="";
			document.getElementById('plQty').focus();
			return false;
		}
		else
		{
			return true;
		}
		
	}
	else
	{
		alert('Please enter numeric value only !');
		document.getElementById('plQty').value="";
		document.getElementById('plQty').focus();
		return false;
	}
}
function checkQuantityOnSave()
{
		var actualQty=document.getElementById('pQty').value;
		if(Number(actualQty) <1)
		{
			alert('Sorry! No qunatity for assign');
			return false;
		}
		else
		{
			return true;	
		}
}
</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>