<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Module Link
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">User Type</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; 
       /// print_r($_SESSION);
		if($this->session->userdata('user_type')=='1')
		{
		?>

          <button class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add </button>
          <br /> <br />
		<?php
		}
		?>
        <div class="box">
          
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>id</th>
                <th>Module Link</th>
                <th>Link Controller</th>
                <th>Priority</th>
              </tr>
              </thead>
				<?php
                $sno=1;
				if($this->data['array'] > 0)
				{
					foreach($this->data['array'] as $vDetails)	
					{
                        // print_r($vDetails);
					?>
                      <tr>
                        <td><?php echo $sno++; ?></td>
                        <td><?php echo $vDetails['link_name']?></td>
                        <td><?php echo $vDetails['link_controller']?></td>
                        <td><?php echo $vDetails['priority']?></td>
                       </tr>
                    <?php	
					}
				}
				?>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- create brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add </h4>
      </div>

      <form role="form" action="<?php echo base_url('Controller_Module/create') ?>" method="post" id="">
        <div class="modal-body">
		  <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Module Link<font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="module_link" name="module_link" placeholder="Enter User Type" autocomplete="off" required>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">Controller Link<font color="#FF0000">*</font></label>
                    <input type="text" class="form-control" id="controller_link" name="controller_link" placeholder="Enter Controller Link" autocomplete="off" required>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="brand_name">priority </label>
                    <input type="text" class="form-control" id="priority" name="priority" placeholder="Enter Remark" autocomplete="off">
                  </div>
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" >Save changes</button>
        </div>
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script>
function refreshPage(Val)
{
	if(Val==1)
	{
		if(document.getElementById('Project_name').value!="" && document.getElementById('active').value!="")
			location.reload();
		else if(document.getElementById('edit_Project_name').value!="" && document.getElementById('edit_unit').value!="")
			location.reload();
		else
			return false;
	}
	else
	{
		location.reload();
	}
}

$("#hidePopUp").click(function(){
	//alert('Hii deep');
	$("#editModal").modal('hide');
});
</script>

<script type="text/javascript">
var manageTable;

$(document).ready(function() {
  // submit the create from 
});

// edit function
function editFunc(id)
{ 
  // alert("hello");
  $.ajax({
    url: 'fetchProjectDataById/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
      alert(response.user_type_id);
		$("#edit_project_name").val(response.project_name);
		$("#edit_project_coordinator").val(response.project_coordinator);
		$("#edit_email").val(response.email_id);
		$("#edit_mobile_no").val(response.mobile_no);
		$("#edit_password").val(response.password);
		$("#edit_project_cost").val(response.project_cost);
		$("#edit_address").val(response.address);
		$("#edit_user_type_id").val(response.user_type_id);
		$("#edit_project_id").val(response.project_id);
		$('#editModal').modal('show');
    }
  });
}

// remove functions 
function removeFunc(id)
{
  document.getElementById('delete_project_id').value=id;
}


</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>