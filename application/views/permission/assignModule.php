 <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Assign Module Link
      
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Assign Module</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
          <br /> <br />

        <div class="box">
          
          <!-- /.box-header -->
          <div class="box-body">
          <form role="form" action="<?php echo base_url('Controller_UserModule/AssignModuleLink') ?>" method="post" id="">
            <table id="manageTable" class="table table-bordered table-striped">
                <tr>
                	<td colspan="6" align="right">
                      <button type="submit" class="btn btn-primary" onclick="return confirm('Please confirm ? Click ok to continue, Cancel to stop.');">Save changes</button>
                    </td>
                </tr>
				<?php
				$CI=& get_instance();
				$moduleLinkArray=$CI->getModuleMenuLink();
				//echo"<pre>";print_r($moduleLinkArray);
				//$moduleLinkArray=array("User Type","Add User","Assign Module","Site","Scrap","Category","Vendor","Panel","DP (Daily Progress)","DPR (Daily Progress Report)","DPR (Date Range Report)","Add Product","Issue & Return","Product Assign Report","Assign Stock","Transfer Stock","Stock Notification","Transfer Stock Report","Assign Stock Report");
				$buttonArray=array("Add","Edit","Delete","Excel","Print");
				//echo"<pre>";print_r($buttonArray);die;
				if($this->data['array'] > 0)
				{
					foreach($this->data['array'] as $vDetails)	
					{
						if($vDetails['user_type_id']==1)
							continue;
							
						$ci=& get_instance();
						$permissionDetail=$ci->GetUserTypeWisePermissionDetail($vDetails['user_type_id']);
						$showDataArray=array();
						$buttonPermissinArr=array();
						if(count($permissionDetail)>0){
							foreach($permissionDetail as $permission)	{
								$buttonPermissinArr=explode(",",$permission['button_permission']);
								$showDataArray[$permission['user_type_id']][$permission['module_name']][]=$permission['button_permission'];
							}
						}
						//echo"<pre>";print_r($showDataArray[$vDetails['user_type_id']]);
						
					?>
                    <input type="hidden" name="userTypeArra[]" id="userTypeArra[]" value="<?php echo $vDetails['user_type_id'];?>" />
                    <tr>
                        <td style="background-color:#4D74D0; color:#ffffff"><input type="checkbox" name="user_type_<?php echo $vDetails['user_type_id']?>" <?php if(array_key_exists($vDetails['user_type_id'],$showDataArray) && isset($showDataArray)) echo "checked"; ?> value="<?php echo $vDetails['user_type_id']?>" id="user_type_<?php echo $vDetails['user_type_id']?>" onClick="checkModuleWiseLine(this.value)"  />&nbsp;&nbsp;<strong><?php echo $vDetails['user_type']?></strong></td>
                    </tr>
                    <tr>
                    	<td>
                            <table id="manageTable" class="table table-bordered table-striped">
								<?php	
                                foreach($moduleLinkArray as $link)
                                {
									$newLink=str_replace(" ","_",$link['link_name']);
									$btnArr=array();
									if(isset($showDataArray[$vDetails['user_type_id']][$newLink]))
										$btnArr=explode(",",$showDataArray[$vDetails['user_type_id']][$newLink][0]);
										
                                ?>
                                       <tr> <td colspan="6"><input type="checkbox" name="module_link_<?php echo $vDetails['user_type_id']."_".$newLink;?>" <?php if(array_key_exists($vDetails['user_type_id'],$showDataArray) && isset($showDataArray)) echo ""; else echo "disabled"; ?> <?php if(isset($showDataArray[$vDetails['user_type_id']][$newLink]) && isset($showDataArray[$vDetails['user_type_id']])) echo "checked";?> id="module_link_<?php echo $vDetails['user_type_id']."_".$newLink;?>" value="<?php echo $link['link_name'];?>" onClick="return checkAllButton('<?php echo $vDetails['user_type_id']?>_<?php echo $newLink;?>')" />&nbsp;&nbsp;<strong><?php echo $link['link_name']?></strong></td></tr>
                                        <tr>
                                        <td width="40%">&nbsp;</td>
											<?php	
                                            foreach($buttonArray as $button)
                                            {
                                            ?>
                                                   <td style="margin-left:50px"><input type="checkbox" name="button_<?php echo $vDetails['user_type_id']."_".$newLink."_".$button;?>" <?php if(isset($showDataArray[$vDetails['user_type_id']][$newLink]) && isset($showDataArray[$vDetails['user_type_id']])) echo ""; else echo "disabled";?> <?php if(in_array($button,$btnArr) && isset($btnArr)) echo 'checked';?> value="<?php echo $button;?>" id="button_<?php echo $vDetails['user_type_id']."_".$newLink."_".$button;?>"  />&nbsp;&nbsp;<strong><?php echo $button?></strong></td>
                                                    <input type="hidden" name="buttonPermission_<?php echo $vDetails['user_type_id']."_".$newLink;?>[]" id="buttonPermission" value="<?php echo $button;?>" />
											<?php
                                            }
                                            ?>
                                        </tr>
                                        <input type="hidden" name="moduleLinkArr_<?php echo $vDetails['user_type_id'];?>[]" id="moduleLinkArr" value="<?php echo $newLink;?>" />
								<?php
                                }
                                ?>
                            </table>
                        </td>
                    </tr>
                        <?php
					}
				}
				?>
                <tr>
                	<td colspan="6" align="right">
                      <button type="submit" class="btn btn-primary" onclick="return confirm('Please confirm ? Click ok to continue, Cancel to stop.');">Save changes</button>
                    </td>
                </tr>
            </table>
           </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script>
function checkModuleWiseLine(empType)
{
	var linkArr=document.getElementsByName('moduleLinkArr_'+empType+'[]');
	for(var i=0;i<linkArr.length;i++)
	{
		if(document.getElementById('user_type_'+empType).checked==true)
		{
			document.getElementById('module_link_'+empType+'_'+linkArr[i].value).disabled=false;
		}
		else
		{
			document.getElementById('module_link_'+empType+'_'+linkArr[i].value).disabled=true;
			document.getElementById('module_link_'+empType+'_'+linkArr[i].value).checked=false;
			checkAllButton(empType+'_'+linkArr[i].value);
		}
	}
}
function checkAllButton(checkId)
{
	var buttonValArr=document.getElementsByName('buttonPermission_'+checkId+'[]');
	if(document.getElementById('module_link_'+checkId).checked==true)
	{
		for(var i=0;i<buttonValArr.length;i++)
		{
			var buttonId=buttonValArr[i].value;
			document.getElementById('button_'+checkId+'_'+buttonId).disabled=false;
			document.getElementById('button_'+checkId+'_'+buttonId).checked=true;
		}
	}
	else
	{
		for(var k=0;k<buttonValArr.length;k++)
		{
			var buttonId=buttonValArr[k].value;
			document.getElementById('button_'+checkId+'_'+buttonId).disabled=true;
			document.getElementById('button_'+checkId+'_'+buttonId).checked=false;
		}
	}
}
</script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
