<header class="main-header">
<?php
ini_set('display_errors',0);
$imgUrl=base_url('assets/images/'.$this->session->userdata['id'].".jpg");
if(file($imgUrl))
{
	$imgUrl=$imgUrl;
}
else
{
	$imgUrl=base_url('assets/images/default.png');
}
?>
    <!-- Logo -->

    <a href="#" class="logo">

      <!-- mini logo for sidebar mini 50x50 pixels -->

      <span class="logo-mini"><img src="https://erp.geosysindia.com/assets/images/minilogo.jpg" title="GeoSys India"></span>

      <!-- logo for regular state and mobile devices -->

      <span class="logo-lg"><img src="<?php echo base_url();?>assets/images/logo.png" style="width: 218px;padding-top: 5px;"></span>

    </a>
    <style>
    .dropdown-content {
    display: none;
    position: absolute;
    right: 0;
    background-color: #f9f9f9;
    min-width: 185px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 16px;
    z-index: 1;
    
}
.profile h1 {
    text-align: right;
    margin-top: 0px;
    line-height: 58px;
    margin-bottom: 0px;
    font-size: 18px;
    width: 92%;
    font-weight: 600;
}
.dropdown {
    position: absolute;
    float: right;
    top: 0;
    right: 0;
}
.profile img {
    float: right;
    background: #efefef;
    border-radius: 50px;
    height: 50px;
    width: 50px;
    margin-top: -51px;
    margin-left: 12px;
    padding: 7px;
    margin-right: 8px;
}
.badge {
    position: absolute;
    top: 0px;
    right: 1spx;
    padding: 3px 6px;
    border-radius: 50%;
    background-color: red;
    color: white;
}
</style>

    

    <!-- Header Navbar: style can be found in header.less -->

    <nav class="navbar navbar-static-top">

    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">

        <span class="sr-only">Toggle navigation</span>

      </a>
      <div class="profile">
      <!-- Sidebar toggle button-->
<h1>Welcome : <span><?php echo $_SESSION['username'];?><?php if($_SESSION['id']!=2) {?>&nbsp&nbsp&nbsp&nbsp|&nbsp&nbsp&nbsp&nbsp <a href="<?php echo base_url('Controller_Products/AcceptAsset');?>"><i class="fa fa-bell" ></i><span class="badge badge-light" style="margin: 8px;margin-left: -4px;"><?php echo $_SESSION['notification_count'];?></span></a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp|<?php }?></span></h1>
      </div>
         
       <div class="dropdown">
       <div class="profile">
        <img src="<?php echo $imgUrl;?>" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
         </div>
           <div class="dropdown-content" style="min-width: 178px;">
         <p><a href="#" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-lock"></i> Change&nbsp;Password</a></p>
         <p><a href="#" data-toggle="modal" data-target="#profileModal"><i class="glyphicon glyphicon-user"></i> Profile</a></p>
         <!-- <p><a href="<?php echo base_url('Controller_Products/AcceptAsset');?>"><i class="fa fa-bell"></i>  (<?php echo $_SESSION['notification_count'];?>) Notification</a></p> -->
         <p><a href="<?php echo base_url('auth/logout');?>"><i class="glyphicon glyphicon-log-out"></i> Logout</a></p>
       </div>
    </div>
    
       
    
    </nav>

  </header>
  <style>
.dropdown {
  position: relative;
  float:right;
  
}

.dropdown-content {
  display: none;
  position: absolute;
  right: 0;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  padding: 12px 16px;
  z-index: 1;
}
.dropdown-content p {
    text-align: center;
    text-align: left;
    background: white;
    padding: 6px 7px 6px;
    box-shadow: 5px 5px 5px whitesmoke;
    border-radius: 5px;
}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>

<!-- CODE TO CHANGE PASSWORD PROFILE AND IMAGE BY DEEP RANA-->
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog">
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title" style="font-weight:bold">Change Password</h4>
    </div>
    <form role="form" action="<?php echo base_url('Controller_UserModule/changepassword') ?>" method="post" id="updateForm"  >
    <div class="modal-body">
           <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                <label for="brand_name">Current Password :<font color="#FF0000">*</font></label>
                <input type="password" class="form-control" name="password" id="password" placeholder="New Password" autocomplete="off"  onblur="currentpassword(this.value)" required>
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-6">
          <?php //print_r($this->session->userdata);?>
              <div class="form-group">
                <label for="brand_name">New Password :<font color="#FF0000">*</font></label>
                <input type="hidden"  class="form-control" name="old_pass" id="old_pass" placeholder="New Password"  autocomplete="off" value="<?php echo $this->session->userdata['password'];?>">
                        <input type="password"  required class="form-control" name="new_pass" id="new_pass" placeholder="New Password"  autocomplete="off">
              </div>
          </div>
      </div>
       <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                <label for="brand_name">Confirm Password :<font color="#FF0000">*</font></label>
                        <input type="password"  required  class="form-control" name="confirm_pass" id="confirm_pass" placeholder="Confirm Password" autocomplete="off" onblur="confirmpass(this.value)">

              </div>
          </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary">Change </button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
    </form>
  </div>
  
</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="profileModal">
<div class="modal-dialog" role="document">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" style="font-weight:bold">Profile</h4>
     <div style="text-align:center;">
        <img class="img-circle" id="profile_picture" height="128" data-src="<?php //echo $_SESSION['image'];?>"  data-holder-rendered="true" style="width: 140px; height: 140px;" src="<?php echo $imgUrl;?>?=<?php echo time();?>"/>
        <br><br>
        <a type="button" class="btn btn-primary" id="change-profile-pic" data-toggle="modal" data-target="#SubModal">Change Profile Picture</a>
    </div>
  </div>
  <form role="form" action="<?php echo base_url('Controller_UserModule/profileClientRegistration') ?>" method="post" id="updateForm1">
  
  <input type="hidden" name="profile_client_id" id="profile_client_id" value="<?php echo $this->session->userdata['id'];?>">
    <input type="hidden" name="registration_type" value="Client"  />
    <div class="modal-body">
    <div class="row">
          <div class="col-md-4">
              <div class="form-group">
                <label for="brand_name">Company Name <font color="#FF0000">*</font></label>
                <input type="text" class="form-control" id="profile_client_name" name="profile_client_name" placeholder="Enter person name" autocomplete="off" readonly value="GeoSysIndia"required>
              </div>
          </div>
           <div class="col-md-4">
              <div class="form-group">
                <label for="brand_name">Email <font color="#FF0000">*</font></label>
                <input type="email" class="form-control" id="profile_email" name="profile_email" placeholder="Enter person email" autocomplete="off" value="<?php echo $_SESSION['email'];?>" required>
              </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                <label for="brand_name">Mobile no <font color="#FF0000">*</font></label>
                <input type="number" class="form-control" maxlength="12" id="profile_mobile_no" name="profile_mobile_no" placeholder="Enter person mobile no" autocomplete="off" value="<?php echo $_SESSION['mobile_no'];?>" required>
              </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                <label for="brand_name">Contact Person Name</label>
                <input type="text" class="form-control" id="profile_contact_person_name" name="profile_contact_person_name" placeholder="Enter contact person name" autocomplete="off"  value="<?php echo $_SESSION['username'];?>">
              </div>
          </div>
          <div class="col-md-8">
            <div class="form-group">
                <label for="brand_name">Address</label>
                <input type="text" class="form-control" id="profile_address" name="profile_address" placeholder="Enter address" autocomplete="off" value="<?php echo $_SESSION['address'];?>">
              </div>
          </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary">Update </button>
    </div>
  </form>
</div>
</div>
</div>
<div class="modal fade" id="SubModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" >&times;</button>
          <h4 class="modal-title" style="font-weight:bold">Upload Image</h4>
        </div>
        <?php echo form_open_multipart('Controller_UserModule/do_upload');?>
<!--                <form role="form" action="<?php //echo base_url() ?>" method="post" id="updateForm" autocomplete="off">
-->                <div class="modal-body">
               <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <input type="file" class="form-control" name="image" id="image" required accept="image/*" onchange="imagepath()">
                     <input type="hidden" class="form-control" name="registration_id" id="registration_id"  value="<?php echo $this->session->userdata['id'];?>">
                  </div>
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" name="submit" id="submit" class="btn btn-primary" >Submit </button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>

<script type='text/javascript'>
jQuery(document).ready(function() {
	$('password').attr('autocomplete', 'off');
 // executes when HTML-Document is loaded and DOM is ready
console.log("document is ready");
  
  
  jQuery('.btn[href^=#]').click(function(e){
    e.preventDefault();
    var href = jQuery(this).attr('href');
    jQuery(href).modal('toggle');
  });
});  
  function confirmpass(value){
      var new_pass = document.getElementById('new_pass').value;
      if(value!=new_pass){
        alert("Password doesn't Match.");
		document.getElementById('confirm_pass').value="";
		return false;
      }
   }  
</script>
<script>
function currentpassword(val){
	var password = document.getElementById('password').value;
    var old_pass = document.getElementById('old_pass').value;
	if(old_pass!=password){
		alert("Current password doesn't match.");
		document.getElementById('password').value="";
	}	
}
function imagepath(){
	var image = document.getElementById('image').value;
	var res = image.split(".");
	if((res[1]!='jpg')&&(res[1]!='JPG'))	{
		alert("Please upload only .jpg extension file only.");
		document.getElementById('image').value="";
	}	
}
</script>
