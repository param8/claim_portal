<?php 

class Model_stores extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get the active store data */
	public function getActiveStore()
	{
		$sql = "SELECT * FROM stores WHERE active ='1'";// AND company_id='".$this->session->userdata['company_id']."'
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getFactoryTransferData($siteId)
	{
		$sql = "SELECT p.*,fs.qty as actual_qty FROM products p left join factory_stock fs ON p.factory_product_id=fs.factory_stock_id WHERE store_id ='".$siteId."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getFactoryData($id=null)
	{
		if($id) {
			$sql = "SELECT s.*,concat(name,' (',unit,')') as cat_name,c.id as cat_id
                    FROM factory_stock s  LEFT JOIN categories c ON s.category_id=c.id where s.factory_stock_id = '".$id."'";
			$query = $this->db->query($sql);
			return $query->row_array();
		}

		$sql = "SELECT s.*,concat(name,' (',unit,')') as cat_name,c.id as cat_id
                    FROM factory_stock s  LEFT JOIN categories c ON s.category_id=c.id where entry_by='2'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getSiteDataData($id=null)
	{
		if($id) {
			$sql = "SELECT s.*,s.name as stock_name,concat(c.name,' (',unit,')') as cat_name,c.id as cat_id
                    FROM products s  LEFT JOIN categories c ON s.category_id=c.id where s.id = '".$id."'";
			$query = $this->db->query($sql);
			return $query->row_array();
		}

		$sql = "SELECT s.*,s.name as stock_name,concat(c.name,' (',unit,')') as cat_name,c.id as cat_id,st.name as site_name
                    FROM products s  LEFT JOIN stores st ON s.store_id=st.id LEFT JOIN categories c ON s.category_id=c.id where entry_by='".$this->session->userdata['id']."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getProductDetailByCatId($catId)
	{
		$sql = "SELECT s.stock_name
				FROM factory_stock s  where s.category_id = '".$catId."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function fetchProductDetailByCatIdForSite($catId,$siteId)
	{
		$sql = "SELECT s.name as stock_name
				FROM products s  where s.category_id = '".$catId."' AND store_id='".$siteId."' AND entry_by='".$this->session->userdata['id']."'"; 
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	/* get the brand data */
	public function getStoresData($id = null)
	{
		if($id) {
			$sql = "SELECT s.*
                    FROM stores s  where s.id = '".$id."'";
			$query = $this->db->query($sql);
			return $query->row_array();
		}

		$sql = "SELECT s.*,sm.username as site_manager,co.username as           coordinator
                    FROM stores s 
                    left join users sm ON s.`site_manager_id`=sm.id
                    left join users co ON s.`coordinator_id`=co.id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getStoresDataListing()
	{
			$sql = "SELECT s.*
                    FROM stores s";
			$query = $this->db->query($sql);
    		return $query->result_array();
	}

	public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('stores', $data);
			return ($insert == true) ? true : false;
		}
	}
	public function createFactoryStock($data,$dataUpdate)
	{
		if($data) {
			$insert = $this->db->insert('factory_stock', $data);
			return ($insert == true) ? true : false;
		}
		if($dataUpdate) {
			$sql1= "UPDATE  factory_stock SET qty=(qty+'".$dataUpdate['qty']."') WHERE category_id ='".$dataUpdate['category_id']."' AND stock_name ='".$dataUpdate['stock_name']."'";
			$query = $this->db->query($sql1);
			return ($query == true) ? true : false;
		}
	}
	public function createSiteStock($data,$dataUpdate)
	{
		if($data) {
			$insert = $this->db->insert('products', $data);
			return ($insert == true) ? true : false;
		}
		if($dataUpdate) {
			$sql1= "UPDATE  products SET qty=(qty+'".$dataUpdate['qty']."'),total_quantity=(total_quantity+'".$dataUpdate['qty']."') WHERE category_id ='".$dataUpdate['category_id']."' AND store_id ='".$dataUpdate['store_id']."'";
			$query = $this->db->query($sql1);
			return ($query == true) ? true : false;
		}
	}

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('stores', $data);
			return ($update == true) ? true : false;
		}
	}
	public function UpdateFactoryStock($data,$factory_stock_id)
	{
		if($data && $factory_stock_id) {
			$this->db->where('factory_stock_id', $factory_stock_id);
			$update = $this->db->update('factory_stock', $data);
			return ($update == true) ? true : false;
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('stores');
			return ($delete == true) ? true : false;
		}
	}
	public function removeStock($id)
	{
		if($id) {
			$this->db->where('factory_stock_id', $id);
			$delete = $this->db->delete('factory_stock');
			return ($delete == true) ? true : false;
		}
	}
	public function removeSiteStock($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('products');
			return ($delete == true) ? true : false;
		}
	}

	public function countTotalStores()
	{
		$sql = "SELECT * FROM stores WHERE active ='1' AND company_id='".$this->session->userdata['company_id']."'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	public function getUserDetails($userTypeId)
	{
		$sql = "SELECT * FROM users WHERE user_type_id='".$userTypeId."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function checkProductExistOrNot($factory_product_id,$store_id)
	{
		$sql = "SELECT COUNT(1) AS RESULT FROM products WHERE factory_product_id='".$factory_product_id."' AND store_id='".$store_id."'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function InsertUpdateTransferDetails($insertTransferHistory,$updateProduct)
	{
		if(count($updateProduct)>0) {
			foreach($updateProduct as $pId=>$updateData){
    			$sql = "UPDATE  factory_stock SET used_qty=(used_qty+'".$updateData['used_qty']."') WHERE factory_stock_id = ?";
    			$query = $this->db->query($sql,$pId);
			}
		}
		if(count($insertTransferHistory)>0) {
			foreach($insertTransferHistory as $insertData){
				$existProUpdate=$this->checkProductExistOrNot($insertData['factory_product_id'],$insertData['store_id']);
				if($existProUpdate['RESULT']>0)
				{
					$sqlUpdate= "UPDATE  products SET qty=(qty+'".$insertData['qty']."'),total_quantity=(total_quantity+'".$insertData['qty']."') WHERE factory_product_id='".$insertData['factory_product_id']."' AND store_id='".$insertData['store_id']."'"; 
					$insert = $this->db->query($sqlUpdate);
				}
				else
				{
					$insert = $this->db->insert('products', $insertData); 
				}
			}
		}
		return ($insert) ? true : false;
	}
}