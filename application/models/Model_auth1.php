<?php 



class Model_auth extends CI_Model

{

	public function __construct()

	{

		parent::__construct();
		$this->load->library('encryption');
	}



	/* 

		This function checks if the email exists in the database

	*/

	public function check_email($email) 
	{

		if($email) {

			$sql = 'SELECT * FROM users WHERE email = ? OR mobile_no=?';
			$query = $this->db->query($sql, array($email,$email));
			$result = $query->num_rows();
			return ($result == 1) ? true : false;

		}



		return false;

	}


	public function check_Menu_Link($userTypeId) 
	{
		if($userTypeId) {
			$sql = "SELECT * FROM module_menu_link_permission WHERE user_type_id = ?";
			$query = $this->db->query($sql, array($userTypeId));
			$linkArray=$query->result_array();
			$menuLink='<li id="dashboardMainMenu">
						  <a href="'.base_url('dashboard').'">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>
						  </a>
						</li>';
			$linkWiseBtnPms=array();
			if(count($linkArray)>0){
				$controllerArray=array("User_Type"=>"Controller_UserModule/","Add_User"=>"Controller_UserModule/indexUser","Assign_Module"=>"Controller_UserModule/assignModule","Add_Site_Stock"=>"Controller_Warehouse/indexSite","Site"=>"Controller_Warehouse/","Scrap"=>"Controller_Products/saleScrap","Category"=>"Controller_Category/","Vendor"=>"Controller_Vendor/","Panel"=>"Controller_Project/Panel","Project"=>"Controller_Project/","DP_(Daily_Progress)"=>"Controller_Project/assignTo","DPR_(Daily_Progress_Report)"=>"Controller_Project/dprReport","DPR_(Date_Range_Report)"=>"Controller_Project/SearchDPRDateRange","Add_Product"=>"Controller_Products/create","Issue_&_Return"=>"Controller_Members","Product_Assign_Report"=>"Controller_Members/Report","Assign_Stock"=>"Controller_Project/assignStock","Transfer_Stock"=>"Controller_Products/TransferAsset","Stock_Notification"=>"Controller_Products/AcceptAsset","Transfer_Stock_Report"=>"Controller_Products/TransferAssetReport","Assign_Stock_Report"=>"Controller_Project/assignStockReport");
				$moduleLinkArray=array("User Type","Add User","Assign Module","Add Site Stock","Site","Scrap","Category","Vendor","Panel","DP (Daily Progress)","DPR (Daily Progress Report)","DPR (Date Range Report)","Add Product","Issue & Return","Product Assign Report","Assign Stock","Transfer Stock","Stock Notification","Transfer Stock Report","Assign Stock Report");
				foreach($linkArray as $link){
					$linkWiseBtnPms[$link['module_name']]=explode(",",$link['button_permission']);
					 $menuLink.='<li id="categoryNav">
									  <a href="'.base_url($controllerArray[$link['module_name']]).'">
										<i class="fa fa-user"></i> <span>'.$link['menu_name'].'</span>
									  </a>
									</li>';
				}
				
			}
			$menuLink.='<li><a href="'.base_url('auth/logout').'">
						<i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>';		
		}
		return array($menuLink,$linkWiseBtnPms);
	}
	/* 

		This function checks if the email and password matches with the database

	*/

	public function login($email, $password) {

		if($email && $password) {

			$sql = "SELECT * FROM users WHERE email = ? OR mobile_no = ?";

			$query = $this->db->query($sql, array($email,$email));
			if($query->num_rows() == 1) {
				$result = $query->row_array();
				$decriptPass=$this->encryption->decrypt($result['password']); 
				if($decriptPass==$password)
				{
					return $result;	
				}
				else {
					return false;
				}
			}

			else {

				return false;

			}

		}

	}

}