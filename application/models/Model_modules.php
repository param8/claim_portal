<?php 

class Model_modules extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getUserTypeDetails() 
	{
        $sql = "SELECT * FROM module_link_name ";
		$query = $this->db->query($sql);
		return $query->result_array();
    }
    public function create($data = '')
	{

		if($data) {
			$create = $this->db->insert('module_link_name', $data);
			return ($create == true) ? true : false;
        }
    }
	
}