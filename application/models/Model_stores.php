<?php 

class Model_stores extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get the active store data */
	public function getActiveStore()
	{
		$sql = "SELECT * FROM stores WHERE active ='1'";// AND company_id='".$this->session->userdata['company_id']."'
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getFactoryTransferData($siteId)
	{
		$sql = "SELECT p.*,fs.qty as actual_qty FROM products p left join factory_stock fs ON p.factory_product_id=fs.factory_stock_id WHERE store_id ='".$siteId."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getFactoryData($id=null)
	{
		if($id) {
			$sql = "SELECT s.*,concat(name,' (',unit,')') as cat_name,c.id as cat_id
                    FROM factory_stock s  LEFT JOIN categories c ON s.category_id=c.id where s.factory_stock_id = '".$id."'";
			$query = $this->db->query($sql);
			return $query->row_array();
		}

		$sql = "SELECT s.*,concat(name,' (',unit,')') as cat_name,c.id as cat_id
                    FROM factory_stock s  LEFT JOIN categories c ON s.category_id=c.id where entry_by='2'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}	
	
	public function getStoreChainData($id=null)
	{
		$where="";
		if($this->session->userdata('user_type')!='1' && $this->session->userdata('user_type')!='7') // for super admin and admin by Deep Rana
		{
			$where="WHERE AND (s.coordinator_id='".$_SESSION['id']."' || s.site_manager_id='".$_SESSION['id']."' )";
		}
		if($id) {
			$sql = "SELECT sc.*,s.name
                    FROM site_chainage sc  LEFT JOIN stores s ON sc.site_id=s.id WHERE site_chainage_id='".$id."'";
			$query = $this->db->query($sql);
			return $query->row_array();
		}

		 $sql = "SELECT sc.*,s.name
                    FROM site_chainage sc LEFT JOIN stores s ON sc.site_id=s.id  $where ORDER BY s.name ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getSiteDataData($id=null)
	{
		if($id) {
			$sql = "SELECT s.*,s.name as stock_name,concat(c.name,' (',unit,')') as cat_name,c.id as cat_id
                    FROM products s  LEFT JOIN categories c ON s.category_id=c.id where s.id = '".$id."'";
			$query = $this->db->query($sql);
			return $query->row_array();
		}

		$sql = "SELECT s.*,s.name as stock_name,concat(c.name,' (',unit,')') as cat_name,c.id as cat_id,st.name as site_name
                    FROM products s  LEFT JOIN stores st ON s.store_id=st.id LEFT JOIN categories c ON s.category_id=c.id where s.entry_by='".$this->session->userdata['id']."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getProductDetailByCatId($catId)
	{
		$sql = "SELECT s.stock_name
				FROM factory_stock s  where s.category_id = '".$catId."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function fetchProductDetailByCatIdForSite($catId,$siteId)
	{
		$sql = "SELECT s.name as stock_name
				FROM products s  where s.category_id = '".$catId."' AND store_id='".$siteId."' AND entry_by='".$this->session->userdata['id']."'"; 
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	/* get the brand data */
	public function getStoresData($id = null)
	{
		if($id) {
			$sql = "SELECT s.*
                    FROM stores s  where s.id = '".$id."'";
			$query = $this->db->query($sql);
			return $query->row_array();
		}

		$sql = "SELECT s.*,sm.username as site_manager,co.username as           coordinator
                    FROM stores s 
                    left join users sm ON s.`site_manager_id`=sm.id
                    left join users co ON s.`coordinator_id`=co.id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getStoresDataListing()
	{
			$sql = "SELECT s.*
                    FROM stores s";
			$query = $this->db->query($sql);
    		return $query->result_array();
	}
   
    public function getStoresListing()
	{
		$where="";
		if($this->session->userdata('user_type')!='1' && $this->session->userdata('user_type')!='7') // for super admin and admin by Deep Rana
		{
			$where="AND (coordinator_id='".$_SESSION['id']."' || site_manager_id='".$_SESSION['id']."' )";
		}
			$sql = "SELECT s.*
                    FROM stores s Where 1=1 $where";
			$query = $this->db->query($sql);
			return $query->result_array();
	}
 
	public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('stores', $data);
			return ($insert == true) ? true : false;
		}
	}
	public function createSiteChain($data)
	{
		if($data) {
			$insert = $this->db->insert('site_chainage', $data);
			return ($insert == true) ? true : false;
		}
	}
	public function createFactoryStock($data,$dataUpdate)
	{
		if($data) {
			$insert = $this->db->insert('factory_stock', $data);
			return ($insert == true) ? true : false;
		}
		if($dataUpdate) {
			$sql1= "UPDATE  factory_stock SET qty=(qty+'".$dataUpdate['qty']."') WHERE category_id ='".$dataUpdate['category_id']."' AND stock_name ='".$dataUpdate['stock_name']."'";
			$query = $this->db->query($sql1);
			return ($query == true) ? true : false;
		}
	}
	public function createSiteStock($data,$dataUpdate)
	{
		if($data) {
			$insert = $this->db->insert('products', $data);
			return ($insert == true) ? true : false;
		}
		if($dataUpdate) {
			$sql1= "UPDATE  products SET qty=(qty+'".$dataUpdate['qty']."'),total_quantity=(total_quantity+'".$dataUpdate['qty']."') WHERE category_id ='".$dataUpdate['category_id']."' AND store_id ='".$dataUpdate['store_id']."'";
			$query = $this->db->query($sql1);
			return ($query == true) ? true : false;
		}
	}

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('stores', $data);
			return ($update == true) ? true : false;
		}
	}
	
	public function updateSiteChain($data, $id)
	{
		if($data && $id) {
			$this->db->where('site_chainage_id', $id);
			$update = $this->db->update('site_chainage', $data);
			return ($update == true) ? true : false;
		}
	}	
	public function UpdateFactoryStock($data,$factory_stock_id)
	{
		if($data && $factory_stock_id) {
			$this->db->where('factory_stock_id', $factory_stock_id);
			$update = $this->db->update('factory_stock', $data);
			return ($update == true) ? true : false;
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('stores');
			return ($delete == true) ? true : false;
		}
	}
	
	public function removeChain($id)
	{
		if($id) {
			$this->db->where('site_chainage_id', $id);
			$delete = $this->db->delete('site_chainage');
			return ($delete == true) ? true : false;
		}
	}
	
	public function removeStock($id)
	{
		if($id) {
			$this->db->where('factory_stock_id', $id);
			$delete = $this->db->delete('factory_stock');
			return ($delete == true) ? true : false;
		}
	}
	public function removeSiteStock($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('products');
			return ($delete == true) ? true : false;
		}
	}

	public function countTotalStores()
	{
		$sql = "SELECT * FROM stores WHERE active ='1'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	public function getUserDetails($userTypeId)
	{
		$sql = "SELECT * FROM users WHERE user_type_id='".$userTypeId."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function checkProductExistOrNot($factory_product_id,$store_id)
	{
		$sql = "SELECT COUNT(1) AS RESULT FROM products WHERE factory_product_id='".$factory_product_id."' AND store_id='".$store_id."' AND status='Inprocess'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
public function InsertUpdateTransferDetails($insertTransferHistory,$updateProduct)
	{
		if(count($updateProduct)>0) {
			foreach($updateProduct as $pId=>$updateData){
    			$sql = "UPDATE  factory_stock SET used_qty=(used_qty+'".$updateData['used_qty']."') WHERE factory_stock_id = ?";
    			$query = $this->db->query($sql,$pId);
			}
		}
		if(count($insertTransferHistory)>0) {
			foreach($insertTransferHistory as $insertData){
					$insert = $this->db->insert('transfer_stock_to_site', $insertData); 
			}
		}
		return ($insert) ? true : false;
	}
	public function fetchsiteStockDataById($id)
	{
		
		$sql = "SELECT p.*,concat(c.name,' (',unit,')') as cat_name,s.name as store_name FROM products as p
		INNER JOIN categories as c ON c.id =p.category_id
		INNER JOIN stores as s ON s.id=p.store_id where p.id='".$id."'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	public function UpdateSiteStock($data,$id)
	{
		if($data) {
			$this->db->where('id', $id);
			$update = $this->db->update('products', $data);
			return ($update == true) ? true : false;
			// $insert = $this->db->insert('products', $data);
			// return ($insert == true) ? true : false;
		}
		// if($dataUpdate) {
		// 	$sql1= "UPDATE  products SET qty=('".$dataUpdate['qty']."'),total_quantity=('".$dataUpdate['qty']."') WHERE category_id ='".$dataUpdate['category_id']."' AND store_id ='".$dataUpdate['store_id']."'";
		// 	$query = $this->db->query($sql1);
		// 	return ($query == true) ? true : false;
		// }
	}
	
	public function getStoresDataListing1()
	{
			$sql = "SELECT s.*
                    FROM stores s";
			$query = $this->db->query($sql);
    		return $query->result_array();
	}
	public function gettransfersiteStockData($id)
	{
		if($id) {
			$sql = "SELECT s.*,concat(c.name,' (',unit,')') as cat_name,c.id as cat_id FROM products s 
			LEFT JOIN categories c ON s.category_id=c.id  WHERE s.factory_product_id IS NULL AND s.store_id='".$id."'";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
	}
	public function getStoresSiteData($id)
	{
			$sql = "SELECT s.*
                    FROM stores s where s.id!='".$id."'";
			$query = $this->db->query($sql);
    		return $query->result_array();
	}
public function InsertUpdateTransfersiteDetails($insertTransferHistory,$updateProduct)
	{
		if(count($updateProduct)>0) {
			foreach($updateProduct as $pId=>$updateData){
    			$sql = "UPDATE  products SET qty=(qty-'".$updateData['used_qty']."'),total_quantity=(total_quantity-'".$updateData['used_qty']."') WHERE id = ?";
    			$query = $this->db->query($sql,$pId);
			}
		}
		if(count($insertTransferHistory)>0) {
			foreach($insertTransferHistory as $insertData){
				$existProUpdate=$this->checkProductExistOrNot1($insertData['id'],$insertData['store_id']);
			//	print_r($insertData);die;
				if($existProUpdate['RESULT']>0)
				{
					$sqlUpdate= "UPDATE  products SET qty=(total_quantity-'".$insertData['qty']."'),total_quantity=(total_quantity-'".$insertData['qty']."') WHERE id='".$insertData['id']."' AND store_id='".$insertData['store_id']."'"; 
					$insert = $this->db->query($sqlUpdate);
				}
				else
				{
					$sqlUpdate= "INSERT INTO `products` (`name`, `category_id`,  `store_id`,`from_store_id`, `status`, `entry_by`,`received_qty`) VALUES ('".$insertData['name']."', '".$insertData['category_id']."',  '".$insertData['store_id']."','".$insertData['from_store_id']."', '".$insertData['status']."', '".$insertData['entry_by']."','".$insertData['received_qty']."')"; 
					$insert = $this->db->query($sqlUpdate);
					// $insert = $this->db->insert('products', $insertData); 
				}
			}
		}
		return ($insert) ? true : false;
	}
	public function checkProductExistOrNot1($factory_product_id,$store_id)
	{
		$sql = "SELECT COUNT(1) AS RESULT FROM products WHERE id='".$factory_product_id."' AND store_id='".$store_id."'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	public function getFactoryTransferDataList($siteId,$from_date,$to_date)
	{
		$sql = "SELECT p.*,fs.qty as actual_qty,stores.name as from_store_name , ps.name as to_store_name FROM products p
		 left join factory_stock fs ON p.factory_product_id=fs.factory_stock_id 
		 LEFT JOIN stores ON stores.id = p.from_store_id
		 LEFT JOIN stores ps ON ps.id = p.store_id WHERE store_id ='".$siteId."' AND status='Accepted' AND (p.entry_date BETWEEN '".$from_date."' AND '".$to_date."') ORDER BY entry_date";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

public function getSiteStockReport($siteId,$from_date,$to_date)
	{
		$sql = "SELECT * FROM `products` Where store_id ='".$siteId."' AND (entry_date BETWEEN '".$from_date."' AND '".$to_date."') ORDER BY entry_date";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
}