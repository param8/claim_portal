<?php 

class Model_users extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getUserData($userId = null) 
	{
		if($userId) {
			$sql = "SELECT * FROM users WHERE id = ?";
			$query = $this->db->query($sql, array($userId));
			return $query->row_array();
		}

		$sql = "SELECT * FROM users WHERE id NOT IN('1','2')";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getUserGroup($userId = null) 
	{
		if($userId) {
			$sql = "SELECT * FROM user_group WHERE user_id = ?";
			$query = $this->db->query($sql, array($userId));
			$result = $query->row_array();

			$group_id = $result['group_id'];
			$g_sql = "SELECT * FROM groups WHERE id = ?";
			$g_query = $this->db->query($g_sql, array($group_id));
			$q_result = $g_query->row_array();
			return $q_result;
		}
	}
	public function ChangePassword($data,$registration_id)
	{
		$decriptPass=$this->encryption->encrypt($data['password']); 
		$insert_data['password'] = $decriptPass;
		$this->db->where('id', $registration_id);
	 	$update = $this->db->update('users', $insert_data);
		return ($update == true) ? true : false;
	}
	public function UpdateClientRegistration($data,$client_id)
	{
		$this->db->where('id', $client_id);
	 	$update = $this->db->update('users', $data);
		return ($update == true) ? true : false;
	}
	public function create($data = '', $group_id = null)
	{

		if($data && $group_id) {
			$create = $this->db->insert('users', $data);

			$user_id = $this->db->insert_id();

			$group_data = array(
				'user_id' => $user_id,
				'group_id' => $group_id
			);

			$group_data = $this->db->insert('user_group', $group_data);

			return ($create == true && $group_data) ? true : false;
		}
	}

	public function edit($data = array(), $id = null, $group_id = null)
	{
		$this->db->where('id', $id);
		$update = $this->db->update('users', $data);

		if($group_id) {
			// user group
			$update_user_group = array('group_id' => $group_id);
			$this->db->where('user_id', $id);
			$user_group = $this->db->update('user_group', $update_user_group);
			return ($update == true && $user_group == true) ? true : false;	
		}
			
		return ($update == true) ? true : false;	
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$delete = $this->db->delete('users');
		return ($delete == true) ? true : false;
	}

	public function countTotalUsers()
	{
		$sql = "SELECT * FROM users";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	public function getProductDataByCatId($searchCat)
	{
		$companyId=$this->session->company_id;
		(isset($searchCat) && $searchCat!="") ? $whereClause=" AND p.category_id='".$searchCat."'" : $whereClause="";
		$sql = "SELECT c.id,c.name as category, count(1) as new_old_count,p.qty
						FROM products p 
						LEFT JOIN categories c ON c.id=p.category_id 
						WHERE c.company_id='".$companyId."'  $whereClause
						GROUP BY p.category_id ORDER BY c.name";
		$query = $this->db->query($sql, array($companyId));
		$returnArray=$query->result_array();
		//echo"<pre>";print_r($returnValue);die;
		return $returnArray;
	}
	public function getProductDataByCompanyId($searchCat)
	{
		$companyId=$this->session->company_id;
		(isset($searchCat) && $searchCat!="") ? $whereClause=" AND p.category_id='".$searchCat."'" : $whereClause="";
		$sql = "SELECT COUNT(*) AS TOTAL,'Issue' as HEADING,p.category_id,c.name 
					FROM `assign_product` ap 
					LEFT JOIN products p ON ap.product_id=p.id 
					LEFT JOIN categories c ON p.category_id=c.id 
					WHERE `return_status` = 'Issue' $whereClause
					GROUP BY p.category_id";
		$query = $this->db->query($sql, array($companyId));
		$returnArray=$query->result_array();
		$sqlAssignPro = "SELECT COUNT(*) AS TOTAL, CASE WHEN s.active=1 THEN 'Active' WHEN s.active=2 THEN 'Scrap' END AS HEADING,p.category_id,c.name 
							FROM `assign_product` ap 
							LEFT JOIN stores s ON ap.return_to_warehouse=s.id 
							LEFT JOIN products p ON ap.product_id=p.id 
							LEFT JOIN categories c ON p.category_id=c.id 
							WHERE `return_status` = 'Return' $whereClause
							GROUP BY p.category_id,s.active 
							ORDER BY s.active";
		$query1 = $this->db->query($sqlAssignPro, array($companyId));
		$returnValue=$query1->result_array();
		$combineArray=array();
		$combineArray=array_merge($returnArray,$returnValue);
		//echo"<pre>";print_r($combineArray);die;
		return $combineArray;
	}
	public function GetCategoryWiseProductReportByCatId($catId,$status)
	{
		$companyId=$this->session->company_id;
		(isset($catId) && $catId!="") ? $whereClause="  AND p.category_id='".$catId."'" : $whereClause="";
		if($status=='Issue')
		{
		 	$sql = "SELECT ap.*,p.name as product_name,s.name as warehouse
						FROM `assign_product` ap 
						LEFT JOIN products p ON ap.product_id=p.id 
						LEFT JOIN categories c ON p.category_id=c.id 
						LEFT JOIN stores s ON s.id=p.store_id
						WHERE `return_status` = 'Issue' $whereClause";
		}
		else if($status=='Active')
		{
		   $sql = "SELECT ap.*,p.name as product_name,s.name as warehouse
							FROM `assign_product` ap 
							LEFT JOIN stores s ON ap.return_to_warehouse=s.id 
							LEFT JOIN products p ON ap.product_id=p.id 
							LEFT JOIN categories c ON p.category_id=c.id 
							WHERE `return_status` = 'Return' $whereClause
							AND s.active=1 ORDER BY s.active";
		}
		else
		{
		   $sql = "SELECT ap.*,p.name as product_name,s.name as warehouse
							FROM `assign_product` ap 
							LEFT JOIN stores s ON ap.return_to_warehouse=s.id 
							LEFT JOIN products p ON ap.product_id=p.id 
							LEFT JOIN categories c ON p.category_id=c.id 
							WHERE `return_status` = 'Return' $whereClause
							AND s.active=2 ORDER BY s.active";
		}
		$query = $this->db->query($sql, array($companyId));
		$returnArray=$query->result_array();
		//echo"<pre>";print_r($returnArray);die;
		return $returnArray;
	}
	public function GetCategoryWiseProductByCatId($catId,$status)
	{
		$companyId=$this->session->company_id;
		(isset($catId) && $catId!="") ? $whereClause="  p.category_id='".$catId."'" : $whereClause="";
		if($status=='Old')
		{
		  $sql = "SELECT p.*,ap.*,s.name as warehouse FROM `assign_product` ap 
		 				LEFT JOIN products p on ap.product_id=p.id 
						LEFT JOIN stores s on s.id=ap.return_to_warehouse 
						WHERE p.category_id='".$catId."' AND ap.`return_status`='Return' AND s.active=2";
		}
		else if($status=='Remaining')
		{
		  echo $sql = "SELECT p.*,s.name as warehouse,ap.return_to_warehouse,s.active,count(1) as quantity
				FROM products p
				LEFT JOIN assign_product ap ON p.id=ap.product_id
				LEFT JOIN stores s ON s.id=ap.return_to_warehouse
				LEFT JOIN categories c ON c.id=p.category_id
				WHERE  $whereClause
				GROUP BY p.id ORDER BY c.name";
		}
		else
		{
		  $sql = "SELECT p.*,s.name as warehouse,ap.return_to_warehouse,s.active
				FROM products p
				LEFT JOIN assign_product ap ON p.id=ap.product_id
				LEFT JOIN stores s ON s.id=ap.return_to_warehouse
				LEFT JOIN categories c ON c.id=p.category_id
				WHERE  $whereClause
				GROUP BY p.id ORDER BY c.name";
		}
		$query = $this->db->query($sql, array($companyId));
		$returnArray=$query->result_array();
		//echo"<pre>";print_r($returnArray);die;
		return $returnArray;
	}
	public function GetProductByPId($pId,$status)
	{
		  $sql = "SELECT ap.* FROM `assign_product` ap 
						WHERE ap.product_id='".$pId."' AND ap.`return_status`='".$status."'";
		$query = $this->db->query($sql);
		$returnArray=$query->result_array();
		return $returnArray;
	}
	
	public function AddUser($data)
	{

		if($data ) {
			$create = $this->db->insert('users', $data);
			return ($create == true) ? true : false;
		}
	}
    public function UpdateUser($data, $id )
	{
		$this->db->where('id', $id);
		$update = $this->db->update('users', $data);
		return ($update == true) ? true : false;	
	}	
}