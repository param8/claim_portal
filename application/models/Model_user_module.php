<?php 

class Model_user_module extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getUserTypeDetails($userType = null) 
	{
		if($userType) {
			$sql = "SELECT * FROM user_type WHERE user_type_id = ?";
			$query = $this->db->query($sql, array($userType));
			return $query->row_array();
		}

		$sql = "SELECT * FROM user_type ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getModuleMenuLink($menuLinkId = null) 
	{
		$sql = "SELECT * FROM module_link_name ORDER BY priority ASC ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}


	public function create($data = '')
	{

		if($data) {
			$create = $this->db->insert('user_type', $data);
			return ($create == true) ? true : false;
		}
	}

	public function UpdateUserType($data,$user_type_id)
	{
		$this->db->where('user_type_id', $user_type_id);
		$update = $this->db->update('user_type', $data);
		return ($update == true) ? true : false;	
	}

	public function delete($id)
	{
		$this->db->where('user_type_id', $id);
		$delete = $this->db->delete('user_type');
		return ($delete == true) ? true : false;
	}

	public function InsertUpdateModuleLink($moduleLinkArray,$userTypeArra)
	{

		if(count($moduleLinkArray)>0) {
			//***********Delete first********************//
			$this->db->where_in('user_type_id', $userTypeArra);
			$delete = $this->db->delete('module_menu_link_permission');
			foreach($moduleLinkArray as $utId=>$userTypeWiseArr){
				foreach($userTypeWiseArr as $link=>$linkWiseBtn){
					//***********And then insert********************//
					$query="INSERT INTO module_menu_link_permission (module_name,menu_name,button_permission,user_type_id) VALUES('".$link."','".str_replace("_"," ",$link)."','".implode(",",$linkWiseBtn)."','".$utId."')";
					$create = $this->db->query($query);
				}
			}
		}
	}
	
	public function GetUserTypeWisePermissionDetail($userTypeId)
	{
		if($userTypeId) {
			$sql = "SELECT * FROM module_menu_link_permission WHERE user_type_id = ?";
			$query = $this->db->query($sql, array($userTypeId));
			return $query->result_array();
		}
	}
	
}