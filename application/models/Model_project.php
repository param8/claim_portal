<?php 

class Model_project extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get active brand infromation */
	public function getProjectData()
	{
		$whereClause="";
		if($this->session->userdata('user_type')=='3')
			$whereClause= "WHERE project_id='".$this->session->userdata('store_id')."'";
		
		$sql = "SELECT * FROM project  $whereClause";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getSiteDetails()
	{
		if($this->session->userdata('user_type')=='1' || $this->session->userdata('user_type')=='7') // for super admin and admin by Deep Rana
			$whereClause= " WHERE id > 0";
		else
			$whereClause= " WHERE  site_manager_id='".$this->session->userdata['id']."' OR coordinator_id='".$this->session->userdata['id']."' ORDER BY name";
			
		$sql = "SELECT id,name FROM stores $whereClause";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	/* get the brand data */
	public function getprojectDataById($id = null)
	{
		if($id) {
			$sql = "SELECT p.*,u.password,user_type.user_type as user_type FROM project p LEFT JOIN users u ON (p.project_id=u.store_id) LEFT JOIN user_type ON user_type.user_type_id = p.user_type_id WHERE project_id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
		else
		{
			$sql = "SELECT * FROM project WHERE status='Active'";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
	}
	public function getProductDetailsByWId($id = null)
	{
		if($id) {
			$sql = "SELECT s.*,c.name as cat,c.unit FROM products s
					LEFT JOIN categories c ON s.category_id=c.id
					 where store_id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->result_array();
		}
	}
	public function getprojectAssignDetails($id = null)
	{
		$siteWhereClause=($id) ? " AND pd.store_id='".$id."'" : "";
		$whereClause= "";
		if($this->session->userdata('user_type')=='1' || $this->session->userdata('user_type')=='7') //***********for super admin and admin by Deep Rana
			$siteWhereClause=($id) ? "  pd.store_id='".$id."'" : " pd.store_id > 0";
		else
			$whereClause= " site_manager_id='".$this->session->userdata['id']."' OR coordinator_id='".$this->session->userdata['id']."'";
			
		$sql = "SELECT pd.id as product_id,pd.name as item,pd.qty as project_qty ,c.name as cat,c.unit,s.name ,us.username as coordinator,s.id
						FROM products pd 
						LEFT JOIN stores s ON s.id=pd.store_id 
						LEFT JOIN users us ON s.coordinator_id=us.id 
						LEFT JOIN categories c ON pd.category_id=c.id 
						WHERE $whereClause $siteWhereClause AND pd.status LIKE 'Accepted'  ORDER BY s.name";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getDPRRejectData($id = null)
	{
		if($id) {
			$sql = "SELECT dpr_type,dpr_date,status,status_reason FROM `daily_progress` WHERE `project_id`='".$id."' AND `status`='Reject' GROUP BY `dpr_type`,`dpr_date`";
			$query = $this->db->query($sql, array($id));
			return $query->result_array();
		}
		else
    		return array();
	}
	
	public function InsertUpdateData($data)
	{
		foreach($data as $val)
		{
			$insert = $this->db->insert('product_project', $val);
			$sql = "UPDATE  products SET qty=(qty-'".$val['project_qty']."') WHERE id = ?";
			$query = $this->db->query($sql, $val['product_id']);
		}
		return ($insert == true) ? true : false;
	}
	
	public function UpdateDprStatus($data,$site_id,$dpr_type,$dpr_date)
	{
		if($data)
		{
			$sql = "UPDATE  daily_progress SET status='".$data['status']."',status_reason='".$data['status_reason']."' WHERE project_id ='".$site_id."' AND dpr_type='".$dpr_type."' AND dpr_date='".$dpr_date."'";
			$query = $this->db->query($sql);
		}
		return ($query == true) ? true : false;
	}



	public function create($data,$dataUserPass)
	{
		if($data) {
			$insert = $this->db->insert('project', $data);
			$maxId=$this->db->insert_id();
			$maxIdArray=array("store_id"=>$maxId);
			$combineArray=array_merge($dataUserPass,$maxIdArray);
			$insertPass = $this->db->insert('users', $combineArray);
			return ($insert == true) ? true : false;
		}
	}
	
	public function update($data,$dataUserPass, $id)
	{		
		
		if($data && $id) {
			$this->db->where('project_id', $id);
			$update = $this->db->update('project', $data);
			$this->db->where('store_id', $id);
			$update = $this->db->update('users', $dataUserPass);
			return ($update == true) ? true : false;
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('project_id', $id);
			$delete = $this->db->delete('project');
			$this->db->where('store_id', $id);
			$delete1 = $this->db->delete('users');
			return ($delete == true) ? true : false;
		}
	}
	
	public function ReturnProduct($returnProjId,$returnProdId,$whId)
	{
		if($returnProjId) {
			 $sql = "UPDATE  products SET qty=(qty+(select project_qty FROM product_project WHERE product_project_id='".$returnProjId."')) WHERE id = ?";
			 $query = $this->db->query($sql, $returnProdId);
			 $sql1= "UPDATE  product_project SET return_to_warehouse='".$whId."' ,return_date='".date('Y-m-d')."',status='Return' WHERE product_project_id = ?";
			$query = $this->db->query($sql1, $returnProjId);
			return ($query == true) ? true : false;
		}
	}
	
	public function AssignToUser($data)
	{
		if(count($data)>0) {
			foreach($data as $rcd)
			{
				if($rcd['entry_type']!='Payment')
				{
					$sql = "UPDATE  products SET qty=(qty-'".$rcd['quantity']."') WHERE id= ? "; 
					$query = $this->db->query($sql, array($rcd['product_id']));
				}
				$insert = $this->db->insert('assign_product_project', $rcd);
			}
			return ($insert == true) ? true : false;
		}
	}
	public function checkChainageStock($data)
	{
		if(count($data)>0) {
			$sql = "SELECT count(1) as total  FROM `chainage_stock` WHERE `site_id`='".$data['site_id']."' AND `product_id`='".$data['product_id']."' AND `chainage_id`='".$data['chainage_id']."'";
			$query = $this->db->query($sql);
			$data=$query->row_array();
			if($data['total']>0)
			{
				return true;
			}
			else
			{
				return false;
			}

		}
	}
	public function AssignToChainage($data)
	{
		if(count($data)>0) {
			foreach($data as $rcd)
			{
				if($rcd['product_id']!='')
				{
					$sql = "UPDATE  products SET qty=(qty-'".$rcd['quantity']."') WHERE id= ? "; 
					$query = $this->db->query($sql, array($rcd['product_id']));
				}
				$existStock=$this->checkChainageStock($rcd);
				if($existStock){
					$sql = "UPDATE  chainage_stock SET quantity=(quantity+'".$rcd['quantity']."'),total_qty=(total_qty+'".$rcd['quantity']."') WHERE `site_id`='".$rcd['site_id']."' AND `product_id`='".$rcd['product_id']."' AND `chainage_id`='".$rcd['chainage_id']."'"; 
					$insert=$query = $this->db->query($sql);
				}
				else{
					$insert = $this->db->insert('chainage_stock', $rcd);
				}
			}
			return ($insert == true) ? true : false;
		}
	}
	
	public function GetAssignProductDetails($prjId,$product_id,$status)
	{
		$sql = "SELECT * FROM assign_product_project WHERE project_id = ?  AND product_id = ?  AND return_status= ?";
		$query = $this->db->query($sql, array($prjId,$product_id,$status));
		return $query->result_array();
	}
	
	public function GetUsedPayment($prjId)
	{
		$sql = "SELECT IFNULL(SUM(payment),0) USEDPAYMENT FROM `assign_product_project` WHERE `project_id`= ? AND entry_type= ? ";
		$query = $this->db->query($sql, array($prjId,'Payment'));
		return $query->result_array();
	}
	
	public function getPaymentDetailsByProjectId($prId)
	{
		$sql = "SELECT *  FROM `assign_product_project` WHERE `project_id`= ? AND entry_type= ? ";
		$query = $this->db->query($sql, array($prId,'Payment'));
		return $query->result_array();
	}

	public function ReturnProductByUser($ProId,$ProjId,$proProjId,$remark)
	{
		if($proProjId) {
			 $sql = "UPDATE  product_project SET project_qty=(project_qty+(select quantity FROM assign_product_project WHERE assign_product_project_id='".$proProjId."')) WHERE project_id = ? AND product_id = ? ";
			 $query = $this->db->query($sql, array($ProjId,$ProId));
			 $sql1= "UPDATE  assign_product_project SET return_remark='".$remark."' ,return_date='".date('Y-m-d')."',return_status='Return' WHERE assign_product_project_id = ?";
			$query = $this->db->query($sql1, $proProjId);
			return ($query == true) ? true : false;
		}
	}
	
	public function getPanelData()
	{
		$sql = "SELECT * FROM panel WHERE 1 ORDER BY panel_priority";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function createPanel($data)
	{
		if($data) {
			$insert = $this->db->insert('panel', $data);
			return ($insert == true) ? true : false;
		}
	}
	
	public function getpanelDataById($id)
	{
		$sql = "SELECT * FROM panel WHERE `panel_id`= ?";
		$query = $this->db->query($sql, array($id));
		return $query->result_array();
	}
	
	public function UpdatePanel($data,$id)
	{
		if($data && $id) {
			$this->db->where('panel_id', $id);
			$update = $this->db->update('panel', $data);
			return ($update == true) ? true : false;
		}
	}
	
	public function removePanel($panel_id)
	{
		if($panel_id) {
			$this->db->where('panel_id', $panel_id);
			$delete = $this->db->delete('panel');
			return ($delete == true) ? true : false;
		}
	}
	
	public function insertDPR($data)
	{
		if(count($data)>0) {
			foreach($data as $rcd)
			{
				$this->db->query("DELETE FROM daily_progress WHERE panel_id='".$rcd['panel_id']."' AND dpr_type='".$rcd['dpr_type']."' AND dpr_date='".$rcd['dpr_date']."' AND entry_by='".$this->session->userdata['id']."'");
				$insert = $this->db->insert('daily_progress', $rcd);
			}
			return ($insert == true) ? true : false;
		}
	}
	
	public function allocatePanel($insertArray)
	{
		if(count($insertArray)>0) {
			$this->db->query("DELETE FROM panel_allocate WHERE site_id='".$insertArray[0]['site_id']."'");
			foreach($insertArray as $panelAllocation)
			{
				$insert = $this->db->insert('panel_allocate', $panelAllocation);
			}
			return ($insert == true) ? true : false;
		}
	}
	
	public function getAllocatePanelDetById($id)
	{
		$sql = "SELECT P.* FROM panel_allocate PA LEFT JOIN panel P ON PA.PANEL_ID=P.PANEL_ID WHERE `site_id`= ?";
		$query = $this->db->query($sql, array($id));
		return $query->result_array();
	}

	public function getdprApproveDetails($sites_id)
	{
		$sql = "SELECT DATE_ADD(dpr_date, INTERVAL -1 DAY) dpr_date,status FROM daily_progress WHERE `project_id`= ?  AND status='Approve' GROUP BY dpr_date ORDER BY dpr_date DESC LIMIT 1";
		$query = $this->db->query($sql, array($sites_id));
		return $query->result_array();
	}

	public function getDailyProgressDtails($type,$panel_id,$flag,$date,$site)
	{
		if($type=="")
			$type='casting';
			
		
		if($date=="")
			$dateVal=date('Y-m-d', strtotime(' -1 day'));
		else
			$dateVal=$date;
		
			
		if($flag=='Received')
		{
			  $sql = "SELECT * FROM daily_progress WHERE panel_id='".$panel_id."'  AND `dpr_date`='".$dateVal."' AND `dpr_type`='".$type."' AND project_id='".$site."'";
		}
		else
		{
			$sql = "SELECT last_month_no,last_month_area FROM daily_progress WHERE `dpr_date` LIKE '%".date('Y-m',strtotime("-1 month"))."%' AND panel_id='".$panel_id."' AND `dpr_type`='".$type."' AND project_id='".$site."'";
		}
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getDailyProgressDtailsByDateRange($type,$panel_id,$flag,$fromDate,$toDate,$site)
	{
		if($type=="")
			$type='casting';

		$sql = "SELECT SUM(`unit_area_sqm`) AS unit_area_sqm,SUM(`month_no`) AS month_no, SUM(`month_area`) AS month_area, SUM(`cummulative_no`) AS cummulative_no, SUM(`cummulative_area`) AS cummulative_area,remark,bl FROM `daily_progress` WHERE `dpr_date` BETWEEN '".$fromDate."' AND '".$toDate."' AND `project_id`='".$site."' AND `dpr_type`='".$type."' AND `panel_id`='".$panel_id."' GROUP by panel_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
		public function getprojectAssignReportDetails($id = null)
	{
		($id) ? $whereClause=" WHERE pd.store_id='".$id."'" : $whereClause="";
		($this->session->userdata('user_type')=='3') ? $whereClause=" WHERE p.project_id='".$this->session->userdata('store_id')."'" : $whereClause="";
		$sql = "SELECT p.*,c.name as cat,c.unit,pp.product_project_id,pp.status,pp.project_qty,pp.product_id,pd.name as item,pd.serial_no,s.name
				FROM project p
				LEFT JOIN product_project pp ON p.project_id=pp.project_id
				LEFT JOIN products pd ON pp.`product_id`=pd.`id`
				LEFT JOIN stores s ON s.id=pd.store_id
				LEFT JOIN categories c ON pd.category_id=c.id
				$whereClause ORDER BY 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getassignprojectDetail($storeId,$pId,$from_date,$to_date)
	{
		$storeIdWhereClause="";
		$PIdWhereClause="";
		$where="";
		if($storeId!=''){
			$storeIdWhereClause=" AND project_id='".$storeId."'";
		}
		if($pId!=''){
			$PIdWhereClause=" AND product_id='".$pId."'";
		}
		if($this->session->userdata('user_type')!='1' && $this->session->userdata('user_type')!='7') // for super admin and admin by Deep Rana
		{
			$where="AND (st.coordinator_id='".$_SESSION['id']."' || st.site_manager_id='".$_SESSION['id']."' )";
		}
		
		
		$sql = "SELECT asp.assign_to, pd.name as productname,asp.quantity, st.name as storename ,asp.entry_date,asp.return_status,asp.entry_type,st.id as store_id,pd.id as product_id,pd.total_quantity,pd.qty FROM assign_product_project as asp
		INNER JOIN products as pd ON pd.id = asp.product_id
		INNER JOIN stores as st ON st.id = asp.project_id
				WHERE asp.`entry_date` BETWEEN '".$from_date."' AND '".$to_date."' $storeIdWhereClause $PIdWhereClause $where ORDER BY pd.name ASC"; 
		$query = $this->db->query($sql);
		return $query->result_array();

	}
	
	public function assignFactoryStockDetail($storeId,$from_date,$to_date)
	{
		$sql = "SELECT ss.*,s.name as site,c.name as category_name FROM `transfer_stock_to_site` ss 
				LEFT JOIN stores s ON ss.store_id=s.id
				LEFT JOIN categories c ON ss.category_id=c.id
				WHERE ss.`entry_date` BETWEEN '".$from_date."' AND '".$to_date."' AND ss.store_id='".$storeId."' ORDER BY ss.status,ss.entry_date DESC"; 
		$query = $this->db->query($sql);
		return $query->result_array();

	}	
	
	public function getStoresListing()
	{
		$where="";
		if($this->session->userdata('user_type')!='1' && $this->session->userdata('user_type')!='7') // for super admin and admin by Deep Rana
		{
			$where="AND (coordinator_id='".$_SESSION['id']."' || site_manager_id='".$_SESSION['id']."' )";
		}
		 	$sql = "SELECT s.*
                    FROM stores s Where 1=1 $where";
			$query = $this->db->query($sql);
			return $query->result_array();
	}	
	public function getSiteChainDetails($siteId)
	{
		 	$sql = "SELECT s.*
                    FROM site_chainage s Where site_id='".$siteId."'";
			$query = $this->db->query($sql);
			return $query->result_array();
	}
	public function getChainageDetailsByid($siteId,$chainageId)
	{
		 	$sql = "SELECT sc.chainage_name,pd.name,cs.* 
						FROM `chainage_stock` cs 
						LEFT JOIN site_chainage sc ON cs.`chainage_id`=sc.site_chainage_id 
						LEFT JOIN products pd ON cs.`product_id`=pd.id 
						WHERE cs.`site_id`='".$siteId."' AND cs.`chainage_id`='".$chainageId."'";
			$query = $this->db->query($sql);
			return $query->result_array();
	}
	public function updateSiteChainQty($siteId,$proId,$chainId,$quantity)
	{
		$sql = "UPDATE  chainage_stock SET quantity=(quantity-'".$quantity."') WHERE `site_id`='".$siteId."' AND `product_id`='".$proId."' AND `chainage_id`='".$chainId."'"; 
		$insert=$query = $this->db->query($sql);
		return $insert;
	}
	public function getExistData($siteId,$proId,$chainId,$consumption_date)
	{
		$sql = "SELECT COUNT(1) TOTAL FROM chainage_daily_consumption  WHERE `site_id`='".$siteId."' AND `product_id`='".$proId."' AND `chainage_id`='".$chainId."' AND consumption_date='".$consumption_date."'";
			$query = $this->db->query($sql);
			$data=$query->row_array();
			if($data['TOTAL'])
				return true;
			else
				return false;
	}
	
	public function insertDailyConsumption($data)
	{
		foreach($data as $val)
		{
			if($this->updateSiteChainQty($val['site_id'],$val['product_id'],$val['chainage_id'],$val['quantity']))
			{
				if($this->getExistData($val['site_id'],$val['product_id'],$val['chainage_id'],$val['consumption_date']))
				{
					$sql = "UPDATE  chainage_daily_consumption SET quantity=(quantity+'".$val['quantity']."') WHERE `site_id`='".$val['site_id']."' AND `product_id`='".$val['product_id']."' AND `chainage_id`='".$val['chainage_id']."' AND consumption_date='".$val['consumption_date']."'"; 
					$insert = $this->db->query($sql);

				}
				else
				{
					$insert = $this->db->insert('chainage_daily_consumption', $val);
				}
			}
		}
		return ($insert == true) ? true : false;
	}
	public function getChainageConsumptionReport($siteId,$chainId,$from_date,$to_date)
	{
		 	$sql = "SELECT sc.chainage_name,pd.name,cs.total_qty,cdc.*,us.username ,s.name,pd.name as item
						FROM chainage_daily_consumption cdc 
						LEFT JOIN chainage_stock cs ON (cdc.`chainage_id`=cs.chainage_id AND cdc.`site_id`=cs.`site_id` AND cdc.`product_id`=cs.`product_id`)
						LEFT JOIN site_chainage sc ON cs.`chainage_id`=sc.site_chainage_id 
						LEFT JOIN products pd ON cs.`product_id`=pd.id 
						LEFT JOIN users us ON us.id=cdc.`entry_by` 
						LEFT JOIN stores s ON s.`id`=cdc.site_id 
					WHERE cdc.`site_id`='".$siteId."' AND cdc.`chainage_id`='".$chainId."' AND cdc.`consumption_date` BETWEEN '".$from_date."' AND '".$to_date."' ORDER BY cdc.consumption_date";
			$query = $this->db->query($sql);
			return $query->result_array();
	}
}