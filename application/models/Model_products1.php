<?php 

class Model_products extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get the brand data */
	public function getProductData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM products s
					LEFT JOIN product_extra_detail ped ON s.id=ped.product_id
					 where id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM products ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getProductDataByCatId($catId= null)
	{
		if($catId) {
			$sql = "SELECT * FROM products where category_id = ?";
			$query = $this->db->query($sql, array($catId));
			return $query->result_array();
		}
		else
		{
			$sql = "SELECT * FROM products ORDER BY id DESC";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
	}


	public function getActiveProductData()
	{
		$sql = "SELECT * FROM products WHERE availability = ? ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	}

	public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('products', $data); 
			$lastid = $this->db->insert_id();
			return ($insert) ? $lastid : false;
		}
	}
	public function create_pro_extra_detail($data)
	{
		if($data) {
			$insert = $this->db->insert('product_extra_detail', $data); 
			return ($insert) ? true : false;
		}
	}
	
	
	public function AssignTo($data)
	{
		if($data) {
			$sql = "UPDATE  products SET qty=(qty-'".$data['quantity']."') WHERE id = ?";
			$query = $this->db->query($sql, $data['product_id']);
			$insert = $this->db->insert('assign_product', $data);
			return ($insert == true) ? true : false;
		}
	}
	public function update_pro_extra_detail($data,$product_id,$category)
	{
		if($category=='1' || $category=='2') {
			$this->db->where('product_id', $product_id);
			$update = $this->db->update('product_extra_detail', $data);
			return ($update == true) ? true : false;
		}
		else
		{
			$this->db->where('product_id', $product_id);
			$delete = $this->db->delete('product_extra_detail');
			return ($delete == true) ? true : false;
		}
	}
	
	
	public function ReturnBy($data)
	{
		if($data) {
			$wareHouseArr=explode("_",$data['to_warehouse']);
			if($wareHouseArr[1]==1)
			{
				$sql = "UPDATE  products SET qty=qty+1 WHERE id = ?";
				$query = $this->db->query($sql, $data['product_id']);
			}
			$sql = "UPDATE  assign_product SET return_status='Return',return_date='".date('Y-m-d')."' ,return_to_warehouse='".$wareHouseArr[0]."' WHERE 	assign_product_id = ?";
			$query = $this->db->query($sql, $data['returnBy']);
			return ($query == true) ? true : false;
		}
	}



	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('products', $data);
			return ($update == true) ? true : false;
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('products');
			return ($delete == true) ? true : false;
		}
	}

	public function countTotalProducts()
	{
		$sql = "SELECT * FROM products";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}


	public function countTotalbrands()
	{
		$sql = "SELECT * FROM brands";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	public function countTotalcategory()
	{
		$sql = "SELECT * FROM categories";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}


	public function countTotalattribures()
	{
		$sql = "SELECT * FROM attributes";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	public function GetAssignProductDetails($product_id,$status)
	{
		$sql = "SELECT * FROM assign_product WHERE product_id = ?  AND return_status= ?";
		$query = $this->db->query($sql, array($product_id,$status));
		return $query->result_array();
	}
	public function GetProdductDetails()
	{
		$sql = "SELECT p.id,p.name,p.category_id,p.qty,c.unit FROM products p LEFT JOIN categories c ON p.category_id=c.id GROUP BY factory_product_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function insertScrapData($data)
	{
		if($data) {
			$insert = $this->db->insert('scrap_item', $data); 
			return ($insert) ? true : false;
		}
	}
	public function GetScrapDetails($id)
	{
		$sql = "SELECT * FROM scrap_item where item_id='".$id."'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	public function updateScrapData($data,$scrap_id)
	{
		if($data) {
			$this->db->where('scrap_item_id', $scrap_id);
			$update = $this->db->update('scrap_item', $data);
			return ($update == true) ? true : false;
		}
	}
	public function updateSalesUnit($salesUnit,$scrapId,$cumulative)
	{
		if($scrapId) {
			date_default_timezone_set('Asia/Kolkata');
			$sql = "UPDATE  scrap_item SET cumulative_unit='".($cumulative-$salesUnit)."',sale_unit=(sale_unit+'".$salesUnit."'),last_sale_date='".date('Y-m-d h:m:s')."' WHERE scrap_item_id = ?"; 
			$query = $this->db->query($sql, $scrapId);
			return ($query == true) ? true : false;
		}
	}
	
	public function GetScrapAllDetails()
	{
		$sql = "SELECT * FROM scrap_item";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function GetAllProductByWarehouseId($whId)// AND qty >0
	{
		$sql = "SELECT *
				FROM products 
				WHERE store_id = ? order by name";
		$query = $this->db->query($sql, array($whId));
		return $query->result_array();
	}
	public function InsertUpadateTransferHistroy($updateProduct,$insertTransferHistory)
	{
		if(count($updateProduct)>0) {
			foreach($updateProduct as $pId=>$updateData){
				$this->db->where('id', $pId);
				$insert = $this->db->update('products', $updateData);
			}
		}
		if(count($insertTransferHistory)>0) {
			foreach($insertTransferHistory as $insertData){
				$insert = $this->db->insert('transfer_history', $insertData); 
			}
		}
		return ($insert) ? true : false;
	}
	public function getStoreWiseAllProductRequest()
	{
		$storeIdWhereClause="";
		$statusWhereClause="";
		if($this->session->userdata['store_id']!=0){
			$storeIdWhereClause=" AND p.store_id='".$this->session->userdata['store_id']."'";
		}
		$statusWhereClause=" AND p.status='Inprocess'";
		//echo"<pre>";print_r($this->session->userdata);die;
		$sql = "SELECT p.id,p.name,p.qty,p.serial_no ,s.id as store_id,s.name as store_name
				FROM products p
				LEFT JOIN stores s ON p.store_id=s.id
				WHERE s.id > 0 $storeIdWhereClause $statusWhereClause"; 
		$query = $this->db->query($sql);
		return $query->result_array();

	}
	public function getTansferAssetDetail($storeId)
	{
		$storeIdWhereClause="";
		$statusWhereClause="";
		if($this->session->userdata['store_id']!=0){
			$storeIdWhereClause=" AND th.to_warehouse_id='".$this->session->userdata['store_id']."'";
			$statusWhereClause=" AND p.status='Inprocess'";
		}
		else{
			if($storeId)
				$storeIdWhereClause=" AND th.to_warehouse_id='".$storeId."'";
		}
		
		$sql = "SELECT th.`entry_date`,p.*,fs.name as from_store_name,ts.name as to_store_name,c.name as cat_name,c.unit
				FROM `transfer_history` th
				LEFT JOIN products p ON th.product_id=p.id
				LEFT JOIN stores fs ON th.`from_warehouse_id`=fs.id
				LEFT JOIN stores ts ON th.`to_warehouse_id`=ts.id
				LEFT JOIN categories c ON p.category_id=c.id
				WHERE 1=1 $storeIdWhereClause"; 
		$query = $this->db->query($sql);
		return $query->result_array();

	}
	public function GetScrapAllDetailsreport($from_date,$to_date)
	{
		$dateWherClause="";
		if($from_date!="" && $to_date!="")
		{
			$dateWherClause= " AND last_sale_date BETWEEN '".$from_date."' AND '".$to_date."'	";
		}
		$sql = "SELECT * FROM scrap_item where last_sale_date!='' $dateWherClause";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}