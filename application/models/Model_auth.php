<?php 



class Model_auth extends CI_Model

{

	public function __construct()

	{

		parent::__construct();
		$this->load->library('encryption');
	}



	/* 

		This function checks if the email exists in the database

	*/

	public function check_email($email) 
	{

		if($email) {

			$sql = 'SELECT * FROM users WHERE email = ? OR mobile_no=?';
			$query = $this->db->query($sql, array($email,$email));
			$result = $query->num_rows();
			return ($result == 1) ? true : false;

		}



		return false;

	}


	public function check_Menu_Link($userTypeId) 
	{
		if($userTypeId) {
			$sql = "SELECT * FROM module_menu_link_permission WHERE user_type_id = ?";
			$query = $this->db->query($sql, array($userTypeId));
			$linkArray=$query->result_array();
			$menuLink='<li id="dashboardMainMenu">
						  <a href="'.base_url('dashboard').'">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>
						  </a>
						</li>';
			$linkWiseBtnPms=array();
			if(count($linkArray)>0){
			$sql_controller = "SELECT * FROM module_link_name";
				$query_controller = $this->db->query($sql_controller);
				$c_Array=$query_controller->result_array();	
				// echo'<pre>';print_r($c_Array);die;
				foreach($c_Array as $arr){
					//echo'<pre>';print_r($arr);
					$moduleLinkArray[]=$arr['link_name'];
					 $controllerArray[str_replace(" ","_",$arr['link_name'])]=$arr['link_controller'];
				}	
				foreach($linkArray as $link){
					$linkWiseBtnPms[$link['module_name']]=explode(",",$link['button_permission']);
					 $menuLink.='<li id="categoryNav">
									  <a href="'.base_url($controllerArray[$link['module_name']]).'">
										<i class="fa fa-user"></i> <span>'.$link['menu_name'].'</span>
									  </a>
									</li>';
				}
				
			}
			$menuLink.='<li><a href="'.base_url('auth/logout').'">
						<i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>';		
		}
		return array($menuLink,$linkWiseBtnPms);
	}
	/* 

		This function checks if the email and password matches with the database

	*/

	public function login($email, $password) {

		if($email && $password) {

			$sql = "SELECT * FROM users WHERE email = ? OR mobile_no = ?";

			$query = $this->db->query($sql, array($email,$email));
			if($query->num_rows() == 1) {
				$result = $query->row_array();
				$decriptPass=$this->encryption->decrypt($result['password']); 
				if($decriptPass==$password)
				{
					return $result;	
				}
				else {
					return false;
				}
			}

			else {

				return false;

			}

		}

	}

}